This file describes all the changes to the I2O Shared header files 
that occured between version 1_5_4 and 1_5_5.


i2odepTemp.h
-------------
* Changed Revision number to 1_5_5.

i2oexec.h
---------
* Changed Revision number to 1_5_5.
* Changed Reserved 1 field in the I2O_EXEC_IOP_CONNECT_MESSAGE to be
  4 bits, not 8 bits.
* Changed Reserved 1 field in the I2O_EXEC_IOP_CONNECT_REPLY to be
  4 bits, not 8 bits.
* In I2O_EXEC_CONN_SETUP_MESSAGE, changed IOP2InboundMFrameSize to
  IOP1InboundMFrameSize.

i2omodule.h
-----------
* Changed Revision number to 1_5_5.

i2omsg.h
--------
* Changed Revision number to 1_5_5.
* Added two now message class definitions:  I2O_CLASS_PEER_TRANSPORT
  and I2O_CLASS_PEER_TRANSPORT_AGENT
* Changed typo I2O_DEATIL_STATUS_DEVICE_BUSY to 
               I2O_DETAIL_STATUS_DEVICE_BUSY
* Made the following changes to I2O_FAILURE_REPLY_MESSAGE_FRAME:
    - Changed Severity to be U8 instead of a BF
    - Changed FailureCode to be U8 instead of a BF
    - Changed PreservedMFA to be U64 instead of PI2O_MESSAGE_FRAME
* In I2O_SG_ELEMENT, make the 'Page' element an array just like the
  'Simple' element.  Requested for coding consistency.
* Remove the definition of I2O_FAILCODE_SEVERITY_SZ and 
  I2O_FAILCODE_CODE_SZ since they are no longer used.


i2opeer.h
---------
* New shared header file 

i2otypes.h
----------
* Changed Revision number to 1_5_5.

i2outil.h
---------
* Changed Revision number to 1_5_5.
* In the I2O_UTIL_REPLY_FAULT_NOTIFY_MESSAGE structure, changed the
  Severity and FailureCode fields to be U8, instead of BF, to match 
  the definition of the I2O_FAILURE_REPLY_MESSAGE_FRAME, defined in
  I2O_MSG.h.
