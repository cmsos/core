#!/bin/bash

multicast_route_set() 
{
    PING_OPTIONS_1='-c1 -w1'
    PING_OPTIONS_2='-c1 -i1'
    MULTICAST_ADDRESS='239.255.255.253'
    PING_ERROR_NO_ROUTE='unreachable'

    MSG_FAILED_TO_FIND='Failed to Detect Multicast Route'
    MSG_SUCCESS_ON_FIND='Multicast Route Enabled'
    MSG_ADDING_ROUTE='Attempting to Add Multicast Route ...'
    MSG_FAILED_TO_ADD=' FAILED - Route NOT Added.'
    MSG_SUCCES_ON_ADD=' SUCCESS - Route Added.'

    CMD_GET_INTERFACE="ip -o link show | awk 'BEGIN{FS=\": \"}!/^:digit:+: lo:.*/{print \$2}'"
    CMD_ADD_ROUTE="ip route add 224.0.0.0/4 dev"

    err_unreachable_found=`ping $PING_OPTIONS_1 $MULTICAST_ADDRESS 2>&1 1>/dev/null`

	if [ $? = 2 ]; then
        err_unreachable_found=`ping $PING_OPTIONS_2 $MULTICAST_ADDRESS 2>&1 1>/dev/null`
	fi


    #If errors, add route. Otherwise, do nothing
    if [ "$err_unreachable_found" ]; then 

        if [ $1 != 0 ]; then
            echo $MSG_FAILED_TO_FIND 
            echo $MSG_ADDING_ROUTE 
        fi

        $CMD_ADD_ROUTE `eval $CMD_GET_INTERFACE` > /dev/null 2>&1
        retval=$?
    
        if [ $1 != 0 ]; then

            if [ $retval = 0 ]; then
                echo $MSG_SUCCES_ON_ADD
            else
                FLAG_ROUTE_ADDED=1
                INTERFACE_LIST=`eval $CMD_GET_INTERFACE`
                for SINGLE_INTERFACE in $INTERFACE_LIST
                 do
                      $CMD_ADD_ROUTE $SINGLE_INTERFACE > /dev/null 2>&1
                      retval=$?
                      if [ $1 != 0 ]; then
                         if [ $retval = 0 ]; then
                             FLAG_ROUTE_ADDED=$retval
                         fi
                      fi
                 done
                 if [ $FLAG_ROUTE_ADDED = 0 ]; then
                         echo $MSG_SUCCES_ON_ADD
                         retval=$FLAG_ROUTE_ADDED
                 fi
            fi
        fi

    else
        if [ $1 != 0 ]; then
            echo -n $MSG_SUCCESS_ON_FIND
        fi
        retval=0
    fi

    return $retval
}

multicast_route_set 1
multicast_enabled=$?
if [ "$multicast_enabled" != "0" ] ; then
  echo "Failure: No Route Available for Multicast Traffic"
  exit 1
fi

exit 0
