// Module:  log4cplus/appender4oracle
// File:    OracleAppender.h
// Created: 6/2004
// Author:  Johannes Gutleber
//
//
// $Log: OracleAppender.cc,v $
// Revision 1.4  2006/06/29 08:46:21  xdaq
// SLC4 64bits: porting of appender for Oracle
//
// Revision 1.3  2006/03/09 14:20:27  xdaq
// Updated for testing with cmsimds
//
// Revision 1.2  2006/03/06 13:22:44  xdaq
// Updated for new schema
//
// Revision 1.1  2004/06/09 13:30:08  xdaq
// Created Oracle appender
//
//

#include <appender4oracle/OracleAppender.h>
#include <log4cplus/layout.h>
#include <log4cplus/helpers/loglog.h>
#include <log4cplus/spi/loggingevent.h>

#include <sys/types.h>
#include <unistd.h>
#include <sys/time.h>

#include <iostream>
#include <sstream>
#include <string>

using namespace std;
using namespace log4cplus;
using namespace log4cplus::helpers;

#define LOG4CPLUS_MESSAGE_VERSION 100



//////////////////////////////////////////////////////////////////////////////
// log4cplus::OracleAppender ctors and dtor
//////////////////////////////////////////////////////////////////////////////

log4cplus::OracleAppender::OracleAppender(const log4cplus::tstring& database,
					   const log4cplus::tstring& username,
					   const log4cplus::tstring& password,
					   const log4cplus::tstring& sessionName,
                       			const log4cplus::tstring& serverName)
: database_(database),
  username_(username),
  password_(password),
  serverName_(serverName),
  sessionName_(sessionName),
  autocommit_("true"),
  sessionId_(0),
  commitEvery_(0),
  commitInterval_(10),
  logCounter_(0)
{
	closed = true; // initially the connection to the database is closed
	
	// create an environment for multithreaded mutexed programs THREADED_MUTEXED
	env_ = Environment::createEnvironment (Environment::THREADED_MUTEXED);
	
	openConnection();
}



log4cplus::OracleAppender::OracleAppender(const Properties properties)
 : Appender(properties)
{
	database_ = properties.getProperty( LOG4CPLUS_TEXT("database") );
	username_ = properties.getProperty( LOG4CPLUS_TEXT("username") );
	password_ = properties.getProperty( LOG4CPLUS_TEXT("password") );
	sessionName_ = properties.getProperty( LOG4CPLUS_TEXT("session") );
	serverName_ = properties.getProperty( LOG4CPLUS_TEXT("servername") );
	autocommit_ = properties.getProperty( LOG4CPLUS_TEXT("autocommit") );
	std::string commitevery = properties.getProperty( LOG4CPLUS_TEXT("commitevery") );
	std::string commitinterval = properties.getProperty( LOG4CPLUS_TEXT("commitinterval") );
	
	commitEvery_ = atoi(commitevery.c_str());
	commitInterval_ = atoi(commitinterval.c_str());

	closed = true; // initially the connection to the database is closed
	
	// create an environment for multithreaded mutexed programs THREADED_MUTEXED
	env_ = Environment::createEnvironment (Environment::THREADED_MUTEXED);
		
	openConnection();
}

void log4cplus::OracleAppender::createCommitThread(unsigned int seconds)
{
	int status = 0;
	
	status = pthread_attr_init(&commit_thread_attr_);
	if (status != 0)
	{
		getLogLog().error(LOG4CPLUS_TEXT("OracleAppender::createCommitThread() - could not create thread attributes"));
		return;
	}
	// set detached: all thread resources are cleared automatically
	// status = pthread_attr_setdetachstate(&commit_thread_attr_,PTHREAD_CREATE_DETACHED);
	
	if (status != 0)
	{
		getLogLog().error(LOG4CPLUS_TEXT("OracleAppender::createCommitThread() - could not detach commit thread"));
		return;
	}
	
	status = pthread_create(&commit_thread_id_, &commit_thread_attr_, commitThreadFunc, (void*) this);
	if (status != 0)
	{
		getLogLog().error(LOG4CPLUS_TEXT("OracleAppender::createCommitThread() - could not create thread"));
		return;
	}

}


void* log4cplus::commitThreadFunc(void* appender)
{
	log4cplus::OracleAppender* a = static_cast<log4cplus::OracleAppender*>(appender);
	if (a != 0)
	{
		a->commitThreadRunning_ = true;
		while (a->commitThreadRunning_)
		{
			::sleep(a->getCommitInterval());
			a->commit();
		}
		pthread_attr_destroy(&a->commit_thread_attr_);
	}
	return 0;
}

unsigned int log4cplus::OracleAppender::getCommitInterval()
{
	return commitInterval_;
}

void log4cplus::OracleAppender::commit()
{
	if (!closed)
	{
		try
		{
			// hard reset of log counter. Not really safe, but sufficient
			logCounter_ = 0;
			conn_->commit();
		} 
		catch (SQLException& e)
		{
			std::string msg = "OracleAppender - Failed to commit, ";
			msg += e.what();
			getLogLog().error(LOG4CPLUS_TEXT(msg));
		}
	}
}

log4cplus::OracleAppender::~OracleAppender()
{
	// destructor Impl calls close()
	destructorImpl();
}



//////////////////////////////////////////////////////////////////////////////
// log4cplus::OracleAppender public methods
//////////////////////////////////////////////////////////////////////////////

void 
log4cplus::OracleAppender::close()
{
    getLogLog().debug(LOG4CPLUS_TEXT("Entering OracleAppender::close()..."));
	
	if (env_ != 0)
	{
		// for safety immediately set to closed
		closed = true;
		
		// stop thread. Takes at least commitinterval_ seconds
		// therefore cancel (harmless)
		if (commitThreadRunning_)
		{
			commitThreadRunning_ = false;
			getLogLog().debug(LOG4CPLUS_TEXT("OracleAppender - waiting for commit thread terminating"));
			pthread_join(commit_thread_id_, 0);
		}
		
		env_->terminateConnection(conn_);
		env_ = 0;
	}
}



//////////////////////////////////////////////////////////////////////////////
// log4cplus::SocketAppender protected methods
//////////////////////////////////////////////////////////////////////////////

void
log4cplus::OracleAppender::openConnection()
{
	if(closed) 
	{
		try 
		{
			conn_ = env_->createConnection (username_, password_, database_);
			closed = false;
		} 
		catch (SQLException& e)
		{
			std::string msg = "OracleAppender - Failed to create connection to ";
			msg += database_;
			msg += ", ";
			msg += e.what();
			getLogLog().error(LOG4CPLUS_TEXT(msg));
			return;
		}
		
		try 
		{
			// Check if the provided session identifier is existing
			Statement* stmt = conn_->createStatement();
			stmt->setAutoCommit(TRUE);
			std::string query = "SELECT id, to_char(end_time, 'dd/mm/yyyy hh24:MI:ss (tzh:tzm)')  FROM log_session WHERE description = '";
			query += sessionName_;
			query += "'";
		
			ResultSet* rs = stmt->executeQuery(query);
			
			// Result set should have one entry only
			if (rs->next())
			{
				sessionId_ = rs->getUInt(1);
				std::string sessionEndTime = rs->getString(2);
							
				// if the end timestamp is not null, the open connection fails, because the session is closed.
				// Oracle returns by default 0, if the value of the column is NULL
				if (sessionEndTime != "")
				{
					std::string msg = "OracleAppender::openConnection() - Failed to open session ";
					msg += sessionName_;
					msg += ", session has been closed at ";
					msg += sessionEndTime;
					getLogLog().error(LOG4CPLUS_TEXT(msg));
					this->close();
					return;
				}
			} 
			else
			{
				// The session does not exist, create it
				std::string createStmt = "INSERT INTO log_session (description, username, start_time) VALUES ('";
				createStmt += sessionName_;
				createStmt += "', '";
				createStmt += username_;
				createStmt += "', ";				
				
				struct tm now_t;
				time_t now_seconds = time(0);
				localtime_r( &now_seconds, &now_t );
				
				std::ostringstream timeString;				
				timeString << now_t.tm_mday << "-" << now_t.tm_mon+1 << "-" << now_t.tm_year+1900 << " ";
				timeString << now_t.tm_hour << ":" << now_t.tm_min << ":" << now_t.tm_sec;
				
				createStmt += "to_timestamp('";
				createStmt += timeString.str();
				createStmt += "', 'dd-mm-yyyy hh24:mi:ss'))";
				
				try
				{
					unsigned int row = stmt->executeUpdate(createStmt);
				
					if (row != 0)
					{				
						// Get now the session_id from the newly created row
						ResultSet* rs = stmt->executeQuery(query);

						// Result set should have one entry only
						if (rs->next())
						{
							sessionId_ = rs->getUInt(1);
						} else 
						{
							// still not there. Give up.
							getLogLog().error(LOG4CPLUS_TEXT("OracleAppender - Failed to access session after creation"));
							this->close();
							return;
						}
					} 
					else
					{
						std::string msg = "OracleAppender - Failed to create session ";
						msg += sessionName_;
						getLogLog().error(LOG4CPLUS_TEXT(msg));
						this->close();
						return;
					}
				}
				catch (SQLException& sqle)
				{
					std::string msg = "OracleAppender - Failed to create new session, ";
					msg += sqle.what();
					getLogLog().error(LOG4CPLUS_TEXT(msg));
					this->close();
					return;
				}
			}
		} 
		catch (SQLException& e)
		{
			std::string msg = "OracleAppender - ";
			msg += e.what();
			getLogLog().error(LOG4CPLUS_TEXT(msg));
			this->close();
			return;
		}
		
		// Prepare SQL statements used for logging
		this->prepareStatements();
		
		// if the commit interval is set, create a commit thread
		if (commitInterval_ > 0)
		{
			this->createCommitThread(commitInterval_);
		}
    }
}


void
log4cplus::OracleAppender::prepareStatements()
{
	try
	{
		std::string stmt = "INSERT INTO log_event (session_id, timestamp, message, logger_name, level_string, ndc, ";
		stmt += "thread_name, id, caller_filename, caller_class, caller_method, caller_line) ";
		stmt += "VALUES (:1, :2, :3, :4, :5, :6, :7, :8, :9, :10, :11, :12 )";
		insertLogEvent_ = conn_->createStatement (stmt);
		
		// use autocommit
		if (autocommit_ == "true")
		{
			insertLogEvent_->setAutoCommit(TRUE);
		}
		
	} 
	catch (SQLException& e)
	{
		getLogLog().error(LOG4CPLUS_TEXT(e.what()));
	}
}


void
log4cplus::OracleAppender::append(const spi::InternalLoggingEvent& event)
{
	if (closed)
	{
		openConnection();
	}

	if(!closed) 
	{
		// fill the SQL parameters
		//
		try
		{
			insertLogEvent_->setUInt (1, sessionId_); // :1 session_id
			
			// For Oracle 9i we can directly use the TIMESTAMP datatype
			struct tm t;
			time_t now_seconds = event.getTimestamp().sec();
			
			localtime_r( &now_seconds, &t ); // convert seconds since /1/1/1970 into a structure called t
			
			// TIMESTAMP(3) gives the timestamp with fraction of seconds in milliseconds
			// convert useconds to milliseconds
			long millis = event.getTimestamp().usec()*1000;
			
			Timestamp ts(env_, 
				t.tm_year+1900, 
				t.tm_mon+1, 
				t.tm_mday, 
				t.tm_hour, 
				t.tm_min, 
				t.tm_sec, 
				millis);
			insertLogEvent_->setTimestamp (2, ts);
			
			insertLogEvent_->setString (3, event.getMessage() ); // :3 message
			insertLogEvent_->setString (4, event.getLoggerName() ); // :4 logger_name
			insertLogEvent_->setString (5, getLogLevelManager().toString(event.getLogLevel()) ); // :5 level_string
			insertLogEvent_->setString (6, event.getNDC() ); // :6 NDC
			insertLogEvent_->setString (7, event.getThread() ); // :7 thread_name

			std::string uid = this->generateId();
			
			// std::cout << "log id is: " << uid << std::endl;

			insertLogEvent_->setString (8, uid ); // :8 id, for the time being always 0x0, would point to properties or exception table

			insertLogEvent_->setString (9, event.getFile() ); // :9 caller_filename
			insertLogEvent_->setString (10, "unknown" ); // :10 caller_class, does not exist in log4cplus
			insertLogEvent_->setString (11, "unknown" ); // :11 caller_method, does not exist in log4cplus
			
			std::ostringstream line;
			line << event.getLine();
			insertLogEvent_->setString (12, line.str() ); // :12 caller_line

			// insert the log message entry
			insertLogEvent_->executeUpdate();
			logCounter_++;
			
			// Check for commit required it commitevery_ is set
			if ((commitEvery_ != 0) && (logCounter_ == commitEvery_))
			{
				logCounter_ = 0;
				conn_->commit();
			}
		} 
		catch (SQLException& e)
		{
			std::string msg = "Failed to insert message: ";
			msg += e.what();
			getLogLog().error(LOG4CPLUS_TEXT(msg));
		}
	
	}
	else 
	{
		    // if we do not manage to create the connection log an error
		std::string msg = "OracleAppender - Failed to connect to database ";
		msg += database_;
            getLogLog().error(LOG4CPLUS_TEXT(msg));
            return;
	}
}

std::string log4cplus::OracleAppender::generateId()
{
	std::stringstream s;
	char host_name[255];
        if (gethostname(host_name, 255) == 0)
        {
                s << host_name;
        } else
        {
                s << "localhost";
        }
	
	s << ":" << getpid() << ":";
	struct timeval tv;
	gettimeofday(&tv,0);
	s << tv.tv_sec << ":" << tv.tv_usec;
	return s.str();
}
