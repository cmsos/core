-- This SQL script creates the required tables by org.apache.log4j.db.DBAppender and 
-- org.apache.log4j.db.DBReceiver.
--
-- It is intended for Oracle databases.

CREATE TABLE logging_event 
  (
    session_id	      NUMBER(10) NOT NULL,
    timestamp         TIMESTAMP(3) NOT NULL,
    rendered_message  VARCHAR2(4000) NOT NULL,
    logger_name       VARCHAR2(254) NOT NULL,
    level_string      VARCHAR2(254) NOT NULL,
    ndc               VARCHAR2(4000),
    thread_name       VARCHAR2(254),
    reference_flag    SMALLINT,
    caller_filename   VARCHAR2(254) NOT NULL,
    caller_class      VARCHAR2(254) NOT NULL,
    caller_method     VARCHAR2(254) NOT NULL,
    caller_line       CHAR(4) NOT NULL
  );


CREATE SEQUENCE logging_session_id_seq MINVALUE 1 START WITH 1;

CREATE TABLE logging_session
  (
    session_id       NUMBER(10) NOT NULL,
    session_name     VARCHAR2(254) NOT NULL,
    start_time       TIMESTAMP NOT NULL,
    end_time         TIMESTAMP,
    PRIMARY KEY (session_name)
  );

CREATE TRIGGER logging_session_id_seq_trig
  BEFORE INSERT ON logging_session
  FOR EACH ROW  
  BEGIN  
    SELECT logging_session_id_seq.NEXTVAL 
    INTO   :NEW.session_id 
    FROM   DUAL;  
  END logging_session_id_seq_trig;
/

