// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _b2in_eventing_version_h_
#define _b2in_eventing_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define CORE_B2INEVENTING_VERSION_MAJOR 3
#define CORE_B2INEVENTING_VERSION_MINOR 0
#define CORE_B2INEVENTING_VERSION_PATCH 2
// If any previous versions available E.g. #define CORE_B2INEVENTING_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define CORE_B2INEVENTING_PREVIOUS_VERSIONS "2.0.0,2.0.1,2.0.2,2.0.3,2.0.4,2.1.0,2.2.0,3.0.0,3.0.1"


//
// Template macros
//
#define CORE_B2INEVENTING_VERSION_CODE PACKAGE_VERSION_CODE(CORE_B2INEVENTING_VERSION_MAJOR,CORE_B2INEVENTING_VERSION_MINOR,CORE_B2INEVENTING_VERSION_PATCH)
#ifndef CORE_B2INEVENTING_PREVIOUS_VERSIONS
#define CORE_B2INEVENTING_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(CORE_B2INEVENTING_VERSION_MAJOR,CORE_B2INEVENTING_VERSION_MINOR,CORE_B2INEVENTING_VERSION_PATCH)
#else 
#define CORE_B2INEVENTING_FULL_VERSION_LIST  CORE_B2INEVENTING_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(CORE_B2INEVENTING_VERSION_MAJOR,CORE_B2INEVENTING_VERSION_MINOR,CORE_B2INEVENTING_VERSION_PATCH)
#endif 

namespace b2ineventing
{
	const std::string project = "core";
	const std::string package  =  "b2ineventing";
	const std::string versions = CORE_B2INEVENTING_FULL_VERSION_LIST;
	const std::string description = "";
	const std::string authors = "Johannes Gutleber, Luciano Orsini";
	const std::string summary = "B2IN fast publish/subscriber";
	const std::string link = "http://xdaqwiki.cern.ch/index.php";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
