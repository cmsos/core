/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2008, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: R. Moser                                                     *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "cgicc/HTTPResponseHeader.h" 


#include "b2in/eventing/Diagnostics.h"


b2in::eventing::Diagnostics::Diagnostics():enabled_(false)
{
}

b2in::eventing::Diagnostics::~Diagnostics()
{
}

std::vector<xdata::UnsignedInteger64T> b2in::eventing::Diagnostics::initCounters()
{
	std::vector<xdata::UnsignedInteger64T> v;
	for( int k = INTERNAL_LOSS ; k <= OUTGOING ; k++ )
	{
		v.push_back(0);
		//v[k] = 0;
	}
	
	return v;
}

void b2in::eventing::Diagnostics::increment(const std::string & topic, const std::string & originator, unsigned int type)
{
	if ( ! enabled_ ) return;

	std::map<std::string, std::map<std::string, std::vector<xdata::UnsignedInteger64T> > >::iterator i = counters_.find(topic);
	if( i != counters_.end() )
	{
		std::map<std::string, std::vector<xdata::UnsignedInteger64T> >::iterator j = (*i).second.find(originator);
		if ( j != (*i).second.end() )
		{
			(*j).second[type] += 1;
		}
		else
		{
			(*i).second[originator] = this->initCounters();
			(*i).second[originator][type]  = 1;
		}
	}
	else
	{
		std::vector<xdata::UnsignedInteger64T> v = this->initCounters();
		v[type]  = 1;

		std::map<std::string, std::vector<xdata::UnsignedInteger64T> > item;
		item[originator] = v;
		counters_[topic] = item;
	}
}

void b2in::eventing::Diagnostics::incrementInternalLoss(const std::string & topic, const std::string & originator)
{
	increment(topic, originator, INTERNAL_LOSS);
}

void b2in::eventing::Diagnostics::incrementOutgoingLoss(const std::string & topic, const std::string & originator)
{
	increment(topic, originator, OUTGOING_LOSS);
}

void b2in::eventing::Diagnostics::incrementMemoryLoss(const std::string & topic, const std::string & originator)
{
	increment(topic, originator, MEMORY_LOSS);
}

void b2in::eventing::Diagnostics::incrementEnqueuingLoss(const std::string & topic, const std::string & originator)
{
	increment(topic, originator, ENQUEUING_LOSS);
}

void b2in::eventing::Diagnostics::incrementIncoming(const std::string & topic, const std::string & originator)
{
	increment(topic, originator, INCOMING);
}

void b2in::eventing::Diagnostics::incrementOutgoing(const std::string & topic, const std::string & originator)
{
	increment(topic, originator, OUTGOING);
}

void b2in::eventing::Diagnostics::enable()
{
	enabled_ = true;
}

void b2in::eventing::Diagnostics::disable()
{
	enabled_ = false;
}


void b2in::eventing::Diagnostics::writeTo(xgi::Output * out)
{
	*out << cgicc::table().set("class","xdaq-table").set("style","width: 100%;");

	*out << cgicc::thead();
	*out << cgicc::tr();
	*out << cgicc::th("Topic").set("class", "xdaq-sortable");
	*out << cgicc::th("Originator");
	*out << cgicc::th("Internal Loss");
	*out << cgicc::th("Outgoing Loss");
	*out << cgicc::th("Out of Memory Loss");
	*out << cgicc::th("Enqueuing Loss");
	*out << cgicc::th("Incoming Counter");
	*out << cgicc::th("Outgoing Counter");
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead();
	
	*out << cgicc::tbody();

	for( std::map<std::string, std::map<std::string, std::vector<xdata::UnsignedInteger64T> > >::iterator i = counters_.begin() ; i != counters_.end() ; i++ )
	{
		for( std::map<std::string, std::vector<xdata::UnsignedInteger64T> >::iterator j = (*i).second.begin() ; j != (*i).second.end() ; j++ )
		{
			*out << cgicc::tr() << std::endl;
			*out << cgicc::td((*i).first) << std::endl;

			*out << cgicc::td() << std::endl;
			*out << cgicc::a((*j).first).set("href",(*j).first);
			*out << cgicc::td() << std::endl;

			for(int k = INTERNAL_LOSS ; k <= OUTGOING ; k++)
			{
				xdata::UnsignedInteger64 tmp = (*j).second[k];
				*out << cgicc::td(tmp.toString()) << std::endl;
			}
			*out << cgicc::tr() << std::endl;
		}
	}

	*out << cgicc::tbody();	
	*out << cgicc::table();
}

