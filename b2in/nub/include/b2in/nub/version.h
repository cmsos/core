// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, L. Orsini and D. Simelevicius                   *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _b2in_nub_version_h_
#define _b2in_nub_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define CORE_B2INNUB_VERSION_MAJOR 2
#define CORE_B2INNUB_VERSION_MINOR 5
#define CORE_B2INNUB_VERSION_PATCH 2
// If any previous versions available E.g. #define CORE_B2INNUB_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define CORE_B2INNUB_PREVIOUS_VERSIONS "2.3.0,2.4.0,2.5.0,2.5.1"


//
// Template macros
//
#define CORE_B2INNUB_VERSION_CODE PACKAGE_VERSION_CODE(CORE_B2INNUB_VERSION_MAJOR,CORE_B2INNUB_VERSION_MINOR,CORE_B2INNUB_VERSION_PATCH)
#ifndef CORE_B2INNUB_PREVIOUS_VERSIONS
#define CORE_B2INNUB_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(CORE_B2INNUB_VERSION_MAJOR,CORE_B2INNUB_VERSION_MINOR,CORE_B2INNUB_VERSION_PATCH)
#else 
#define CORE_B2INNUB_FULL_VERSION_LIST  CORE_B2INNUB_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(CORE_B2INNUB_VERSION_MAJOR,CORE_B2INNUB_VERSION_MINOR,CORE_B2INNUB_VERSION_PATCH)
#endif 


namespace b2innub 
{
	const std::string project = "core";
	const std::string package  =  "b2innub";
   	const std::string versions = CORE_B2INNUB_FULL_VERSION_LIST;
	const std::string summary = "B2IN protocol pluggable core";
	const std::string description = "";
	const std::string authors = "Johannes Gutleber, Luciano Orsini";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/B2IN_Messaging";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
