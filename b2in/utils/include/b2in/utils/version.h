// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _b2in_utils_version_h_
#define _b2in_utils_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define CORE_B2INUTILS_VERSION_MAJOR 4
#define CORE_B2INUTILS_VERSION_MINOR 4
#define CORE_B2INUTILS_VERSION_PATCH 5
// If any previous versions available E.g. #define CORE_B2INUTILS_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define CORE_B2INUTILS_PREVIOUS_VERSIONS "4.3.0,4.3.1,4.3.2,4.4.2,4.4.3,4.4.4"

//
// Template macros
//
#define CORE_B2INUTILS_VERSION_CODE PACKAGE_VERSION_CODE(CORE_B2INUTILS_VERSION_MAJOR,CORE_B2INUTILS_VERSION_MINOR,CORE_B2INUTILS_VERSION_PATCH)
#ifndef CORE_B2INUTILS_PREVIOUS_VERSIONS
#define CORE_B2INUTILS_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(CORE_B2INUTILS_VERSION_MAJOR,CORE_B2INUTILS_VERSION_MINOR,CORE_B2INUTILS_VERSION_PATCH)
#else 
#define CORE_B2INUTILS_FULL_VERSION_LIST  CORE_B2INUTILS_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(CORE_B2INUTILS_VERSION_MAJOR,CORE_B2INUTILS_VERSION_MINOR,CORE_B2INUTILS_VERSION_PATCH)
#endif 


namespace b2inutils 
{
	const std::string project = "core";
	const std::string package  =  "b2inutils";
   	const std::string versions = CORE_B2INUTILS_FULL_VERSION_LIST;
	const std::string summary = "B2IN protocol utilities";
	const std::string description = "";
	const std::string authors = "Johannes Gutleber, Luciano Orsini";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/B2IN_Messaging";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
