// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "b2in/utils/Statistics.h"

b2in::utils::Statistics::Statistics()
{
	monitorFireCounter_              = 0;
	monitorPulseCounter_             = 0;
	monitorInternalLossCounter_      = 0;
	monitorCommunicationLossCounter_ = 0;
	monitorMemoryLossCounter_        = 0;
	monitorUnassignedLossCounter_    = 0;
}

b2in::utils::Statistics::~Statistics()
{
}

void b2in::utils::Statistics::incrementPulseCounter()
{
	monitorPulseCounter_++;
}

void b2in::utils::Statistics::incrementFireCounter()
{
	monitorFireCounter_++;
}

void b2in::utils::Statistics::incrementInternalLossCounter()
{
	monitorInternalLossCounter_++;
}

void b2in::utils::Statistics::incrementCommunicationLossCounter()
{
	monitorCommunicationLossCounter_++;
}

void b2in::utils::Statistics::incrementMemoryLossCounter()
{
	monitorMemoryLossCounter_++;
}

void b2in::utils::Statistics::incrementUnassignedLossCounter()
{
	monitorUnassignedLossCounter_++;
}

xdata::UnsignedInteger64T b2in::utils::Statistics::getFireCounter()
{
	return monitorFireCounter_;
}

xdata::UnsignedInteger64T b2in::utils::Statistics::getPulseCounter()
{
	return monitorPulseCounter_;
}

xdata::UnsignedInteger64T b2in::utils::Statistics::getInternalLossCounter()
{
	return monitorInternalLossCounter_;
}

xdata::UnsignedInteger64T b2in::utils::Statistics::getCommunicationLossCounter()
{
	return monitorCommunicationLossCounter_;
}

xdata::UnsignedInteger64T b2in::utils::Statistics::getMemoryLossCounter()
{
	return monitorMemoryLossCounter_;
}

xdata::UnsignedInteger64T b2in::utils::Statistics::getUnassignedLossCounter()
{
	return monitorUnassignedLossCounter_;
}

