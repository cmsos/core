// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

//
// Version definition for MStreamIO
//
#ifndef _exampleshighcharts_Version_h_
#define _exampleshighcharts_Version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define CORE_EXAMPLESHIGHCHARTS_VERSION_MAJOR 1
#define CORE_EXAMPLESHIGHCHARTS_VERSION_MINOR 0
#define CORE_EXAMPLESHIGHCHARTS_VERSION_PATCH 0
// If any previous versions available E.g. #define CORE_EXAMPLESHIGHCHARTS_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef CORE_EXAMPLESHIGHCHARTS_PREVIOUS_VERSIONS


//
// Template macros
//
#define CORE_EXAMPLESHIGHCHARTS_VERSION_CODE PACKAGE_VERSION_CODE(CORE_EXAMPLESHIGHCHARTS_VERSION_MAJOR,CORE_EXAMPLESHIGHCHARTS_VERSION_MINOR,CORE_EXAMPLESHIGHCHARTS_VERSION_PATCH)
#ifndef CORE_EXAMPLESHIGHCHARTS_PREVIOUS_VERSIONS
#define CORE_EXAMPLESHIGHCHARTS_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(CORE_EXAMPLESHIGHCHARTS_VERSION_MAJOR,CORE_EXAMPLESHIGHCHARTS_VERSION_MINOR,CORE_EXAMPLESHIGHCHARTS_VERSION_PATCH)
#else 
#define CORE_EXAMPLESHIGHCHARTS_FULL_VERSION_LIST  CORE_EXAMPLESHIGHCHARTS_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(CORE_EXAMPLESHIGHCHARTS_VERSION_MAJOR,CORE_EXAMPLESHIGHCHARTS_VERSION_MINOR,CORE_EXAMPLESHIGHCHARTS_VERSION_PATCH)
#endif 

namespace exampleshighcharts
{
	const std::string project = "core";
	const std::string package  =  "exampleshighcharts";
	const std::string versions = CORE_EXAMPLESHIGHCHARTS_FULL_VERSION_LIST;
	const std::string summary = "Example for using the highcharts library";
	const std::string description = "";
	const std::string authors = "Andy Forrest";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/Power_Pack";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
