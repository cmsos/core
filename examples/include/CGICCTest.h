// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2022, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _CGICCTest_h_
#define _CGICCTest_h_

#include "xdaq/WebApplication.h"
#include "xgi/Method.h"
#include "xdata/UnsignedLong.h"
#include "cgicc/HTMLClasses.h"

#include <string>

const std::string styles =
"body { color: black; background: white; }\n"
"span.red { color:red; }\n"
"hr.half { width: 60%; margin-left: auto; margin-right: auto; }\n"
"div.center { text-align: center; }\n"
"div.notice { border: solid thin; padding: 1em; margin: 1em 0; "
"background: #ddd; text-align: center; }"
"table { width: 90%; margin-left: auto; margin-right: auto; }\n"
"tr.title, td.title { color: white; background: black; font-weight: bold; "
"text-align: center; }\n"
"tr.data, td.data { background: #ddd; }\n"
"td.form { background: #ddd; text-align: center; }\n"
;


class CGICCTest: public xdaq::Application 
{
	public:
	
	XDAQ_INSTANTIATOR();
	
	CGICCTest(xdaq::ApplicationStub * s) ;
	
	void Default(xgi::Input * in, xgi::Output * out ) ;
	
	
};

#endif
