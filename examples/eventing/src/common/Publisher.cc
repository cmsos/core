// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2022, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Forrest, D. Simelevicius                       *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "examples/eventing/Publisher.h"

#include "xgi/framework/Method.h"
#include "xgi/Method.h"
#include "toolbox/mem/CommittedHeapAllocator.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include <unistd.h>

XDAQ_INSTANTIATOR_IMPL (Publisher);

Publisher::Publisher (xdaq::ApplicationStub* c) 
	: xdaq::Application(c), xgi::framework::UIManager(this), eventing::api::Member(this)
{
	eventingBusName_ = "eventing1";
	topicName_ = "test";
	this->getApplicationInfoSpace()->fireItemAvailable("eventingBusName", &eventingBusName_);
	this->getApplicationInfoSpace()->fireItemAvailable("topicName", &topicName_);

	xgi::framework::deferredbind(this, this, &Publisher::Default, "Default");
	xgi::bind(this, &Publisher::sendNext, "sendNext");
	
	this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");

	toolbox::mem::CommittedHeapAllocator* a = new toolbox::mem::CommittedHeapAllocator(0x2000000);
	toolbox::net::URN urn("publisher", "example");
	pool_ = toolbox::mem::getMemoryPoolFactory()->createPool(urn, a);

}

void Publisher::actionPerformed (toolbox::Event& event)
{
	if (event.type() == "eventing::api::BusReadyToPublish")
	{
		std::string busname = (static_cast<eventing::api::Bus*>(event.originator()))->getBusName();
		std::cout << "event Bus '" << busname << "' is ready to publish"  << std::endl;
	}
}

void Publisher::actionPerformed (xdata::Event& event)
{
	if (event.type() == "urn:xdaq-event:setDefaultValues")
	{
		// User should listen for publsh readiness, but should also check immediately
		this->getEventingBus(eventingBusName_).addActionListener(this);
		if (this->getEventingBus(eventingBusName_).canPublish())
		{
			// ready now
			std::cout << "Eventing bus is ready at setDefaultValues" << std::endl;
		}
	}
}

void Publisher::sendNext (xgi::Input * in, xgi::Output * out)
{
	std::thread iotaThread([&](int startArg)
	{
		while(1)
		{
			// create fake message with fake property
			xdata::Properties plist;
			plist.setProperty("counter", "0");
			 toolbox::mem::Reference* ref = 0 ;
			try
			{
				ref = toolbox::mem::getMemoryPoolFactory()->getFrame(this->pool_, 0x100000);
				ref->setDataSize(2048);
				this->getEventingBus(eventingBusName_).publish(this->topicName_, ref, plist);
				//std::cout << "message sent" << std::endl;
				//sleep(1);
				usleep(1000);
				//break;
			}
			catch (eventing::api::exception::Exception& err)
			{
				ref->release();
				std::string const msg = toolbox::toString("Failed to publish to eventing. Bus name '%s', topic name '%s'. Problem: '%s'.", this->eventingBusName_.c_str(), this->topicName_.c_str(), err.what());
				LOG4CPLUS_ERROR(this->getApplicationLogger(), msg);
			}
		}
	}, 10);

	iotaThread.detach();

	std::cout << "done" << std::endl;

	out->getHTTPResponseHeader().addHeader("Content-Type", "text/plain");
}

void Publisher::Default (xgi::Input * in, xgi::Output * out) 
{
	*out << "<div class=\"xdaq-tab-wrapper\">";
	*out << "<div class=\"xdaq-tab\" title=\"Control\">";
	std::string triggerURL;
	triggerURL = "/";
	triggerURL += getApplicationDescriptor()->getURN();
	triggerURL += "/sendNext";
	*out << "<a onclick=\"xdaqAJAX({url: '" << triggerURL << "'});\" href=\"#\">" << "Send message" << "</a>" << std::endl;

	*out << "<br/><br/><hr/><br/>" << std::endl;

	// test eventing table

	*out << "</div>";
	*out << "<div class=\"xdaq-tab\" title=\"Eventing\">";

	*out << "<h3>Buses</h3>";

	*out << this->busesToHTML() << std::endl;

	*out << "</div>";
	*out << "</div>";
}


