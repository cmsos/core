/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, L. Orsini and D. Simelevicius                   *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _xrelay_version_h_
#define _xrelay_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define CORE_XRELAY_VERSION_MAJOR 4
#define CORE_XRELAY_VERSION_MINOR 2
#define CORE_XRELAY_VERSION_PATCH 0
// If any previous versions available E.g. #define CORE_XRELAY_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef CORE_XRELAY_PREVIOUS_VERSIONS


//
// Template macros
//
#define CORE_XRELAY_VERSION_CODE PACKAGE_VERSION_CODE(CORE_XRELAY_VERSION_MAJOR,CORE_XRELAY_VERSION_MINOR,CORE_XRELAY_VERSION_PATCH)
#ifndef CORE_XRELAY_PREVIOUS_VERSIONS
#define CORE_XRELAY_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(CORE_XRELAY_VERSION_MAJOR,CORE_XRELAY_VERSION_MINOR,CORE_XRELAY_VERSION_PATCH)
#else 
#define CORE_XRELAY_FULL_VERSION_LIST  CORE_XRELAY_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(CORE_XRELAY_VERSION_MAJOR,CORE_XRELAY_VERSION_MINOR,CORE_XRELAY_VERSION_PATCH)
#endif 

namespace xrelay 
{
	const std::string project = "core";
	const std::string package  =  "xrelay";
   	const std::string versions = CORE_XRELAY_FULL_VERSION_LIST;
	const std::string summary = "multi-hop relaying of one-way SOAP";
	const std::string description = "";
	const std::string authors = "Johannes Gutleber, Luciano Orsini";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/XRelay";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
