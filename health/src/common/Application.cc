/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini and D. Simelevicius                                *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "health/Application.h"

#include "b2in/nub/Method.h"

#include "xdaq/Event.h"
#include "xdaq/EndpointAvailableEvent.h"
#include "xdaq/InstantiateApplicationEvent.h"
#include "xdaq/ApplicationContextImpl.h"

#include "xgi/framework/Method.h"
#include "xgi/Utils.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTMLClasses.h"

#include "toolbox/task/TimerFactory.h"

#include "toolbox/regex.h"
#include "toolbox/utils.h"
#include "toolbox/Runtime.h"

#include "xcept/tools.h"

#include "xoap/DOMParserFactory.h"
#include "xoap/domutils.h"

XDAQ_INSTANTIATOR_IMPL (health::Application)

health::Application::Application(xdaq::ApplicationStub * s)
	: xdaq::Application(s), xgi::framework::UIManager(this), alive_(false), ready_(false), eventCounter_(0)
{
	s->getDescriptor()->setAttribute("icon", "/health/images/health.jpg");

	s->getDescriptor()->setAttribute("service", "health");

	xgi::framework::deferredbind(this, this, &health::Application::Default, "Default");
	xgi::bind(this, &health::Application::startup, "startup");
	xgi::bind(this, &health::Application::liveness, "liveness");
	xgi::bind(this, &health::Application::readiness, "readiness");
	xgi::bind(this, &health::Application::inject, "inject");

	// Listen to events indicating the setting of the application's default values
	getApplicationContext()->addActionListener(this);

	//Define that application is ready
	auto readiness = dynamic_cast<xdata::Boolean*>(getApplicationInfoSpace()->find("readiness"));
	*readiness = true;
}

health::Application::~Application()
{
}

void health::Application::actionPerformed(toolbox::Event & e)
{
	eventCounter_++;
	if (e.type() == "urn:xdaq-event:InstantiateApplication")
	{	
		//Retrieving application lid
		xdaq::InstantiateApplicationEvent & iae = dynamic_cast<xdaq::InstantiateApplicationEvent&>(e);
		unsigned int lid = iae.getApplicationDescriptor()->getLocalId();
		
		//Adding listener to application infospace to listen for changes in application liveness and readiness
		auto application = getApplicationContext()->getApplicationRegistry()->getApplication(lid);
		auto infospace = application->getApplicationInfoSpace();
		infospace->addItemChangedListener("liveness", this);
		auto liveness = dynamic_cast<xdata::Boolean*>(infospace->find("liveness"));
		infospace->addItemChangedListener("readiness", this);
		auto readiness = dynamic_cast<xdata::Boolean*>(infospace->find("readiness"));

		std::lock_guard<std::mutex> guard(mutex_);

		//Marking the application readiness
		readiness_[lid] = *readiness;
		refreshReadiness();

		//Marking the application as alive
		liveness_[lid] = *liveness;
		refreshLiveness();
	}
	else if ((e.type() == "urn:xdaq-event:profile-loaded") || (e.type() == "urn:xdaq-event:configuration-loaded"))
	{
		const xdaq::ContextDescriptor * context = getApplicationContext()->getContextDescriptor();

		std::lock_guard<std::mutex> guard(mutex_);
		configured_ = getApplicationContext()->getDefaultZone()->getApplicationDescriptors(context);
		refreshLiveness();
		refreshReadiness();
	}
}

void health::Application::actionPerformed(xdata::Event & e)
{
	if (e.type() == "ItemChangedEvent")
	{
		xdata::ItemEvent& itemEvent = dynamic_cast<xdata::ItemEvent&>(e);
		if ((itemEvent.itemName() == "readiness") || (itemEvent.itemName() == "liveness"))
		{
			auto stub = dynamic_cast<xdaq::ApplicationStub*>(itemEvent.infoSpace()->find("stub"));
			unsigned int lid = stub->getDescriptor()->getLocalId();
			auto item = dynamic_cast<xdata::Boolean*>(itemEvent.item());

			std::lock_guard<std::mutex> guard(mutex_);
			if (itemEvent.itemName() == "readiness")
			{
				readiness_[lid] = *item;
				refreshReadiness();
			}
			else if (itemEvent.itemName() == "liveness")
			{
				liveness_[lid] = *item;
				refreshLiveness();
			}
		}
	}
	else
	{
		std::stringstream msg;
		msg << "Failed to process unknown event type '" << e.type() << "'";
		LOG4CPLUS_FATAL(this->getApplicationLogger(), msg.str());
	}
}

// Hyperdaq

void health::Application::Default(xgi::Input * in, xgi::Output * out)
{
	std::lock_guard<std::mutex> guard(mutex_);

	// Total Events table
	*out << cgicc::table().set("class", "xdaq-table-vertical") << std::endl;
	*out << cgicc::caption("Total events") << std::endl;

	*out << cgicc::thead() << std::endl;
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Incoming Events";
	*out << cgicc::th();
	*out << cgicc::tr();
	*out << cgicc::thead() << std::endl;

	*out << cgicc::tbody() << std::endl;
	*out << cgicc::tr();
	*out << cgicc::td();
	*out << eventCounter_;
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;
	*out << cgicc::tbody() << std::endl;

	*out << cgicc::table() << std::endl;

	//Configured applications
	*out << cgicc::table().set("class", "xdaq-table-vertical") << std::endl;
	*out << cgicc::caption("Configured applications") << std::endl;

	*out << cgicc::thead() << std::endl;
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Class";
	*out << cgicc::th();
	*out << cgicc::th();
	*out << "Lid";
	*out << cgicc::th();
	*out << cgicc::tr();
	*out << cgicc::thead() << std::endl;

	*out << cgicc::tbody() << std::endl;
	for (auto app : configured_)
	{
		*out << cgicc::tr();
		*out << cgicc::td();
		*out << app->getClassName();
		*out << cgicc::td();
		*out << cgicc::td();
		*out << app->getLocalId();
		*out << cgicc::td();
		*out << cgicc::tr() << std::endl;
	}
	*out << cgicc::tbody() << std::endl;
	
	*out << cgicc::table() << std::endl;

	//Liveness
	*out << cgicc::table().set("class", "xdaq-table-vertical") << std::endl;
	*out << cgicc::caption("Liveness") << std::endl;

	*out << cgicc::thead() << std::endl;
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Lid";
	*out << cgicc::th();
	*out << cgicc::th();
	*out << "Alive";
	*out << cgicc::th();
	*out << cgicc::tr();
	*out << cgicc::thead() << std::endl;

	*out << cgicc::tbody() << std::endl;
	for (auto const& [key, val] : liveness_)
	{
		*out << cgicc::tr();
		*out << cgicc::td();
		*out << key;
		*out << cgicc::td();
		*out << cgicc::td();
		*out << (val?"True":"False");
		*out << cgicc::td();
		*out << cgicc::tr() << std::endl;
	}
	*out << cgicc::tbody() << std::endl;

	*out << cgicc::table() << std::endl;

	//Readiness
	*out << cgicc::table().set("class", "xdaq-table-vertical") << std::endl;
	*out << cgicc::caption("Readiness") << std::endl;

	*out << cgicc::thead() << std::endl;
	*out << cgicc::tr();
	*out << cgicc::th();
	*out << "Lid";
	*out << cgicc::th();
	*out << cgicc::th();
	*out << "Ready";
	*out << cgicc::th();
	*out << cgicc::tr();
	*out << cgicc::thead() << std::endl;

	*out << cgicc::tbody() << std::endl;
	for (auto const& [key, val] : readiness_)
	{
		*out << cgicc::tr();
		*out << cgicc::td();
		*out << key;
		*out << cgicc::td();
		*out << cgicc::td();
		*out << (val?"True":"False");
		*out << cgicc::td();
		*out << cgicc::tr() << std::endl;
	}
	*out << cgicc::tbody() << std::endl;

	*out << cgicc::table() << std::endl;
}

void health::Application::startup(xgi::Input * in, xgi::Output * out)
{
	if (alive_)
	{
		out->getHTTPResponseHeader().getStatusCode(200);
		out->getHTTPResponseHeader().getReasonPhrase("OK");
	}
	else
	{
		out->getHTTPResponseHeader().getStatusCode(503);
		out->getHTTPResponseHeader().getReasonPhrase("Service Unavailable");
	}
}

void health::Application::liveness(xgi::Input * in, xgi::Output * out)
{
	if (alive_)
	{
		out->getHTTPResponseHeader().getStatusCode(200);
		out->getHTTPResponseHeader().getReasonPhrase("OK");
	}
	else
	{
		out->getHTTPResponseHeader().getStatusCode(503);
		out->getHTTPResponseHeader().getReasonPhrase("Service Unavailable");
	}
}

void health::Application::readiness(xgi::Input * in, xgi::Output * out)
{
	if (ready_)
	{
		out->getHTTPResponseHeader().getStatusCode(200);
		out->getHTTPResponseHeader().getReasonPhrase("OK");
	}
	else
	{
		out->getHTTPResponseHeader().getStatusCode(503);
		out->getHTTPResponseHeader().getReasonPhrase("Service Unavailable");
	}
}

void health::Application::refreshLiveness()
{
	bool alive = false;
	if (configured_.size() == liveness_.size())
	{
		alive = true;
		for (auto app : configured_)
		{
			auto it = liveness_.find(app->getLocalId());
			if (it != liveness_.end())
			{
				alive = it->second;
				if (!alive)
				{
					break;
				}
			}
			else
			{
				alive = false;
				break;
			}
		}
	}
	alive_ = alive;
}

void health::Application::refreshReadiness()
{
	bool ready = false;
	if (configured_.size() == readiness_.size())
	{
		ready = true;
		for (auto app : configured_)
		{
			auto it = readiness_.find(app->getLocalId());
			if (it != readiness_.end())
			{
				ready = it->second;
				if (!ready)
				{
					break;
				}
			}
			else
			{
				ready = false;
				break;
			}
		}
	}
	ready_ = ready;
}

void health::Application::inject(xgi::Input * in, xgi::Output * out)
{
	cgicc::Cgicc cgi(in);

	auto fi = cgi.getElement("liveness");
	if (fi != cgi.getElements().end())
	{
		auto value = (*fi).getValue();
		auto liveness = dynamic_cast<xdata::Boolean*>(getApplicationInfoSpace()->find("liveness"));
		*liveness = (value == "true" ? true : false);
		getApplicationInfoSpace()->fireItemValueChanged("liveness", this);
	}

	fi = cgi.getElement("readiness");
	if (fi != cgi.getElements().end())
	{
		auto value = (*fi).getValue();
		auto readiness = dynamic_cast<xdata::Boolean*>(getApplicationInfoSpace()->find("readiness"));
		*readiness = (value == "true" ? true : false);
		getApplicationInfoSpace()->fireItemValueChanged("readiness", this);
	}
}
