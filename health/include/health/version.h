/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini and D. Simelevicius                                *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _health_version_h_
#define _health_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define CORE_HEALTH_VERSION_MAJOR 1
#define CORE_HEALTH_VERSION_MINOR 0
#define CORE_HEALTH_VERSION_PATCH 0
// If any previous versions available E.g. #define CORE_HEALTH_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef CORE_HEALTH_PREVIOUS_VERSIONS

#define CORE_HEALTH_VERSION_CODE PACKAGE_VERSION_CODE(CORE_HEALTH_VERSION_MAJOR,CORE_HEALTH_VERSION_MINOR,CORE_HEALTH_VERSION_PATCH)
#ifndef CORE_HEALTH_PREVIOUS_VERSIONS
#define CORE_HEALTH_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(CORE_HEALTH_VERSION_MAJOR,CORE_HEALTH_VERSION_MINOR,CORE_HEALTH_VERSION_PATCH)
#else
#define CORE_HEALTH_FULL_VERSION_LIST  CORE_HEALTH_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(CORE_HEALTH_VERSION_MAJOR,CORE_HEALTH_VERSION_MINOR,CORE_HEALTH_VERSION_PATCH)
#endif

namespace health
{
	const std::string project = "core";
	const std::string package = "health";
	const std::string versions = CORE_HEALTH_FULL_VERSION_LIST;
	const std::string summary = "Probe for readiness, liveness and startup of XDAQ";
	const std::string description = "Probe for readiness, liveness and startup of XDAQ";
	const std::string authors = "Luciano Orsini, Dainius Simelevicius";
	const std::string link = "http://xdaq.web.cern.ch/";
	config::PackageInfo getPackageInfo ();
	void checkPackageDependencies () ;
	std::set<std::string, std::less<std::string> > getPackageDependencies ();
}

#endif

