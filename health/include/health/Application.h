/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini and D. Simelevicius                                *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _health_Application_h_
#define _health_Application_h_

#include <list>
#include <mutex>

#include "xdaq/Application.h"
#include "xgi/framework/UIManager.h"

#include "toolbox/ActionListener.h"
#include "xdata/ActionListener.h"
#include "xdata/Properties.h"
#include "toolbox/task/TimerListener.h"

#include "xgi/Method.h"
#include "xgi/Output.h"
#include "xgi/exception/Exception.h"

namespace health
{
	class Application: public xdaq::Application, public xgi::framework::UIManager, public toolbox::ActionListener, public xdata::ActionListener
	{
		public:
			XDAQ_INSTANTIATOR();

			Application(xdaq::ApplicationStub * s);
			~Application();

			void actionPerformed(toolbox::Event & event);
			void actionPerformed(xdata::Event & e);

			void Default(xgi::Input * in, xgi::Output * out);
			void startup(xgi::Input * in, xgi::Output * out);
			void liveness(xgi::Input * in, xgi::Output * out);
			void readiness(xgi::Input * in, xgi::Output * out);
			void inject(xgi::Input * in, xgi::Output * out);
		private:
			void refreshLiveness();
			void refreshReadiness();
			//Mutex for configured_, liveness_ and readiness_ containers
			std::mutex mutex_;
			std::set<const xdaq::ApplicationDescriptor*> configured_;
			std::map<unsigned int, bool> liveness_;
			std::map<unsigned int, bool> readiness_;
			bool alive_;
			bool ready_;
			unsigned int eventCounter_;
	};
}

#endif
