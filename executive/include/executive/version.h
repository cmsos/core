// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, L. Orsini and D. Simelevicius                   *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _executive_version_h_
#define _executive_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define CORE_EXECUTIVE_VERSION_MAJOR 4
#define CORE_EXECUTIVE_VERSION_MINOR 4
#define CORE_EXECUTIVE_VERSION_PATCH 0
// If any previous versions available E.g. #define CORE_EXECUTIVE_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define CORE_EXECUTIVE_PREVIOUS_VERSIONS "4.0.0,4.0.1,4.0.2,4.0.3,4.0.4,4.0.5,4.0.6,4.1.0,4.1.1,4.2.0,4.3.0"


//
// Template macros
//
#define CORE_EXECUTIVE_VERSION_CODE PACKAGE_VERSION_CODE(CORE_EXECUTIVE_VERSION_MAJOR,CORE_EXECUTIVE_VERSION_MINOR,CORE_EXECUTIVE_VERSION_PATCH)
#ifndef CORE_EXECUTIVE_PREVIOUS_VERSIONS
#define CORE_EXECUTIVE_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(CORE_EXECUTIVE_VERSION_MAJOR,CORE_EXECUTIVE_VERSION_MINOR,CORE_EXECUTIVE_VERSION_PATCH)
#else 
#define CORE_EXECUTIVE_FULL_VERSION_LIST  CORE_EXECUTIVE_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(CORE_EXECUTIVE_VERSION_MAJOR,CORE_EXECUTIVE_VERSION_MINOR,CORE_EXECUTIVE_VERSION_PATCH)
#endif 

namespace executive 
{
	const std::string project = "core";
	const std::string package  =  "executive";
   	const std::string versions = CORE_EXECUTIVE_FULL_VERSION_LIST;
	const std::string summary = "XDAQ process controller";
	const std::string description = "";
	const std::string authors = "Johannes Gutleber, Luciano Orsini";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/Executive_configuration";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
