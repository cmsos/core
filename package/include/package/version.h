// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _config_version_h_
#define _config_version_h_

#include "PackageInfo.h"

// !!! Edit this line to reflect the latest package version !!!

#define CORE_CONFIG_VERSION_MAJOR 3
#define CORE_CONFIG_VERSION_MINOR 9
#define CORE_CONFIG_VERSION_PATCH 0
// If any previous versions available E.g. #define CORE_CONFIG_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef CORE_CONFIG_PREVIOUS_VERSIONS 


//
// Template macros
//
#define CORE_CONFIG_VERSION_CODE PACKAGE_VERSION_CODE(CORE_CONFIG_VERSION_MAJOR,CORE_CONFIG_VERSION_MINOR,CORE_CONFIG_VERSION_PATCH)
#ifndef CORE_CONFIG_PREVIOUS_VERSIONS
#define CORE_CONFIG_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(CORE_CONFIG_VERSION_MAJOR,CORE_CONFIG_VERSION_MINOR,CORE_CONFIG_VERSION_PATCH)
#else 
#define CORE_CONFIG_FULL_VERSION_LIST  CORE_CONFIG_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(CORE_CONFIG_VERSION_MAJOR,CORE_CONFIG_VERSION_MINOR,CORE_CONFIG_VERSION_PATCH)
#endif 

namespace config 
{
	const std::string project = "core";
	const std::string package  =  "config";
	// !!! Edit this line to reflect the latest package version and previous compatible versions !!!
   	const std::string versions = CORE_CONFIG_FULL_VERSION_LIST;
	const std::string description = "";
	const std::string authors = "Johannes Gutleber, Luciano Orsini";
	const std::string summary = "Package versioning and checking module";
	const std::string link = "http://xdaqwiki.cern.ch/index.php";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
