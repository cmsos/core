// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2012, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, C.Wakefield				 	 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _tcpla_Event_h_
#define _tcpla_Event_h_

#include <iostream>
#include <string>
#include <memory>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "xcept/Exception.h"
#include "toolbox/exception/Handler.h"
#include "tcpla/EndPoint.h"

namespace tcpla
{
	class EventDispatcher;
	class PublicServicePoint;
	class InterfaceAdapter;
	//class EndPoint;

	const size_t TCPLA_MAX_IOV_SIZE = 2;

	typedef enum tcpla_flags
	{
		TCPLA_CONNECTION_CLOSE 		= 0x0001,
		TCPLA_CONNECTION_KEEPALIVE 	= 0x0002

	} TCPLA_FLAGS;

	typedef enum dat_event_number
	{
		TCPLA_UKNOWN_EVENT 								= 0x00000,
		TCPLA_DTO_RCV_COMPLETION_EVENT 					= 0x00001,
		TCPLA_DTO_SND_COMPLETION_EVENT 					= 0x00002,
		TCPLA_CONNECTION_REQUEST_EVENT 					= 0x00003,
		TCPLA_CONNECTION_EVENT_ESTABLISHED 				= 0x00004,
		TCPLA_CONNECTION_EVENT_PEER_REJECTED 			= 0x00005,
		TCPLA_CONNECTION_EVENT_NON_PEER_REJECTED 		= 0x00006,
		TCPLA_CONNECTION_EVENT_ACCEPT_COMPLETION_ERROR 	= 0x00007,
		TCPLA_CONNECTION_EVENT_DISCONNECTED 			= 0x00008,
		TCPLA_CONNECTION_EVENT_BROKEN 					= 0x00009,
		TCPLA_CONNECTION_EVENT_TIMED_OUT 				= 0x0000A,
		TCPLA_CONNECTION_EVENT_UNREACHABLE 				= 0x0000B,
		TCPLA_ASYNC_ERROR_EVD_OVERFLOW 					= 0x0000C,
		TCPLA_ASYNC_ERROR_IA_CATASTROPHIC 				= 0x0000D,
		TCPLA_ASYNC_ERROR_EP_BROKEN 					= 0x0000E,
		TCPLA_ASYNC_ERROR_TIMED_OUT 					= 0x0000F,
		TCPLA_ASYNC_ERROR_PROVIDER_INTERNAL_ERROR 		= 0x00010,
		TCPLA_SOFTWARE_EVENT 							= 0x00011,
		TCPLA_DTO_SND_UNDELIVERABLE_BLOCK 				= 0x00012,
		TCPLA_DTO_RCV_UNUSED_BLOCK 						= 0x00013,
		TCPLA_CONNECTION_CLOSED_BY_PEER 				= 0x00014,
		TCPLA_POST_BLOCK_FAILED 						= 0x00015,
		TCPLA_CONNECTION_RESET_BY_PEER 					= 0x00016,
		TCPLA_CONNECTION_EVENT_ACCEPTED 				= 0x00017,
		TCPLA_MEMORY_UNDERFLOW_EVENT 					= 0x00018,
		TCPLA_BROKEN_PIPE 						= 0x00019,
		MAX_TCPLA_EVENT_NUMBER = TCPLA_BROKEN_PIPE + 1
	} TCPLA_EVENT_NUMBER;

	typedef std::shared_ptr<void> TCPLA_USER_DATA;

	//Event strings
	const std::string TCPLA_UKNOWN_EVENT_STR 								= "TCPLA_UKNOWN_EVENT";
	const std::string TCPLA_DTO_RCV_COMPLETION_EVENT_STR 					= "TCPLA_DTO_RCV_COMPLETION_EVENT";
	const std::string TCPLA_DTO_SND_COMPLETION_EVENT_STR 					= "TCPLA_DTO_SND_COMPLETION_EVENT";
	const std::string TCPLA_CONNECTION_REQUEST_EVENT_STR 					= "TCPLA_CONNECTION_REQUEST_EVENT";
	const std::string TCPLA_CONNECTION_EVENT_ESTABLISHED_STR 				= "TCPLA_CONNECTION_EVENT_ESTABLISHED";
	const std::string TCPLA_CONNECTION_EVENT_PEER_REJECTED_STR 				= "TCPLA_CONNECTION_EVENT_PEER_REJECTED";
	const std::string TCPLA_CONNECTION_EVENT_NON_PEER_REJECTED_STR 			= "TCPLA_CONNECTION_EVENT_NON_PEER_REJECTED";
	const std::string TCPLA_CONNECTION_EVENT_ACCEPT_COMPLETION_ERROR_STR 	= "TCPLA_CONNECTION_EVENT_ACCEPT_COMPLETION_ERROR";
	const std::string TCPLA_CONNECTION_EVENT_DISCONNECTED_STR 				= "TCPLA_CONNECTION_EVENT_DISCONNECTED";
	const std::string TCPLA_CONNECTION_EVENT_BROKEN_STR 					= "TCPLA_CONNECTION_EVENT_BROKEN";
	const std::string TCPLA_CONNECTION_EVENT_TIMED_OUT_STR 					= "TCPLA_CONNECTION_EVENT_TIMED_OUT";
	const std::string TCPLA_CONNECTION_EVENT_UNREACHABLE_STR 				= "TCPLA_CONNECTION_EVENT_UNREACHABLE";
	const std::string TCPLA_ASYNC_ERROR_EVD_OVERFLOW_STR 					= "TCPLA_ASYNC_ERROR_EVD_OVERFLOW";
	const std::string TCPLA_ASYNC_ERROR_IA_CATASTROPHIC_STR 				= "TCPLA_ASYNC_ERROR_IA_CATASTROPHIC";
	const std::string TCPLA_ASYNC_ERROR_EP_BROKEN_STR 						= "TCPLA_ASYNC_ERROR_EP_BROKEN";
	const std::string TCPLA_ASYNC_ERROR_TIMED_OUT_STR 						= "TCPLA_ASYNC_ERROR_TIMED_OUT";
	const std::string TCPLA_ASYNC_ERROR_PROVIDER_INTERNAL_ERROR_STR 		= "TCPLA_ASYNC_ERROR_PROVIDER_INTERNAL_ERROR";
	const std::string TCPLA_SOFTWARE_EVENT_STR 								= "TCPLA_SOFTWARE_EVENT";
	const std::string TCPLA_DTO_SND_UNDELIVERABLE_BLOCK_STR 				= "TCPLA_DTO_SND_UNDELIVERABLE_BLOCK";
	const std::string TCPLA_DTO_RCV_UNUSED_BLOCK_STR 						= "TCPLA_DTO_RCV_UNUSED_BLOCK";
	const std::string TCPLA_CONNECTION_CLOSED_BY_PEER_STR 					= "TCPLA_CONNECTION_CLOSED_BY_PEER";
	const std::string TCPLA_POST_BLOCK_FAILED_STR 							= "TCPLA_POST_BLOCK_FAILED";
	const std::string TCPLA_CONNECTION_RESET_BY_PEER_STR 					= "TCPLA_CONNECTION_RESET_BY_PEER";
	const std::string TCPLA_CONNECTION_EVENT_ACCEPTED_STR 					= "TCPLA_CONNECTION_EVENT_ACCEPTED";
	const std::string TCPLA_MEMORY_UNDERFLOW_EVENT_STR 						= "TCPLA_MEMORY_UNDERFLOW_EVENT";
	const std::string TCPLA_BROKEN_PIPE_STR 						= "TCPLA_BROKEN_PIPE";

	const std::string UNKNOWN_EVENT_STR 									= "UNKNOWN_EVENT";

	typedef union tcpla_context
	{
			void * as_ptr;
			size_t as_64;
			size_t as_index;
	} TCPLA_CONTEXT;

	typedef struct tcpla_dto_completion_event_data
	{
			tcpla::PublicServicePoint* psp;
			tcpla::EndPoint ep_handle;
			TCPLA_CONTEXT user_cookie;

			//chain info
			uint16_t identification;
			uint8_t flags;
			size_t transfered_length;

			size_t block_length;
			char * buffer;
			void* user_context;
	} TCPLA_DTO_COMPLETION_EVENT_DATA;

	typedef struct tcpla_dto_block_event_data
	{
			TCPLA_CONTEXT user_cookie;
			size_t block_length;
			char * buffer;
			toolbox::exception::HandlerSignature* handler;
			void* user_context;
	} TCPLA_DTO_BLOCK_EVENT_DATA;

	typedef struct tcpla_cr_arrival_event_data
	{
			tcpla::PublicServicePoint* psp;
			int cr_handle;
	} TCPLA_CR_ARRIVAL_EVENT_DATA;

	typedef struct connection_event_broken_data
	{
			tcpla::InterfaceAdapter* ia;
			tcpla::EndPoint ep_handle;
			size_t reason;
			xcept::Exception * exception;
			TCPLA_CONTEXT user_cookie;
	} TCPLA_CONNECTION_EVENT_BROKEN_DATA;

	typedef struct tcpla_connection_event_data
	{
			tcpla::EndPoint ep_handle;
			TCPLA_CONTEXT user_cookie;
	} TCPLA_CONNECTION_EVENT_DATA;

	typedef struct tcpla_asynch_error_event_data
	{
			tcpla::EndPoint ep_handle;
			size_t reason;
			xcept::Exception * exception;
			toolbox::exception::HandlerSignature* handler;
			TCPLA_CONTEXT user_cookie;
			void* user_context;
	} TCPLA_ASYNCH_ERROR_EVENT_DATA;

	typedef struct tcpla_connect_error_event_data
	{
			tcpla::EndPoint ep_handle;
			size_t reason;
			xcept::Exception * exception;
			TCPLA_CONTEXT user_cookie;
	} TCPLA_CONNECT_ERROR_EVENT_DATA;

	typedef struct tcpla_memory_underflow_event_data
	{
			tcpla::PublicServicePoint* psp;
			tcpla::EndPoint ep_handle;
	} TCPLA_MEMORY_UNDERFLOW_EVENT_DATA;

	typedef struct tcpla_software_event_data
	{
			size_t reason;
			xcept::Exception * exception;
	} TCPLA_SOFTWARE_EVENT_DATA;
	/* Union for event Data */

	typedef union dat_event_data
	{
			TCPLA_CONNECTION_EVENT_BROKEN_DATA connection_broken_event_data;
			TCPLA_DTO_COMPLETION_EVENT_DATA dto_completion_event_data;
			TCPLA_CR_ARRIVAL_EVENT_DATA cr_event_data;
			TCPLA_CONNECTION_EVENT_DATA connect_event_data;
			TCPLA_ASYNCH_ERROR_EVENT_DATA asynch_error_event_data;
			TCPLA_DTO_BLOCK_EVENT_DATA tcpla_dto_block_event_data;
			TCPLA_MEMORY_UNDERFLOW_EVENT_DATA tcpla_memory_underflow_event_data;
			TCPLA_SOFTWARE_EVENT_DATA software_event_data;
			TCPLA_CONNECT_ERROR_EVENT_DATA connect_error_event_data;
	} TCPLA_EVENT_DATA;

	typedef struct RecvPostDesc
	{
			char * local_iov; //buffer
			size_t size;
			TCPLA_CONTEXT cookie;
			uint16_t identification;
			uint8_t flags;
			void* context;
			//TCPLA_USER_DATA user_data;

	} TCPLA_RECV_POST_DESC;

	typedef struct SendPostDesc
	{
			struct iovec local_iov[TCPLA_MAX_IOV_SIZE]; //buffer
			size_t iovs;
			TCPLA_CONTEXT cookie;
			uint16_t identification;
			toolbox::exception::HandlerSignature* handler;
			void* context;
			uint8_t flags;
			//TCPLA_USER_DATA user_data;

	} TCPLA_SEND_POST_DESC;

	/* Event struct that holds all event information */

	class Event
	{
		public:
			TCPLA_EVENT_NUMBER event_number;
			TCPLA_USER_DATA user_data;
			TCPLA_EVENT_DATA event_data;
	};
}

#endif
