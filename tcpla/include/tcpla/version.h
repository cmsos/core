/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, C.Wakefield, A.Forrest, D.Simelevicius *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

//
// Version definition for UDAPL peer transport
//
#ifndef _tcpla_version_h_
#define _tcpla_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define CORE_TCPLA_VERSION_MAJOR 1
#define CORE_TCPLA_VERSION_MINOR 11
#define CORE_TCPLA_VERSION_PATCH 0
// If any previous versions available E.g. #define CORE_TCPLA_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef CORE_TCPLA_PREVIOUS_VERSIONS
#define CORE_TCPLA_PREVIOUS_VERSIONS "1.9.0,1.9.1,1.9.2,1.9.3,1.9.4,1.9.5,1.9.6,1.9.7,1.9.8,1.9.9,1.9.10,1.10.0"


//
// Template macros
//
#define CORE_TCPLA_VERSION_CODE PACKAGE_VERSION_CODE(CORE_TCPLA_VERSION_MAJOR,CORE_TCPLA_VERSION_MINOR,CORE_TCPLA_VERSION_PATCH)
#ifndef CORE_TCPLA_PREVIOUS_VERSIONS
#define CORE_TCPLA_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(CORE_TCPLA_VERSION_MAJOR,CORE_TCPLA_VERSION_MINOR,CORE_TCPLA_VERSION_PATCH)
#else 
#define CORE_TCPLA_FULL_VERSION_LIST  CORE_TCPLA_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(CORE_TCPLA_VERSION_MAJOR,CORE_TCPLA_VERSION_MINOR,CORE_TCPLA_VERSION_PATCH)
#endif 
namespace tcpla
{
	const std::string project = "core";
    const std::string package  =  "tcpla";
    const std::string versions = CORE_TCPLA_FULL_VERSION_LIST;
    const std::string summary = "TCP Layered Architecture";
    const std::string description = "TCP Layered Architecture based on Direct Access Protocol Layer";
    const std::string authors = "Luciano Orsini, Andrea Petrucci, Chris Wakefield, Andy Forrest, Dainius Simelevicius";
    const std::string link = "http://xdaq.web.cern.ch";
    config::PackageInfo getPackageInfo();
    void checkPackageDependencies() ;
    std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif

