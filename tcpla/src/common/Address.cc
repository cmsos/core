// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2012, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, C.Wakefield					 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include <iostream>
#include <sstream>
#include <list>

#include "tcpla/Address.h"
#include "tcpla/Utils.h"
#include "toolbox/net/URL.h"
#include "toolbox/net/Utils.h"
#include "toolbox/exception/Exception.h"
#include "toolbox/Properties.h"
#include "toolbox/string.h"

tcpla::Address::Address (std::map<std::string, std::string, std::less<std::string> >& address)
{

	for (std::map<std::string, std::string, std::less<std::string> >::iterator i = address.begin(); i != address.end(); i++)
	{
		this->setProperty((*i).first, (*i).second);
		//std::cout << "Address: " << (*i).second << std::endl;
	}

	if (!this->hasProperty("rcvTimeout"))
	{
		this->setProperty("rcvTimeout", "10");
	}

	if (!this->hasProperty("sndTimeout"))
	{
		this->setProperty("sndTimeout", "10");
	}

	if (!this->hasProperty("connectOnRequest"))
	{
		this->setProperty("connectOnRequest", "false");
	}

	if (!this->hasProperty("targetId"))
	{
		this->setProperty("targetId", "0");
	}

	if (!this->hasProperty("assignedPort"))
	{
		this->setProperty("assignedPort", "0");
	}

	if (!this->hasProperty("affinity"))
	{
		this->setProperty("affinity", "");
	}

	if (!this->hasProperty("singleThread"))
	{
		this->setProperty("singleThread", "false");
	}

	if (!this->hasProperty("pollingCycle"))
	{
		this->setProperty("pollingCycle", "1");
	}

	if (!this->hasProperty("smode"))
	{
		this->setProperty("smode", "poll");
	}

	if (!this->hasProperty("rmode"))
	{
		this->setProperty("rmode", "poll");
	}

	if (this->hasProperty("subnet"))
	{
		this->setProperty("hostname", tcpla::getHostBySubnet(this->getProperty("subnet"))); // it re-throw the same exeption
	}
	else if (this->hasProperty("interface"))
	{
		this->setProperty("hostname", tcpla::getHostByInterface(this->getProperty("interface"))); // it re-throw the same exeption
	}

	if (!this->hasProperty("family"))
	{
		this->setProperty("family", "AF_INET");
	}

	if (!this->hasProperty("nonblock"))
	{
		this->setProperty("nonblock", "true");
	}

	if (!this->hasProperty("pspDispatcherThread"))
	{
		this->setProperty("pspDispatcherThread", "true");
	}

	if (!this->hasProperty("underflowTimeout"))
	{
		this->setProperty("underflowTimeout", "2000");
	}

}

std::map<std::string, std::string> tcpla::Address::getAffinity ()
{
	// The affinity is set as follows [THREAD:MODE,CORE] in fours triplets ( waiting mode and no affinity by default)
	// where
	//	THREAD is : RCV is the PublicServicePoint , SND is the InterfaceAdapter , DSR Dispatcher Receiver, DSS Dispatcher Sender
	//	MODE is: W or P ( waiting or polling)
	//    CORE 1..N (processor number)
	//  e.g. RCV:P,SND:W,DSR:P,DSS:W

	std::map < std::string, std::string > affinity;
	affinity["RCV"] = "W";
	affinity["SND"] = "W";
	affinity["DSR"] = "W";
	affinity["DSS"] = "W";

	if (this->getProperty("affinity") != "")
	{
		std::list < std::string > alist = toolbox::parseTokenList(this->getProperty("affinity"), ",");

		for (std::list<std::string>::iterator i = alist.begin(); i != alist.end(); i++)
		{
			std::list < std::string > mlist = toolbox::parseTokenList((*i), ":");

			std::string thread = mlist.front();
			mlist.pop_front();

			std::string mode = mlist.front();
			mlist.pop_front();

			affinity[thread] = mode;
		}
	}

	return affinity;
}

/*struct sockaddr_in tcpla::Address::getSocketAddress ()
 {
 struct sockaddr_in writeAddr;
 try
 {
 writeAddr = toolbox::net::getSocketAddressByName(hostname_, url_.getPort());

 }
 catch ( toolbox::exception::Exception& e )
 {
 std::cout << e.what() << std::endl;
 }

 return writeAddr;
 }

 std::string tcpla::Address::getIPAddress ()
 {
 return inet_ntoa(getSocketAddress().sin_addr);
 }*/

tcpla::Address::~Address ()
{
}

std::string tcpla::Address::getService ()
{
	return this->getProperty("service");
}

std::string tcpla::Address::getProtocol ()
{
	return this->getProperty("protocol");
}

std::string tcpla::Address::toString ()
{
	std::stringstream val;
	if (this->getProperty("assignedPort") == "0")
	{
		// unassigned local port means this is probably a remote address, and therefore does not have an 'assigned' port locally. the displayed port is the destination address port
		val << this->getProperty("protocol") << "://" << this->getProperty("hostname") << ":" << this->getProperty("port") << " (unassigned local port)";
	}
	else
	{
		val << this->getProperty("protocol") << "://" << this->getProperty("hostname") << ":" << this->getProperty("assignedPort");
	}
	return val.str();
}

std::string tcpla::Address::toURL ()
{
	std::stringstream val;
	val << this->getProperty("service") << "+" << this->getProperty("protocol") << "://" << this->getProperty("hostname");

	return val.str();
}

std::string tcpla::Address::getHost ()
{
	return this->getProperty("hostname");
}

std::string tcpla::Address::getConfigurePort ()
{
	return this->getProperty("port");
}

std::string tcpla::Address::getServiceParameters ()
{
	return "none";
}

bool tcpla::Address::equals (pt::Address::Reference address)
{
	std::vector < std::string > names = this->propertyNames();
	tcpla::Address & a = dynamic_cast<tcpla::Address &>(*address);

	for (std::vector<std::string>::iterator i = names.begin(); i != names.end(); i++)
	{
		if (a.getProperty(*i) != this->getProperty(*i))
		{
			return false;
		}

	}

	return true;
}
