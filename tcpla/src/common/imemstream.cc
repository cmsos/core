// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 		 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, C. Wakefield			 				 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         	 *
 *************************************************************************/

#include "tcpla/imemstream.h"

tcpla::imemstream::imemstream (char * base, size_t size)
	: base_(base), size_(size), offset_(0)
{

}

tcpla::imemstream::imemstream ()
	: base_(0), size_(0), offset_(0)
{

}

void tcpla::imemstream::init (char * base, size_t size)
{
	base_ = base;
	size_ = size;
	offset_ = 0;
}

size_t tcpla::imemstream::readsome (char * buf, size_t len)
{
	if ((len + offset_) <= size_)
	{
		::memcpy(buf, base_ + offset_, len);

		offset_ = offset_ + len;

		return len;
	}
	else if ((size_ - offset_) > 0)
	{
		size_t actualReadSize = size_ - offset_;
		::memcpy(buf, base_ + offset_, actualReadSize);
		offset_ = size_;

		return actualReadSize;
	}

	return 0;
}

size_t tcpla::imemstream::seekg (size_t len)
{
	if ((len + offset_) <= size_)
	{
		offset_ = offset_ + len;
		//std::cout << "A offset_: " << offset_ << " size" << size_ << std::endl;
		return len;
	}
	else if ((size_ - offset_) > 0)
	{
		size_t actualReadSize = size_ - offset_;
		offset_ = size_;
		// std::cout << "B offset_: " << actualReadSize << " size" << size_ << std::endl;
		return actualReadSize;
	}

	return 0;
}

size_t tcpla::imemstream::tellg ()
{
	return offset_;
}

bool tcpla::imemstream::eof ()
{
	//std::cout << "eof " << (offset_ == size_) << std::endl;
	return (offset_ == size_);
}

bool tcpla::imemstream::empty ()
{
	return (offset_ == 0 && base_ == 0 && size_ == 0);
}
