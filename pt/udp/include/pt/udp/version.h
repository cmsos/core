// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _pt_udp_version_h_
#define _pt_udp_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define CORE_PTUDP_VERSION_MAJOR 1
#define CORE_PTUDP_VERSION_MINOR 0
#define CORE_PTUDP_VERSION_PATCH 0
// If any previous versions available E.g. #define CORE_PTUDP_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef CORE_PTUDP_PREVIOUS_VERSIONS 


//
// Template macros
//
#define CORE_PTUDP_VERSION_CODE PACKAGE_VERSION_CODE(CORE_PTUDP_VERSION_MAJOR,CORE_PTUDP_VERSION_MINOR,CORE_PTUDP_VERSION_PATCH)
#ifndef CORE_PTUDP_PREVIOUS_VERSIONS
#define CORE_PTUDP_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(CORE_PTUDP_VERSION_MAJOR,CORE_PTUDP_VERSION_MINOR,CORE_PTUDP_VERSION_PATCH)
#else 
#define CORE_PTUDP_FULL_VERSION_LIST  CORE_PTUDP_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(CORE_PTUDP_VERSION_MAJOR,CORE_PTUDP_VERSION_MINOR,CORE_PTUDP_VERSION_PATCH)
#endif 


namespace ptudp
{
	const std::string project = "core";
	const std::string package  =  "ptudp";
   	const std::string versions = CORE_PTUDP_FULL_VERSION_LIST;
	const std::string summary = "UDP peer transport with I2O service and SOAP implementation";
	const std::string description = "Does not support multicasting";
	const std::string authors = "Johannes Gutleber, Luciano Orsini";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/xdaq";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
