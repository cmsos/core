// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2015, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D.Simelevicius                                     *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/
#include "pt/blit/PipeConnectionClosedByPeer.h"

pt::blit::PipeConnectionClosedByPeer::PipeConnectionClosedByPeer(tcpla::EndPoint ep_handle, int index, int sid ):
ep_handle_(ep_handle),index_(index),sid_(sid)
{

}

int pt::blit::PipeConnectionClosedByPeer::getPipeIndex()
{
	return index_;
}

int pt::blit::PipeConnectionClosedByPeer::getSID()
{
	return sid_;
}



tcpla::EndPoint pt::blit::PipeConnectionClosedByPeer::getEndPoint()
{
	return ep_handle_;
}

