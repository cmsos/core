// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini and D. Simelevicius                                *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

//
// Version definition for bulk peer transport
//
#ifndef _ptblit_version_h_
#define _ptblit_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define CORE_PTBLIT_VERSION_MAJOR 1
#define CORE_PTBLIT_VERSION_MINOR 2
#define CORE_PTBLIT_VERSION_PATCH 4
// If any previous versions available
// #define CORE_PTBLIT_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef CORE_PTBLIT_PREVIOUS_VERSIONS


//
// Template macros
//
#define CORE_PTBLIT_VERSION_CODE PACKAGE_VERSION_CODE(CORE_PTBLIT_VERSION_MAJOR,CORE_PTBLIT_VERSION_MINOR,CORE_PTBLIT_VERSION_PATCH)
#ifndef CORE_PTBLIT_PREVIOUS_VERSIONS
#define CORE_PTBLIT_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(CORE_PTBLIT_VERSION_MAJOR,CORE_PTBLIT_VERSION_MINOR,CORE_PTBLIT_VERSION_PATCH)
#else 
#define CORE_PTBLIT_FULL_VERSION_LIST  CORE_PTBLIT_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(CORE_PTBLIT_VERSION_MAJOR,CORE_PTBLIT_VERSION_MINOR,CORE_PTBLIT_VERSION_PATCH)
#endif 
namespace ptblit
{
	const std::string project = "core";
    const std::string package = "ptblit";
    const std::string versions = CORE_PTBLIT_FULL_VERSION_LIST;
    const std::string summary = "Bulk Peer Transport";
    const std::string description = "";
    const std::string authors = "Luciano Orsini, Dainius Simelevicius";
    const std::string link = "http://xdaq.web.cern.ch";
    config::PackageInfo getPackageInfo();
    void checkPackageDependencies() ;
    std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif

