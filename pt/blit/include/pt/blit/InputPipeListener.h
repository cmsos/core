// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2015, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_blit_InputPipeListener_h_
#define _pt_blit_InputPipeListener_h_


#include "pt/blit/PipeConnectionAccepted.h"
#include "pt/blit/PipeConnectionClosedByPeer.h"
#include "pt/blit/PipeConnectionResetByPeer.h"




namespace pt
{
	namespace blit
	{

		class InputPipeListener
		{
			public:

				virtual void connectionAcceptedEvent(pt::blit::PipeConnectionAccepted & pc) = 0;
				virtual void connectionClosedByPeerEvent(pt::blit::PipeConnectionClosedByPeer & pc) = 0;
				virtual void connectionResetByPeerEvent(pt::blit::PipeConnectionResetByPeer & pc) = 0;

		};

	}
}

#endif
