// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_pipe_BrokenPipe_h_
#define _pt_pipe_BrokenPipe_h_

#include "pt/pipe/Output.h"
#include "toolbox/mem/Reference.h"

namespace pt 
{
	namespace pipe 
	{

		class BrokenPipe
		{
			public:

			BrokenPipe(pt::pipe::Output * opipe): opipe_(opipe) {}

			pt::pipe::Output * getPipe()
			{
				return opipe_;
			}

			protected:
	
			pt::pipe::Output * opipe_;
			

		};

	}
}

#endif
