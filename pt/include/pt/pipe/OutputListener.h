// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2015, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_pipe_OutputListener_h_
#define _pt_pipe_OutputListener_h_


#include "pt/pipe/UndeliverableBlock.h"
#include "pt/pipe/BrokenPipe.h"




namespace pt
{
	namespace pipe 
	{

		class OutputListener
		{
			public:

				virtual void actionPerformed(pt::pipe::UndeliverableBlock & event) = 0;
				virtual void actionPerformed(pt::pipe::BrokenPipe & event) = 0;

		};

	}
}

#endif
