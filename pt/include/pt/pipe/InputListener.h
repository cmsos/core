// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_pipe_InputListener_h_
#define _pt_pipe_InputListener_h_


#include "pt/pipe/ConnectionClosedByPeer.h"
#include "pt/pipe/UnusedBlock.h"
namespace pt
{
	namespace pipe 
	{

		class InputListener
		{
			public:

				virtual void actionPerformed(pt::pipe::ConnectionClosedByPeer & event) = 0;
				virtual void actionPerformed(pt::pipe::UnusedBlock & event) = 0;

		};

	}
}

#endif
