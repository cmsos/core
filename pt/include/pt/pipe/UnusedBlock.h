// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_pipe_UnusedBlock_h_
#define _pt_pipe_UnusedBlock_h_

#include "pt/pipe/Input.h"
#include "toolbox/mem/Reference.h"

namespace pt 
{
	namespace pipe 
	{

		class UnusedBlock
		{
			public:

			UnusedBlock(pt::pipe::Input * ipipe, toolbox::mem::Reference * ref): ipipe_(ipipe), ref_(ref) {}

			pt::pipe::Input * getPipe()
			{
				return ipipe_;
			}

			toolbox::mem::Reference * getReference()
                        {
                                return ref_;
                        }

			protected:
	
			pt::pipe::Input * ipipe_;
			toolbox::mem::Reference * ref_;
			

		};

	}
}

#endif
