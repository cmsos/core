// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_pipe_ConnectionClosedByPeer_h_
#define _pt_pipe_ConnectionClosedByPeer_h_

#include "pt/pipe/Input.h"
#include "toolbox/mem/Reference.h"

namespace pt 
{
	namespace pipe 
	{

		class ConnectionClosedByPeer
		{
			public:

			ConnectionClosedByPeer(pt::pipe::Input * ipipe): ipipe_(ipipe) {}

			pt::pipe::Input * getPipe()
			{
				return ipipe_;
			}

			protected:
	
			pt::pipe::Input * ipipe_;
			

		};

	}
}

#endif
