// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_pipe_ConnectionError_h_
#define _pt_pipe_ConnectionError_h_

#include <memory> 
#include "xcept/Exception.h"
#include "pt/Address.h"

namespace pt 
{
	namespace pipe 
	{

		class ConnectionError
		{
			public:

			ConnectionError(std::shared_ptr<void> & context, xcept::Exception & e): context_(context), e_(e) {}

	                virtual pt::Address::Reference getLocalAddress()
       	         	{
                        	return source_;
                	}

                	pt::Address::Reference getDestinationAddress()
                	{
                       	 	return destination_;
                	}

 			void * getContext()
                	{
                        	return context_.get();
                	}

			xcept::Exception getException()
			{
				return e_;
			}

			
			protected:

                	pt::Address::Reference source_;
                	pt::Address::Reference destination_;
			std::shared_ptr<void> context_;
			xcept::Exception e_;

		};

	}
}

#endif
