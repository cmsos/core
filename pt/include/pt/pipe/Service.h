// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_pipe_Service_h_
#define _pt_pipe_Service_h_

#include <memory>
#include "pt/Address.h"
#include "pt/pipe/ServiceListener.h"
#include "pt/pipe/OutputListener.h"
#include "pt/pipe/InputListener.h"
#include "pt/pipe/Output.h"
#include "pt/pipe/Input.h"

namespace pt 
{
	namespace pipe 
	{

		class Service
		{
			public:

			virtual void connect(pt::Address::Reference source,  pt::Address::Reference destination, pt::pipe::ServiceListener * l, std::shared_ptr<void> & user_cookie ) = 0;
            virtual pt::pipe::Output * createOutputPipe( std::shared_ptr<pt::pipe::EstablishedConnection> & ec, pt::pipe::OutputListener * listener ) = 0 ;
			virtual pt::pipe::Input * createInputPipe( std::shared_ptr<pt::pipe::ConnectionRequest> & ec, pt::pipe::InputListener * listener ) = 0;
            virtual pt::pipe::Output * createOutputPipe( std::shared_ptr<pt::pipe::EstablishedConnection> & ec, pt::pipe::OutputListener * listener, size_t n) = 0 ;
            virtual pt::pipe::Input * createInputPipe( std::shared_ptr<pt::pipe::ConnectionRequest> & ec, pt::pipe::InputListener * listener, size_t n) = 0;
            
			virtual void destroyInputPipe(pt::pipe::Input * ip)  = 0;
            virtual void destroyOutputPipe(pt::pipe::Output * ip)  = 0;

		};

	}
}

#endif
