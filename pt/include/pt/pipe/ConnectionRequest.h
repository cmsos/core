// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_pipe_ConnectionRequest_h_
#define _pt_pipe_ConnectionRequest_h_

#include <string>
#include <memory>

namespace pt 
{
	class PeerTransport;

	namespace pipe 
	{

		class ConnectionRequest
		{
			public:

		        virtual std::shared_ptr<void> getConnectionHandle() = 0;
                 	virtual std::string getNetwork() = 0;
                	virtual pt::PeerTransport * getPeerTransport() = 0;
				 

		};

	}
}

#endif
