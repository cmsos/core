// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_pipe_ServiceListener_h_
#define _pt_pipe_ServiceListener_h_

#include <memory>

#include "pt/Listener.h"
#include "pt/pipe/EstablishedConnection.h"
#include "pt/pipe/ConnectionRequest.h"
#include "pt/pipe/ConnectionError.h"

namespace pt 
{
	namespace pipe
	{
		class ServiceListener: public pt::Listener
		{
			public:
	
			virtual void actionPerformed(std::shared_ptr<pt::pipe::EstablishedConnection>  pec) = 0;
			// return TRUE if pcr has been processed by listener FALSE otherwise
			virtual bool actionPerformed(std::shared_ptr<pt::pipe::ConnectionRequest>  pcr) = 0;
			virtual void actionPerformed(std::shared_ptr<pt::pipe::ConnectionError>  pce) = 0;

			std::string getService()
			{
				return "pipe";
			}


				 

		};
	}
}

#endif
