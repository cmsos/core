// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_pipe_UndeliverableBlock_h_
#define _pt_pipe_UndeliverableBlock_h_

#include "pt/pipe/Output.h"
#include "toolbox/mem/Reference.h"

namespace pt 
{
	namespace pipe 
	{

		class UndeliverableBlock
		{
			public:

			UndeliverableBlock(pt::pipe::Output * opipe, toolbox::mem::Reference * ref): opipe_(opipe), ref_(ref) {}

			pt::pipe::Output * getPipe()
			{
				return opipe_;
			}

			toolbox::mem::Reference * getReference()
                        {
                                return ref_;
                        }

			protected:
	
			pt::pipe::Output * opipe_;
			toolbox::mem::Reference * ref_;
			

		};

	}
}

#endif
