// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_pipe_Input_h_
#define _pt_pipe_Input_h_

#include "toolbox/mem/Reference.h"

namespace pt 
{
	namespace pipe 
	{

		class Input
		{
			public:

			virtual ~Input() {}

			virtual void postFrame(toolbox::mem::Reference * ref ) = 0; 
			virtual bool empty() = 0;
			virtual toolbox::mem::Reference * completed() = 0;

		};

	}
}

#endif
