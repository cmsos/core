/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, C.Wakefield, A.Forrest, D.Simelevicius *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

//
// Version definition for UDAPL peer transport
//
#ifndef _ptutcp_version_h_
#define _ptutcp_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define CORE_PTUTCP_VERSION_MAJOR 4
#define CORE_PTUTCP_VERSION_MINOR 5
#define CORE_PTUTCP_VERSION_PATCH 1
// If any previous versions available E.g. #define CORE_PTUTCP_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define CORE_PTUTCP_PREVIOUS_VERSIONS "4.0.0,4.1.0,4.1.1,4.1.2,4.1.3,4.1.4,4.1.5,4.2.0,4.2.1,4.2.2,4.2.3,4.2.4,4.2.5,4.2.6,4.3.0,4.3.1,4.4.0,4.5.0"

//
// Template macros
//
#define CORE_PTUTCP_VERSION_CODE PACKAGE_VERSION_CODE(CORE_PTUTCP_VERSION_MAJOR,CORE_PTUTCP_VERSION_MINOR,CORE_PTUTCP_VERSION_PATCH)
#ifndef CORE_PTUTCP_PREVIOUS_VERSIONS
#define CORE_PTUTCP_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(CORE_PTUTCP_VERSION_MAJOR,CORE_PTUTCP_VERSION_MINOR,CORE_PTUTCP_VERSION_PATCH)
#else 
#define CORE_PTUTCP_FULL_VERSION_LIST  CORE_PTUTCP_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(CORE_PTUTCP_VERSION_MAJOR,CORE_PTUTCP_VERSION_MINOR,CORE_PTUTCP_VERSION_PATCH)
#endif 
namespace ptutcp
{
	const std::string project = "core";
    const std::string package  =  "ptutcp";
    const std::string versions = CORE_PTUTCP_FULL_VERSION_LIST;
    const std::string summary = "ptuTCP";
    const std::string description = "Universal TCP peer transport";
    const std::string authors = "Luciano Orsini, Andrea Petrucci, Chris Wakefield, Andy Forrest";
    const std::string link = "http://xdaq.web.cern.ch";
    config::PackageInfo getPackageInfo();
    void checkPackageDependencies() ;
    std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif

