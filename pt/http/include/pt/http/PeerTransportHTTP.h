// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _pt_http_PeerTransportHTTP_h_
#define _pt_http_PeerTransportHTTP_h_

#include "xdaq/Application.h"
#include "xgi/framework/UIManager.h"

#include "pt/http/Alias.h"
#include "pt/http/HeaderField.h"
#include "pt/http/PeerTransportSender.h"
#include "pt/http/PeerTransportReceiver.h"
#include "xdata/String.h"
#include "xdata/Boolean.h"

namespace pt
{
namespace http 
{

//! this is the XDAQ Peer Transport Appliction Wrapper
//
class PeerTransportHTTP: public xdaq::Application, public xgi::framework::UIManager, public xdata::ActionListener
{
	public:
	
	XDAQ_INSTANTIATOR();

	
	PeerTransportHTTP(xdaq::ApplicationStub * s) ;
	virtual ~PeerTransportHTTP();

	void actionPerformed(xdata::Event & e);

	private:
	
	http::PeerTransportSender* pts_;
	http::PeerTransportReceiver* ptr_;
	
	//! Temporary possibility to define an alias, e.g.:
	//! http://x.y/z -> http://x.y/myPath
	xdata::Vector< xdata::Bag<pt::http::Alias> > aliases_;
	xdata::Vector< xdata::Bag<pt::http::HeaderField> > headers_;
	xdata::Vector< xdata::Bag<pt::http::HeaderField> > expires_;
	xdata::String documentRoot_;
	xdata::String aliasName_;
	xdata::String aliasPath_;
	//
	// Amount of time (in secs) the server allocates to delivering an HTTP response.
	// This timeout guards against a client that is alive but consumes data at a slow rate,
	// by either acknowledging few bytes or at the extreme, zero.
	// Since the client is responding the connection can only be closed by the application
	xdata::UnsignedInteger responseTimeout_;

	xdata::Boolean allowCompression_;

};
}
}
#endif
