/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2023, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, L. Orsini and D. Simelevicius                   *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

//
// Version definition for HTTP peer transport
//
#ifndef _pt_http_h_
#define _pt_http_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define CORE_PTHTTP_VERSION_MAJOR 4
#define CORE_PTHTTP_VERSION_MINOR 7
#define CORE_PTHTTP_VERSION_PATCH 1
// If any previous versions available E.g. #define CORE_PTHTTP_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef CORE_PTHTTP_PREVIOUS_VERSIONS


//
// Template macros
//
#define CORE_PTHTTP_VERSION_CODE PACKAGE_VERSION_CODE(CORE_PTHTTP_VERSION_MAJOR,CORE_PTHTTP_VERSION_MINOR,CORE_PTHTTP_VERSION_PATCH)
#ifndef CORE_PTHTTP_PREVIOUS_VERSIONS
#define CORE_PTHTTP_FULL_VERSION_LIST PACKAGE_VERSION_STRING(CORE_PTHTTP_VERSION_MAJOR,CORE_PTHTTP_VERSION_MINOR,CORE_PTHTTP_VERSION_PATCH)
#else 
#define CORE_PTHTTP_FULL_VERSION_LIST CORE_PTHTTP_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(CORE_PTHTTP_VERSION_MAJOR,CORE_PTHTTP_VERSION_MINOR,CORE_PTHTTP_VERSION_PATCH)
#endif 
namespace pthttp
{
	const std::string project = "core";
	const std::string package = "pthttp";
	const std::string versions = CORE_PTHTTP_FULL_VERSION_LIST;
	const std::string summary = "HTTP peer transport with SOAP and CGI service implementations";
	const std::string description = "";
	const std::string authors = "Johannes Gutleber, Luciano Orsini, Dainius Simelevicius";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/Core_Tools";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif

