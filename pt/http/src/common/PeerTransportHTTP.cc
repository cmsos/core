/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, L. Orsini and D. Simelevicius                   *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "pt/http/PeerTransportHTTP.h"
#include "pt/PeerTransportAgent.h"

XDAQ_INSTANTIATOR_IMPL(pt::http::PeerTransportHTTP)

pt::http::PeerTransportHTTP::PeerTransportHTTP (xdaq::ApplicationStub * s) 
	: xdaq::Application(s), xgi::framework::UIManager(this)
{
	s->getDescriptor()->setAttribute("icon","/pt/http/images/pt-http-icon.png");
	s->getDescriptor()->setAttribute("icons","/pt/images");

	getApplicationInfoSpace()->fireItemAvailable("aliasName",&aliasName_); 
	getApplicationInfoSpace()->fireItemAvailable("aliasPath",&aliasPath_);
	getApplicationInfoSpace()->fireItemAvailable("aliases",&aliases_);
	getApplicationInfoSpace()->fireItemAvailable("httpHeaderFields",&headers_);
	getApplicationInfoSpace()->fireItemAvailable("expiresByType",&expires_);
	getApplicationInfoSpace()->fireItemAvailable("documentRoot", &documentRoot_);
	allowCompression_ = false;
	getApplicationInfoSpace()->fireItemAvailable("allowCompression", &allowCompression_);

	responseTimeout_ = 30;
	getApplicationInfoSpace()->fireItemAvailable("responseTimeout", &responseTimeout_);

	// Listen to events indicating the setting of the application's default values
	this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
}

pt::http::PeerTransportHTTP::~PeerTransportHTTP()
{
	pt::PeerTransportAgent* pta = pt::getPeerTransportAgent();
	pta->removePeerTransport(pts_);
	pta->removePeerTransport(ptr_);
	delete pts_;
	delete ptr_;
}

void pt::http::PeerTransportHTTP::actionPerformed(xdata::Event & e)
{
	if (e.type() == "urn:xdaq-event:setDefaultValues")
	{
		pts_ = new pt::http::PeerTransportSender(getApplicationLogger());
		ptr_ = new pt::http::PeerTransportReceiver(this,getApplicationLogger(), getApplicationInfoSpace());
		pt::PeerTransportAgent* pta = pt::getPeerTransportAgent();
		pta->addPeerTransport(pts_);
		pta->addPeerTransport(ptr_);

		//Reporting that application is ready
		auto readiness = dynamic_cast<xdata::Boolean*>(getApplicationInfoSpace()->find("readiness"));
		*readiness = true;
		getApplicationInfoSpace()->fireItemValueChanged("readiness", this);
	}
	else
	{
		std::stringstream msg;
		msg << "Failed to process unknown event type '" << e.type() << "'";
		LOG4CPLUS_FATAL(this->getApplicationLogger(), msg.str());
	}
}
