<?xml version='1.0'?>
<!-- Order of specification will determine the sequence of installation. all modules are loaded prior instantiation of plugins -->
<xp:Profile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xp="http://xdaq.web.cern.ch/xdaq/xsd/2005/XMLProfile-11">
	<!-- Compulsory  Plugins -->
	<xp:Application heartbeat="false" class="executive::Application" id="0" group="profile" service="executive" network="local">
		<properties xmlns="urn:xdaq-application:Executive" xsi:type="soapenc:Struct">
			<logUrl xsi:type="xsd:string">console</logUrl>
                	<logLevel xsi:type="xsd:string">INFO</logLevel>
                </properties>
	</xp:Application>
	<xp:Module>${XDAQ_ROOT}/lib/libb2innub.so</xp:Module>
	<xp:Module>${XDAQ_ROOT}/lib/libexecutive.so</xp:Module>


	<xp:Application heartbeat="false" class="pt::http::PeerTransportHTTP" id="1" group="profile" network="local">
		 <properties xmlns="urn:xdaq-application:pt::http::PeerTransportHTTP" xsi:type="soapenc:Struct">
		 	<documentRoot xsi:type="xsd:string">${XDAQ_DOCUMENT_ROOT}</documentRoot>
			 <aliases xsi:type="soapenc:Array" soapenc:arrayType="xsd:ur-type[2]">
                                <item xsi:type="soapenc:Struct" soapenc:position="[0]">
                                        <name xsi:type="xsd:string">/temporary</name>
                                        <path xsi:type="xsd:string">/tmp</path>
                                </item>
				<item xsi:type="soapenc:Struct" soapenc:position="[1]">
                                        <name xsi:type="xsd:string">/pippo</name>
                                        <path xsi:type="xsd:string">/tmp</path>
                                </item>
                        </aliases>
			<httpHeaderFields xsi:type="soapenc:Array" soapenc:arrayType="xsd:ur-type[2]">
                                <item xsi:type="soapenc:Struct" soapenc:position="[0]">
                                        <name xsi:type="xsd:string">Max-Forwards</name>
                                        <value xsi:type="xsd:string">10</value>
                                </item>
                                <item xsi:type="soapenc:Struct" soapenc:position="[1]">
                                        <name xsi:type="xsd:string">Access-Control-Allow-Origin</name>
                                        <value xsi:type="xsd:string">*</value>
                                </item>
                        </httpHeaderFields>
			<autoFooter xsi:type="xsd:boolean">false</autoFooter>
                </properties>
	</xp:Application>
	<xp:Module>${XDAQ_ROOT}/lib/libpthttp.so</xp:Module>

	<xp:Application heartbeat="false" class="pt::fifo::PeerTransportFifo" id="8" group="profile" network="local"/>
	<xp:Module>${XDAQ_ROOT}/lib/libptfifo.so</xp:Module>
	
	<!-- HyperDAQ -->
	<xp:Application heartbeat="false" class="hyperdaq::Application" id="3" group="profile" service="hyperdaq" network="local"/>
	<xp:Module>${XDAQ_ROOT}/lib/libhyperdaq.so</xp:Module>	

	
</xp:Profile>
