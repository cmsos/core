/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, L. Orsini and D. Simelevicius                   *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

//
// Version definition for FIFO peer transport
//
#ifndef _pt_fifo_h_
#define _pt_fifo_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define CORE_PTFIFO_VERSION_MAJOR 4
#define CORE_PTFIFO_VERSION_MINOR 4
#define CORE_PTFIFO_VERSION_PATCH 0
// If any previous versions available E.g. #define CORE_PTFIFO_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define CORE_PTFIFO_PREVIOUS_VERSIONS "4.3.0,4.3.1"


//
// Template macros
//
#define CORE_PTFIFO_VERSION_CODE PACKAGE_VERSION_CODE(CORE_PTFIFO_VERSION_MAJOR,CORE_PTFIFO_VERSION_MINOR,CORE_PTFIFO_VERSION_PATCH)
#ifndef CORE_PTFIFO_PREVIOUS_VERSIONS
#define CORE_PTFIFO_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(CORE_PTFIFO_VERSION_MAJOR,CORE_PTFIFO_VERSION_MINOR,CORE_PTFIFO_VERSION_PATCH)
#else 
#define CORE_PTFIFO_FULL_VERSION_LIST  CORE_PTFIFO_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(CORE_PTFIFO_VERSION_MAJOR,CORE_PTFIFO_VERSION_MINOR,CORE_PTFIFO_VERSION_PATCH)
#endif 

namespace ptfifo
{
	const std::string project = "core";
	const std::string package  =  "ptfifo";
	const std::string versions = CORE_PTFIFO_FULL_VERSION_LIST;
	const std::string summary = "FIFO peer transport for process internal communication";
	const std::string description = "";
	const std::string authors = "Johannes Gutleber, Luciano Orsini";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/Core_Tools";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
