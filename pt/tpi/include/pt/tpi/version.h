// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2012, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, C.Wakefield, A.Forrest			 	 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

//
// Version definition for UDAPL peer transport
//
#ifndef _pttpi_version_h_
#define _pttpi_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define CORE_PTTPI_VERSION_MAJOR 4
#define CORE_PTTPI_VERSION_MINOR 3
#define CORE_PTTPI_VERSION_PATCH 0
// If any previous versions available E.g. #define CORE_PTTPI_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define CORE_PTTPI_PREVIOUS_VERSIONS "4.0.0,4.1.0,4.1.1,4.1.2,4.1.3,4.1.4,4.1.5,4.2.0,4.2.1,4.2.2,4.2.3,4.2.4,4.2.5,4.2.6"


//
// Template macros
//
#define CORE_PTTPI_VERSION_CODE PACKAGE_VERSION_CODE(CORE_PTTPI_VERSION_MAJOR,CORE_PTTPI_VERSION_MINOR,CORE_PTTPI_VERSION_PATCH)
#ifndef CORE_PTTPI_PREVIOUS_VERSIONS
#define CORE_PTTPI_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(CORE_PTTPI_VERSION_MAJOR,CORE_PTTPI_VERSION_MINOR,CORE_PTTPI_VERSION_PATCH)
#else 
#define CORE_PTTPI_FULL_VERSION_LIST  CORE_PTTPI_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(CORE_PTTPI_VERSION_MAJOR,CORE_PTTPI_VERSION_MINOR,CORE_PTTPI_VERSION_PATCH)
#endif 
namespace pttpi
{
	const std::string project = "core";
    const std::string package  =  "pttpi";
    const std::string versions = CORE_PTTPI_FULL_VERSION_LIST;
    const std::string summary = "ptuTCP";
    const std::string description = "Universal TCP peer transport";
    const std::string authors = "Luciano Orsini, Andrea Petrucci, Chris Wakefield, Andy Forrest";
    const std::string link = "http://xdaq.web.cern.ch";
    config::PackageInfo getPackageInfo();
    void checkPackageDependencies() ;
    std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif

