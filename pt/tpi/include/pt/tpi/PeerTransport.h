// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.	                 		 *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius				 	 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                   	 *
 * For the list of contributors see CREDITS.   			      	 *
 *************************************************************************/

#ifndef _pt_tpi_PeerTransport_h
#define _pt_tpi_PeerTransport_h

#include <map>
#include <iostream>
#include <string>
#include <list>
#include <chrono>
#include <thread>
#include <mutex>
#include <deque>

#include "pt/Address.h"
#include "pt/PeerTransportSender.h"
#include "pt/PeerTransportReceiver.h"
#include "pt/PeerTransportAgent.h"
#include "pt/exception/Exception.h"
#include "pt/exception/UnknownProtocolOrService.h"
#include "pt/Listener.h"
#include "pt/Address.h"

#include "pt/tpi/Messenger.h"
#include "pt/tpi/Input.h"
#include "pt/tpi/Output.h"
#include "pt/pipe/EstablishedConnection.h"
#include "pt/pipe/ConnectionRequest.h"
#include "pt/pipe/Service.h"

#include "toolbox/mem/Reference.h"
#include "toolbox/BSem.h"
#include "toolbox/mem/MemoryPoolFactory.h"


#include "tcpla/InterfaceAdapter.h"
#include "tcpla/EndPoint.h"
#include "tcpla/Event.h"
#include "tcpla/EventHandler.h"

#include "xdata/Double.h"
#include "xdata/UnsignedInteger.h"

#include "xdaq/Object.h"

namespace pt
{

	namespace tpi
	{
		class InterfaceAdapter;

		class PeerTransport : public pt::pipe::Service, public pt::PeerTransportSender, public pt::PeerTransportReceiver, public tcpla::EventHandler, public xdaq::Object
		{
			public:

				PeerTransport (xdaq::Application * parent, toolbox::mem::Pool * pool, size_t maxClients, size_t ioSize, size_t eventQueueSize, const std::string & protocol);

				~PeerTransport ();

				friend std::ostream& operator<< (std::ostream &sout, pt::tpi::PeerTransport & pt);

				//! Enqueue a binary message to the peer transport's work loop. Raise an exception if queue is full
				//
				//void post (toolbox::mem::Reference * ref, pt::tpi::EndPoint * ep, toolbox::exception::HandlerSignature * handler, void * context)
				//;

				//! Retrieve the type of peer transport (Sender or Receiver or both)
				//
				pt::TransportType getType ();

				pt::Address::Reference createAddress (const std::string& url, const std::string& service) ;

				pt::Address::Reference createAddress (std::map<std::string, std::string, std::less<std::string> > & address) ;

				//! Returns the implemented protocol ("loopback" in this version)
				//
				std::string getProtocol ();

				//! Retrieve a list of supported services (i2o, b2in, frl)
				//
				std::vector<std::string> getSupportedServices ();

				//! Returns true if a service is supported (i2o, b2in, frl), false otherwise
				//
				bool isServiceSupported (const std::string & service);

				//! Retrieve a loopback messenger for the fifo peer transport that allows context internal application communication
				//
				pt::Messenger::Reference getMessenger (pt::Address::Reference destination, pt::Address::Reference local) ;

				//! Internal function to add a message processing listener for this peer transport
				//
				void addServiceListener (pt::Listener * listener) ;

				//! Internal function to remove a message processing listener for this peer transport
				//
				void removeServiceListener (pt::Listener * listener) ;

				//! Internal function to remove all message processing listeners for this peer transport
				//
				void removeAllServiceListeners ();

				//! Function to configure this peer transport with a loopback address
				//
				void config (pt::Address::Reference address) ;

				void handleEvent (tcpla::Event & e);

				std::string toHTML ();

				// Pipes srvice support 
			        void connect(pt::Address::Reference source,  pt::Address::Reference destination, pt::pipe::ServiceListener * l, std::shared_ptr<void> & user_data  );
				pt::pipe::Output * createOutputPipe( std::shared_ptr<pt::pipe::EstablishedConnection> & ec, pt::pipe::OutputListener * listener );
				pt::pipe::Input * createInputPipe( std::shared_ptr<pt::pipe::ConnectionRequest> & ec, pt::pipe::InputListener * listener );

				void destroyInputPipe(pt::pipe::Input * ip);
				void destroyOutputPipe(pt::pipe::Output * op);


				std::map<std::string, tcpla::InterfaceAdapter *> iav_;
				std::vector<tcpla::PublicServicePoint*> psp_;

			protected:

				toolbox::mem::Pool * pool_;
				std::mutex lock_;


				//std::map<std::string, size_t> eventCounter_;

				std::string lastEvent_;

				toolbox::mem::MemoryPoolFactory * factory_;

				size_t maxClients_;
				size_t ioQueueSize_;
				size_t eventQueueSize_;

				std::string protocol_;

				std::list<pt::Messenger::Reference> messengers_;
				size_t eventCounter_[tcpla::MAX_TCPLA_EVENT_NUMBER];
                                pt::Listener * listener_;

				std::list<pt::tpi::Input*> ipipes_;
				std::list<pt::tpi::Output*> opipes_;

				std::deque<toolbox::rlist<toolbox::mem::Reference*>*> freeCompletionQueues_;
				std::deque<toolbox::rlist<toolbox::mem::Reference*>*> allocatedCompletionQueues_;


			private:

				tcpla::InterfaceAdapter * getInterfaceAdapter (pt::Address::Reference address) ;

		};

	}
}

#endif
