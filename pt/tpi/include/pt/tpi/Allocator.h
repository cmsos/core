// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius					 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_tpi_Allocator_h_
#define _pt_tpi_Allocator_h_


#include "toolbox/mem/CommittedHeapAllocator.h"

namespace pt 
{
	namespace  tpi 
	{

	class Allocator : public toolbox::mem::CommittedHeapAllocator
	{
		public:

			Allocator (const std::string & name, size_t committedSize);

			virtual ~Allocator ();

			std::string type ();
			size_t getOffset();


		private:

	};

	}
}

#endif
