// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_tpi_EstablishedConnection_h_
#define _pt_tpi_EstablishedConnection_h_

#include "pt/pipe/EstablishedConnection.h"

namespace pt 
{
	namespace tpi 
	{

		class EstablishedConnection: public pt::pipe::EstablishedConnection
		{
			public:

                	EstablishedConnection(pt::PeerTransport * pt, std::shared_ptr<void> & ch, std::shared_ptr<void> &  context): pt_(pt), ch_(ch), context_(context) 
			{	
			}

			virtual ~EstablishedConnection()
                	{
				using PipeConnectionHandle = std::tuple<std::string, const xdaq::ApplicationDescriptor*, const xdaq::ApplicationDescriptor*, tcpla::EndPoint>;
				PipeConnectionHandle * ch = (PipeConnectionHandle*)ch_.get();
				tcpla::EndPoint ep = std::get<3>(*ch);
				ep.ia_->destroyEndPoint(ep);
                	}

			std::shared_ptr<void> getConnectionHandle()
	                {
                        	return ch_;
                	}

                	void * getContext()
                	{
                        	return context_.get();
                	}

			pt::PeerTransport * getPeerTransport()
			{
				return pt_;
			}

			protected:

			pt::PeerTransport * pt_;
                	std::shared_ptr<void> ch_;
                	std::shared_ptr<void> context_;

			

		};

	}
}

#endif
