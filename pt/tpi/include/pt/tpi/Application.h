// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _pt_tpi_Application_h_
#define _pt_tpi_Application_h_

#include "xdaq/Application.h"
#include "xgi/framework/UIManager.h"


#include "xdata/UnsignedInteger.h"
#include "xdata/UnsignedInteger64.h"
#include "xdata/UnsignedLong.h"
#include "xdata/Double.h"
#include "xdata/String.h"
#include "xdata/Boolean.h"
#include "xdata/Integer.h"
#include "xdata/Bag.h"
#include "xdata/Vector.h"
#include "xdaq/ApplicationGroup.h"
#include "xdaq/ApplicationStub.h"
#include "xdaq/ApplicationContext.h"

#include "toolbox/mem/Reference.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/mem/CommittedHeapAllocator.h"
#include "toolbox/exception/Handler.h"
#include "toolbox/net/URN.h"
#include <list>
#include <algorithm>
#include <map>
#include "toolbox/PerformanceMeter.h"

#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerEvent.h"
#include "toolbox/task/TimerListener.h"

#include "pt/tpi/PeerTransport.h"

//
// cgicc
//
#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "xgi/Utils.h"
#include "xgi/Input.h"
#include "xgi/Output.h"

#include "tcpla/WaitingWorkLoop.h"

namespace pt
{
	namespace tpi
	{

		const std::string TPI_PROTOCOL = "tpi";

		class Application : public xdaq::Application, public xgi::framework::UIManager, public xdata::ActionListener, public toolbox::task::TimerListener
		{

			public:

				XDAQ_INSTANTIATOR();

				Application (xdaq::ApplicationStub* stub) ;

				//
				// Callback for requesting current exported parameter values
				//
				void actionPerformed (xdata::Event& e);

				// Web callback functions
				void Default (xgi::Input * in, xgi::Output * out) ;

				pt::tpi::PeerTransport * pt_;
			private:
				void SenderTabPage (xgi::Output * out);
				void ReceiverTabPage (xgi::Output * out);
				void SettingsTabPage (xgi::Output * out);
				void EventsTabPage (xgi::Output * out);
				void timeExpired (toolbox::task::TimerEvent& e);
				void WorkloopsTabPage (xgi::Output * out);

			protected:

				xdata::Double committedPoolSize_; //socket pool size
				xdata::UnsignedInteger maxClients_;
				xdata::UnsignedInteger ioQueueSize_;
				xdata::UnsignedInteger eventQueueSize_;
				xdata::String protocol_;
		};
	}
}

#endif
