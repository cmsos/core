// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_tpi_Input_h_
#define _pt_tpi_Input_h_

#include "pt/pipe/Input.h"
#include "pt/pipe/ConnectionRequest.h"
#include "pt/pipe/ConnectionClosedByPeer.h"
#include "pt/pipe/UnusedBlock.h"

#include "toolbox/mem/Reference.h"
#include "toolbox/rlist.h"

#include "tcpla/EndPoint.h"
#include "tcpla/PublicServicePoint.h"

#include "pt/tpi/HeaderInfo.h"
#include "pt/tpi/exception/Exception.h"

namespace pt 
{
	namespace  tpi 
	{

		class Input: public pt::pipe::Input
		{
			public:

			Input(std::shared_ptr<pt::pipe::ConnectionRequest> & pcr, pt::pipe::InputListener * listener, toolbox::rlist<toolbox::mem::Reference*> * cqueue):  pcr_(pcr), listener_(listener)
			{
				using PipeConnectionHandle = std::tuple<tcpla::EndPoint,int, std::string>;
                                PipeConnectionHandle * ch = (PipeConnectionHandle*)pcr_->getConnectionHandle().get();
                                ep_ = std::get<0>(*ch);

				using AcceptContext =std::tuple<pt::pipe::InputListener *, pt::pipe::Input *>;
			 	AcceptContext * acontext = new std::tuple<pt::pipe::InputListener *, pt::pipe::Input *>(listener,this);	
				tcpla::TCPLA_CONTEXT cookie;
                        	cookie.as_ptr = acontext;
				ep_.psp_->accept(ep_,std::get<1>(*ch),cookie );


				cq = cqueue;
				
			}

			virtual ~Input()
			{
			}


			void postFrame(toolbox::mem::Reference * ref ) 
			{

				if (ref != 0)
				{
					// reset frame to original
					//ref->setDataOffset(0);	
					//ref->setDataSize(ref->getBuffer()->getSize());	
					//size_t size = ref->getDataSize();
					//char * frame = (char *) ref->getDataLocation();

					if ( ref->getDataOffset() != pt::tpi::HeaderInfo::getHeaderSize() )
					{
						XCEPT_RAISE(pt::tpi::exception::Exception, "invalid buffer reference, offset");
                        		}

					char * frame = (char*)ref->getBuffer()->getAddress(); // without offset
					size_t size = ref->getBuffer()->getSize(); // including offset

					//std::cout << "Input Pipe post frame for receiving with offset " << ref->getDataOffset() << " block size " << size << std::endl;

					tcpla::TCPLA_CONTEXT cookie;
					cookie.as_ptr = cq; // remember which reference

                                       	ep_.psp_->postRecvBuffer(ep_, cookie, ref, (char *)frame, size);

				}
				
			}

			bool empty()
			{
				return cq->empty();
			}

			toolbox::mem::Reference * completed()
			{
				toolbox::mem::Reference * ref = cq->front();
				cq->pop_front();
				return ref;
			}


                        void fire(pt::pipe::UnusedBlock & event)
                        {
                                //std::cout << "fireUnusedBlock inside pt::tpi::Input (poiter is " << std::hex << (size_t)listener_ << std::dec << ")" << std::endl;
                                if (listener_ != 0 )
                                        listener_->actionPerformed(event);
                        }

			pt::pipe::InputListener * getListener()
			{
				return listener_;
			}

			public:

			toolbox::rlist<toolbox::mem::Reference*>* cq;
			
			protected:

			std::shared_ptr<pt::pipe::ConnectionRequest> pcr_;
			tcpla::EndPoint ep_;

			pt::pipe::InputListener * listener_;

			

		};

	}
}

#endif
