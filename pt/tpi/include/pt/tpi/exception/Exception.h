// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2011, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J.Adamczewski(EE/GSI), S.Linev(EE/GSI), L.Orsini, A.Petrucci *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_tpi_exception_Exception_h_
#define _pt_tpi_exception_Exception_h_

#include "pt/exception/Exception.h"

#define UTCP_XCEPT_ALLOC( EXCEPTION, MSG ) \
new EXCEPTION ( #EXCEPTION, MSG, __FILE__, __LINE__, __FUNCTION__)

#define UTCP_XCEPT_ALLOC_NESTED( EXCEPTION, MSG, PREVIOUS ) \
new EXCEPTION ( #EXCEPTION, MSG, __FILE__, __LINE__, __FUNCTION__, PREVIOUS)

namespace pt
{
	namespace tpi
	{
		namespace exception
		{

			class Exception: public pt::exception::Exception
			{
				public:
					Exception ( std::string name, std::string message, std::string module, int line, std::string function ) :
							pt::exception::Exception(name, message, module, line, function)
					{
					}

					Exception ( std::string name, std::string message, std::string module, int line, std::string function, xcept::Exception& e ) :
							pt::exception::Exception(name, message, module, line, function, e)
					{
					}
			};
		}
	}
}

#endif
