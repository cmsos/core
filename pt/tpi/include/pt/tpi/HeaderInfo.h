// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_tpi_header_info_h_
#define _pt_tpi_header_info_h_

#include <cstddef>
#include <cstdint>

namespace pt
{
	namespace tpi 
	{

		class HeaderInfo
		{
			public:

				static size_t getLength (char * bufferPtr)
				{
					return (*((uint64_t*)bufferPtr));
				}

				static size_t getHeaderSize ()
				{
					return sizeof(uint64_t);
				}
				
				static bool insertHeader() 
				{
                                        return true;
                                }

				static void writeLength (char * bufferPtr, size_t len)
				{
					*((uint64_t*)bufferPtr) = len;
				}
		};
	}


}
#endif
