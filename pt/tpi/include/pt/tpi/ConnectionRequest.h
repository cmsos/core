// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_tpi_ConnectionRequest_h_
#define _pt_tpi_ConnectionRequest_h_

#include"pt/pipe/ConnectionRequest.h"

namespace pt 
{
	namespace tpi 
	{

		class ConnectionRequest: public pt::pipe::ConnectionRequest
		{
			public:
			ConnectionRequest(pt::PeerTransport * pt, std::shared_ptr<void> & ch, const std::string & network): pt_(pt), ch_(ch), network_(network)
                        {
                        }

                        virtual ~ConnectionRequest()
                        {
                                using PipeConnectionHandle = std::tuple<tcpla::EndPoint,int, std::string>;
                                PipeConnectionHandle * ch = (PipeConnectionHandle*)ch_.get();
                                tcpla::EndPoint ep = std::get<0>(*ch);
                                ep.ia_->destroyEndPoint(ep);
                        }

                        std::shared_ptr<void> getConnectionHandle()
                        {
                                return ch_;
                        }
			
			std::string getNetwork()
			{
				/*
				using PipeConnectionHandle = std::tuple<tcpla::EndPoint,std::string>;
                                PipeConnectionHandle * ch = (PipeConnectionHandle*)ch_.get();
                                return std::get<1>(*ch);
				*/
				return network_;
			}

                       pt::PeerTransport * getPeerTransport()
                       {
                                return pt_;
                       }


			protected:

			pt::PeerTransport *pt_;
			std::shared_ptr<void> ch_;
			std::string network_;

				 

		};

	}
}

#endif
