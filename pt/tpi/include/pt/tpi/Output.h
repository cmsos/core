// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_tpi_Output_h_
#define _pt_tpi_Output_h_

#include "pt/pipe/Output.h"
#include "pt/pipe/OutputListener.h"
#include "pt/pipe/EstablishedConnection.h"

#include "toolbox/mem/Reference.h"
#include "toolbox/rlist.h"
#include "toolbox/hexdump.h"

#include "tcpla/EndPoint.h"

namespace pt 
{
	namespace  tpi 
	{

		class PeerTransport;
		
		class Output: public pt::pipe::Output
		{
			public:


			Output(std::shared_ptr<pt::pipe::EstablishedConnection> & pec, pt::pipe::OutputListener * listener, toolbox::rlist<toolbox::mem::Reference*> * cqueue);

			virtual ~Output();

			void postFrame(toolbox::mem::Reference * ref ) ;

			bool empty();

			toolbox::mem::Reference * completed();


			void fire(pt::pipe::UndeliverableBlock & event);

			pt::pipe::OutputListener *  getOutputListener();

			public:

			toolbox::rlist<toolbox::mem::Reference*>* cq;
			
			protected:

			std::shared_ptr<pt::pipe::EstablishedConnection> pec_;
			tcpla::EndPoint ep_;
                        tcpla::InterfaceAdapter * ia_;

			pt::pipe::OutputListener * listener_;


		};

	}
}

#endif
