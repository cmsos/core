// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D.Simelevicius				 	 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _pt_tpi_Messenger_h
#define _pt_tpi_Messenger_h

#include "toolbox/mem/Reference.h"

#include "tcpla/Address.h"
#include "tcpla/EndPoint.h"
#include "tcpla/InterfaceAdapter.h"
#include "tcpla/Event.h"

#include "pt/tpi/exception/Exception.h"
#include "tcpla/exception/ConnectionNotEstablished.h"
#include "tcpla/exception/CannotConnect.h"
#include "tcpla/exception/CorruptMessageFrame.h"

#include "toolbox/BSem.h"

#include "pt/Messenger.h"

namespace pt
{
	namespace tpi
	{

		class PeerTransport;
		class Application;

		class Messenger: public pt::Messenger 
		{
				friend class PeerTransport;
				friend class Application;

			public:

				Messenger (pt::tpi::PeerTransport * pt, tcpla::InterfaceAdapter * ia, pt::Address::Reference local, pt::Address::Reference destination);

				virtual ~Messenger ();

				bool match (tcpla::EndPoint & ep);
				void reset ();
				void connectionEstablished (tcpla::EndPoint & ep);
				void postConnect () ;

                                pt::Address::Reference getLocalAddress ();
                                pt::Address::Reference getDestinationAddress ();

                                void send (toolbox::mem::Reference* msg, toolbox::exception::HandlerSignature* handler, void* context) ;
				bool established_;

				std::string getService();

			protected:

				pt::tpi::PeerTransport * pt_;
				pt::Address::Reference destination_;
				pt::Address::Reference local_;

				uint16_t identification_;
				toolbox::BSem mutex_;

			public:

				tcpla::EndPoint ep_;
				tcpla::InterfaceAdapter * ia_;
                                bool connectOnRequest_;
                                size_t  numberOfRetries_; // connection timeout

		};
	}
}

#endif

