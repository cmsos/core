// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, A.Petrucci, C.Wakefield					 *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "xcept/tools.h"

#include "pt/tpi/PeerTransport.h"
#include "pt/tpi/HeaderInfo.h"
#include "pt/exception/ReceiveFailure.h"

#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/hexdump.h"

#include "tcpla/PublicServicePoint.h"
#include "tcpla/InterfaceAdapterPoll.h"
#include "tcpla/InterfaceAdapterSelect.h"
#include "tcpla/Utils.h"

#include "pt/pipe/ConnectionError.h"
#include "pt/tpi/EstablishedConnection.h"
#include "pt/tpi/ConnectionRequest.h"
#include "pt/tpi/Output.h"
#include "pt/tpi/Input.h"

pt::tpi::PeerTransport::PeerTransport (xdaq::Application * owner, toolbox::mem::Pool * pool, size_t maxClients, size_t ioQueueSize, size_t eventQueueSize, const std::string & protocol)
	: xdaq::Object(owner), pool_(pool)
{
	this->ioQueueSize_ = ioQueueSize;
	this->eventQueueSize_ = eventQueueSize;
	this->maxClients_ = maxClients;
	this->protocol_ = protocol;

	for (size_t i = 0; i < tcpla::MAX_TCPLA_EVENT_NUMBER; i++)
	{
		eventCounter_[i] = 0;
	}

	factory_ = toolbox::mem::getMemoryPoolFactory();
}

pt::tpi::PeerTransport::~PeerTransport ()
{
	for (std::map<std::string, tcpla::InterfaceAdapter *>::iterator i = iav_.begin(); i != iav_.end(); i++)
	{
		delete (*i).second;
		iav_.erase(i);
	}

}

pt::Messenger::Reference pt::tpi::PeerTransport::getMessenger (pt::Address::Reference destination, pt::Address::Reference local) 
{
	XCEPT_RAISE(pt::exception::Exception, "message passing not supportedi for this peer transport, please use pipe");
}

tcpla::InterfaceAdapter * pt::tpi::PeerTransport::getInterfaceAdapter (pt::Address::Reference address) 
{
	tcpla::Address & a = dynamic_cast<tcpla::Address &>(*address);
	tcpla::InterfaceAdapter * ia = 0;

	std::string id = a.getProperty("hostname");
	std::string service = a.getProperty("service");
	std::string smode = a.getProperty("smode");

	if (iav_.find(id) != iav_.end())
	{
		LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "retrieving cached interface adapter for host " << id);
		ia = iav_[id];
	}
	else
	{
		try
		{
			if (service == "pipe")
			{
				if (smode == "poll")
				{
					LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "creating interface adapter for host " << id);
					ia = new tcpla::InterfaceAdapterPoll(this->getOwnerApplication(), address, this, ioQueueSize_, maxClients_, ioQueueSize_, eventQueueSize_, &pt::tpi::HeaderInfo::getHeaderSize, &pt::tpi::HeaderInfo::getLength); //new listening socket on config


				}
				else if (smode == "select")
				{
					ia = new tcpla::InterfaceAdapterSelect(this->getOwnerApplication(), address, this, ioQueueSize_, maxClients_, ioQueueSize_, eventQueueSize_, &pt::tpi::HeaderInfo::getHeaderSize, &pt::tpi::HeaderInfo::getLength); //new listening socket on config
				}
				else
				{
					XCEPT_RAISE(pt::tpi::exception::Exception, "invalid mode");
				}
			}
		}
		catch (tcpla::exception::InvalidSndTimeOut & e)
		{
			XCEPT_RETHROW(pt::tpi::exception::Exception, "Bad configuration, failed to create interface adapter", e);
		}
		catch (tcpla::exception::Exception & e)
		{
			XCEPT_RETHROW(pt::tpi::exception::Exception, "Failed to create interface adapter", e);
		}

		iav_[id] = ia;
	}

	return ia;
}

pt::TransportType pt::tpi::PeerTransport::getType ()
{
	return pt::SenderReceiver;
}

pt::Address::Reference pt::tpi::PeerTransport::createAddress (const std::string& url, const std::string& service) 
{
	//std::cout << "pt::tpi::PeerTransport::createAddress URL:" << url << " Service: " << service << std::endl;
	std::map<std::string, std::string, std::less<std::string> > plist;

	toolbox::net::URL urltmp(url);

	plist["protocol"] = urltmp.getProtocol();
	plist["service"] = service;
	plist["hostname"] = urltmp.getHost();
	plist["port"] = toolbox::toString("%d", urltmp.getPort());

	/*
	for (std::map<std::string, std::string>::iterator i = plist.begin(); i != plist.end(); i++)
	{
		std::cout << "tpi address " << i->first << ": " << i->second << std::endl;
	}
	*/

	return this->createAddress(plist);
}

pt::Address::Reference pt::tpi::PeerTransport::createAddress (std::map<std::string, std::string, std::less<std::string> >& address) 
{

	std::string protocol = address["protocol"];

	if (protocol == this->protocol_)
	{
		//port scanning here

		return pt::Address::Reference(new tcpla::Address(address));
	}
	else
	{
		std::string msg = "Cannot create address, protocol not supported: ";
		msg += protocol;
		XCEPT_RAISE(pt::exception::InvalidAddress, msg);
	}
}

std::string pt::tpi::PeerTransport::getProtocol ()
{
	return this->protocol_;
}

void pt::tpi::PeerTransport::config (pt::Address::Reference address) 
{
 	std::lock_guard<std::mutex> guard(lock_);
	// just tell interface adapter we are list
	tcpla::PublicServicePoint* psp = 0;
	try
	{
		//tcpla::Address & a = dynamic_cast<tcpla::Address &>(*address);
		tcpla::InterfaceAdapter * ia = this->getInterfaceAdapter(address);

		psp = ia->listen(address);
		psp_.push_back(psp);

	}
	catch (pt::tpi::exception::Exception &e)
	{
		XCEPT_RETHROW(pt::exception::Exception, "Failed to configure receiver port in IA", e);

	}
}

std::vector<std::string> pt::tpi::PeerTransport::getSupportedServices ()
{

	std::vector < std::string > s;
	s.push_back("pipe");

	return s;
}

bool pt::tpi::PeerTransport::isServiceSupported (const std::string& service)
{

	if (service.find("pipe") !=std::string::npos )
	{
		return true;
	}

	return false;
}

void pt::tpi::PeerTransport::addServiceListener (pt::Listener* listener) 
{

	pt::PeerTransportReceiver::addServiceListener(listener);

	if (listener->getService() == "pipe")
	{
		listener_ = listener;
		return;
	}

	XCEPT_RAISE(pt::exception::Exception, "Failed to add listener of unknown type");
}

void pt::tpi::PeerTransport::removeServiceListener (pt::Listener* listener) 
{
	pt::PeerTransportReceiver::removeServiceListener(listener);

	if ( listener_ == listener )
	{
		listener_ = 0;
	}
	XCEPT_RAISE(pt::exception::Exception, "Failed to remove listener of unknown type");
}

void pt::tpi::PeerTransport::removeAllServiceListeners ()
{
	pt::PeerTransportReceiver::removeAllServiceListeners();

	listener_ = 0;
}


std::string pt::tpi::PeerTransport::toHTML ()
{
        std::stringstream ss;

        ss << "                 <table class=\"xdaq-table\">" << std::endl;
        ss << "                         <caption>I2O Events</caption>";
        ss << "                         <thead>" << std::endl;
        ss << "                         <tr>" << std::endl;
        ss << "                         <th>Name</th>" << std::endl;
        ss << "                         <th>Count</th>" << std::endl;
        ss << "                         </tr>" << std::endl;
        ss << "                         </thead>" << std::endl;
        ss << "                         <tbody>" << std::endl;

        for (size_t i = 0; i < tcpla::MAX_TCPLA_EVENT_NUMBER; i++)
        {
                ss << "                                 <tr>" << std::endl;
                ss << "                                 <td>" << tcpla::eventToString(i) << "</td>" << std::endl;
                ss << "                                 <td>" << toolbox::toString("%d", eventCounter_[i]) << "</td>" << std::endl;
                ss << "                                 </tr>" << std::endl;
        }

        ss << "                         </tbody>" << std::endl;
        ss << "                 </table>" << std::endl;

        return ss.str();
}


// this callback can be invoked by several threads

namespace pt
{
	namespace tpi
	{
		std::ostream& operator<< (std::ostream &sout, pt::tpi::PeerTransport & pt)
		{
 			std::lock_guard<std::mutex> guard(pt.lock_);

			sout << "<table>" << std::endl;
			sout << "<tbody>" << std::endl;
			sout << "<tr>" << std::endl;
			sout << "<td style=\"vertical-align: top; padding-right: 10px;\">" << std::endl;
			sout << pt.toHTML() << std::endl;
			sout << "</td>" << std::endl;
			sout << "</tr>" << std::endl;
			sout << "</tbody>" << std::endl;
			sout << "</table>" << std::endl;

			return sout;
		}
	}
}

// this callback can be invoked by several threads
void pt::tpi::PeerTransport::handleEvent (tcpla::Event & event)
{
	// All event handling goes here
	//std::cout << "Event Number: " << tcpla::eventToString(event.event_number) << std::endl;

	eventCounter_[event.event_number]++;

	switch (event.event_number)
	{
		case tcpla::TCPLA_DTO_RCV_COMPLETION_EVENT:
		{
			toolbox::rlist<toolbox::mem::Reference*> * cq = (toolbox::rlist<toolbox::mem::Reference*> *)event.event_data.dto_completion_event_data.user_cookie.as_ptr;
			toolbox::mem::Reference * ref = (toolbox::mem::Reference*) event.event_data.dto_completion_event_data.user_context;
			if (ref != 0)
			{
				try 
				{
					//std::cout << " --- >> receive  completion " << std::hex << (size_t)ref << std::dec << std::endl;
					//std::cout << "--->> buffer data size is " << ref->getDataSize() << " actual transferred len " << event.event_data.dto_completion_event_data.transfered_length << " offset " << ref->getDataOffset() << " original block size " << ref->getBuffer()->getSize() << std::endl;

					// set actual data segmenment and size
					//char * frame = (char*)ref->getBuffer()->getAddress();
					//size_t len = *(uint64_t*)frame;
					//std::cout << " +++++++> size within frame is " << len << std::endl;
					ref->setDataSize( event.event_data.dto_completion_event_data.transfered_length - sizeof(uint64_t));

					//ref->setDataOffset(sizeof(uint64_t)); 
					//toolbox::hexdump ((void*) frame, 48);

					cq->push_back(ref);
				}
				catch(...)
				{
					std::cout << "something wrong may be queue full " << std::endl;
					exit(-1);
				}
			}

		}
			break;

		case tcpla::TCPLA_DTO_SND_COMPLETION_EVENT:
		{
			toolbox::rlist<toolbox::mem::Reference*> * cq = (toolbox::rlist<toolbox::mem::Reference*> *)event.event_data.dto_completion_event_data.user_cookie.as_ptr;
			toolbox::mem::Reference * ref = (toolbox::mem::Reference*) event.event_data.dto_completion_event_data.user_context;
			if (ref != 0)
			{
				try 
				{
					//std::cout << " --- >> send  completion " << std::hex << (size_t)ref << std::dec << std::endl;
					cq->push_back(ref);
				}
				catch(...)
				{
					std::cout << "something wrong may be queue full " << std::endl;
					exit(-1);
				}
			}

		}
			break;

		case tcpla::TCPLA_CONNECTION_REQUEST_EVENT: // server only
		{
			LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "Connection request");
			tcpla::EndPoint ep_handle = event.event_data.cr_event_data.psp->ia_->createEndPoint(event.event_data.cr_event_data);
			std::string network = event.event_data.cr_event_data.psp->getAddress()->getProperty("network");
			using PipeConnectionHandle = std::tuple<tcpla::EndPoint,int,std::string>;
			std::shared_ptr<void> ch = std::make_shared<PipeConnectionHandle>(ep_handle,event.event_data.cr_event_data.cr_handle, network);
                        std::shared_ptr<pt::tpi::ConnectionRequest> pcr = std::make_shared<pt::tpi::ConnectionRequest>(this,ch,network);

			std::list<pt::pipe::ServiceListener*> & dispatcher = dynamic_cast<std::list<pt::pipe::ServiceListener*>&>(*listener_);
        		for ( auto i = dispatcher.begin(); i != dispatcher.end(); i++)
        		{
                        	if ( (*i)->actionPerformed(pcr) )
				{
					// in practice it should allocate buffers for receiving and then accept to prevent receiving before but with pipes it is not a problem
					//std::cout << "call back TRUE on network " << network  <<  ", skip other listeners" << std::endl;
					break;
				}
        		}


		}
			break;


		case tcpla::TCPLA_CONNECTION_EVENT_ESTABLISHED:  // client only
		{
			LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "Connection event established");
                        if (event.event_data.connect_event_data.user_cookie.as_ptr != 0)
                        {
				// retrieve context information
        			using PipeConnectionRequestContext = std::tuple<pt::pipe::ServiceListener*, std::shared_ptr<void> >;
                                PipeConnectionRequestContext * cookie = (PipeConnectionRequestContext*)event.event_data.connect_event_data.user_cookie.as_ptr;
				pt::pipe::ServiceListener * l = std::get<0>(*cookie);
				std::shared_ptr<void> user_context = std::get<1>(*cookie);
				delete cookie;

				// create Pipe Connection Handle
				using PipeConnectionHandle = std::tuple<tcpla::EndPoint>;
 				std::shared_ptr<void> pch = std::make_shared<PipeConnectionHandle>(event.event_data.connect_event_data.ep_handle);


				// Call back requiring service for connection
				std::shared_ptr<pt::tpi::EstablishedConnection> pec = std::make_shared<pt::tpi::EstablishedConnection>(this,pch,user_context);
                                l->actionPerformed(pec);
                        }

		}
			break;

		case tcpla::TCPLA_CONNECTION_EVENT_ACCEPTED:
		{

			LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "Establishing connection for receiver");

		}
			break;

		case tcpla::TCPLA_CONNECTION_EVENT_ACCEPT_COMPLETION_ERROR:
		case tcpla::TCPLA_CONNECTION_EVENT_NON_PEER_REJECTED:
		case tcpla::TCPLA_CONNECTION_EVENT_PEER_REJECTED:
		{
			if (event.event_data.asynch_error_event_data.exception != 0)
			{
				LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), xcept::stdformat_exception_history(*(event.event_data.asynch_error_event_data.exception)));

				delete event.event_data.asynch_error_event_data.exception;
			}
		}
			break;

		case tcpla::TCPLA_CONNECTION_EVENT_DISCONNECTED:
		{
			// sender connected endpoint
			LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "disconnection event, not yet implemented");

			if (event.event_data.connection_broken_event_data.exception != 0)
			{
					LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), xcept::stdformat_exception_history(*(event.event_data.connection_broken_event_data.exception)));
					delete event.event_data.connection_broken_event_data.exception;
			}

		}
			break;

		case tcpla::TCPLA_CONNECTION_CLOSED_BY_PEER: // server only
		{
			if (event.event_data.connection_broken_event_data.exception != 0)
			{
				LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), xcept::stdformat_exception_history(*(event.event_data.connection_broken_event_data.exception)));
				delete event.event_data.connection_broken_event_data.exception;
			}

                        using AcceptContext  = std::tuple<pt::pipe::InputListener *, pt::pipe::Input *>;
			AcceptContext * acontext  = (AcceptContext *) event.event_data.connection_broken_event_data.user_cookie.as_ptr;

			pt::pipe::InputListener * l = std::get<0>(*acontext);
			pt::pipe::Input *  ipipe = std::get<1>(*acontext);
			delete acontext;
			if (l != 0 )
			{
				LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "tcpla::TCPLA_CONNECTION_CLOSED_BY_PEER call back input listener" );
				pt::pipe::ConnectionClosedByPeer  event(ipipe);
                        	l->actionPerformed(event);
			}
			else
			{
				LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), "tcpla::TCPLA_CONNECTION_CLOSED_BY_PEER is ignored, pipe memeber already destroyed");
			}
		}
			break;

		case tcpla::TCPLA_CONNECTION_EVENT_BROKEN: // server only
		{
			LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "connection event broken, not yet implemented");
			if (event.event_data.connection_broken_event_data.exception != 0)
			{
				LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), xcept::stdformat_exception_history(*(event.event_data.connection_broken_event_data.exception)));
				delete event.event_data.connection_broken_event_data.exception;
			}

			try
			{
				event.event_data.connection_broken_event_data.ia->destroyEndPoint(event.event_data.connection_broken_event_data.ep_handle);
			}
			catch (tcpla::exception::IACommandQueueFull & e)
			{
				std::stringstream msg;
				msg << "Failed to destroy endpoint";
				XCEPT_DECLARE_NESTED(pt::exception::Exception, ex, msg.str(), e);
				this->getOwnerApplication()->notifyQualified("fatal", ex);
				return;

			}
		}
			break;

		case tcpla::TCPLA_CONNECTION_RESET_BY_PEER: //server only
		{
			LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "connection reset by peer, not yet implemented");
			if (event.event_data.connection_broken_event_data.exception != 0)
			{
				LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), xcept::stdformat_exception_history(*(event.event_data.connection_broken_event_data.exception)));

				delete event.event_data.connection_broken_event_data.exception;
			}

			try
			{
				event.event_data.connection_broken_event_data.ia->destroyEndPoint(event.event_data.connection_broken_event_data.ep_handle);
			}
			catch (tcpla::exception::IACommandQueueFull & e)
			{
				std::stringstream msg;
				msg << "Failed to destroy endpoint";
				XCEPT_DECLARE_NESTED(pt::exception::Exception, ex, msg.str(), e);
				this->getOwnerApplication()->notifyQualified("fatal", ex);
				return;
			}
		}
			break;

		case tcpla::TCPLA_CONNECTION_EVENT_TIMED_OUT:
			std::cout << "tcpla::TCPLA_CONNECTION_EVENT_TIMED_OUT, not implemented " << std::endl;

			break;

		case tcpla::TCPLA_CONNECTION_EVENT_UNREACHABLE:
		{	//server unreachable in IA
			LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "Host unreachable");
			if (event.event_data.connect_error_event_data.user_cookie.as_ptr != 0)
			{
				LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "Found cookie");
        			using PipeConnectionRequestContext = std::tuple<pt::pipe::ServiceListener*, std::shared_ptr<void> >;
                                PipeConnectionRequestContext * cookie = (PipeConnectionRequestContext*)event.event_data.connect_error_event_data.user_cookie.as_ptr;
				pt::pipe::ServiceListener * l = std::get<0>(*cookie);
				std::shared_ptr<void> user_context = std::get<1>(*cookie);
				delete cookie;

				if (event.event_data.connect_error_event_data.exception != 0)
				{
					//std::cout << "tcpla::TCPLA_CONNECTION_EVENT_UNREACHABLE , assign exception " << std::endl;
					std::shared_ptr<pt::pipe::ConnectionError> pce = std::make_shared<pt::pipe::ConnectionError>(user_context, *event.event_data.connect_error_event_data.exception);
					l->actionPerformed(pce);
				}
				else
				{
					std::cout << "*** Going to call back pipe service listener is EMPTY" << std::endl;
				}
			}

			LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "Clean up");
			try
			{
				if (!event.event_data.connect_error_event_data.ep_handle.isNull())
				{
					//std::cout << "tcpla::TCPLA_CONNECTION_EVENT_UNREACHABLE , destroy endpoint " << std::endl;
					event.event_data.connect_error_event_data.ep_handle.ia_->destroyEndPoint(event.event_data.connect_error_event_data.ep_handle);
				}
			}
			catch (tcpla::exception::IACommandQueueFull & e)
			{
				std::stringstream msg;
				msg << "Failed to destroy endpoint";
				XCEPT_DECLARE_NESTED(pt::exception::Exception, ex, msg.str(), e);
				this->getOwnerApplication()->notifyQualified("fatal", ex);
				return;
			}

			if (event.event_data.connect_error_event_data.exception != 0)
			{
				LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), xcept::stdformat_exception_history(*(event.event_data.connect_error_event_data.exception)));

				delete event.event_data.connect_error_event_data.exception;
			}

		}
			break;

		case tcpla::TCPLA_ASYNC_ERROR_EVD_OVERFLOW:
		{
			if (event.event_data.asynch_error_event_data.exception != 0)
			{
				LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), xcept::stdformat_exception_history(*(event.event_data.asynch_error_event_data.exception)));

				delete event.event_data.asynch_error_event_data.exception;
			}
		}
			break;

		case tcpla::TCPLA_ASYNC_ERROR_IA_CATASTROPHIC:
		{

			if (event.event_data.asynch_error_event_data.exception != 0)
			{
				LOG4CPLUS_FATAL(this->getOwnerApplication()->getApplicationLogger(), xcept::stdformat_exception_history(*(event.event_data.asynch_error_event_data.exception)));

				delete event.event_data.asynch_error_event_data.exception;
			}
		}
			break;

		case tcpla::TCPLA_ASYNC_ERROR_EP_BROKEN:
		{
			if (event.event_data.asynch_error_event_data.exception != 0)
			{
				LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), xcept::stdformat_exception_history(*(event.event_data.asynch_error_event_data.exception)));

				delete event.event_data.asynch_error_event_data.exception;
			}
		}
			break;

		case tcpla::TCPLA_ASYNC_ERROR_TIMED_OUT:

			break;

		case tcpla::TCPLA_BROKEN_PIPE:
		{

			// broken pipe
			if (event.event_data.asynch_error_event_data.exception != 0)
			{
				LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), xcept::stdformat_exception_history(*(event.event_data.asynch_error_event_data.exception)));

				delete event.event_data.asynch_error_event_data.exception;
			}


			toolbox::rlist<toolbox::mem::Reference*> * cq  = (toolbox::rlist<toolbox::mem::Reference*> *) event.event_data.asynch_error_event_data.user_cookie.as_ptr;
                        lock_.lock();
			pt::tpi::Output * opipe = 0;
			pt::pipe::OutputListener * listener = 0;
                        for (auto o: opipes_)
                        {
                                if (o->cq ==  cq)
                                {
					opipe = o;
					listener = o->getOutputListener();
					break;
                                }
                        }
                        lock_.unlock();

                        if (listener != 0 )
			{
				pt::pipe::BrokenPipe  event(opipe);
                                listener->actionPerformed(event);
			}
			else
			{
				LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), "cannot find corresponding opipe for this event");
			}

		}
			break;
		case tcpla::TCPLA_ASYNC_ERROR_PROVIDER_INTERNAL_ERROR:
		{
			LOG4CPLUS_ERROR(this->getOwnerApplication()->getApplicationLogger(), "tcpla::TCPLA_ASYNC_ERROR_PROVIDER_INTERNAL_ERROR, not implemented");
			if (event.event_data.asynch_error_event_data.exception != 0)
			{
				LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), xcept::stdformat_exception_history(*(event.event_data.asynch_error_event_data.exception)));

				delete event.event_data.asynch_error_event_data.exception;
			}
		}

			break;


		case tcpla::TCPLA_SOFTWARE_EVENT:

			break;

		case tcpla::TCPLA_POST_BLOCK_FAILED:
		case tcpla::TCPLA_DTO_SND_UNDELIVERABLE_BLOCK:
		{
			std::stringstream ss;
			ss << "Event: " << tcpla::eventToString(event.event_number);
			LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), ss.str());

                        toolbox::mem::Reference * ref = (toolbox::mem::Reference*) event.event_data.tcpla_dto_block_event_data.user_context;
                        if (ref != 0)
                        {
				ref->release();
			}
		}
			break;

		case tcpla::TCPLA_DTO_RCV_UNUSED_BLOCK:
		{
			std::stringstream ss;
			ss << "Event: " << tcpla::eventToString(event.event_number);
			LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), ss.str());

                        toolbox::mem::Reference * ref = (toolbox::mem::Reference*) event.event_data.tcpla_dto_block_event_data.user_context;
                        if (ref != 0)
                        {
				ref->release();
                        }

		}
			break;

		default:
		{
			LOG4CPLUS_ERROR(this->getOwnerApplication()->getApplicationLogger(), "Unknown event");
		}
			break;
	}
}

void pt::tpi::PeerTransport::connect(pt::Address::Reference from,  pt::Address::Reference  to, pt::pipe::ServiceListener * listener, std::shared_ptr<void> & user_cookie)
{
 	std::lock_guard<std::mutex> guard(lock_);
        tcpla::Address  & local = dynamic_cast<tcpla::Address&>(*from);
        tcpla::Address  & destination = dynamic_cast<tcpla::Address&>(*to);

	tcpla::InterfaceAdapter * ia = 0;

	try
	{
		ia = this->getInterfaceAdapter(from);
	}
	catch (pt::tpi::exception::Exception & e)
	{
		XCEPT_RETHROW(pt::exception::UnknownProtocolOrService, "Failed to get interface adapter", e);
	}

	tcpla::EndPoint ep = ia->createEndPoint();

	using PipeConnectionRequestContext = std::tuple<pt::pipe::ServiceListener*, std::shared_ptr<void> >;
        PipeConnectionRequestContext * pcrc = new PipeConnectionRequestContext(listener,user_cookie);


        tcpla::TCPLA_CONTEXT cookie;
        cookie.as_ptr = pcrc; 

        LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "Going to post connect messenger local: " << local.toString() << " remote: " << destination.getProtocol() << "://" << destination.getHost() << ":" << destination.getConfigurePort());

        try
        {
        	ia->connect(ep, &local, &destination, cookie); // asynchronous call, if EP object failed to connect it returns in the corresponding event
        }
        catch (tcpla::exception::IACommandQueueFull & e)
        {
        	std::stringstream ss;
        	ss << "Failed to post to connect EndPoint in Messenger for local address " << local.toString() << " to destination address " << destination.toString();

		XCEPT_RETHROW(pt::tpi::exception::Exception, ss.str(), e);
        }
        catch (tcpla::exception::InvalidFamily & e)
        {
              	std::stringstream ss;
              	ss << "Failed to post to connect EndPoint in Messenger for local address " << local.toString() << " to destination address " << destination.toString();

        	XCEPT_RETHROW(pt::tpi::exception::Exception, ss.str(), e);
        }
        catch (tcpla::exception::HostnameResolveFailed & e)
        {
             	std::stringstream ss;
               ss << "Failed to post to connect EndPoint in Messenger for local address " << local.toString() << " to destination address " << destination.toString();

               XCEPT_RETHROW(pt::tpi::exception::Exception, ss.str(), e);
       }

}

pt::pipe::Input *  pt::tpi::PeerTransport::createInputPipe( std::shared_ptr<pt::pipe::ConnectionRequest> & cr , pt::pipe::InputListener * listener)
{
 	std::lock_guard<std::mutex> guard(lock_);
	toolbox::rlist<toolbox::mem::Reference*> * cq = 0;
	if ( ! freeCompletionQueues_.empty() )
	{
		cq = freeCompletionQueues_.front();
		freeCompletionQueues_.pop_front();
	}
	else
	{
		time_t _tm =time(NULL );
		struct tm * curtime = localtime ( &_tm );
		std::string t(asctime(curtime));
		cq = toolbox::rlist<toolbox::mem::Reference*>::create("pt-tpi-completion-queue-" + t , ioQueueSize_);
		allocatedCompletionQueues_.push_back(cq);
	}

	pt::tpi::Input * ipipe =  new pt::tpi::Input (cr, listener, cq);
	ipipes_.push_back(ipipe);
	return ipipe; 
}
pt::pipe::Output *  pt::tpi::PeerTransport::createOutputPipe( std::shared_ptr<pt::pipe::EstablishedConnection> & ec , pt::pipe::OutputListener * listener)
{
 	std::lock_guard<std::mutex> guard(lock_);
	toolbox::rlist<toolbox::mem::Reference*> * cq = 0;
	if ( ! freeCompletionQueues_.empty() )
        {
                cq = freeCompletionQueues_.front();
                freeCompletionQueues_.pop_front();
        }
        else
        {
                time_t _tm =time(NULL );
                struct tm * curtime = localtime ( &_tm );
		std::string t(asctime(curtime));
                cq = toolbox::rlist<toolbox::mem::Reference*>::create("pt-tpi-completion-queue-" + t , ioQueueSize_);
                allocatedCompletionQueues_.push_back(cq);
        }
	pt::tpi::Output * opipe =  new pt::tpi::Output (ec, listener,cq);
	opipes_.push_back(opipe);
	return opipe; 
}

void pt::tpi::PeerTransport::destroyInputPipe(pt::pipe::Input * ip)
{
 	std::lock_guard<std::mutex> guard(lock_);

	auto i = std::find(ipipes_.begin(), ipipes_.end(), ip);
	if ( i != ipipes_.end() )
	{
		freeCompletionQueues_.push_back((*i)->cq);
		delete *i;
		ipipes_.erase(i);
	}
	else
	{
		XCEPT_RAISE(pt::tpi::exception::Exception, "cannot destroy input pipe, not found");
	}
}

void pt::tpi::PeerTransport::destroyOutputPipe(pt::pipe::Output * op) 
{
 	std::lock_guard<std::mutex> guard(lock_);

	auto o = std::find(opipes_.begin(), opipes_.end(), op);
	if ( o != opipes_.end() )
        {
		freeCompletionQueues_.push_back((*o)->cq);
                delete *o;
                opipes_.erase(o);
        }
	else
	{
		XCEPT_RAISE(pt::tpi::exception::Exception, "cannot destroy output pipe, not found");
	}
}

