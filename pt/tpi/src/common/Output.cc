// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "pt/tpi/PeerTransport.h"
#include "pt/tpi/Output.h"
#include "pt/tpi/exception/Exception.h"
#include "xdaq/ApplicationDescriptor.h"
#include "tcpla/Event.h"

pt::tpi::Output::Output(std::shared_ptr<pt::pipe::EstablishedConnection> & pec, pt::pipe::OutputListener * listener, toolbox::rlist<toolbox::mem::Reference*> * cqueue):  pec_(pec), listener_(listener)
			{
				using PipeConnectionHandle = std::tuple<std::string, const xdaq::ApplicationDescriptor*, const xdaq::ApplicationDescriptor*, tcpla::EndPoint>;
                                PipeConnectionHandle * ch = (PipeConnectionHandle*)pec_->getConnectionHandle().get();
                                ep_ = std::get<3>(*ch);
                                ia_ = ep_.ia_;

				cq = cqueue;
			}

			pt::tpi::Output::~Output()
			{
			}

			void pt::tpi::Output::postFrame(toolbox::mem::Reference * ref ) 
			{
				if (ref != 0)
				{
			
					// // include embedded size of frame header size + user payload
					size_t size = ref->getDataSize() + sizeof(uint64_t);
					//ref->setDataOffset(0); // move to header  
					char * frame = (char *) ref->getBuffer()->getAddress();
					*((uint64_t*)frame) = size;
			
					if (ref->getNextReference() != 0)
					{
						XCEPT_RAISE(pt::tpi::exception::Exception, "reference chain is not supported for pipe");
					}

					tcpla::TCPLA_CONTEXT cookie;
					cookie.as_ptr = cq; // remember completion queue 
					//std::cout << "OutputPipe Going to send size " <<  size << " offset " << ref->getDataOffset() << " ref ptr " << std::hex << (size_t)ref << std::dec << " original buffer size " << ref->getBuffer()->getSize()<< std::endl;
					//toolbox::hexdump ((void*) frame, 24);	
					ia_->postSendBuffer(ep_, cookie, (char*) frame, size, 0, ref, 0, tcpla::TCPLA_CONNECTION_KEEPALIVE);
			
				}
				
			}

			bool pt::tpi::Output::empty()
			{
				return cq->empty();
			}

			toolbox::mem::Reference * pt::tpi::Output::completed()
			{
				toolbox::mem::Reference * ref = cq->front();
				cq->pop_front();
				return ref;
			}


			void pt::tpi::Output::fire(pt::pipe::UndeliverableBlock & event)
			{
				std::cout << "fireUndeliverableBlock inside pt::tpi::Output (poiter is " << std::hex << (size_t)listener_ << std::dec << ")" << std::endl;
				if (listener_ != 0 )
					listener_->actionPerformed(event);
			}

			pt::pipe::OutputListener *  pt::tpi::Output::getOutputListener()
			{
				return listener_;
			}

