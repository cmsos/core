// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius					 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "xcept/tools.h"

#include "toolbox/string.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/task/WorkLoopFactory.h"

#include "pt/tpi/Application.h"
#include "pt/PeerTransportAgent.h"
#include "pt/tpi/PeerTransport.h"
#include "pt/tpi/Messenger.h"
#include "pt/tpi/Allocator.h"

#include "xdaq/NamespaceURI.h"

#include "tcpla/InterfaceAdapter.h"
#include "tcpla/PublicServicePoint.h"
#include "tcpla/WaitingWorkLoop.h"
#include "tcpla/Address.h"

#include "xgi/framework/Method.h"

XDAQ_INSTANTIATOR_IMPL (pt::tpi::Application)

pt::tpi::Application::Application (xdaq::ApplicationStub* stub) 
	: xdaq::Application(stub), xgi::framework::UIManager(this)
{
	stub->getDescriptor()->setAttribute("icon", "/pt/tpi/images/tpi.png");
	stub->getDescriptor()->setAttribute("icon16x16", "/pt/tpi/images/tpi.png");

	// Bind CGI callbacks
	xgi::framework::deferredbind(this, this, &pt::tpi::Application::Default, "Default");

	this->maxClients_ = 1024;
	this->ioQueueSize_ = 2048;
	this->eventQueueSize_ = 10000;
	this->protocol_ = TPI_PROTOCOL;
	//this->committedPoolSize_ = 0x100000 * 100;

	getApplicationInfoSpace()->fireItemAvailable("committedPoolSize", &committedPoolSize_);
	getApplicationInfoSpace()->fireItemAvailable("maxClients", &maxClients_);
	getApplicationInfoSpace()->fireItemAvailable("ioQueueSize", &ioQueueSize_);
	getApplicationInfoSpace()->fireItemAvailable("eventQueueSize", &eventQueueSize_);

	getApplicationInfoSpace()->fireItemAvailable("protocol", &protocol_);

	// Bind setting of default parameters
	getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");

}

//
// run control requests current paramater values
//
void pt::tpi::Application::actionPerformed (xdata::Event& e)
{

	if (e.type() == "urn:xdaq-event:setDefaultValues")
	{


		toolbox::mem::Pool * pool = 0;
		try
		{
			toolbox::net::URN urn("toolbox-mem-pool", "tpi");

			pt::tpi::Allocator* a = new pt::tpi::Allocator( "pt-tpi-allocator", committedPoolSize_);
			pool = toolbox::mem::getMemoryPoolFactory()->createPool(urn, a);

			pool->setHighThreshold((unsigned long) (committedPoolSize_ * 0.9));
			pool->setLowThreshold((unsigned long) (committedPoolSize_ * 0.8));
		}
		catch (toolbox::mem::exception::Exception & e)
		{
			XCEPT_RETHROW(xdaq::exception::Exception, "Cannot create default memory pool", e);
		}

		pt_ = 0;

		try
		{
			pt_ = new pt::tpi::PeerTransport(this, pool, (xdata::UnsignedIntegerT) maxClients_, (xdata::UnsignedIntegerT) ioQueueSize_, (xdata::UnsignedIntegerT) eventQueueSize_, protocol_);

		}
		catch (pt::exception::Exception & e)
		{
			this->notifyQualified("fatal", e);
			return;
		}

		pt::PeerTransportAgent* pta = pt::getPeerTransportAgent();
		pta->addPeerTransport(pt_);

		//auto connect timer

		toolbox::task::Timer * timer = 0;

		if (!toolbox::task::getTimerFactory()->hasTimer("urn:pt::tpi-timer"))
		{
			timer = toolbox::task::getTimerFactory()->createTimer("urn:pt::tpi-timer");
		}
		else
		{
			timer = toolbox::task::getTimerFactory()->getTimer("urn:pt::tpi-timer");
		}

		toolbox::TimeInterval interval;
		interval.fromString("PT5S");

		toolbox::TimeVal start;

		timer->scheduleAtFixedRate(start, this, interval, 0, "urn:tpi-watchdog:gc");
	}
}

void pt::tpi::Application::Default (xgi::Input * in, xgi::Output * out) 
{
	// Begin of tabs
	*out << "<div class=\"xdaq-tab-wrapper\" id=\"tabPane1\">" << std::endl;

	// Tab 1
	*out << "<div class=\"xdaq-tab\" title=\"Output\">" << std::endl;
	this->SenderTabPage(out);
	*out << "</div>";

	// Tab 2
	*out << "<div class=\"xdaq-tab\" title=\"Input\">" << std::endl;
	this->ReceiverTabPage(out);
	*out << "</div>";

	// Tab 3
	*out << "<div class=\"xdaq-tab\" title=\"Settings\">" << std::endl;
	this->SettingsTabPage(out);
	*out << "</div>";

	// Tab 4
	*out << "<div class=\"xdaq-tab\" title=\"Events\">" << std::endl;
	this->EventsTabPage(out);
	*out << "</div>";

	// Tab 5
	*out << "<div class=\"xdaq-tab\" title=\"Workloops\">" << std::endl;
	this->WorkloopsTabPage(out);
	*out << "</div>";

	*out << "</div>"; // end of tab pane
}

void pt::tpi::Application::SenderTabPage (xgi::Output * out)
{
	for (std::map<std::string, tcpla::InterfaceAdapter *>::iterator i = pt_->iav_.begin(); i != pt_->iav_.end(); i++)
	{
		*out << (*(*i).second) << std::endl;
	}
}

void pt::tpi::Application::ReceiverTabPage (xgi::Output * out)
{
	for (size_t i = 0; i < pt_->psp_.size(); i++)
	{
		*out << (*pt_->psp_[i]);
	}
}

void pt::tpi::Application::SettingsTabPage (xgi::Output * out)
{
	*out << cgicc::table().set("class", "xdaq-table-vertical") << std::endl;
	*out << cgicc::tbody() << std::endl;

	*out << cgicc::tr();
	*out << cgicc::th("Memory Pool");
	*out << cgicc::td(committedPoolSize_.toString());
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tr();
	*out << cgicc::th("Maximum Clients Per Address");
	*out << cgicc::td();
	*out << maxClients_.toString();
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tr();
	*out << cgicc::th("I/O Queue");
	*out << cgicc::td();
	*out << ioQueueSize_.toString();
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tr();
	*out << cgicc::th("Event Queue");
	*out << cgicc::td();
	*out << eventQueueSize_.toString();
	*out << cgicc::td();
	*out << cgicc::tr() << std::endl;

	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table();
}

void pt::tpi::Application::EventsTabPage (xgi::Output * out)
{
	*out << (*pt_) << std::endl;
}

void pt::tpi::Application::WorkloopsTabPage (xgi::Output * out)
{
	std::list<toolbox::task::WorkLoop *> workloops = toolbox::task::getWorkLoopFactory()->getWorkLoops();

	*out << cgicc::table().set("class", "xdaq-table") << std::endl;
	*out << "<caption>pt::tpi workloops</caption>";

	*out << cgicc::thead() << std::endl;
	*out << cgicc::tr();
	*out << cgicc::th("Name");
	*out << cgicc::th("Type");
	*out << cgicc::tr() << std::endl;
	*out << cgicc::thead() << std::endl;

	*out << cgicc::tbody() << std::endl;
	for (std::list<toolbox::task::WorkLoop *>::iterator i = workloops.begin(); i != workloops.end(); i++)
	{
		*out << cgicc::tr();
		*out << cgicc::td((*i)->getName());
		*out << cgicc::td((*i)->getType());
		*out << cgicc::tr();
	}

	*out << cgicc::tbody() << std::endl;
	*out << cgicc::table();
}

void pt::tpi::Application::timeExpired (toolbox::task::TimerEvent& e)
{
	if (e.getTimerTask()->name == "urn:tpi-watchdog:autoconnect")
	{
	}
	else if (e.getTimerTask()->name == "urn:tpi-watchdog:gc")
	{
	}
}

