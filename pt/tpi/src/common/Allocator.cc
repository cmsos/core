// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius   				 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "pt/tpi/Allocator.h"

#include "pt/tpi/HeaderInfo.h"
#include "pt/tpi/exception/Exception.h"

#include "toolbox/PolicyFactory.h"
#include "toolbox/AllocPolicy.h"

#include <sstream>
#include <string>
#include <limits>

#include "toolbox/string.h"

pt::tpi::Allocator::Allocator (const std::string & name, size_t committedSize): toolbox::mem::CommittedHeapAllocator (name, committedSize)
{
}

pt::tpi::Allocator::~Allocator ()
{
}

size_t pt::tpi::Allocator::getOffset ()
{
        return pt::tpi::HeaderInfo::getHeaderSize();
}
std::string pt::tpi::Allocator::type ()
{
	return "pipe";
}

