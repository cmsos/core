// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _pt_tcp_version_h_
#define _pt_tcp_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define CORE_PTTCP_VERSION_MAJOR 2
#define CORE_PTTCP_VERSION_MINOR 14
#define CORE_PTTCP_VERSION_PATCH 1
// If any previous versions available E.g. #define CORE_PTTCP_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define CORE_PTTCP_PREVIOUS_VERSIONS "2.14.0"


//
// Template macros
//
#define CORE_PTTCP_VERSION_CODE PACKAGE_VERSION_CODE(CORE_PTTCP_VERSION_MAJOR,CORE_PTTCP_VERSION_MINOR,CORE_PTTCP_VERSION_PATCH)
#ifndef CORE_PTTCP_PREVIOUS_VERSIONS
#define CORE_PTTCP_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(CORE_PTTCP_VERSION_MAJOR,CORE_PTTCP_VERSION_MINOR,CORE_PTTCP_VERSION_PATCH)
#else 
#define CORE_PTTCP_FULL_VERSION_LIST  CORE_PTTCP_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(CORE_PTTCP_VERSION_MAJOR,CORE_PTTCP_VERSION_MINOR,CORE_PTTCP_VERSION_PATCH)
#endif 


namespace pttcp 
{
	const std::string project = "core";
	const std::string package  =  "pttcp";
   	const std::string versions = CORE_PTTCP_FULL_VERSION_LIST;
	const std::string summary = "TCP peer transport with I2O service implementation";
	const std::string description = "";
	const std::string authors = "Johannes Gutleber, Luciano Orsini";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/xdaq";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
