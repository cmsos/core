// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _pt_tcp_PeerTransportTCP_h_
#define _pt_tcp_PeerTransportTCP_h_

#include "xdaq/Application.h"
#include "xgi/framework/UIManager.h"
#include "pt/tcp/PeerTransportSender.h"
#include "pt/tcp/PeerTransportReceiver.h"
#include "xdata/Boolean.h"
#include "xdata/String.h"
#include "xdata/Double.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/ActionListener.h"

#include "toolbox/fsm/FiniteStateMachine.h"
#include "xoap/MessageReference.h"
#include "xoap/MessageFactory.h"

#include "xgi/WSM.h"
#include "xgi/Utils.h"
#include "xgi/Method.h"
#include "xgi/framework/Method.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"


namespace pt
{

namespace tcp
{

//! this is the XDAQ Peer Transport Appliction Wrapper
//
class PeerTransportTCP: public xdaq::Application, public xgi::framework::UIManager, xdata::ActionListener
{
	public:
	
	XDAQ_INSTANTIATOR();
	
	PeerTransportTCP(xdaq::ApplicationStub * s) ;
	
	~PeerTransportTCP();
	
	//! Default page, displays configuration page
	//
	void Default(xgi::Input * in, xgi::Output * out ) ;
	
	//! apply new configuration 
	//
	void apply(xgi::Input * in, xgi::Output * out ) ;
	
	
	void failurePage(xgi::Output * out, xcept::Exception & e)  ;
	
	private:
	void actionPerformed (xdata::Event& e) ;
	void reset() ;

	void StatisticsTabPage (xgi::Output * out) ;
	
	pt::tcp::PeerTransportSender* pts_;
	pt::tcp::PeerTransportReceiver* ptr_;

	xdata::Boolean autosize_;
	xdata::UnsignedInteger32 maxPacketSize_;
	xdata::UnsignedInteger32 maxClients_;
	xdata::String poolName_;
	
	xdata::Double committedPoolSize_;
	xdata::Double lowThreshold_;
	xdata::Double highThreshold_;
	xdata::UnsignedInteger32 outputQueueSize_;
	
};

}
}
#endif
