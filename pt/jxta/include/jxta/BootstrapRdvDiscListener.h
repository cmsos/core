
/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, G. Lo Presti and L. Orsini                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _jxta_BootstrapRdvDiscListener_h
#define _jxta_BootstrapRdvDiscListener_h

#include "jxta/DiscoveryService.h"
#include "jxta/RdvService.h" 
#include "jxta/DiscoveryListener.h"

namespace jxta {

//! A builtin discovery listener used by the NetPeerGroup during bootstrap:
//! it provides rdv connection to the first discovered rdv peer.
class BootstrapRdvDiscListener: public DiscoveryListener
{
	private:
	DiscoveryService* ds_;
	RdvService* rs_;

	public:
	BootstrapRdvDiscListener(DiscoveryService* ds, RdvService* rs): ds_(ds), rs_(rs)
	{
	}
	
	void discoveryEvent(jxta::AdvertisementList::Reference advList)
	{
		if(rs_->isConnectedToRdv()) {
			ds_->removeServiceListener(this);
			return;
		}
		
		// get a rdv peer and explicitly connect to it
		for (int i = 0; i < advList->getLength(); i++) {
			jxta::Advertisement::Reference adv = advList->getItem(i);
			//printf("BSRdvDiscListener: try to use adv %s\n", adv->getName().c_str());
			
			std::string pname = adv->getName();
			if(pname.find("rdv") == 0) {
				pname = pname.substr(pname.find('@')+1);
				rs_->addRdvPeer(pname);
				ds_->removeServiceListener(this);   // this is allowed by the discovery listeners schema
				break;
			}
		}
	}
};

}

#endif

