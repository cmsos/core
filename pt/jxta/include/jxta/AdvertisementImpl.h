
/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2004, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, G. Lo Presti and L. Orsini                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _jxta_AdvertisementImpl_h
#define _jxta_AdvertisementImpl_h

#include <string>
#include "jxta/Advertisement.h"
#include "jxta/exception/Exception.h"
#include "jxta.h"

namespace jxta
{

class AdvertisementImpl: public Advertisement
{
	public:
	AdvertisementImpl(Jxta_advertisement* adv);
	AdvertisementImpl(std::string type, std::string xmlDoc);
	~AdvertisementImpl();

	std::string getType();
	std::string getID();
	std::string getName();
	
	std::string getXmlDocument();

	// implementation related function
	Jxta_advertisement* getJxtaAdv();
	
	private:
	Jxta_advertisement* adv_; 
	std::string type_;
};

}

#endif


