// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2017, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/exdr/UnsignedInteger16Serializer.h"
#include "xdata/exdr/Serializer.h"

xdata::exdr::UnsignedInteger16Serializer::~UnsignedInteger16Serializer()
{
}

std::string xdata::exdr::UnsignedInteger16Serializer::type() const
{
	return "unsigned int 16";
}


void xdata::exdr::UnsignedInteger16Serializer::exportAll(xdata::exdr::Serializer * serializer,  xdata::Serializable * serializable, xdata::exdr::OutputStreamBuffer * sbuf)
{
	// cast serializable to Integer
	xdata::UnsignedInteger16 *  i = dynamic_cast<xdata::UnsignedInteger16*>(serializable);
	if ( i == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::UnsignedInteger16 object during export " );
        }

	serializer->encodeTag( xdata::exdr::Serializer::UnsignedInteger16, i->limits_, sbuf);
	sbuf->encodeUInt16(i->value_);
}

void xdata::exdr::UnsignedInteger16Serializer::import (xdata::exdr::Serializer * serializer,  xdata::Serializable * serializable, xdata::exdr::InputStreamBuffer * sbuf)
{
	xdata::UnsignedInteger16 * i = dynamic_cast<xdata::UnsignedInteger16*>(serializable);
	if ( i == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::UnsignedInteger16 object during import" );
        }

	serializer->decodeTag(xdata::exdr::Serializer::UnsignedInteger16, i->limits_, sbuf);
	sbuf->decodeUInt16(i->value_);
}
