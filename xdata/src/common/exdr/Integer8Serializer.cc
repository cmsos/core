// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2017, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/exdr/Integer8Serializer.h"
#include "xdata/exdr/Serializer.h"

xdata::exdr::Integer8Serializer::~Integer8Serializer()
{
}

std::string xdata::exdr::Integer8Serializer::type() const
{
	return "int 8";
}


void xdata::exdr::Integer8Serializer::exportAll
(
	xdata::exdr::Serializer * serializer,
	xdata::Serializable * serializable,
	xdata::exdr::OutputStreamBuffer * sbuf
) 
{
	// cast serializable to Integer
	xdata::Integer8 *  i = dynamic_cast<xdata::Integer8*>(serializable);
	if ( i == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::Integer8 object during export " );
        }

	serializer->encodeTag( xdata::exdr::Serializer::Integer8, i->limits_, sbuf);
	sbuf->encodeInt8(i->value_);
}

void xdata::exdr::Integer8Serializer::import
(
	xdata::exdr::Serializer * serializer,
	xdata::Serializable * serializable,
	xdata::exdr::InputStreamBuffer * sbuf
) 
{
	xdata::Integer8 * i = dynamic_cast<xdata::Integer8*>(serializable);
	if ( i == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::Integer8 object during import" );
        }

	serializer->decodeTag(xdata::exdr::Serializer::Integer8, i->limits_, sbuf);
	sbuf->decodeInt8(i->value_);
}
