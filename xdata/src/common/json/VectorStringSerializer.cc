// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/json/VectorStringSerializer.h"
#include "xdata/AbstractVector.h"
#include "xdata/json/Serializer.h"
#include "xdata/exception/Exception.h"
#include "xdata/String.h"

#include "xdata/Vector.h"

xdata::json::VectorStringSerializer::~VectorStringSerializer()
{
}

std::string xdata::json::VectorStringSerializer::type() const
{
	return "vector string";
}

void xdata::json::VectorStringSerializer::exportAll
(
	xdata::json::Serializer * serializer,  
	xdata::Serializable * serializable, 
	nlohmann::json & json
) 
	
{
	xdata::Vector<xdata::String> & serial = dynamic_cast<xdata::Vector<xdata::String>&>(*serializable);
	//nlohmann::json tvec(serial);
	//json = tvec;
	json = serial;
//	std::cout << "FAST " << json << std::endl;

/*
	
	xdata::AbstractVector * v = dynamic_cast<xdata::AbstractVector*>(serializable);
	json = nlohmann::json::array({}); 
	
	size_t size = v->elements();
	for ( size_t i = 0; i < size; i++)
        {
                xdata::String *  e = dynamic_cast<xdata::String*>(v->elementAt(i));
                if ( e == 0 )
                {
                        XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::String object during export " );
                }
		json.insert(json.end(), e->value_);
        }
*/

}


void xdata::json::VectorStringSerializer::import 
(
	xdata::json::Serializer * serializer,  
	xdata::Serializable * serializable, 
	nlohmann::json & json
) 
	
{
/* use a copy
	auto serial = json.get<std::vector<std::string>>();
        xdata::Vector<xdata::String> & v = dynamic_cast<xdata::Vector<xdata::String>&>(*serializable);
	std::copy(serial.begin(), serial.end(), std::back_inserter(v));
*/

	xdata::AbstractVector * v = dynamic_cast<xdata::AbstractVector*>(serializable);
	uint32_t size = json.size();
        v->setSize(size);

	size_t index = 0;
        for (auto& element : json)
        {
                xdata::String * e = dynamic_cast<xdata::String*>(v->elementAt(index));
                if ( e == 0 )
                {
                        XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::String object during import" );
                }

                e->value_ = element;
		index++;
        }
	
}
