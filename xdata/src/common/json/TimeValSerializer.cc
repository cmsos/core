// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/json/TimeValSerializer.h"
#include "xdata/json/Serializer.h"
#include "xdata/TimeVal.h"

xdata::json::TimeValSerializer::~TimeValSerializer()
{
}

std::string xdata::json::TimeValSerializer::type() const
{
	return "time";
}

void xdata::json::TimeValSerializer::exportAll ( xdata::json::Serializer * serializer,  xdata::Serializable * serializable, nlohmann::json & json) 
{
	// cast serializable to xdata::TimeVal pointer
	xdata::TimeVal * t  = dynamic_cast<xdata::TimeVal*>(serializable);
	if ( t == 0 )
	{
		XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::TimeVal object during export" );
	}
	json = t->value_.toString("", toolbox::TimeVal::gmt);
}

void xdata::json::TimeValSerializer::import ( xdata::json::Serializer * serializer,  xdata::Serializable * serializable, nlohmann::json & json) 
{
	xdata::TimeVal * t = dynamic_cast<xdata::TimeVal*>(serializable);
	if ( t == 0 )
	{
		XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::TimeVal object during import" )	;
	}
	t->fromString(json);
	//serializer->decodeTag(xdata::exdr::Serializer::TimeVal, sbuf);
	
	// the time types have different sizes on different machines
	// therefore the only safe way to decode the time is to
	// read it from a string in GMT time zone format
	//
	//std::string timeString;
	//sbuf->decode(timeString);
	//t->fromString(timeString);
}
