// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/json/UnsignedLongSerializer.h"
#include "xdata/json/Serializer.h"

xdata::json::UnsignedLongSerializer::~UnsignedLongSerializer()
{
}

std::string xdata::json::UnsignedLongSerializer::type() const
{
	return "unsigned long";
}


void xdata::json::UnsignedLongSerializer::exportAll(xdata::json::Serializer * serializer,  xdata::Serializable * serializable, nlohmann::json & json) 
{
	// cast serializable to Integer
	xdata::UnsignedLong *  i = dynamic_cast<xdata::UnsignedLong*>(serializable);
	if ( i == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::UnsignedLong object during export" );
        }

	json = i->value_; 
}

void xdata::json::UnsignedLongSerializer::import (xdata::json::Serializer * serializer,  xdata::Serializable * serializable, nlohmann::json & json) 
{
	xdata::UnsignedLong * i = dynamic_cast<xdata::UnsignedLong*>(serializable);
	if ( i == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::UnsignedLong object during import" );
        }

	auto l = json.get<xdata::UnsignedLongT>();
	*i = l;
}
