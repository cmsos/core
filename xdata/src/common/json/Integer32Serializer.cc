// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/json/Integer32Serializer.h"
#include "xdata/json/Serializer.h"

xdata::json::Integer32Serializer::~Integer32Serializer()
{
}

std::string xdata::json::Integer32Serializer::type() const
{
	return "int 32";
}


void xdata::json::Integer32Serializer::exportAll(xdata::json::Serializer * serializer,  xdata::Serializable * serializable, nlohmann::json & json)
{
        xdata::Integer32 *  i = dynamic_cast<xdata::Integer32*>(serializable);
        if ( i == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::Integer32 object during export " );
        }

        json = i->value_;
}

void xdata::json::Integer32Serializer::import (xdata::json::Serializer * serializer,  xdata::Serializable * serializable, nlohmann::json & json)
{
        xdata::Integer32 * i = dynamic_cast<xdata::Integer32*>(serializable);
        if ( i == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::Integer32 object during import" );
        }

        auto l = json.get<xdata::Integer32T>();
        *i = l;
}

