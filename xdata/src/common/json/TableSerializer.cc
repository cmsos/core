// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, D. Simelevicius				 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <sstream>
#include "xdata/json/TableSerializer.h"
#include "xdata/json/Serializer.h"
#include "xdata/exception/Exception.h"
#include "xdata/TableIterator.h"
#include "xdata/Table.h"

/*
 {
	"table": {
		"properties": {
			"Name": "urn:xdaq-flashlist:hostInfo",
			"LastUpdate": "Mon, May 18 2020 15:48:29 GMT",
			"Version": "$Revision: 15125 $",
			"Tag": "unit",
			"LastOriginator": "http://d3vrubu-c2e34-14-01.cms:9999/urn:xdaq-application:lid=5",
			"Rows": 23
		},
		"definition": [{
			"key": "context",
			"type": "string"
		}, {
			"key": "cpuUsage",
			"type": "double"
		}, {
			"key": "sessionid",
			"type": "string"
		}, {
			"key": "timestamp",
			"type": "time"
		}],
		"rows": [{
			"context": "http://d3vrubu-c2e34-18-01.cms:9999",
			"cpuUsage": 7.64313290298375e-01,
			"sessionid": "",
			"timestamp": "2020-05-18T15:48:23.649355Z"
		}, {
			"context": "http://d3vrubu-c2e34-33-01.cms:9999",
			"cpuUsage": 1.97586963334378e+00,
			"sessionid": "",
			"timestamp": "2020-05-18T15:48:24.733652Z"
		}]
	}
}
 */

xdata::json::TableSerializer::~TableSerializer()
{
}

std::string xdata::json::TableSerializer::type() const
{
	return "table";
}

void xdata::json::TableSerializer::exportAll ( xdata::json::Serializer * serializer,  xdata::Serializable * serializable, nlohmann::json & json) 
{
	//json.insert(json.begin(),jsont);
        json["table"]["properties"] = nlohmann::json::object(); 

	xdata::AbstractTable * t = dynamic_cast<xdata::AbstractTable*>(serializable);
        xdata::Table * table = dynamic_cast<xdata::Table*>(serializable);
                
        std::vector<std::string> columns = t->getColumns();
	std::sort(columns.begin(),columns.end());

	std::vector<std::string>::size_type c = 0;
	nlohmann::json jsondefs = nlohmann::json::array(); 
	while(c < columns.size())
	{
		std::string localName = columns[c].substr(columns[c].rfind(":")+1);
		std::string localType =  t->getColumnType(columns[c]);
		nlohmann::json jsond = nlohmann::json::object();
		jsond["key"] = localName;
		jsond["type"] = localType; 
		jsondefs.insert(jsondefs.end(),jsond);
		c++;
	}

        json["table"]["definition"] = jsondefs; 
        
	nlohmann::json jsonrows = nlohmann::json::array(); 
	size_t rows = t->getRowCount();
	size_t j = 0;
	while ( j <  rows )
	{
		nlohmann::json jsonr =  nlohmann::json::object();
		for ( std::vector<std::string>::iterator i = columns.begin(); i != columns.end(); ++i )
		{
			std::string colType = t->getColumnType(*i);
			//std::cout << "serializing:" << columns[i] << " of type: " << colType << std::endl;
			//	 if ( colType == "unsigned long" )
			xdata::Serializable* cv = dynamic_cast<xdata::Serializable*>(table->columnData_[(*i)]);
			
			xdata::AbstractVector * v = dynamic_cast<xdata::AbstractVector*>(cv);
			serializer->exportAll(v->elementAt(j),jsonr[*i]);
		}
		jsonrows.insert(jsonrows.end(),jsonr);
		j++;
	}
        json["table"]["rows"] = jsonrows;
}


void xdata::json::TableSerializer::import ( xdata::json::Serializer * serializer,  xdata::Serializable * serializable, nlohmann::json & json) 
{

	xdata::AbstractTable * t = dynamic_cast<xdata::AbstractTable*>(serializable);
        xdata::Table * table = dynamic_cast<xdata::Table*>(serializable);
	table->clear();

	size_t rows = json["table"]["rows"].size();
	for (auto& definition : json["table"]["definition"]) 
	{
  		//std::cout << definition << std::endl;
		std::string key = definition["key"];
		std::string type = definition["type"];
		t->addColumn(key, type);
	       	xdata::AbstractVector * v = dynamic_cast<xdata::AbstractVector*>(table->columnData_[key]);
		v->setSize(rows);
		size_t i = 0;
		for (auto& r : json["table"]["rows"]) 
		{
           		std::cout << r << std::endl;
			//xdata::Table::Row& row = *(table->append());
			std::cout << "selected " << key << " of value " <<  r[key] << "of type " << type << std::endl;
        		serializer->import(v->elementAt(i), r[key]);
        		i++;
       		}
	}
	table->numberOfRows_ = rows;
	table->memorySize_ = rows;
}
