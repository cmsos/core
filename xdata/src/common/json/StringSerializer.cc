// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/json/StringSerializer.h"
#include "xdata/json/Serializer.h"

xdata::json::StringSerializer::~StringSerializer()
{
}

std::string xdata::json::StringSerializer::type()  const
{
	return "string";
}


void xdata::json::StringSerializer::exportAll(xdata::json::Serializer * serializer,  xdata::Serializable * serializable, nlohmann::json & json) 
{
	// cast serializable to String
        xdata::String * s = dynamic_cast<xdata::String*>(serializable);
        if ( s == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::String object during export " );
        }
	json = s->value_;

/*
	xdata::String * s = dynamic_cast<xdata::String*>(serializable);
 	if ( s == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::String object during export " );
        }

	serializer->encodeTag( xdata::json::Serializer::String, sbuf);
	sbuf->encode(s->value_);
*/
}

void xdata::json::StringSerializer::import (xdata::json::Serializer * serializer,  xdata::Serializable * serializable, nlohmann::json & json) 
{
	xdata::String * s = dynamic_cast<xdata::String*>(serializable);
        if ( s == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::String object during import" );
        }
	s->value_ = json;
/*
	xdata::String * s = dynamic_cast<xdata::String*>(serializable);
	if ( s == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::String object during import" );
        }

	serializer->decodeTag(xdata::json::Serializer::String, sbuf);
	sbuf->decode(s->value_); // this is a force cast to non const string
*/
}
