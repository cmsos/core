// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/json/Integer16Serializer.h"
#include "xdata/json/Serializer.h"

xdata::json::Integer16Serializer::~Integer16Serializer()
{
}

std::string xdata::json::Integer16Serializer::type() const
{
	return "int 16";
}

void xdata::json::Integer16Serializer::exportAll(xdata::json::Serializer * serializer,  xdata::Serializable * serializable, nlohmann::json & json)
{
        xdata::Integer16 *  i = dynamic_cast<xdata::Integer16*>(serializable);
        if ( i == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::Integer16 object during export " );
        }

        json = i->value_;
}

void xdata::json::Integer16Serializer::import (xdata::json::Serializer * serializer,  xdata::Serializable * serializable, nlohmann::json & json)
{
        xdata::Integer16 * i = dynamic_cast<xdata::Integer16*>(serializable);
        if ( i == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::Integer16 object during import" );
        }

        auto l = json.get<xdata::Integer16T>();
        *i = l;
}

