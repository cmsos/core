// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/json/FloatSerializer.h"
#include "xdata/json/Serializer.h"

xdata::json::FloatSerializer::~FloatSerializer()
{
}

std::string xdata::json::FloatSerializer::type() const
{
	return "float";
}


void xdata::json::FloatSerializer::exportAll(xdata::json::Serializer * serializer,  xdata::Serializable * serializable, nlohmann::json & json) 
{
	// cast serializable to Float
	xdata::Float * f = dynamic_cast<xdata::Float*>(serializable);
	if ( f == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::Float object during export " );
        }

	uint8_t buf[sizeof(xdata::FloatT)];
        memset(buf,0,sizeof(xdata::FloatT));
        xdata::FloatT value = f->value_;
        std::copy(reinterpret_cast<uint8_t*>(&value), buf + sizeof(xdata::FloatT), buf);
        std::vector<uint8_t> v(buf, buf + sizeof(xdata::FloatT));
        nlohmann::json vec(v);
        json = vec;


}

void xdata::json::FloatSerializer::import (xdata::json::Serializer * serializer,  xdata::Serializable * serializable, nlohmann::json & json) 
{
	xdata::Float * f = dynamic_cast<xdata::Float*>(serializable);
	if ( f == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::Float object during import " );
        }

        xdata::FloatT value = 0.0;
        std::vector<uint8_t> binary  = json.get<std::vector<uint8_t>>();
        std::copy(&binary[0], &binary[0] + sizeof(xdata::FloatT), reinterpret_cast<uint8_t*>(&value));
        //std::cout << "binary is " << value << std::endl;
        *f = value;
}
