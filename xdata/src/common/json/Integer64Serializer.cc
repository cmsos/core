// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/json/Integer64Serializer.h"
#include "xdata/json/Serializer.h"

xdata::json::Integer64Serializer::~Integer64Serializer()
{
}

std::string xdata::json::Integer64Serializer::type() const
{
	return "int 64";
}


void xdata::json::Integer64Serializer::exportAll(xdata::json::Serializer * serializer,  xdata::Serializable * serializable, nlohmann::json & json)
{
        xdata::Integer64 *  i = dynamic_cast<xdata::Integer64*>(serializable);
        if ( i == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::Integer64  object during export" );
        }

        json = nlohmann::json::array({ (i->value_ >> 32) & 0xffffffff, i->value_ & 0xffffffff });

}

void xdata::json::Integer64Serializer::import (xdata::json::Serializer * serializer,  xdata::Serializable * serializable, nlohmann::json & json)
{
        xdata::Integer64 * i = dynamic_cast<xdata::Integer64*>(serializable);
        if ( i == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::Integer64  object during import" );
        }

        auto l = json[0].get<xdata::Integer64T>();
        auto r = json[1].get<xdata::Integer64T>();
        *i = (xdata::Integer64T)((l << 32) | r);
}       
