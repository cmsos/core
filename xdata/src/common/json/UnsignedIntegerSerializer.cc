// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/json/UnsignedIntegerSerializer.h"
#include "xdata/json/Serializer.h"

xdata::json::UnsignedIntegerSerializer::~UnsignedIntegerSerializer()
{
}

std::string xdata::json::UnsignedIntegerSerializer::type() const
{
	return "unsigned int";
}


void xdata::json::UnsignedIntegerSerializer::exportAll(xdata::json::Serializer * serializer,  xdata::Serializable * serializable, nlohmann::json & json) 
{
	xdata::UnsignedInteger *  i = dynamic_cast<xdata::UnsignedInteger*>(serializable);
	if ( i == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::UnsignedInteger object during export " );
        }

	json = i->value_;
}

void xdata::json::UnsignedIntegerSerializer::import (xdata::json::Serializer * serializer,  xdata::Serializable * serializable, nlohmann::json & json) 
{
	xdata::UnsignedInteger * i = dynamic_cast<xdata::UnsignedInteger*>(serializable);
	if ( i == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::UnsignedInteger object during import" );
        }

	auto l = json.get<xdata::UnsignedIntegerT>();
	*i = l;
}
