// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/json/Serializer.h"  
#include "xdata/json/TableSerializer.h"  
#include "xdata/json/DoubleSerializer.h"  
#include "xdata/json/FloatSerializer.h"  
#include "xdata/json/TimeValSerializer.h"  
#include "xdata/json/UnsignedInteger64Serializer.h"  
#include "xdata/json/UnsignedInteger32Serializer.h"  
#include "xdata/json/UnsignedLongSerializer.h"  
#include "xdata/json/UnsignedIntegerSerializer.h"  
#include "xdata/json/UnsignedShortSerializer.h"  
#include "xdata/json/StringSerializer.h"  
#include "xdata/json/BooleanSerializer.h"  
#include "xdata/json/VectorStringSerializer.h"  
#include "xdata/json/VectorDoubleSerializer.h"  
#include "xdata/json/VectorUnsignedInteger32Serializer.h"  
#include "xdata/json/VectorUnsignedInteger64Serializer.h"  
#include "xdata/json/VectorUnsignedShortSerializer.h"  
#include "xdata/json/VectorBooleanSerializer.h"  
#include "xdata/json/VectorFloatSerializer.h"  
#include "xdata/json/VectorTimeValSerializer.h"  
#include "xdata/json/BagSerializer.h"
#include "xdata/json/IntegerSerializer.h"
#include "xdata/json/Integer8Serializer.h"
#include "xdata/json/Integer16Serializer.h"
#include "xdata/json/Integer32Serializer.h"
#include "xdata/json/Integer64Serializer.h"
#include "xdata/json/PropertiesSerializer.h"
#include "xdata/json/UnsignedInteger8Serializer.h"
#include "xdata/json/UnsignedInteger16Serializer.h"
#include "xdata/json/VectorIntegerSerializer.h"
#include "xdata/json/VectorInteger8Serializer.h"
#include "xdata/json/VectorInteger16Serializer.h"
#include "xdata/json/VectorInteger32Serializer.h"
#include "xdata/json/VectorInteger64Serializer.h"
#include "xdata/json/VectorUnsignedIntegerSerializer.h"
#include "xdata/json/VectorUnsignedInteger8Serializer.h"
#include "xdata/json/VectorUnsignedInteger16Serializer.h"

#include <sstream>
#include <iostream>
#include <iomanip>

xdata::json::Serializer::Serializer()
{
	// grabage collection is performed automatically at destruction time
	this->addObjectSerializer(new xdata::json::TableSerializer());
	this->addObjectSerializer(new xdata::json::DoubleSerializer());
	this->addObjectSerializer(new xdata::json::FloatSerializer());
	this->addObjectSerializer(new xdata::json::TimeValSerializer());
	this->addObjectSerializer(new xdata::json::UnsignedInteger64Serializer());
	this->addObjectSerializer(new xdata::json::UnsignedInteger32Serializer());
	this->addObjectSerializer(new xdata::json::UnsignedShortSerializer());
	this->addObjectSerializer(new xdata::json::UnsignedLongSerializer());
	this->addObjectSerializer(new xdata::json::UnsignedIntegerSerializer());
	this->addObjectSerializer(new xdata::json::StringSerializer());
	this->addObjectSerializer(new xdata::json::BooleanSerializer());
	this->addObjectSerializer(new xdata::json::VectorStringSerializer());
	this->addObjectSerializer(new xdata::json::VectorDoubleSerializer());
	this->addObjectSerializer(new xdata::json::VectorUnsignedInteger32Serializer());
	this->addObjectSerializer(new xdata::json::VectorUnsignedInteger64Serializer());
	this->addObjectSerializer(new xdata::json::VectorUnsignedShortSerializer());
	this->addObjectSerializer(new xdata::json::VectorBooleanSerializer());
	this->addObjectSerializer(new xdata::json::VectorFloatSerializer());
	this->addObjectSerializer(new xdata::json::VectorTimeValSerializer());
	this->addObjectSerializer(new xdata::json::BagSerializer());
	this->addObjectSerializer(new xdata::json::PropertiesSerializer());
	this->addObjectSerializer(new xdata::json::IntegerSerializer());
	this->addObjectSerializer(new xdata::json::Integer8Serializer());
	this->addObjectSerializer(new xdata::json::Integer16Serializer());
	this->addObjectSerializer(new xdata::json::Integer32Serializer());
	this->addObjectSerializer(new xdata::json::UnsignedInteger8Serializer());
	this->addObjectSerializer(new xdata::json::UnsignedInteger16Serializer());
	this->addObjectSerializer(new xdata::json::Integer64Serializer());
	this->addObjectSerializer(new xdata::json::VectorIntegerSerializer());
	this->addObjectSerializer(new xdata::json::VectorInteger8Serializer());
	this->addObjectSerializer(new xdata::json::VectorInteger16Serializer());
	this->addObjectSerializer(new xdata::json::VectorInteger32Serializer());
	this->addObjectSerializer(new xdata::json::VectorInteger64Serializer());
	this->addObjectSerializer(new xdata::json::VectorUnsignedIntegerSerializer());
	this->addObjectSerializer(new xdata::json::VectorUnsignedInteger8Serializer());
	this->addObjectSerializer(new xdata::json::VectorUnsignedInteger16Serializer());

}
	
void xdata::json::Serializer::exportAll (xdata::Serializable * s, nlohmann::json & json)
{
	std::string type = s->type();
        if ( type  == "vector" )
        {
                 type =  "vector " + dynamic_cast<xdata::AbstractVector*>(s)->getElementType();
        }
        xdata::json::ObjectSerializer * os = dynamic_cast<xdata::json::ObjectSerializer*>(this->getObjectSerializer(type));
        return os->exportAll(this, s, json);
}

void xdata::json::Serializer::exportAll (xdata::Serializable * s, std::ostream & outs) 
{
	nlohmann::json json = nlohmann::json::object();;
	this->exportAll(s, json);
	outs << std::setw(4) << json;
}
	
void xdata::json::Serializer::import ( xdata::Serializable * s, std::istream & ins) 
{
	try
	{
		nlohmann::json json = nlohmann::json::parse(ins);
		std::string type = s->type();
		return this->import(s, json);
	}
	catch (nlohmann::json::exception& e)
	{
		XCEPT_RAISE(xdata::exception::Exception,e.what());
	}
}

void xdata::json::Serializer::import ( xdata::Serializable * s, nlohmann::json & json)
{
	std::string type = s->type();
        if ( type  == "vector" )
        {
                 type =  "vector " + dynamic_cast<xdata::AbstractVector*>(s)->getElementType();
        }
        xdata::json::ObjectSerializer * os = dynamic_cast<xdata::json::ObjectSerializer*>(this->getObjectSerializer(type));
        return os->import(this, s, json);
}
