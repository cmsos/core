// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/json/VectorDoubleSerializer.h"
#include "xdata/AbstractVector.h"
#include "xdata/json/Serializer.h"
#include "xdata/exception/Exception.h"
#include "xdata/Double.h"

xdata::json::VectorDoubleSerializer::~VectorDoubleSerializer()
{
}

std::string xdata::json::VectorDoubleSerializer::type() const
{
	return "vector double";
}

void xdata::json::VectorDoubleSerializer::exportAll
(
	xdata::json::Serializer * serializer,  
	xdata::Serializable * serializable, 
	nlohmann::json & json
) 
	
{
        xdata::AbstractVector * v = dynamic_cast<xdata::AbstractVector*>(serializable);
        json = nlohmann::json::array({}); 
        
        size_t size = v->elements();
        for ( size_t i = 0; i < size; i++)
        {
		xdata::Double *  e = dynamic_cast<xdata::Double*>(v->elementAt(i));
                if ( e == 0 )
                {
                        XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::String object during export " );
                }

		nlohmann::json  d;
		serializer->exportAll(e, d);
                json.insert(json.end(), d);
        }
}


void xdata::json::VectorDoubleSerializer::import 
(
	xdata::json::Serializer * serializer,  
	xdata::Serializable * serializable, 
	nlohmann::json & json
) 
	
{
        xdata::AbstractVector * v = dynamic_cast<xdata::AbstractVector*>(serializable);
        uint32_t size = json.size();
        v->setSize(size);

        size_t index = 0;
        for (auto& element : json)
        {
		xdata::Double *  e = dynamic_cast<xdata::Double*>(v->elementAt(index));
                if ( e == 0 )
                {
                        XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::String object during import" );
                }

		nlohmann::json  d;
		serializer->import(e, element);
                index++;
        }
}
