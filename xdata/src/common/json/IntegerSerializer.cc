// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/json/IntegerSerializer.h"
#include "xdata/json/Serializer.h"
#include "xdata/Serializable.h"


xdata::json::IntegerSerializer::~IntegerSerializer()
{
}

std::string xdata::json::IntegerSerializer::type() const
{
	return "int";
}


void xdata::json::IntegerSerializer::exportAll(xdata::json::Serializer * serializer,  xdata::Serializable * serializable, nlohmann::json & json)
{
        xdata::Integer *  i = dynamic_cast<xdata::Integer*>(serializable);
        if ( i == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::Integer object during export " );
        }

        json = i->value_;
}

void xdata::json::IntegerSerializer::import (xdata::json::Serializer * serializer,  xdata::Serializable * serializable, nlohmann::json & json)
{
        xdata::Integer * i = dynamic_cast<xdata::Integer*>(serializable);
        if ( i == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::Integer object during import" );
        }

        auto l = json.get<xdata::IntegerT>();
        *i = l;
}

