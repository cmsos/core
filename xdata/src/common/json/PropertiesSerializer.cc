// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/json/PropertiesSerializer.h"
#include "xdata/json/Serializer.h"
#include "xdata/exception/Exception.h"

xdata::json::PropertiesSerializer::~PropertiesSerializer()
{
}

std::string xdata::json::PropertiesSerializer::type() const
{
	return "properties";
}


void xdata::json::PropertiesSerializer::exportAll(xdata::json::Serializer * serializer,  xdata::Serializable * serializable, nlohmann::json & json) 
{
	xdata::Properties & p = dynamic_cast<xdata::Properties&>(*serializable);

	for (std::map<std::string, std::string, std::less<std::string> >::const_iterator mi = p.begin(); mi != p.end(); mi++)
	{
		json[(*mi).first] = (*mi).second;
		
	}
}

void xdata::json::PropertiesSerializer::import (xdata::json::Serializer * serializer,  xdata::Serializable * serializable, nlohmann::json & json) 
{
	std::string name;
        std::string value;
        
        xdata::Properties* m = dynamic_cast<xdata::Properties*>(serializable);
        
        for (auto& p : json.items())
        {
                m->setProperty(p.key(), p.value());    
        }
}
