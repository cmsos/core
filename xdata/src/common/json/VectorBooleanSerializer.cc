// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/json/VectorBooleanSerializer.h"
#include "xdata/AbstractVector.h"
#include "xdata/json/Serializer.h"
#include "xdata/exception/Exception.h"
#include "xdata/Boolean.h"

xdata::json::VectorBooleanSerializer::~VectorBooleanSerializer()
{
}

std::string xdata::json::VectorBooleanSerializer::type() const
{
	return "vector bool";
}

void xdata::json::VectorBooleanSerializer::exportAll
(
	xdata::json::Serializer * serializer,  
	xdata::Serializable * serializable, 
	nlohmann::json & json
) 
	
{
      	xdata::AbstractVector * v = dynamic_cast<xdata::AbstractVector*>(serializable);
       	json = nlohmann::json::array({});
        size_t size = v->elements();
        for ( size_t index = 0; index < size; index++)
        {
                xdata::Boolean *  e = dynamic_cast<xdata::Boolean*>(v->elementAt(index));
                if ( e == 0 )
                {
                        XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::Boolean object during export " );
                }

                nlohmann::json  i;
                serializer->exportAll(e, i);
                json.insert(json.end(), i);
        }
}


void xdata::json::VectorBooleanSerializer::import 
(
	xdata::json::Serializer * serializer,  
	xdata::Serializable * serializable, 
	nlohmann::json & json
) 
	
{
        xdata::AbstractVector * v = dynamic_cast<xdata::AbstractVector*>(serializable);
        uint32_t size = json.size();
        v->setSize(size);

        size_t index = 0;
        for (auto& element : json)
        {
                xdata::Boolean *  i = dynamic_cast<xdata::Boolean*>(v->elementAt(index));
                if ( i == 0 )
                {
                        XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::Boolean object during import" );
                }

                nlohmann::json  d;
                serializer->import(i, element);
                index++;
        }
}
