// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/json/UnsignedShortSerializer.h"
#include "xdata/json/Serializer.h"

xdata::json::UnsignedShortSerializer::~UnsignedShortSerializer()
{
}

std::string xdata::json::UnsignedShortSerializer::type() const
{
	return "unsigned short";
}


void xdata::json::UnsignedShortSerializer::exportAll(xdata::json::Serializer * serializer,  xdata::Serializable * serializable, nlohmann::json & json) 
{
	// cast serializable to Integer
	xdata::UnsignedShort *  i = dynamic_cast<xdata::UnsignedShort*>(serializable);
	if ( i == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::UnsignedShort object during export" );
        }
	json = i->value_;
}

void xdata::json::UnsignedShortSerializer::import (xdata::json::Serializer * serializer,  xdata::Serializable * serializable, nlohmann::json & json) 
{
	xdata::UnsignedShort * i = dynamic_cast<xdata::UnsignedShort*>(serializable);
	if ( i == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::UnsignedShort object during import" );
        }

        auto l = json.get<xdata::UnsignedShortT>();
        *i = l;
}
