// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <stdint.h> 
#include <inttypes.h>
#include <sstream>
#include <algorithm>
#include <vector>
#include "xdata/json/DoubleSerializer.h"
#include "xdata/json/Serializer.h"


xdata::json::DoubleSerializer::~DoubleSerializer()
{
}

std::string xdata::json::DoubleSerializer::type() const
{
	return "double";
}


void xdata::json::DoubleSerializer::exportAll ( xdata::json::Serializer * serializer,  xdata::Serializable * serializable, nlohmann::json & json) 
{
	// cast serializable to Float
	xdata::Double * d = dynamic_cast<xdata::Double*>(serializable);
	if ( d == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::Double object during export " );
        }

	uint8_t buf[sizeof(xdata::DoubleT)];            
        memset(buf,0,sizeof(xdata::DoubleT));
        xdata::DoubleT value = d->value_;
        std::copy(reinterpret_cast<uint8_t*>(&value), buf + sizeof(xdata::DoubleT), buf);
        std::vector<uint8_t> v(buf, buf + sizeof(xdata::DoubleT));
        nlohmann::json vec(v);
        json = vec; 

/*
	if ( d->isFinite() )
	{
		uint8_t buf[sizeof(xdata::DoubleT)];		
		memset(buf,0,sizeof(xdata::DoubleT));
		xdata::DoubleT value = d->value_;
 		std::copy(reinterpret_cast<uint8_t*>(&value), buf + sizeof(xdata::DoubleT), buf);
		std::vector<uint8_t> v(buf, buf + sizeof(xdata::DoubleT));
		nlohmann::json vec(v);
		json = nlohmann::json::array({ d->isFinite(), vec }); 
	{
		json = nlohmann::json::array({ d->isFinite(), d->toString() }); 
	}
*/


	return;
}

void xdata::json::DoubleSerializer::import ( xdata::json::Serializer * serializer,  xdata::Serializable * serializable, nlohmann::json & json) 
{
	xdata::Double * d = dynamic_cast<xdata::Double*>(serializable);
	if ( d == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::Double object during import " );
        }

	xdata::DoubleT value = 0.0;
	std::vector<uint8_t> binary  = json.get<std::vector<uint8_t>>();
 	std::copy(&binary[0], &binary[0] + sizeof(xdata::DoubleT), reinterpret_cast<uint8_t*>(&value));
	//std::cout << "binary is " << value << std::endl;
	*d = value;

/*
	if ( json[0].get<bool>() ) // is finite value
	{
		auto v = json[1].get<std::vector<uint8_t>>();
		xdata::DoubleT value = 0.0;
 		std::copy(v.begin(),v.end(), reinterpret_cast<uint8_t*>(&value));
		d->value_ = value;
		
				
	}
	else 
	{
		d->fromString(json[1].get<std::string>());	
	}
*/
}
