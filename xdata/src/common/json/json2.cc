// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
#include <iostream>
 
#include "xcept/tools.h"
#include "xdata/Table.h"
#include "xdata/Boolean.h"
#include "xdata/TimeVal.h"
#include "xdata/Float.h"
#include "xdata/UnsignedLong.h"
#include "xdata/UnsignedShort.h"
#include "xdata/UnsignedInteger.h"
#include "xdata/UnsignedInteger64.h"
#include "xdata/UnsignedInteger32.h"
#include "xdata/String.h"
#include "xdata/Double.h"
#include "xdata/Vector.h"
#include "xdata/json/Serializer.h"

int main (int argc, char** argv)
{
	try
	{
		std::cout << "Declare a table object" << std::endl;
		xdata::Table t;
		t.addColumn("sessionid", "string");
		t.addColumn("timestamp", "time");
		t.addColumn("context", "string");
		t.addColumn("instance", "string");
		t.addColumn("lumiSectionIndex", "unsigned int 32");
		t.addColumn("prescaleSetIndex","unsigned int 32");
 		t.addColumn("scalersTable", "table");

                for (size_t i = 0; i< 2; i++)
                {
			std::cout << "." << std::flush;
                        xdata::String  pathname;
                        pathname = "01234567890123456789i0123456789";

                        xdata::Table  r;
                        r.addColumn("pathname", "string");
                        r.addColumn("l1Pass", "unsigned int 32");
                        r.addColumn("psPass", "unsigned int 32");
                        r.addColumn("pAccept", "unsigned int 32");
                        r.addColumn("pExcept", "unsigned int 32");
                        r.addColumn("pReject", "unsigned int 32");
		 	for (size_t j = 0; j< 100; j++)
                	{
                        	r.setValueAt(j,"pathname", pathname);
			}

                        t.setValueAt(i,"scalersTable",r);
                }

		std::cout << "--- Before serializing ---" << std::endl;
		std::vector<std::string> columns = t.getColumns();
		for (std::vector<std::string>::iterator ci = columns.begin(); ci != columns.end(); ++ci)
		{
			std::cout << "Column '" << (*ci) << "', type '" << t.getColumnType ( *ci ) << "'" << std::endl;
		}

		xdata::json::Serializer serializer;

        	serializer.exportAll (&t, std::cout);

		std::cout << "row count: " << t.getRowCount() <<  " " <<  std::endl;
		

	} 
	catch (xdata::exception::Exception& e)
	{
		std::cout << "Error in create table";
		std::cout << xcept::stdformat_exception_history(e) << std::endl;
	}

	return 0;
}

