// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
#include <iostream>
#include <sstream>
#include <string>       // std::string
 
#include "toolbox/TimeVal.h"
#include "xcept/tools.h"
#include "xdata/Table.h"
#include "xdata/TimeVal.h"
#include "xdata/UnsignedLong.h"
#include "xdata/UnsignedInteger.h"
#include "xdata/UnsignedInteger64.h"
#include "xdata/Vector.h"
#include "xdata/String.h"
#include "xdata/Double.h"
#include "xdata/Float.h"
#include "xdata/json/Serializer.h"



int main (int argc, char** argv)
{
	try
	{
		xdata::Table in;
                in.addColumn("MyDouble","double");
                in.addColumn("MyTime", "time");
                in.addColumn("MyUI64", "unsigned int 64");
                in.addColumn("MyFloat", "float");
                in.addColumn("String", "string");
		in.addColumn("MySPlot", "vector string");
		in.addColumn("MyDPlot", "vector double");
                /*in.addColumn("Number", "unsigned long");
		*/

                for (size_t i = 0; i< 3; ++i)
                {
                        xdata::Double  d;
                        d = 39.654321 + i;
                        in.setValueAt(i,"MyDouble", d);

                        xdata::TimeVal time(toolbox::TimeVal::gettimeofday());
                        in.setValueAt(i,"MyTime", time);

			xdata::UnsignedInteger64  ui64;
                        ui64 = 34262153162536153 * i;
                        in.setValueAt(i,"MyUI64", ui64);

                        xdata::Float  f;
                        f = 123.456 + i;
                        in.setValueAt(i,"MyFloat", f);

                        xdata::String  s("Test String");
                        in.setValueAt(i,"String", s);


			xdata::Vector<xdata::String>  splot;
			xdata::Vector<xdata::Double>  dplot;
                        for (size_t j = 0; j< i; j++)
                        {
                                splot.push_back(ui64.toString());
                                dplot.push_back(d);
                        }
                        in.setValueAt(i,"MySPlot", splot);
                        in.setValueAt(i,"MyDPlot", dplot);

/*
                        xdata::UnsignedLong  number;
                        number = i;
                        in.setValueAt(i,"Number", number);

                        xdata::String  s("Test String");
                        in.setValueAt(i,"String", s);
*/
                }
                        xdata::Double  d;
                        //d.fromString("NaN") ;
			d = std::numeric_limits<double>::quiet_NaN();
                        in.setValueAt(0,"MyDouble", d);
                        //d.fromString("infinite") ;
			d = std::numeric_limits<double>::infinity();
                        in.setValueAt(1,"MyDouble", d);

			xdata::UnsignedInteger64  ui64;
                        ui64 = std::numeric_limits<uint64_t>::quiet_NaN();;
                        in.setValueAt(0,"MyUI64", ui64);
                        ui64 = std::numeric_limits<uint64_t>::infinity();;
                        in.setValueAt(1,"MyUI64", ui64);

			xdata::Float  f;
			f = std::numeric_limits<float>::quiet_NaN();
                        in.setValueAt(0,"MyFloat", f);
			f = std::numeric_limits<float>::infinity();
                        in.setValueAt(1,"MyFloat", f);	


		xdata::json::Serializer serializer;

        	//serializer.exportAll (&in, std::cout);
		std::stringstream jsonstream;
        	serializer.exportAll (&in, jsonstream );
		std::cout << jsonstream.str()  << std::endl;
		
		xdata::Table loaded;
		std::cout << "import" << std::endl;
        	serializer.import(&loaded, jsonstream); 
		std::stringstream jsonstreami;
		std::cout << "export" << std::endl;
        	serializer.exportAll (&loaded, jsonstreami );
		if ( jsonstream.str() != jsonstreami.str() )
			std::cout << " conversion did not work" << std::endl;

		std::cout <<  "Original" << std::endl;
		in.writeTo(std::cout);
		std::cout << std::endl;
		std::cout << "Converted" << std::endl;
		loaded.writeTo(std::cout);
		std::cout << std::endl;
		
		
	} 
	catch (xdata::exception::Exception& e)
	{
		std::cout << "Error in create table";
		std::cout << xcept::stdformat_exception_history(e) << std::endl;
	}

	return 0;
}

