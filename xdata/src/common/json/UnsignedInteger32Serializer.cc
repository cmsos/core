// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/json/UnsignedInteger32Serializer.h"
#include "xdata/json/Serializer.h"

xdata::json::UnsignedInteger32Serializer::~UnsignedInteger32Serializer()
{
}

std::string xdata::json::UnsignedInteger32Serializer::type() const
{
	return "unsigned int 32";
}


void xdata::json::UnsignedInteger32Serializer::exportAll(xdata::json::Serializer * serializer,  xdata::Serializable * serializable, nlohmann::json & json) 
{
	// cast serializable to Integer
	xdata::UnsignedInteger32 *  i = dynamic_cast<xdata::UnsignedInteger32*>(serializable);
	if ( i == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::UnsignedInteger32 object during export " );
        }

	json = i->value_;
}

void xdata::json::UnsignedInteger32Serializer::import (xdata::json::Serializer * serializer,  xdata::Serializable * serializable, nlohmann::json & json) 
{
	xdata::UnsignedInteger32 * i = dynamic_cast<xdata::UnsignedInteger32*>(serializable);
	if ( i == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::UnsignedInteger32 object during import" );
        }
	auto l = json.get<xdata::UnsignedInteger32T>();
	*i = l;
}
