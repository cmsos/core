// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/json/VectorUnsignedShortSerializer.h"
#include "xdata/AbstractVector.h"
#include "xdata/json/Serializer.h"
#include "xdata/exception/Exception.h"
#include "xdata/UnsignedShort.h"

xdata::json::VectorUnsignedShortSerializer::~VectorUnsignedShortSerializer()
{
}

std::string xdata::json::VectorUnsignedShortSerializer::type() const
{
	return "vector unsigned short";
}

void xdata::json::VectorUnsignedShortSerializer::exportAll
(
	xdata::json::Serializer * serializer,  
	xdata::Serializable * serializable, 
	nlohmann::json & json
) 
	
{
        xdata::AbstractVector * v = dynamic_cast<xdata::AbstractVector*>(serializable);
        json = nlohmann::json::array({});
        size_t size = v->elements();
        for ( size_t index = 0; index < size; index++)
        {
                xdata::UnsignedShort *  e = dynamic_cast<xdata::UnsignedShort*>(v->elementAt(index));
                if ( e == 0 )
                {
                        XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::UnsignedShort object during export " );
                }

                nlohmann::json  i;
                serializer->exportAll(e, i);
                json.insert(json.end(), i);
        }
}


void xdata::json::VectorUnsignedShortSerializer::import 
(
	xdata::json::Serializer * serializer,  
	xdata::Serializable * serializable, 
	nlohmann::json & json
) 
	
{
        xdata::AbstractVector * v = dynamic_cast<xdata::AbstractVector*>(serializable);
        uint32_t size = json.size();
        v->setSize(size);

        size_t index = 0;
        for (auto& element : json)
        {
                xdata::UnsignedShort *  i = dynamic_cast<xdata::UnsignedShort*>(v->elementAt(index));
                if ( i == 0 )
                {
                        XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::UnsignedShort object during import" );
                }

                nlohmann::json  d;
                serializer->import(i, element);
                index++;
        }
}
