// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/json/UnsignedInteger64Serializer.h"
#include "xdata/json/Serializer.h"

xdata::json::UnsignedInteger64Serializer::~UnsignedInteger64Serializer()
{
}

std::string xdata::json::UnsignedInteger64Serializer::type() const
{
	return "unsigned int 64";
}


void xdata::json::UnsignedInteger64Serializer::exportAll(xdata::json::Serializer * serializer,  xdata::Serializable * serializable, nlohmann::json & json) 
{
	xdata::UnsignedInteger64 *  i = dynamic_cast<xdata::UnsignedInteger64*>(serializable);
	if ( i == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::UnsignedInteger64  object during export" );
        }

	json = nlohmann::json::array({ (i->value_ >> 32) & 0xffffffff, i->value_ & 0xffffffff }); 

}

void xdata::json::UnsignedInteger64Serializer::import (xdata::json::Serializer * serializer,  xdata::Serializable * serializable, nlohmann::json & json) 
{
	xdata::UnsignedInteger64 * i = dynamic_cast<xdata::UnsignedInteger64*>(serializable);
	if ( i == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::UnsignedInteger64  object during import" );
        }

	auto l = json[0].get<xdata::UnsignedInteger64T>();
	auto r = json[1].get<xdata::UnsignedInteger64T>();
	*i = (xdata::UnsignedInteger64T)((l << 32) | r);
}
