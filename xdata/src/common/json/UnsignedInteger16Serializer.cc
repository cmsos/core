// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/json/UnsignedInteger16Serializer.h"
#include "xdata/json/Serializer.h"

xdata::json::UnsignedInteger16Serializer::~UnsignedInteger16Serializer()
{
}

std::string xdata::json::UnsignedInteger16Serializer::type() const
{
	return "unsigned int 16";
}

void xdata::json::UnsignedInteger16Serializer::exportAll(xdata::json::Serializer * serializer,  xdata::Serializable * serializable, nlohmann::json & json)
{
        xdata::UnsignedInteger16 *  i = dynamic_cast<xdata::UnsignedInteger16*>(serializable);
        if ( i == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::UnsignedInteger16:w object during export " );
        }

        json = i->value_;
}

void xdata::json::UnsignedInteger16Serializer::import (xdata::json::Serializer * serializer,  xdata::Serializable * serializable, nlohmann::json & json)
{
        xdata::UnsignedInteger16 * i = dynamic_cast<xdata::UnsignedInteger16*>(serializable);
        if ( i == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::UnsignedInteger16:w object during import" );
        }

        auto l = json.get<xdata::UnsignedInteger16T>();
        *i = l;
}

