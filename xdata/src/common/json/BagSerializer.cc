// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/json/BagSerializer.h"
#include "xdata/json/Serializer.h"
#include "xdata/exception/Exception.h"

xdata::json::BagSerializer::~BagSerializer()
{
}

std::string xdata::json::BagSerializer::type() const
{
	return "bag";
}


void xdata::json::BagSerializer::exportAll(xdata::json::Serializer * serializer,  xdata::Serializable * serializable, nlohmann::json & json) 
{
	std::map< std::string, xdata::Serializable*, std::less<std::string> > * b = dynamic_cast<std::map< std::string, xdata::Serializable*, std::less<std::string> >* >(serializable);
        std::map< std::string, xdata::Serializable*, std::less<std::string> >::iterator i;
        for ( i = b->begin(); i != b->end(); i++)
        {
		nlohmann::json  field;
		dynamic_cast<xdata::json::Serializer*>(serializer)->exportAll((*i).second, field);
		json[(*i).first] = field;
	}
}

void xdata::json::BagSerializer::import (xdata::json::Serializer * serializer,  xdata::Serializable * serializable, nlohmann::json & json) 
{
	std::map< std::string, xdata::Serializable*, std::less<std::string> > * b = dynamic_cast<std::map< std::string, xdata::Serializable*, std::less<std::string> >* >(serializable);
	std::map< std::string, xdata::Serializable*, std::less<std::string> >::iterator i;
	for ( i = b->begin(); i != b->end(); i++)
	{	
		if (json.find((*i).first) != json.end()) 
		{
			std::string msg = "No field found in input. Expected (";
			msg += (*i).first;
			msg += ")";
			XCEPT_RAISE(xdata::exception::Exception, msg);
		
		}
		// recursively decode the field value
		dynamic_cast<xdata::json::Serializer*>(serializer)->import((*i).second, json[(*i).first]);
	}		
}
