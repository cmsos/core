// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/json/Integer8Serializer.h"
#include "xdata/json/Serializer.h"

xdata::json::Integer8Serializer::~Integer8Serializer()
{
}

std::string xdata::json::Integer8Serializer::type() const
{
	return "int 8";
}

void xdata::json::Integer8Serializer::exportAll(xdata::json::Serializer * serializer,  xdata::Serializable * serializable, nlohmann::json & json)
{
        xdata::Integer8 *  i = dynamic_cast<xdata::Integer8*>(serializable);
        if ( i == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::Integer8 object during export " );
        }

        json = i->value_;
}

void xdata::json::Integer8Serializer::import (xdata::json::Serializer * serializer,  xdata::Serializable * serializable, nlohmann::json & json)
{
        xdata::Integer8 * i = dynamic_cast<xdata::Integer8*>(serializable);
        if ( i == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::Integer8 object during import" );
        }

        auto l = json.get<xdata::Integer8T>();
        *i = l;
}

