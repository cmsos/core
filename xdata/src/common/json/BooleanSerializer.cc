// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdata/json/BooleanSerializer.h"
#include "xdata/json/Serializer.h"

xdata::json::BooleanSerializer::~BooleanSerializer()
{
}

std::string xdata::json::BooleanSerializer::type() const
{
	return "bool";
}


void xdata::json::BooleanSerializer::exportAll(xdata::json::Serializer * serializer,  xdata::Serializable * serializable, nlohmann::json & json) 
{
	// cast serializable to Boolean
	xdata::Boolean * b  = dynamic_cast<xdata::Boolean*>(serializable);
	if ( b == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::Boolean object during export " );
        }

	json = b->value_;
}

void xdata::json::BooleanSerializer::import (xdata::json::Serializer * serializer,  xdata::Serializable * serializable, nlohmann::json & json) 
{
	xdata::Boolean * b = dynamic_cast<xdata::Boolean*>(serializable);
	if ( b == 0 )
        {
                XCEPT_RAISE(xdata::exception::Exception, "failed to cast serializable of xdata::Boolean object during import " );
        }

      	auto l = json.get<xdata::BooleanT>();
        *b = l;

}
