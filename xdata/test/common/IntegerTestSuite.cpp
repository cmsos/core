#include "IntegerTestSuite.h"

CPPUNIT_TEST_SUITE_REGISTRATION( IntegerTestSuite );

void IntegerTestSuite::setUp()
{
  i1 = 2;
  i2 = 3;
}

void IntegerTestSuite::tearDown()
{
}

void IntegerTestSuite::testAdd()
{
	xdata::Integer result = i1 + i2;
	CPPUNIT_ASSERT( (int)result == 5 );
}


void IntegerTestSuite::testEquals()
{  
	xdata::Integer i3 = i2 - 1;
	CPPUNIT_ASSERT_EQUAL( (int) i1, (int) i3 );
}

void IntegerTestSuite::testNaN()
{  
	xdata::Integer i3;
	CPPUNIT_ASSERT_EQUAL( i3.isNaN(), true );
}

void IntegerTestSuite::testInfOver()
{  
	xdata::Integer i3 = std::numeric_limits<xdata::Integer>::max();
	++i3;
	CPPUNIT_ASSERT_EQUAL( i3.isInf(), true );
}

void IntegerTestSuite::testInfUnder()
{  
	xdata::Integer i3 = std::numeric_limits<xdata::Integer>::min();
	--i3;
	CPPUNIT_ASSERT_EQUAL( i3.isInf(), true );
}
