#include "FloatTestSuite.h"

CPPUNIT_TEST_SUITE_REGISTRATION( FloatTestSuite );

void FloatTestSuite::setUp()
{
  i1 = 2.1;
  i2 = 3.3;
}

void FloatTestSuite::tearDown()
{
}

void FloatTestSuite::testAdd()
{
	xdata::Float result = i1 + i2;
	float expected = 5.4;
	std::cout << sizeof(double) << std:: endl;		
	CPPUNIT_ASSERT_DOUBLES_EQUAL( (float) result, expected, 0.000001 );
}


void FloatTestSuite::testEquals()
{  
	xdata::Float i3 = i2 - 1.2;
	CPPUNIT_ASSERT_EQUAL( (float) i1, (float) i3 );
}

void FloatTestSuite::testNaN()
{  
	xdata::Float f3;
	CPPUNIT_ASSERT_EQUAL( f3.isNaN(), true );
}

void FloatTestSuite::testAssignNaN()
{  
	xdata::Float f3;
	f3 =12.1;
	CPPUNIT_ASSERT_EQUAL( f3.isFinite(), true );
	f3 = std::numeric_limits<xdata::Float>::quiet_NaN();
	
	CPPUNIT_ASSERT_EQUAL( f3.isNaN(), true );
	

}
