// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2017, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _xdata_UnsignedInteger8_h_
#define _xdata_UnsignedInteger8_h_

#include <string>
#include <errno.h>
#include <stdint.h>
#include <limits>
#include "xdata/SimpleType.h"

namespace xdata {

template <class T>
class UnsignedInteger8Type: public xdata::SimpleType<T>
{
	public:
		//! Initialize with a number
		//
		UnsignedInteger8Type(T value);
		
		//! Uninitialized representing NaN
		//
		UnsignedInteger8Type();
		
		//! CTOR from string containing value
		//
		UnsignedInteger8Type (const std::string & value);

		virtual ~UnsignedInteger8Type();
		
		//! Prefix ++
		T& operator++();

		//! Prefix --
		T& operator--();

		//! Postfix ++
		T operator++(int);

		//! Postfix --
		T operator--(int);
		
		std::string type() const;

		std::string toString () const;

		void  fromString (const std::string & value);

};

typedef  UnsignedInteger8Type<uint8_t> UnsignedInteger8;
typedef  UnsignedInteger8Type<uint8_t &> UnsignedInteger8Ref;
typedef  uint8_t UnsignedInteger8T;

} // end namespace xdata

namespace std {
template<>
struct numeric_limits<xdata::UnsignedInteger8>
{
	static const bool is_specialized = true;

	static xdata::UnsignedInteger8 min()
	{ 
		return xdata::UnsignedInteger8(std::numeric_limits<xdata::UnsignedInteger8T>::min());
	}

	static xdata::UnsignedInteger8 max()
	{ 
		return xdata::UnsignedInteger8(std::numeric_limits<xdata::UnsignedInteger8T>::max());

	}

	static const int digits = std::numeric_limits<xdata::UnsignedInteger8T>::digits;
	static const int digits10 = std::numeric_limits<xdata::UnsignedInteger8T>::digits10;
	static const bool is_signed = false;
	static const bool is_integer = true;
	static const bool is_exact = true;
	static const int radix = 2;
	static xdata::UnsignedInteger8 epsilon()
	{ 
		return xdata::UnsignedInteger8(0);
	}
	static xdata::UnsignedInteger8 round_error()
	{ 
		return xdata::UnsignedInteger8(0);
	}

	static const int min_exponent = 0;
	static const int min_exponent10 = 0;
	static const int max_exponent = 0;
	static const int max_exponent10 = 0;

	static const bool has_infinity = true;
	static const bool has_quiet_NaN = true;
	static const bool has_signaling_NaN = false;
	static const float_denorm_style has_denorm = denorm_absent;
	static const bool has_denorm_loss = false;

	static xdata::UnsignedInteger8 infinity()
	{ 
		xdata::UnsignedInteger8 i(0);
		i.limits_.set(xdata::Infinity);
		return i; 
	}
	static xdata::UnsignedInteger8 quiet_NaN() 
	{ 
		return xdata::UnsignedInteger8();
	}

	static xdata::UnsignedInteger8 signaling_NaN()
	{ 
		return xdata::UnsignedInteger8(0);
	}

	static xdata::UnsignedInteger8 denorm_min()
	{ 
		return xdata::UnsignedInteger8(0);
	}

	static const bool is_iec559 = false;
	static const bool is_bounded = true;
	static const bool is_modulo = true;

	static const bool traps = std::numeric_limits<xdata::UnsignedInteger8T>::traps;
	static const bool tinyness_before = false;
	static const float_round_style round_style = round_toward_zero;
};
} // end of namespace std

#include "xdata/UnsignedInteger8.i"


#endif
