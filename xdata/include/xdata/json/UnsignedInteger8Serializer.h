// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _xdata_json_UnsignedInteger8Serializer_h_
#define _xdata_json_UnsignedInteger8Serializer_h_

#include "xdata/json/ObjectSerializer.h"
#include "xdata/Serializable.h"
#include "xdata/exception/Exception.h"
#include "xdata/UnsignedInteger8.h"



namespace xdata {
namespace json {


class UnsignedInteger8Serializer : public xdata::json::ObjectSerializer
{	
	public:
	virtual ~UnsignedInteger8Serializer();

	std::string type() const;
	
	void exportAll(xdata::json::Serializer * serializer,  xdata::Serializable * serializable, nlohmann::json & json);
	void import (xdata::json::Serializer * serializer,  xdata::Serializable * serializable, nlohmann::json & json);
};


}}

#endif
