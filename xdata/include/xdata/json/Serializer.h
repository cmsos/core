// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelvicius					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _xdata_json_Serializer_h_
#define _xdata_json_Serializer_h_

#include <iostream>

#include "xdata/exception/Exception.h"
#include "xdata/Serializer.h"
#include "xdata/Serializable.h"

#include "nlohmann/json.hpp"


namespace xdata {

namespace json {

class Serializer : public xdata::Serializer 
{
	public:

	Serializer();
	
	/*! 
	*/
	void exportAll ( xdata::Serializable * s, std::ostream & os);
	void exportAll ( xdata::Serializable * s, nlohmann::json & json);

	/*! 
	*/
	void import ( xdata::Serializable * s, std::istream & is);	
	void import ( xdata::Serializable * s, nlohmann::json & json);


	
};

}}

#endif

