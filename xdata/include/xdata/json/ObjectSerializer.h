// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelvicius 					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _xdata_json_ObjectSerializer_h_
#define _xdata_json_ObjectSerializer_h_

#include <string>
#include "xdata/ObjectSerializer.h"
#include "xdata/exception/Exception.h"
#include "xdata/json/Serializer.h"

#include "nlohmann/json.hpp"

namespace xdata {
namespace json {

class ObjectSerializer : public xdata::ObjectSerializer 
{
	public:
	
	virtual void exportAll(xdata::json::Serializer* serializer, xdata::Serializable* serializable, nlohmann::json & json ) = 0;
	virtual void import (xdata::json::Serializer * serializer,  xdata::Serializable * serializable, nlohmann::json & json)  = 0;
};

}}

#endif
