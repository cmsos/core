// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2017, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _xdata_Integer16_h_
#define _xdata_Integer16_h_

#include <string>
#include <errno.h>
#include <stdint.h>
#include <limits>
#include "xdata/SimpleType.h"

namespace xdata {

template <class T>
class Integer16Type: public xdata::SimpleType<T>
{
	public:
		//! Initialize with a number
		//
		Integer16Type(T value);
		
		//! Uninitialized representing NaN
		//
		Integer16Type();
		
		//! CTOR from string containing value
		//
		Integer16Type (const std::string & value);

		virtual ~Integer16Type();
		
		//! Prefix ++
		T& operator++();
		
		//! Prefix --
		T& operator--();

		//! Postfix ++
		T operator++(int);

		//! Postfix --
		T operator--(int);
		
		std::string type() const;

		std::string toString () const;

		void fromString (const std::string & value);			

};

typedef  Integer16Type<int16_t> Integer16;
typedef  Integer16Type<int16_t &> Integer16Ref;
typedef  int16_t Integer16T;
 
} // end namespace xdata

namespace std {
template<>
struct numeric_limits<xdata::Integer16>
{
	static const bool is_specialized = true;

	static xdata::Integer16 min()
	{ 
		return xdata::Integer16(std::numeric_limits<xdata::Integer16T>::min());
	}

	static xdata::Integer16 max()
	{ 
		return xdata::Integer16(std::numeric_limits<xdata::Integer16T>::max());

	}

	static const int digits = std::numeric_limits<xdata::Integer16T>::digits;
	static const int digits10 = std::numeric_limits<xdata::Integer16T>::digits10;
	static const bool is_signed = true;
	static const bool is_integer = true;
	static const bool is_exact = true;
	static const int radix = 2;
	
	static xdata::Integer16 epsilon()
	{ 
		return xdata::Integer16(0);
	}
	
	static xdata::Integer16 round_error()
	{ 
		return xdata::Integer16(0);
	}

	static const int min_exponent = 0;
	static const int min_exponent10 = 0;
	static const int max_exponent = 0;
	static const int max_exponent10 = 0;

	static const bool has_infinity = true;
	static const bool has_quiet_NaN = true;
	static const bool has_signaling_NaN = false;
	static const float_denorm_style has_denorm = denorm_absent;
	static const bool has_denorm_loss = false;

	static xdata::Integer16 infinity()
	{ 
		xdata::Integer16 i(0);
		i.limits_.set(xdata::Infinity);
		return i; 
	}
	static xdata::Integer16 quiet_NaN()
	{ 
		return xdata::Integer16();
	}

	static xdata::Integer16 signaling_NaN() 
	{ 
		return xdata::Integer16(0);
	}

	static xdata::Integer16 denorm_min()
	{ 
		return xdata::Integer16(0);
	}

	static const bool is_iec559 = false;
	static const bool is_bounded = true;
	static const bool is_modulo = true;

	static const bool traps = std::numeric_limits<xdata::Integer16T>::traps;
	static const bool tinyness_before = false;
	static const float_round_style round_style = round_toward_zero;
};
} // end of namespace std

#include "xdata/Integer16.i"


#endif
