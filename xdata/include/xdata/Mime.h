// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _xdata_MIME_h_
#define _xdata_MIME_h_

#include <map>
#include <string>
#include <vector>

#include "xdata/Serializable.h"
#include "xdata/exception/Exception.h"

#include "mimetic/mimetic.h"

namespace xdata {

// typedef toolbox::mem::CountingPtr<mimetic::MimeEntity, toolbox::mem::ThreadSafeReferenceCount,toolbox::mem::StandardObjectPolicy> MimeEntityReference;

class Mime: public xdata::Serializable
{
	public:
	
	Mime() ;
	
	Mime(const Mime & m) ;
	
	virtual ~Mime();
	
	//! Assign a MimeEntity object to this object
	void setEntity(mimetic::MimeEntity* entity);
	
	Mime & operator=(const Mime & m) ;

	int operator==(const Mime & m) const;

	int operator!=(const Mime & m) const;
	
	int equals(const xdata::Serializable & s) const;
	
	void setValue(const xdata::Serializable & s) ;
	
	std::string type() const;
	
	std::string toString()  const ;

	void fromString(const std::string& value) ;
	
	//! Returns entity or 0 if not set
	mimetic::MimeEntity* getEntity();
	
	protected:

	mimetic::MimeEntity* mimeEntity_;		
};

} // end namescape xdata

#endif
