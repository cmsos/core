// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2017, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, L. Orsini and D. Simelevicius                   *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _xdata_version_h_
#define _xdata_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define CORE_XDATA_VERSION_MAJOR 12
#define CORE_XDATA_VERSION_MINOR 1
#define CORE_XDATA_VERSION_PATCH 0

// If any previous versions available E.g. #define CORE_XDATA_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define CORE_XDATA_PREVIOUS_VERSIONS "12.0.0"

//
// Template macros
//
#define CORE_XDATA_VERSION_CODE PACKAGE_VERSION_CODE(CORE_XDATA_VERSION_MAJOR,CORE_XDATA_VERSION_MINOR,CORE_XDATA_VERSION_PATCH)
#ifndef CORE_XDATA_PREVIOUS_VERSIONS
#define CORE_XDATA_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(CORE_XDATA_VERSION_MAJOR,CORE_XDATA_VERSION_MINOR,CORE_XDATA_VERSION_PATCH)
#else 
#define CORE_XDATA_FULL_VERSION_LIST  CORE_XDATA_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(CORE_XDATA_VERSION_MAJOR,CORE_XDATA_VERSION_MINOR,CORE_XDATA_VERSION_PATCH)
#endif 

namespace xdata 
{
	const std::string project = "core";
	const std::string package  =  "xdata";
   	const std::string versions = CORE_XDATA_FULL_VERSION_LIST;
	const std::string summary = "SOAP C++ programming package, Xerces, namespace xoap";
	const std::string description = "";
	const std::string authors = "Johannes Gutleber, Luciano Orsini, Dainius Simelevicious";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/Core_Tools";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
