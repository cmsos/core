// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2017, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _xdata_exdr_Integer16Serializer_h_
#define _xdata_exdr_Integer16Serializer_h_

#include "xdata/exdr/ObjectSerializer.h"
#include "xdata/Serializable.h"
#include "xdata/exception/Exception.h"
#include "xdata/Integer16.h"



namespace xdata {
namespace exdr {


class Integer16Serializer : public xdata::exdr::ObjectSerializer
{	
	public:
	virtual ~Integer16Serializer();

	std::string type() const;
	
	void exportAll
	(
		xdata::exdr::Serializer * serializer,
		xdata::Serializable * serializable,
		xdata::exdr::OutputStreamBuffer * sbuf
	);
	
	void import 
	(
		xdata::exdr::Serializer * serializer,
		xdata::Serializable * serializable,
		xdata::exdr::InputStreamBuffer * sbuf
	);
};

}}

#endif
