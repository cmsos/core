// $Id$

/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius                                   *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************
 */

#ifndef _toolbox_net_IP_h_
#define _toolbox_net_IP_h_

#include <string>


namespace toolbox {
namespace net {

class  IP 
{
    public:
	IP(const std::string & hostAddress, int addressFamily);
	std::string hostAddress() const;
	int addressFamily() const;

    protected:
	std::string hostAddress_;
	int addressFamily_;

};

} }


#endif
