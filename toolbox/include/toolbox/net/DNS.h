// $Id$

/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius                                   *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************
 */

#ifndef _toolbox_net_DNS_h_
#define _toolbox_net_DNS_h_

#include"toolbox/net/IP.h" 
#include <string>
#include <list>


namespace toolbox {
namespace net {

class  DNS
{
    public:


	// waitAndRetry (N, T): will make up to maximum N attempts and wait for T time between each attempts (the animation case)
   	static std::list<toolbox::net::IP> waitAndRetry(const std::string& address, unsigned int n, int t);
    
	// RetryForever: make attempts until you succeed, or timeout
	static std::list<toolbox::net::IP> retryForever(const std::string& address, time_t timeout);
    
	// return local hostname
	static std::string hostName();

};


} }


#endif
