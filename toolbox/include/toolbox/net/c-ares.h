/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2024, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini and D. Simelevicius                                *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************
 */

#ifndef _toolbox_net_c_ares_h_
#define _toolbox_net_c_ares_h_

#include <ares.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <string>
#include <atomic>
#include <mutex>

namespace toolbox
{
	namespace net
	{
		class careslib
		{
			public:
				careslib();
				~careslib();
				static void init();
				static void cleanup();
			protected:
				static careslib * instance_;
		};

		class cares
		{
			public:
				cares();
				~cares();
				std::string resolve(const std::string& name);
				void callback(int status, int timeouts, struct hostent *hostent);
			private:
				int wait(int timeout_ms);
				std::string hostname_;
				std::string error_;
				ares_channel channel_;
				std::mutex mutex_;
		};
	}
}
#endif
