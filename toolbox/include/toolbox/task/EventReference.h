// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _toolbox_task_EventReference_h_
#define _toolbox_task_EventReference_h_

#include "toolbox/Event.h"
#include <memory>

namespace toolbox 
{
	namespace task 
	{
		typedef std::shared_ptr<toolbox::Event> EventReference;
	}
}
#endif
