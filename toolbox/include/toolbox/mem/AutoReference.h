// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _toolbox_mem_AutoReference_h_
#define _toolbox_mem_AutoReference_h_

#include <memory>
#include "toolbox/mem/Reference.h"

namespace toolbox 
{
	namespace mem 
	{
		void release(toolbox::mem::Reference * object);

		class AutoReference : public std::shared_ptr<toolbox::mem::Reference>
		{ 
			public:
			AutoReference();
			AutoReference(toolbox::mem::Reference * p);
		};
	}
}
#endif
