// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _toolbox_AbortEvent_h_
#define _toolbox_AbortEvent_h_

#include <string>
#include <memory>
#include "toolbox/Event.h"

namespace toolbox 
{

class AbortEvent: public toolbox::Event
{
	public:
	
	typedef std::shared_ptr<AbortEvent> Reference;
	
	AbortEvent(): toolbox::Event("Abort", 0)
	{
	
	}
	
	AbortEvent(void* originator): toolbox::Event("Abort", originator)
	{
	
	}
		
	protected:
	
};

}

#endif
