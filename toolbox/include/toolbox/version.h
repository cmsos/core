/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2024, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, L. Orsini and D. Simelevicius                   *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _toolbox_version_h_
#define _toolbox_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define CORE_TOOLBOX_VERSION_MAJOR 9
#define CORE_TOOLBOX_VERSION_MINOR 16
#define CORE_TOOLBOX_VERSION_PATCH 0
// If any previous versions available E.g. #define CORE_TOOLBOX_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define CORE_TOOLBOX_PREVIOUS_VERSIONS "9.10.0,9.11.0,9.12.0,9.13.0,9.14.0,9.15.0,9.15.1,9.15.2,9.15.3"

// List of required RPMs ( they are all XDAQ packages prefixed with daq- as a convention, RPM spec format cam be used)
#define TOOLBOX_REQUIRED_PACKAGE_LIST config,xcept,log4cplus,xdaq

//
// Template macros
//
#define CORE_TOOLBOX_VERSION_CODE PACKAGE_VERSION_CODE(CORE_TOOLBOX_VERSION_MAJOR,CORE_TOOLBOX_VERSION_MINOR,CORE_TOOLBOX_VERSION_PATCH)
#ifndef CORE_TOOLBOX_PREVIOUS_VERSIONS
#define CORE_TOOLBOX_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(CORE_TOOLBOX_VERSION_MAJOR,CORE_TOOLBOX_VERSION_MINOR,CORE_TOOLBOX_VERSION_PATCH)
#else 
#define CORE_TOOLBOX_FULL_VERSION_LIST  CORE_TOOLBOX_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(CORE_TOOLBOX_VERSION_MAJOR,CORE_TOOLBOX_VERSION_MINOR,CORE_TOOLBOX_VERSION_PATCH)
#endif 

namespace toolbox 
{
	const std::string project = "core";
	const std::string package  =  "toolbox";
	const std::string versions = CORE_TOOLBOX_FULL_VERSION_LIST;
	const std::string summary = "System and basic programming support classes";
	const std::string description = "";
	const std::string authors = "Johannes Gutleber, Luciano Orsini, Dainius Simelevicius";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/Core_Tools";

	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
