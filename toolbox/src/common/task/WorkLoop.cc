// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "toolbox/task/WorkLoop.h"
#include <signal.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include "toolbox/string.h"
#include "toolbox/PolicyFactory.h"
#include "toolbox/Policy.h"
#include "toolbox/exception/Exception.h"
#include <iostream>
#include "xcept/tools.h"
#include <unistd.h>

#include <sys/syscall.h>    /* For SYS_xxx definitions */


// LOG4CPLUS INCLUDES
#include "log4cplus/logger.h"
#include "log4cplus/consoleappender.h"
#include "log4cplus/helpers/appenderattachableimpl.h"
#include "log4cplus/helpers/loglog.h"
#include "log4cplus/helpers/pointer.h"
#include "log4cplus/spi/loggingevent.h"
#include "log4cplus/loggingmacros.h"

using namespace log4cplus;
using namespace log4cplus::helpers;
using namespace log4cplus::spi;

static void terminate_workloophandler()
{
	std::cout << "Uncaught exception in workloop thread" << std::endl;
#ifdef __GNUC__
	__gnu_cxx::__verbose_terminate_handler();
#else
	abort()
#endif /* __GNUC__ */
}

static void unexpected_workloophandler()
{
	std::cout << "Unexpected exception in workloop thread" << std::endl;
#ifdef __GNUC__
	__gnu_cxx::__verbose_terminate_handler();
#else
	abort()
#endif /* __GNUC__ */
}

void * toolbox::task::thread_func (void* workLoop)
{
	// block all signals, only main thread shall handle signals
	sigset_t mask;
	sigemptyset(&mask);
	sigaddset(&mask, SIGINT);
	sigaddset(&mask, SIGQUIT);
	sigaddset(&mask, SIGTERM);
	sigaddset(&mask, SIGPIPE);
	sigaddset(&mask, SIGABRT);
	sigaddset(&mask, SIGSEGV);		
	sigprocmask(SIG_BLOCK,&mask,0);

        std::set_terminate(terminate_workloophandler);
        std::set_unexpected(unexpected_workloophandler);

	toolbox::task::WorkLoop* wl = (toolbox::task::WorkLoop*) workLoop;
	wl->run();
	pthread_exit(0);
	return 0;
}


toolbox::task::WorkLoop::WorkLoop(std::string name, std::string type)
{
	name_ = name;
	type_ = type;
	active_ = false;
	size_ = 1024;
	this->addExceptionListener(this); 
}

toolbox::task::WorkLoop::~WorkLoop()
{
	if ( this->isActive() )
		this->cancel();
}

	
bool toolbox::task::WorkLoop::isActive()
{
	return active_;
}
	
void toolbox::task::WorkLoop::activate() 
{
	if (this->isActive())
	{
		std::string msg = "Work loop cannot be activated, is already active: ";
		msg += name_;
		XCEPT_RAISE (toolbox::task::exception::Exception, msg);
	}
	else
	{
		// Initialize thread attributes with default values
		pthread_attr_init(&thread_attributes_);

		// set detached so that all resources are cleaned up automatically
		//pthread_attr_setdetachstate(&thread_attributes_, PTHREAD_CREATE_DETACHED);
		pthread_attr_setdetachstate(&thread_attributes_, PTHREAD_CREATE_JOINABLE);

		//int cancelState;

		//pthread_setcanceltype(PTHREAD_CANCEL_ENABLE, &cancelState);
		//pthread_setcanceltype(PTHREAD_CANCEL_DEFERRED, &cancelState);
		
		active_ = false;
		errno = 0;
		if (pthread_create ( &thread_id_, &thread_attributes_, thread_func, (void*) this) != 0)
		{
			XCEPT_RAISE (toolbox::task::exception::Exception, strerror(errno));
		}
		// the workloop  will become active within the run() method(in the new created thread)
		 while (! active_ ) { sched_yield();}
		//active_ = true;
	}

}


void toolbox::task::WorkLoop::cancel() 
{
	if (!this->isActive())
	{
		std::string msg = toolbox::toString("Cannot cancel non active WorkLoop: %s",this->getName().c_str());
		XCEPT_RAISE (toolbox::task::exception::Exception, msg);
	}
	else
	{
		//trigger the exiting of the thread
		stopped_ = true;

		// make sure the thread is over
		pthread_join(thread_id_, NULL);
		pthread_attr_destroy(&thread_attributes_);
		// now it is possible to active the thread again
		active_ = false;
	}
}



std::string toolbox::task::WorkLoop::getName()
{
	return name_;
}

std::string toolbox::task::WorkLoop::getType()
{
	return type_;
}

void toolbox::task::WorkLoop::run()
{
	stopped_ = false;
	// now I can really say that all conditions are satisifed and the thread is active
	// the cancel() will always work

	toolbox::PolicyFactory * factory = toolbox::getPolicyFactory();
	toolbox::Policy * policy = 0;
	
	try
	{
		toolbox::net::URN urn("toolbox-task-workloop", name_);
		policy = factory->getPolicy(urn, "thread");
		policy->setDefaultPolicy();
#ifdef linux                
		// add SYS_gettid
		pid_t const thread_id = pid_t(::syscall(SYS_gettid));
		toolbox::net::URN tidurn("toolbox-task-workloop-SYS_gettid",name_);
		policy->setProperty(tidurn.toString(),toolbox::toString("%d",thread_id));
#else
		// not implemented yet
#endif
	}
	catch (toolbox::exception::Exception & te)
	{
		std::stringstream oss;
		oss << "Failed to set thread policy for WorkLoop '" << name_ << "'"; 
		xcept::Exception e( "xcept::Exception", oss.str() , __FILE__, __LINE__, __FUNCTION__, te);
		dispatcher_.fire(e);

	}

	active_ = true;
	while ( ! stopped_ )
	{
		try
		{
			this->process();
		}
                catch (xcept::Exception& xe)
                {
                        std::stringstream oss;
                        oss << xe.what();
                        xcept::Exception e( "xcept::Exception", "Caught xcept exception in workloop process.\n" + oss.str(), __FILE__, __LINE__, __FUNCTION__, xe);
                        dispatcher_.fire(e);
                }
                catch (std::exception& se)
                {
                        std::stringstream oss;
                        oss << se.what();
                        xcept::Exception e( "xcept::Exception", "Caught std exception in workloop process.\n" + oss.str(), __FILE__, __LINE__, __FUNCTION__);
                        dispatcher_.fire(e);
                }
                catch (...)
                {
                	xcept::Exception e( "xcept::Exception", "Caught Unknown exception in workloop process", __FILE__, __LINE__, __FUNCTION__);
                        dispatcher_.fire(e);
                }
	}
}

void toolbox::task::WorkLoop::resize(size_t n) 
{
}

void toolbox::task::WorkLoop::addExceptionListener ( toolbox::exception::Listener * listener)
{
        dispatcher_.addListener(listener);
}

void toolbox::task::WorkLoop::removeExceptionListener ( toolbox::exception::Listener * listener)
{
        dispatcher_.removeListener(listener);
}
void toolbox::task::WorkLoop::onException(xcept::Exception& e)
{
       //In the case no other client listen to this event at least a log statment present
       LOG4CPLUS_ERROR ( Logger::getRoot(), xcept::stdformat_exception_history(e));
}

