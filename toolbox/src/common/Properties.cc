
#include "toolbox/Properties.h"

std::string toolbox::Properties::getProperty(const std::string & name)
{
	std::map<std::string, std::string, std::less<std::string> >::iterator i =  properties_.find(name); 
	if ( i != properties_.end() )
		return (*i).second;
	else 
		return "";
}
	
		
void toolbox::Properties::setProperty(const std::string & name, const std::string & value) 
{
	properties_[name] = value;
}


std::vector<std::string> toolbox::Properties::propertyNames()
{
	std::vector<std::string> names;
	std::map<std::string, std::string, std::less<std::string> >::iterator i = properties_.begin();
	while ( i !=  properties_.end() )
	{
		names.push_back((*i).first);
		i++;
	}
	return names;
}

bool toolbox::Properties::hasProperty (const std::string& name)
{
	if (properties_.find(name) != properties_.end())
	{
		return true;
	}
	else
	{
		return false;
	}
}

void toolbox::Properties::clear() 
{
	properties_.clear();
}

bool toolbox::Properties::empty() 
{
	return properties_.empty();
}

