// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini and D. Simelevicius				 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "toolbox/mem/AutoReference.h"

void toolbox::mem::release(toolbox::mem::Reference * object)
{
	if (object != nullptr ) object->release();
}

toolbox::mem::AutoReference::AutoReference():shared_ptr(nullptr, toolbox::mem::release)
{

}
toolbox::mem::AutoReference::AutoReference(toolbox::mem::Reference * p):shared_ptr(p, toolbox::mem::release) 
{
}

