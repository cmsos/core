/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius                                   *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************
 */


#include "toolbox/net/DNS.h"
#include "toolbox/TimeVal.h"

#include "toolbox/exception/Timeout.h"
#include "toolbox/net/exception/UnresolvedAddress.h"


#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <arpa/inet.h>


std::list<toolbox::net::IP> toolbox::net::DNS::waitAndRetry(const std::string& address, unsigned int n, int t)
{
    std::list<toolbox::net::IP> resolved;
    unsigned int retrycnt = 0;
    bool found = false;
    while (retrycnt < n && !found)
    {
        /* resolve the domain name into a list of addresses */
        struct addrinfo *result;

        int s = getaddrinfo(address.c_str(), NULL, NULL, &result);
        if (s != 0)
        {
            //fprintf(stderr, "error in getaddrinfo: %s\n", gai_strerror(error));
            //return EXIT_FAILURE;
            retrycnt++;
            ::sleep(t);
        }
        else
        {
            // loop over all returned results and get IP number
            for (struct addrinfo * r = result; r != NULL; r = r->ai_next)
            {
                char ipstr[INET6_ADDRSTRLEN];
                const char * s = inet_ntop(AF_INET, &((struct sockaddr_in *)r->ai_addr)->sin_addr, ipstr, sizeof(ipstr));
                if( s != NULL )
                {
                    resolved.push_back(toolbox::net::IP(ipstr,r->ai_family));
                }
                else
                {
                    freeaddrinfo(result);
                    XCEPT_RAISE(toolbox::net::exception::Exception, "failed to convert address '" +address + "', with error " + std::string(strerror(errno)));
                }
            }
            
            freeaddrinfo(result);
            found = true;
        }
    }
    
    if (!found)
    {
        XCEPT_RAISE(toolbox::net::exception::UnresolvedAddress, "failed to resolve address for '" + address + "'");
    }
    
    return resolved;
}

std::list<toolbox::net::IP> toolbox::net::DNS::retryForever(const std::string& address, time_t timeout)
{
    toolbox::TimeVal start = toolbox::TimeVal::gettimeofday();
    bool timeExpired = false;
    while (! timeExpired)
    {
        try
        {
            return toolbox::net::DNS::waitAndRetry(address, 4, 1); // 4 attempt sleeps 1s for each attempt
        }
        catch(toolbox::net::exception::UnresolvedAddress & e)
        {
            // ignore exception, keep going till timeout
            toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();
            time_t delta = now.sec() - start.sec();
            if ( delta > timeout)
                timeExpired = true;
        }
    }
    
    XCEPT_RAISE( toolbox::exception::Timeout, "failed to resolve address " + address + ", timeout");
}

std::string toolbox::net::DNS::hostName()
{
	char buffer[NI_MAXHOST];
	int status = gethostname(buffer, sizeof(buffer));
	if (status != 0)
    {
		XCEPT_RAISE( toolbox::net::exception::Exception, "failed to retrieve hostname");
    }
    
    return std::string(buffer);
}







