/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius                                   *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************
 */


#include "toolbox/net/IP.h"

toolbox::net::IP::IP(const std::string & hostAddress, int addressFamily): hostAddress_(hostAddress),addressFamily_(addressFamily)
{
}

std::string toolbox::net::IP::hostAddress() const
{
	return hostAddress_;
}

int toolbox::net::IP::addressFamily() const
{
	return addressFamily_;
}






