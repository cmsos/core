/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2024, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini and D. Simelevicius                                *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************
 */

#include <mutex>
#include <unistd.h>
#include <sstream>
#include <sys/poll.h>
#include "toolbox/net/c-ares.h"
#include "toolbox/exception/Exception.h"
#include "toolbox/exception/Timeout.h"
#include "toolbox/net/exception/Exception.h"
#include "toolbox/TimeVal.h"

static std::mutex careslibmutex_;
toolbox::net::careslib * toolbox::net::careslib::instance_ = nullptr;

void ares_cb(void *arg, int status, int timeouts, struct hostent *hostent)
{
	static_cast<toolbox::net::cares *>(arg)->callback(status, timeouts, hostent);
}

void toolbox::net::careslib::init()
{
	std::lock_guard<std::mutex> guard(careslibmutex_);

	if (instance_ == nullptr)
	{
		instance_ = new toolbox::net::careslib();
	}
}

void toolbox::net::careslib::cleanup()
{
	std::lock_guard<std::mutex> guard(careslibmutex_);

	if (instance_ != nullptr)
	{
		delete instance_;
		instance_ = nullptr;
	}
}

toolbox::net::careslib::careslib()
{
	int ret;

	ret = ares_library_init(ARES_LIB_INIT_ALL);
	if (ret)
	{
		std::stringstream ss;
		ss << "failed to initialize hostname resolve function, reason: ";
		ss << ares_strerror(ret);
		XCEPT_RAISE(toolbox::net::exception::Exception, ss.str());
	}
}

toolbox::net::careslib::~careslib()
{
	ares_library_cleanup();
}

toolbox::net::cares::cares()
{
	int ret;
	struct ares_options ares_opts;

	toolbox::net::careslib::init();

	ares_opts.timeout = 1000;
	ret = ares_init_options(&channel_, &ares_opts, ARES_OPT_TIMEOUTMS);
	if (ret != ARES_SUCCESS)
	{
		std::stringstream ss;
		ss << "hostname resolve module failed to setup local data structure, reason: ";
		if (ret == ARES_ENOMEM)
		{
			ss << "out of memory";
		}
		else if (ret == ARES_EFORMERR)
		{
			ss << "invalid /etc/resolv.conf";
		}
		else if (ret == ARES_ENOTINITIALIZED)
		{
			ss << "module uninitialized";
		}
		else
		{
			ss << "unknown reason";
		}
		XCEPT_RAISE(toolbox::net::exception::Exception, ss.str());
	}
}

toolbox::net::cares::~cares()
{
	ares_destroy(channel_);
}

int toolbox::net::cares::wait(int timeout_ms)
{
	if (timeout_ms < 0)
	{
		ares_process_fd(channel_, ARES_SOCKET_BAD, ARES_SOCKET_BAD);
		ares_cancel(channel_);
		XCEPT_RAISE(toolbox::exception::Timeout, "Timeout reached");
	}
	int nfds;
	int bitmask;
	ares_socket_t socks[ARES_GETSOCK_MAXNUM];
	struct pollfd pfd[ARES_GETSOCK_MAXNUM];
	int i;
	int num = 0;

	bitmask = ares_getsock(channel_, socks, ARES_GETSOCK_MAXNUM);

	for (i = 0; i < ARES_GETSOCK_MAXNUM; i++)
	{
		pfd[i].events = 0;
		pfd[i].revents = 0;
		if (ARES_GETSOCK_READABLE(bitmask, i))
		{
			pfd[i].fd = socks[i];
			pfd[i].events |= POLLRDNORM | POLLIN;
		}
		if (ARES_GETSOCK_WRITABLE(bitmask, i))
		{
			pfd[i].fd = socks[i];
			pfd[i].events |= POLLWRNORM | POLLOUT;
		}
		if (pfd[i].events != 0)
		{
			num++;
		}
		else
		{
			break;
		}
	}

	if (num)
	{
		nfds = poll(pfd, num, timeout_ms/*milliseconds */);
	}
	else
	{
		nfds = 0;
	}

	if (!nfds)
	{
		ares_process_fd(channel_, ARES_SOCKET_BAD, ARES_SOCKET_BAD);
		ares_cancel(channel_);
	}
	else
	{
		for (i = 0; i < num; i++)
		{
			ares_process_fd(channel_, (pfd[i].revents & (POLLRDNORM | POLLIN)) ? pfd[i].fd : ARES_SOCKET_BAD, (pfd[i].revents & (POLLWRNORM | POLLOUT)) ? pfd[i].fd : ARES_SOCKET_BAD);
		}
	}
	return nfds;
}

void toolbox::net::cares::callback(int status, int timeouts, struct hostent *hostent)
{
	//char buf[INET6_ADDRSTRLEN];
	//int proto, i;
	switch (status)
	{
	case ARES_SUCCESS:
		hostname_ = hostent->h_name;
		/*
		printf("got address of host %s, %d timeouts:\n", (char *)arg, timeouts);
		printf("  official name: %s\n", hostent->h_name);
		for (i = 0; hostent->h_aliases[i]; ++i) {
			printf("  alias: %s\n", hostent->h_aliases[i]);
		}
		proto = hostent->h_addrtype;
		for (i = 0; hostent->h_addr_list[i]; ++i) {
			inet_ntop(proto, hostent->h_addr_list[i], buf, INET6_ADDRSTRLEN);
			printf("  %s\n", buf);
		}
		*/
		break;
	case ARES_ENOTIMP:
		error_ = "The ares library does not know how to find addresses of type family.";
		break;
	case ARES_EBADNAME:
		error_ = "The hostname name is composed entirely of numbers and periods, but is not a valid representation of an Internet address.";
		break;
	case ARES_ENOTFOUND:
		error_ = "The address addr was not found.";
		break;
	case ARES_ENOMEM:
		error_ = "Memory was exhausted.";
		break;
	case ARES_ECANCELLED:
		error_ = "The query was cancelled.";
		break;
	case ARES_EDESTRUCTION:
		error_ = "The name service channel channel is being destroyed; the query will not be completed.";
		break;
	default:
		error_ = "got unknown return value";
	}

	return;
}

std::string toolbox::net::cares::resolve(const std::string& name)
{
	std::lock_guard<std::mutex> guard(mutex_);

	hostname_ = "";
	error_ = "";

	ares_gethostbyname(channel_, name.c_str(), AF_UNSPEC, ares_cb, this);

	int timeout = 1000 * 10; // 10000ms = 10s
	struct timeval last, now;
	gettimeofday(&last, NULL);
	int nfds = 1;
	while (nfds)
	{
		struct timeval *tvp, tv, store = {timeout / 1000, (timeout % 1000) * 1000};

		// return maximum time to wait
		tvp = ares_timeout(channel_, &store, &tv);
		int timeout_ms = tvp->tv_sec * 1000 + tvp->tv_usec / 1000;

		//printf("timeout_ms(%d)\n", timeout_ms);
		nfds = this->wait(timeout_ms);
		//printf("dns_wait_resolve nfds(%d)\n", nfds);

		gettimeofday(&now, NULL);
		timeout -= (now.tv_sec - last.tv_sec) * 1000 + (now.tv_usec - last.tv_usec) / 1000;
		last = now;
	}

	if (error_ != "")
	{
		XCEPT_RAISE(toolbox::net::exception::Exception, error_);
	}

	return hostname_;
}
