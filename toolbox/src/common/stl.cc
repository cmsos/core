// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini                                    *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "toolbox/stl.h"
#include "toolbox/string.h"

size_t cifind( const std::string& str1, const std::string& str2, const std::locale& loc)
{
			auto to_upper = [&loc] (char ch) { return std::use_facet<std::ctype<char>>(loc).toupper(ch); };
			std::string s1 = str1;
			std::transform(s1.begin(), s1.end(), s1.begin(), to_upper);
			std::string s2 = str2;
			std::transform(s2.begin(), s2.end(), s2.begin(), to_upper);

                        typename std::string::const_iterator it = std::search( s1.begin(), s1.end(), s2.begin(), s2.end());
                        if ( it != s1.end() ) return it - s1.begin();
                        else return -1; // not found
}

