#include <iostream>
#include <string>
#include "toolbox/net/DNS.h"
#include "toolbox/exception/Timeout.h"
#include "toolbox/net/exception/UnresolvedAddress.h"

int main(int argc, char** argv)
{
	if (argc < 2)
	{
		std::cout << "Usage: " << argv[0] << " hostname " << std::endl;
		return 1;
	}

	try
	{
		std::string hostname = toolbox::net::DNS::hostName();
		std::cout << "running on  " << hostname << std::endl;
		auto resolved = toolbox::net::DNS::waitAndRetry(argv[1],10,1);
		std::cout << "found " << resolved.front().hostAddress() << std::endl;
	} 
	catch (toolbox::net::exception::UnresolvedAddress & e )
	{
		std::cout << "Exception: " << e.what() << std::endl;
	}
	catch (toolbox::net::exception::Exception& e )
	{
		std::cout << "Exception: " << e.what() << std::endl;
	}

	try
	{
		auto resolved = toolbox::net::DNS::retryForever(argv[1],30);
		std::cout << "found " << resolved.front().hostAddress() << std::endl;
	} 
	catch (toolbox::exception::Timeout & e )
	{
		std::cout << "Exception: " << e.what() << std::endl;
	}
	catch (toolbox::net::exception::UnresolvedAddress & e )
	{
		std::cout << "Exception: " << e.what() << std::endl;
	}
	catch (toolbox::net::exception::Exception& e )
	{
		std::cout << "Exception: " << e.what() << std::endl;
	}


	return 0;
}
