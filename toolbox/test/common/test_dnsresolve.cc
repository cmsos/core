#include <iostream>
#include <climits>
#include <map>
#include <unistd.h>
#include <limits>
#include <math.h>
#include <thread>
#include "toolbox/net/Utils.h"
#include "toolbox/net/c-ares.h"

const int count = 4;
const std::string dnsnames[count] = { "xdaqdev4", "xdaqdev5.cms", "kvm-s3562-1-ip151-66", "kvm-s3562-1-ip151-67.cms" };
const std::string hostnames[count] = { "kvm-s3562-1-ip151-69.cms", "kvm-s3562-1-ip156-64.cms", "kvm-s3562-1-ip151-66.cms", "kvm-s3562-1-ip151-67.cms" };

void getDNSHostName1()
{
	std::string hostname = "";
	std::string dnsname = "";
	for (int i = 0; i < count; i++)
	{
		dnsname = dnsnames[i];
		hostname = toolbox::net::getDNSHostName(dnsname);
		if (hostname != hostnames[i])
		{
			std::cout << "Error! dnsname = " << dnsname << ", hostname = " << hostname << std::endl;
		}
	}
}

void getDNSHostName2(toolbox::net::cares * dns)
{
	std::string hostname = "";
	std::string dnsname = "";
	for (int i = 0; i < count; i++)
	{
		dnsname = dnsnames[i];
		hostname = dns->resolve(dnsname);
		if (hostname != hostnames[i])
		{
			std::cout << "Error! dnsname = " << dnsname << ", hostname = " << hostname << std::endl;
		}
	}
}

int main()
{
	const int num_threads = 10;
	std::thread t[num_threads];

	//getDNSHostName1
	for (int i = 0; i < num_threads; ++i) {
		t[i] = std::thread(getDNSHostName1);
	}

	for (int i = 0; i < num_threads; ++i) {
		t[i].join();
	}

	//getDNSHostName2
	toolbox::net::cares * dns = new toolbox::net::cares();
	for (int i = 0; i < num_threads; ++i) {
		t[i] = std::thread(getDNSHostName2, dns);
	}

	for (int i = 0; i < num_threads; ++i) {
		t[i].join();
	}
	delete dns;

	std::cout << "Done." << std::endl;

	return 0;
}
