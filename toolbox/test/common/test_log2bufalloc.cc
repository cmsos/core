#include <iostream>
#define BUF_LOG2_64                     6       
#define BUFL_LOG2_128                   7
#define BUF_LOG2_256                    8
#define BUF_LOG2_512                    9
#define BUF_LOG2_1024                   10
#define BUF_LOG2_2048                   11
#define BUF_LOG2_4096                   12
#define BUF_LOG2_8192                   13
#define BUF_LOG2_16384                  14
#define BUF_LOG2_32768                  15
#define BUF_LOG2_65536                  16
#define BUF_LOG2_131072                 17
#define BUF_LOG2_262144                 18
#define BUF_LOG2_524288                 19
#define BUF_LOG2_1048576                20
#define BUF_LOG2_2097152                21
#define BUF_LOG2_4194304                22
#define BUF_LOG2_8388608                23
#define BUF_LOG2_16777216               24
#define BUF_LOG2_33554432               25 

#define BUF_SIZE_64                     64
#define BUF_SIZE_128                    128
#define BUF_SIZE_256                    256
#define BUF_SIZE_512                    512
#define BUF_SIZE_1024                   1024
#define BUF_SIZE_2048                   2048
#define BUF_SIZE_4096                   4096
#define BUF_SIZE_8192                   8192
#define BUF_SIZE_16384                  16384
#define BUF_SIZE_32768                  32768
#define BUF_SIZE_65536                  65536
#define BUF_SIZE_131072                 131072
#define BUF_SIZE_262144                 262144
#define BUF_SIZE_524288                 524288
#define BUF_SIZE_1048576                1048576
#define BUF_SIZE_2097152                2097152
#define BUF_SIZE_4194304                4194304
#define BUF_SIZE_8388608                8388608
#define BUF_SIZE_16777216               16777216
#define BUF_SIZE_33554432               33554432 

#define BUF_LOG2_MIN                    BUF_LOG2_64
//#define BUF_LOG2_MAX                  BUF_LOG2_4194304
//#define BUF_LOG2_MAX                  BUF_LOG2_8388608
#define BUF_LOG2_MAX                    BUF_LOG2_33554432
#define BUF_SIZE_MAX                    (1 << BUF_LOG2_MAX)
#define BUF_SIZE_MIN                    (1 << BUF_LOG2_MIN)
//#define BUF_INDX_MIN                  0
//#define BUF_INDX_MAX                  (BUF_LOG2_MAX - BUF_LOG2_MIN)

// The table indexes directly all pool entries by log2 number, From 0 to the maximum number allowed BUF_LOG2_MAX
#define BUF_TBL_SIZE                    ((BUF_LOG2_MAX + 1) - (BUF_LOG2_MIN))

#define BUF_INDEX_TO_LOG2(x)                    (BUF_LOG2_MIN + (x))
#define BUF_LOG2_TO_BUF_INDEX(x)                ((x) - BUF_LOG2_MIN)
#define BUF_LOG2_TO_BUF_SIZE(x)                 (1 << (x))
#define LMAX(x,y) (x>y?x:y)
#define BUF_SIZE_TO_LOG2(size)                  (log2_roundup(LMAX(size,BUF_SIZE_MIN)))
#define BUF_SIZE_TO_BUF_INDEX(bufSize)          (log2_roundup(LMAX(bufSize,BUF_SIZE_MIN)) - (BUF_LOG2_MIN))

#define MAX_ENTRIES_SIZE               65536

const unsigned char log2_roundup_table[257] = {
  0,
  0, 1, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4,
  5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5,
  6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
  6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
  7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
  7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
  7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
  7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
  8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
  8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
  8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
  8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
  8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
  8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
  8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
  8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
};

size_t log2_roundup (size_t n)
{
  size_t ret;

  if (n <= 0x100)
    ret = log2_roundup_table[n] + 0;
  else
    {
      n = (n+0xff)>>8 & 0xffffff;
      if (n <= 0x100)
        ret = log2_roundup_table[n] + 8;
      else
        {
          n = (n+0xff)>>8;
          if (n <= 0x100)
            ret = log2_roundup_table[n] + 16;
          else
            {
              n = (n+0xff)>>8;
              ret = log2_roundup_table[n] + 24;
            }
        }
    }
  return ret;
}

main()
{
	for (size_t i=0; i < BUF_TBL_SIZE; i++ )
        {
                std::cout << "inserting pool entry for size :" << BUF_LOG2_TO_BUF_SIZE(BUF_INDEX_TO_LOG2(i)) << "on :" << i  << std::endl;
        }

	std::cout << " ---> allocate for size 48000000 index is: " << BUF_SIZE_TO_BUF_INDEX(48000000) << std::endl;

	std::cout << " ---> max table size is " << BUF_TBL_SIZE << std::endl;
	std::cout << " ---> max buf size is " << BUF_SIZE_MAX << std::endl;
}

