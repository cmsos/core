package
{
	import mx.collections.*;
	import mx.controls.treeClasses.*;
	import mx.controls.treeClasses.TreeItemRenderer;
	
	public class TreeNodeRenderer extends TreeItemRenderer
	{
		public function TreeNodeRenderer()
		{
			super();
		}
		
		
		// Override the set method for the data property
		// to set the font color and style of each node.        
		override public function set data(value:Object):void {
			super.data = value;
			
			if(super.data)
			{
				
				var numActive:Number = (value as Node).totalCount;
				
				if ( numActive > 0 )
				{
					setStyle("color", 0xff0000);
					setStyle("fontWeight", 'bold');
				}
				else
				{
					setStyle("color", 0x000000);
					setStyle("fontWeight", 'normal');
				}  
			}
			
		}
		
		// Override the updateDisplayList() method 
		// to set the text for each tree node.      
		override protected function updateDisplayList(unscaledWidth:Number, 
													  unscaledHeight:Number):void {
			
			super.updateDisplayList(unscaledWidth, unscaledHeight);
			
			if(super.data)
			{
				var numActive:Number = super.data.totalCount;
				var numAck:Number = super.data.totalAckCount;
				var pathId:Number = super.data.pathId;
				
				super.label.text =  TreeListData(super.listData).label 
					+ /* "[" + pathId + "]" 		+ */"(" + numActive + "/" + numAck + ")";
				
				//
			}
			
		}
	}
	
	
}