/**
 * This is a generated sub-class of _Xmasadmin.as and is intended for behavior
 * customization.  This class is only generated when there is no file already present
 * at its target location.  Thus custom behavior that you add here will survive regeneration
 * of the super-class. 
 **/
 
package services.xmasadmin
{

public class Xmasadmin extends _Super_Xmasadmin
{
    /**
     * Override super.init() to provide any initialization customization if needed.
     */
    protected override function preInitializeService():void
    {
        super.preInitializeService();
        // Initialization customization goes here
    }
	public function configureBaseURL(url:String):void
	{
		// initialize service control
		_serviceControl.baseURL = url;
	}              
}

}
