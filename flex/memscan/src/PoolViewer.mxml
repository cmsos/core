<?xml version="1.0" encoding="utf-8"?> 
<!-- Component containing the interface elements that compose the Pool Viewer tab -->
<mx:Canvas xmlns:mx="http://www.adobe.com/2006/mxml" width="100%" height="100%" label="Pool Viewer">
	<mx:Script>
			<![CDATA[
				import mx.rpc.events.ResultEvent;
            	import mx.rpc.events.FaultEvent;
            	import mx.messaging.messages.HTTPRequestMessage;
            	import mx.controls.Alert;
				
				private var memProbeUrl:String;
				
            	
            	// Request the list of memory pools from an XMem Probe URL
				// host: full URL of server hosting the memory pools, e.g. http://srv-c2d06-12.cms:1972/urn:xdaq-application:lid=254
	            public function requestMemoryPools(host:String):void
	            {
	            	memProbeUrl = host;
	            	
	            	// Check if host URL does not already end with '/'
	            	var index:int = host.indexOf("/", host.length - 1);
	            	if (index == -1)
	            		host += "/";
	            	
	            	requestPools.url = host + "listPools";
	            	requestPools.send();
	            }


	            // Send request for data on a memory pool
		        // name: name of the memory pool
		        // host: server hosting the memory pool
		        private function requestMemoryPoolData(name:String, host:String):void
		        {
		        	// Check if host URL does not already end with '/'
		        	var index:int = host.indexOf("/", host.length - 1);
		        	if (index == -1)
		        		host += "/";
		        	
		        	requestPoolData.url = host + 'displayUsage?name=' + name;
		            requestPoolData.send();
		        }

		        
		        // Re-requests the list of memory pools on the service
		        private function refreshPoolsList():void
		        {
		        	if (memProbeUrl.length > 0)
		        		requestMemoryPools(memProbeUrl);
		        }
		        
		        
		        // Re-requests data on on the selected memory pool
				private function refreshCurrentPoolSelection():void
	            {
	            	if (memProbeUrl.length > 0 && poolsTree.selectedIndex >= 0)
	            		requestMemoryPoolData(poolsTree.selectedItem.@name, memProbeUrl);
	            }
				
	            
	            // Received XML data concerning a specific memory pool
	            private function requestPoolData_result(evt:ResultEvent):void
	            {
	            	xmlPoolSummary = XML(evt.result).summary.statistic;
	                xmlPoolData = XML(evt.result).blocks.block;
	                xmlMemoryUsage = XML(evt.result).usage.statistic;
	            }
	            
	            
	            // Callback for an error encountered by a HTTPService request
	            private function httpService_fault(evt:FaultEvent):void
	            {
	                var title:String = evt.type + " (" + evt.fault.faultCode + ")";
	                var text:String = evt.fault.faultString + "\n" + HTTPRequestMessage(evt.token.message).url;
	                Alert.show(text, title);
	            }
	            
	            
	            // Received XML list of memory pools
	            private function requestPools_result(evt:ResultEvent):void
	            {
	                xmlPoolsList = XML(evt.result).pool;
	                poolsTree.dataProvider = xmlPoolsList;
	            }
			]]>
	</mx:Script>
	<mx:XMLList id="xmlPoolsList" />				<!-- list of pools on the selected service -->
    <mx:XMLList id="xmlPoolSummary" />				<!-- data summarrising the selected memory pool -->
    <mx:XMLList id="xmlPoolData" />					<!-- data on memory allocation of the selected pool -->
    <mx:XMLList id="xmlMemoryUsage" />				<!-- to show used vs formatted free vs unformatted free -->
    
	<!-- Connection to retrieve pool catalog -->
	<mx:HTTPService id="requestPools"
            resultFormat="e4x"
            fault="httpService_fault(event);"
            result="requestPools_result(event)" />
            
	<!-- Connection to retrieve data on a specific pool -->
    <mx:HTTPService id="requestPoolData"
            resultFormat="e4x"
            fault="httpService_fault(event);"
            result="requestPoolData_result(event)" />
            
	<mx:HDividedBox x="0" y="30" height="100%" width="100%">
		<mx:Tree id="poolsTree" width="325" height="100%" labelField="@name" change="refreshCurrentPoolSelection();" dataProvider="{xmlPoolsList}" />
		<mx:TabNavigator width="100%" height="100%">
			<mx:Canvas label="Summary" width="100%" height="100%">
				<mx:VBox width="100%" x="10" y="10">
					<mx:DataGrid id="summaryGrid" width="50%" height="100%" rowCount="8" dataProvider="{xmlPoolSummary}" >
						<mx:columns>
							<mx:DataGridColumn headerText="Label" dataField="label" />
							<mx:DataGridColumn headerText="Value" dataField="value" />
						</mx:columns>
					</mx:DataGrid>
				</mx:VBox>
			</mx:Canvas>
			<mx:Canvas label="Memory Format" width="100%" height="100%">
				<mx:PieChart x="29" y="20" width="90%" height="90%" id="memoryFormatChart" showDataTips="true" dataProvider="{xmlPoolData}">
					<mx:series>
						<mx:PieSeries field="bytesUsed" nameField="label" displayName="Size" />
					</mx:series>
				</mx:PieChart>
				<mx:Legend dataProvider="{memoryFormatChart}" y="20"/>
			</mx:Canvas>
			<mx:Canvas label="Number of Block" width="100%" height="100%">
				<mx:HBox y="20" width="100%" height="100%">
					<mx:ColumnChart id="blocksChart" width="100%" type="stacked" showDataTips="true" dataProvider="{xmlPoolData}">
						<mx:horizontalAxis>
							<mx:CategoryAxis id="blocksChartCatAxis" categoryField="label" dataProvider="{xmlPoolData}"/>
						</mx:horizontalAxis>
						<mx:series>
							<mx:ColumnSeries xField="label" yField="blocksUsed" displayName="Memory Allocated">
								<mx:fill>
									<mx:SolidColor color="0xcc3333" />
								</mx:fill>
							</mx:ColumnSeries>
							<mx:ColumnSeries xField="label" yField="blocksFree" displayName="Memory Free">
								<mx:fill>
									<mx:SolidColor color="0x33bb33" />
								</mx:fill>
							</mx:ColumnSeries>
						</mx:series>
					</mx:ColumnChart>
					<mx:Legend dataProvider="{blocksChart}" />
				</mx:HBox>
			</mx:Canvas>
			<mx:Canvas label="Memory Usage" width="100%" height="100%">
				<mx:PieChart x="48" y="10" width="90%" height="90%" id="memoryUsageChart" showDataTips="false" showAllDataTips="true" dataProvider="{xmlMemoryUsage}">
					<mx:series>
						<mx:PieSeries field="bytes" nameField="label" displayName="Size" />
					</mx:series>
				</mx:PieChart>
				<mx:Legend dataProvider="{memoryUsageChart}" y="20"/>
			</mx:Canvas>
		</mx:TabNavigator>
	</mx:HDividedBox>
	<mx:HBox width="100%">
		<mx:Button id="reloadPoolsList" label="Reload Memory Pools List" click="refreshPoolsList();" />
		<mx:Button id="refreshSelection" label="Refresh Current Selection" click="refreshCurrentPoolSelection();" />
	</mx:HBox>
</mx:Canvas>
