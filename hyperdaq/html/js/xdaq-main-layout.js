/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: A.Forrest, L.Orsini, A.Petrucci								 *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

function xdaqHeaderFooterStick() 
{
	// Store the main divs in local variables for convienience
	var sidebar = $('#xdaq-sidebar');
	var header = $('#xdaq-header');
	var content = $('#xdaq-main');
	var footer = $('#xdaq-footer');
				
	//////////
	// Footer position
	//////////
	
	// Set the height to auto to force an auto height calculation (or the page will not be able to decrease in size)
	content.css({'height': 'auto'});
	
	// Height of the user content area
	contentHeight = content.outerHeight();
	
	// Combined height of the header and footer
	headFootHeight = header.outerHeight() + footer.outerHeight();
	
	// if content doesnt need scrolling, force content height to be full height
	if (contentHeight < $(window).height() - headFootHeight)
	{
		content.css({'height': $(window).height() - headFootHeight});
	}
			
	//////////
	// Header and footer width
	//////////
				
	// make sure header and footer are the right size allowing for user content to be wider than the viewport
	
	header.css({'width': 'auto'});
	footer.css({'width': 'auto'});
	
	// viewport width
	var minWidth = $(window).width();
	
	// if the user content plus the sidebar (inc padding) is wider than the viewport, use that width
	var wrapper = content.parent().width();
	if (minWidth < wrapper)
	{
		minWidth = wrapper;
	}

	// Set the css
	header.css({'width': minWidth});
	footer.css({'width': minWidth});
}

function xdaqSidebarStick() 
{
	// Store the main divs in local variables for convienience
	var sidebar = $('#xdaq-sidebar');
	var header = $('#xdaq-header');
	var content = $('#xdaq-main');
	var footer = $('#xdaq-footer');
	
	//////////
	// Sidebar height and position
	//////////
	
	// Get the vertical scroll amount, then check to see if this is more than the header amount
	visibleHeader = $(document).scrollTop();
	if (visibleHeader > header.outerHeight())
	{
		visibleHeader = header.outerHeight()
	}
	
	// Set the position of the sidebar to be the height of the header minus the amount of the header hidden from view (up to 100% of the header height)
	newTop = header.outerHeight() - visibleHeader;
			
	// Compensate for the change in position with a change in height
	newHeight = $(window).height() - newTop;
	
	// Set the new height and position
	sidebar.css({'height': newHeight, 'top': newTop});
			
	xdaqHeaderFooterStick();
}

function xdaqBuildLayout()
{
	$(window).on("scroll", function() {
		xdaqSidebarStick();
	});
	$(window).on("resize", function() {
		xdaqSidebarStick();
	});
}