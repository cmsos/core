var len = 10000;
var position = 0;

var callback = "";

var outOfRange = false;

// Get the callback URL
//function xdaqWindowPostLoad()
//{
$(document).on("xdaq-post-load", function(){
	callback = $("#xdaq-logviewer-wrapper").attr("data-callback");
	
	if (callback == "")
	{
		console.error("Failed to get URL for callback");
	}	
	else
	{
		callback = callback + "/getLogFile?";
		
		logFrameLoad('url=' + $("#xdaq-log-viewer-file").val() + '&tail=' + Number($("#xdaq-log-viewer-length").val()), "tail");
	}
});

function checkLength() 
{ 
	len = Number($("#xdaq-log-viewer-length").val());
}

function decrementPosition() 
{ 
	checkLength();
	position = ((position-len) <= 0) ? 0 : (position-len);
}

function incrementPosition() 
{ 
	checkLength();
	position += len;
}

function viewAll()
{
	position = 0;			
	logFrameLoad('url=' + $("#xdaq-log-viewer-file").val(), "all");
}

function viewTail()
{
	position = 0;
	logFrameLoad('url=' + $("#xdaq-log-viewer-file").val() + '&tail=' + Number($("#xdaq-log-viewer-length").val()), "tail");
}

function viewHead()
{
	position = 0;
	logFrameLoad('url=' + $("#xdaq-log-viewer-file").val() + '&head=' + Number($("#xdaq-log-viewer-length").val()), "head");
}

function viewForward()
{
	incrementPosition();
	logFrameLoad('url=' + $("#xdaq-log-viewer-file").val() + '&from=' + position + '&len=' + Number($("#xdaq-log-viewer-length").val()), "forward");
}

function viewBackward()
{
	decrementPosition();
	logFrameLoad('url=' + $("#xdaq-log-viewer-file").val() + '&from=' + position + '&len=' + Number($("#xdaq-log-viewer-length").val()), "backward");
}

// operation is the operation type. "forward" == dec on repsonse length = 0, "tail" == position set to reponse length
function logFrameLoad(params, operation) 
{
	var options = {
		url: callback+params,
		error: function (xhr, textStatus, errorThrown) {
			$("#xdaq-logviewer-textarea").html("Failed to open log : " + callback+params);
		}
	};
	
	xdaqAJAX(options, function (data, textStatus, xhr) {
		$("#xdaq-logviewer-textarea").html(xhr.responseText);
		if (operation == "forward" && xhr.responseText.length == 0)
		{
			// too far forward
			//decrementPosition();		
			if (!outOfRange)
			{
				outOfRange = true;
			}
			else
			{
				decrementPosition();
			}
		}
		else
		{
			outOfRange = false;
		}
		if (operation == "forward" && xhr.responseText.length < len)
		{
			// too far forward
			decrementPosition();	
			position = position + (len - xhr.responseText.length);
		}	
	});	
}