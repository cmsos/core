

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simlevicius					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <thread>

#include "shub/Node.h"
#include "shub/Node.h"

#include "xcept/Exception.h"
#include "xcept/tools.h"

#include "toolbox/BSem.h"
#include "toolbox/rlist.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/CommittedHeapAllocator.h"
#include "toolbox/task/TimerTask.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"

#include "xdaq/Application.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationGroup.h"
#include "xoap/MessageFactory.h"
#include "xdaq/NamespaceURI.h"
#include "xdaq/pipe/Input.h"
#include "xdaq/pipe/Output.h"
#include "xdaq/pipe/Interface.h"

#include "xgi/Method.h"
#include "xgi/framework/Method.h"

XDAQ_INSTANTIATOR_IMPL(shub::Node)

//#define MESSAGE_SIZE 8196 
#define MESSAGE_SIZE 131072 

shub::Node::Node (xdaq::ApplicationStub* c): xdaq::Application(c), xgi::framework::UIManager(this)
{
    poolName_ =  "tpi";
    c->getDescriptor()->setAttribute("icon", "/shub/images/node.png");
    c->getDescriptor()->setAttribute("icon16x16", "/shub/images/node.png");
    
    this->getApplicationInfoSpace()->fireItemAvailable("poolName", &poolName_);
    this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
    
    this->maxReceiveBuffers_ = 8;
    this->maxBlockSize_ = 4096;
    this->ioQueueSize_ = 4096;
    getApplicationInfoSpace()->fireItemAvailable("maxReceiveBuffers", &maxReceiveBuffers_);
    getApplicationInfoSpace()->fireItemAvailable("maxBlockSize", &maxBlockSize_);
    getApplicationInfoSpace()->fireItemAvailable("ioQueueSize", &ioQueueSize_);
    counter_ = 0;
    currentFragmentId_ = 0;
    enabled_ = false;
}

shub::Node::~Node()
{
}


void shub::Node::actionPerformed (xdata::Event& event)
{
    if (event.type() == "urn:xdaq-event:setDefaultValues")
    {
        try
        {
            counter_ = 0;
            
            destination_ = getApplicationContext()->getDefaultZone()->getApplicationDescriptors("shub::Node");
            servers_ = new xdaq::ApplicationDescriptor*[destination_.size()];
            for (size_t v = 0; v <destination_.size(); v++ )
            {
                servers_[v] = 0;
                resources_.push_back(0);
            }
            
            std::set<const xdaq::ApplicationDescriptor*>::iterator iter;
            for (iter = destination_.begin(); iter != destination_.end(); iter++)
            {
                size_t inum = (*iter)->getInstance();
                if ( servers_[inum] != 0 )
                {
                    XCEPT_RAISE(xdaq::exception::Exception, "instance already exisiting for node");
                }
                
                servers_[inum] = const_cast<xdaq::ApplicationDescriptor*>(*iter);
                LOG4CPLUS_INFO(getApplicationLogger(), "Found Node " <<  (*iter) ->getContextDescriptor()->getURL() <<  "/" <<  (*iter)->getURN() << " instance " << (*iter)->getInstance() );
                
                if ( this->getApplicationDescriptor()->getInstance() != (*iter)->getInstance() ) // found remote node
                {
                    connections_[servers_[inum]] = ConnectionStatus::idle;
                }
                else
                {
                    // loopback pipe always connected
                    connections_[servers_[inum]] = ConnectionStatus::established;
                    
                }
            }
            
            replicas_ =  destination_.size();
            events_ = new shub::Events(replicas_);
            LOG4CPLUS_INFO(getApplicationLogger(), "Found " << replicas_ << " replica Nodes");
        }
        catch (xdaq::exception::Exception& e)
        {
            LOG4CPLUS_FATAL(getApplicationLogger(), xcept::stdformat_exception_history(e));
            return;
        }
        
        try
        {
            std::string pname = poolName_.toString();
            toolbox::net::URN urn("toolbox-mem-pool", pname);
            pool_ = toolbox::mem::getMemoryPoolFactory()->findPool(urn);
            
        }
        catch (toolbox::mem::exception::Exception & e)
        {
            LOG4CPLUS_FATAL(getApplicationLogger(), xcept::stdformat_exception_history(e));
            return;
        }
        
        // Available buffer for Local Node input
        freedatalist_ = toolbox::rlist<toolbox::mem::Reference*>::create("shub-node-freedatalist", ioQueueSize_ );
        for (size_t k = 0; k < this->maxReceiveBuffers_  * replicas_; k++)
        {
            toolbox::mem::Reference * reference = 0;
            
            try
            {
                reference = toolbox::mem::getMemoryPoolFactory()->getFrame(pool_, this->maxBlockSize_);
            }
            catch (toolbox::mem::exception::Exception & e)
            {
                LOG4CPLUS_FATAL(getApplicationLogger(), toolbox::toString("Could not allocate block for input, %s", e.what()));
            }
            
            freedatalist_->push_back(reference);
        }
        // Available data for remote Node input
        freeinputlist_ = toolbox::rlist<toolbox::mem::Reference*>::create("shub-node-freeinputlist", ioQueueSize_ );
        for (size_t k = 0; k < this->maxReceiveBuffers_  * replicas_; k++)
        {
            toolbox::mem::Reference * reference = 0;
            
            try
            {
                reference = toolbox::mem::getMemoryPoolFactory()->getFrame(pool_, this->maxBlockSize_);
            }
            catch (toolbox::mem::exception::Exception & e)
            {
                LOG4CPLUS_FATAL(getApplicationLogger(), toolbox::toString("Could not allocate block for input, %s", e.what()));
            }
            
            freeinputlist_->push_back(reference);
        }
        
        // create input support
        idata_ = toolbox::rlist<toolbox::mem::Reference*>::create("shub-node-idata", ioQueueSize_ );
        idatafree_ = toolbox::rlist<toolbox::mem::Reference*>::create("shub-node-idatafree", ioQueueSize_ );
        
        // create loopback pipes
        auto io = toolbox::rlist<toolbox::mem::Reference*>::create("shub-node-loopback-io", ioQueueSize_ );
        auto cq = toolbox::rlist<toolbox::mem::Reference*>::create("shub-node-loopback-cq", ioQueueSize_ );
        ilb_ = new shub::InputLoopback(new shub::Input(io,cq), 0);
        olb_ = new shub::OutputLoopback(new shub::Output(io,cq), 0);
        opipes_[this->getApplicationDescriptor()] = olb_;
        
        /* It might not be necessary
        for (size_t k = 0; k < this->maxReceiveBuffers_; k++)
        {
            toolbox::mem::Reference * reference = 0;
            
            try
            {
                reference = toolbox::mem::getMemoryPoolFactory()->getFrame(pool_, this->maxBlockSize_);
            }
            catch (toolbox::mem::exception::Exception & e)
            {
                LOG4CPLUS_FATAL(getApplicationLogger(), toolbox::toString("Could not allocate block for input, %s", e.what()));
            }
            
            ilb_->post(reference);
            
        }
        */

        ipipes_.push_back(ilb_);
        
        pipeService_ = xdaq::getPipeInterface()->join(this,this);
        
        // start connector thread
        std::thread thc(&Node::connect, this, "connector loop");
        thc.detach();
        
        // start shuffle thread
        std::thread ths(&Node::shuffle, this, "shuffle loop");
        ths.detach();
        
        // start builder thread
        std::thread thb(&Node::builder, this, "builder loop");
        thb.detach();
        
        // start input generator thread
        std::thread thi(&Node::input, this, "input generator loop");
        thi.detach();
        
        // start measurement generator thread
        std::thread thm(&Node::measure, this, "benchmark loop");
        thm.detach();
    }
}

//
// Connection error handling
//

void shub::Node::actionPerformed(xdaq::pipe::ConnectionError & ce ) 
{
    std::lock_guard<std::mutex> guard(omutex_);
    //std::cout << "Connection error from " << ce.getSourceDescriptor()->getURN() <<  " to " <<  ce.getDestinationDescriptor()->getURN()<< std::endl;
    const xdaq::ApplicationDescriptor* d = ce.getDestinationDescriptor();
    std::string durl = d->getContextDescriptor()->getURL() + "/" + d->getURN();
    LOG4CPLUS_WARN(getApplicationLogger(), "Connection failed for " << durl <<  ", with error " << ce.getException().what());
    connections_[d] = ConnectionStatus::idle;
    // disable input generator
    enabled_ = false;
}


void shub::Node::actionPerformed(xdaq::pipe::BrokenPipe & bp)
{
    LOG4CPLUS_WARN(getApplicationLogger(), "Broken Pipe, shoud destroy output pipe here");
    std::lock_guard<std::mutex> guard(omutex_);
    
    for ( std::map<const xdaq::ApplicationDescriptor*, xdaq::pipe::Output*>::iterator it = opipes_.begin(); it != opipes_.end(); it++ )
    {
        if ( (*it).second->match(bp.getPipeHandle()))
        {
            LOG4CPLUS_DEBUG(getApplicationLogger(),"Found matching output pipe, going to destroy") ;
            
            xdaq::pipe::Output * opipe = (*it).second;
            connections_[(*it).first] = ConnectionStatus::idle;
            opipes_.erase(it);
            pipeService_->destroyOutputPipe(opipe);
            // disable input generator
            enabled_ = false;
            return;
        }
    }
}

void shub::Node::actionPerformed(xdaq::pipe::ConnectionClosedByPeer & ccbp )
{
    std::lock_guard<std::mutex> guard(imutex_);
    
    LOG4CPLUS_WARN(getApplicationLogger(), "Connection closed by peer in server going to delete input pipe from input list");
    for ( auto it = ipipes_.begin(); it != ipipes_.end(); it++ )
    {
        if ( (*it)->match(ccbp.getPipeHandle()))
        {
            LOG4CPLUS_WARN(getApplicationLogger(), "Found matching input pipe, going to destroy");
            xdaq::pipe::Input * ipipe = *it;
            ipipes_.erase(it);
            pipeService_->destroyInputPipe(ipipe);
            // disable input generator
            enabled_ = false;
            return;
        }
    }
    
}

//
// Callbacks for establishing connection to/and from remote
//

bool shub::Node::actionPerformed(xdaq::pipe::ConnectionRequest & cr)
{
    LOG4CPLUS_INFO(getApplicationLogger(),"Server - Connection request received ");

    xdaq::pipe::Input * ipipe =  pipeService_->createInputPipe(cr);
    
    size_t blockSize = this->maxBlockSize_;
    for (size_t k = 0; k < this->maxReceiveBuffers_; k++)
    {
        toolbox::mem::Reference * reference = 0;
        
        try
        {
            reference = toolbox::mem::getMemoryPoolFactory()->getFrame(pool_, blockSize);
        }
        catch (toolbox::mem::exception::Exception & e)
        {
            LOG4CPLUS_FATAL(getApplicationLogger(), toolbox::toString("Could not allocate block for input, %s", e.what()));
        }
        
        ipipe->post(reference);
        
    }
    
    std::lock_guard<std::mutex> guard(imutex_);
    ipipes_.push_back(ipipe);
    return true;
}

void shub::Node::actionPerformed(xdaq::pipe::EstablishedConnection & ec )
{
    std::lock_guard<std::mutex> guard(omutex_);
    
    LOG4CPLUS_DEBUG(getApplicationLogger(), "Connection is established from " << ec.getSourceDescriptor()->getURN() <<  " to " <<  ec.getDestinationDescriptor()->getURN());
    
    opipes_[ec.getDestinationDescriptor()] = pipeService_->createOutputPipe(ec);
    connections_[ec.getDestinationDescriptor()] = ConnectionStatus::established;
}

//
// Work threads for connection, input, shuffling and building
//
void shub::Node::connect(std::string name)
{
    LOG4CPLUS_DEBUG (this->getApplicationLogger(), name);
    while(1)
    {
        ::sleep(5);
        std::lock_guard<std::mutex> guard(omutex_);
        
        std::string surl = this->getApplicationContext()->getContextDescriptor()->getURL() + "/" + this->getApplicationDescriptor()->getURN();
        
        for (unsigned int i = 0; i < replicas_; i++)
        {
            xdaq::ApplicationDescriptor* d = servers_[i];
            if ( connections_[d] == ConnectionStatus::idle )
            {
                std::string durl = d->getContextDescriptor()->getURL() + "/" + d->getURN();
                LOG4CPLUS_INFO(getApplicationLogger(), "requesting connection from " << surl << " to " << durl);
                connections_[d]  = ConnectionStatus::connecting;
                pipeService_->connect(this->getApplicationDescriptor(), d);
                LOG4CPLUS_DEBUG(getApplicationLogger(), "done requesting connection");
            }
        }
    }
}

void shub::Node::input(std::string name)
{
    LOG4CPLUS_DEBUG(getApplicationLogger(), "starting input generation loop...");
    size_t len = MESSAGE_SIZE;
    for(;;)
    {
        // reset for a rough synchonization
        currentFragmentId_ = 0;
        
        bool established = false;
        while ( ! established )
        {
            LOG4CPLUS_INFO(getApplicationLogger(), "waiting nodes connection...");
            ::sleep(5);
            
            std::lock_guard<std::mutex> guardi(imutex_);
            std::lock_guard<std::mutex> guardo(omutex_);
            
            
            if ( opipes_.size() == replicas_  && ipipes_.size() == replicas_ )
            {
                established = true;
            }
        }
        
        LOG4CPLUS_INFO(getApplicationLogger(), "all nodes connected");
        sleep(5);
        enabled_ = true;
        while (enabled_)
        {
            LOG4CPLUS_TRACE(getApplicationLogger(), "input - recycle frames or fill with new frames from pool");
            while ( ! idatafree_->empty() )
            {
                LOG4CPLUS_TRACE(getApplicationLogger(), "input - going to input recycled fragment");
                toolbox::mem::Reference * ref = idatafree_->front();
                idatafree_->pop_front();
                
                //std::cout << " Node post frame of size" << ref->getDataSize() << " offset " << ref->getDataOffset() << std::endl;
                shub::FragmentDescriptor * fragment = (FragmentDescriptor*)ref->getDataLocation();
                fragment->id = currentFragmentId_;
                fragment->node = this->getApplicationDescriptor()->getInstance();
                fragment->resource = 0;
                ref->setDataSize(len);
                LOG4CPLUS_TRACE(getApplicationLogger(), "input - going to input recycled fragment with fragmentid " << currentFragmentId_  );
                idata_->push_back(ref);
                currentFragmentId_++;
                //::sleep(10);
            }
            LOG4CPLUS_TRACE(getApplicationLogger(), "input - idatafree is empty sleep a bit");
            //::sleep(1);
        } // input enabled loop
        LOG4CPLUS_WARN(getApplicationLogger(), "input - input generator was disabled");
        
    } // forever loop
    
    return;
}


void shub::Node::shuffle(std::string name)
{
    while(1)
    {
        std::lock_guard<std::mutex> guard(omutex_);
        
        // Free blocks for Node local input
        while ( ! freedatalist_->empty() )
        {
            toolbox::mem::Reference * r = freedatalist_->front();
            freedatalist_->pop_front();
            idatafree_->push_back(r);
            LOG4CPLUS_TRACE(getApplicationLogger(), "shuffle -  available free buffer for local node input");
        }
        
        // Local node input data processing
        if ( ! idata_->empty() )
        {
            toolbox::mem::Reference * ref = idata_->front();
            idata_->pop_front();
            
            //std::cout << "--- Node --- >>> received size  " << ref->getDataSize()  << " offset " << ref->getDataOffset() << std::endl;
            FragmentDescriptor * fragment = (FragmentDescriptor*)ref->getDataLocation();
            
            // shuffle fragment id to corresponding Node
            size_t destination = fragment->id  % replicas_;
            LOG4CPLUS_DEBUG(getApplicationLogger(), "shuffle - Found input fragmentid " << fragment->id << "  for destination " << destination  );
            xdaq::ApplicationDescriptor *remoteNode = servers_[destination];
            
            LOG4CPLUS_TRACE(getApplicationLogger(), "shuffle - matching descriptor on instance " << remoteNode->getInstance() );
            
            
            auto oit = opipes_.find(remoteNode);
            if ( oit != opipes_.end() )
            {
                LOG4CPLUS_TRACE(getApplicationLogger(), "shuffle - posting send fragmentid " << fragment->id << " to outputpipe on destination instance  " << remoteNode->getInstance() << " matching " << destination);
                oit->second->post(ref);  // send frame to node
            }
            else
            {
                std::cout << "*** warning: frame cannot be send to output , build will be incompleted " << std::endl;
                idatafree_->push_back(ref);
            }
        }
        
        // recycles completed frames
        for (auto oit = opipes_.begin() ; oit != opipes_.end(); oit++ )
        {
            while ( ! oit->second->empty() )
            {
                toolbox::mem::Reference * r = oit->second->get();
                //std::cout << "Shuffle recycle buffer in pool " << std::hex << (size_t)r << std::dec << " offset " << r->getDataOffset() << " original size " << r->getBuffer()->getSize() << std::endl;
                LOG4CPLUS_TRACE(getApplicationLogger(), "shuffle -  recycle output completed frame send in freeinputlist_");
                freeinputlist_->push_back(r);
            }
        }
    }
}


void shub::Node::builder(std::string name)
{
    while(1)
    {
        std::lock_guard<std::mutex> guard(imutex_);
        
        //for (auto i =  ipipes_.begin(); i != ipipes_.end(); i++ )
        for (size_t i =  0; i < ipipes_.size(); i++ )
        {
            if ( (! ipipes_[i]->empty()) && ( resources_[i] < maxReceiveBuffers_ ))
            {
                resources_[i]++;
                toolbox::mem::Reference * ref = ipipes_[i]->get(); // receive from remote node (or loopack)
                
                // pop a new frame to replace received
                if ( ! freeinputlist_->empty() )
                {
                    //std::cout << " pop a new frame to replace received" << std::endl;
                    toolbox::mem::Reference * nref = freeinputlist_->front();
                    freeinputlist_->pop_front();
                    ipipes_[i]->post(nref);
                }
                else
                {
                    LOG4CPLUS_ERROR(getApplicationLogger(), "failed to allocate buffer for input, out of memory" );
                    LOG4CPLUS_ERROR(getApplicationLogger(), "resource num is " << resources_[i] << " maxReceiveBuffers_ " << maxReceiveBuffers_ );
                    exit(0);
                }
                //std::cout << "--- Node --- >>> received size  " << ref->getDataSize()  << " offset " << ref->getDataOffset() << std::endl;
                FragmentDescriptor * fragment = (FragmentDescriptor*)ref->getDataLocation();
                LOG4CPLUS_DEBUG(getApplicationLogger(), "builder - going to build event with fragmentid " << fragment->id << " from node " << fragment->node << " total needed " << replicas_ << " total ipipes " << ipipes_.size() );
                LOG4CPLUS_DEBUG(getApplicationLogger(), "resource num is " << resources_[i] << " maxmaxReceiveBuffers_" << maxReceiveBuffers_  );
                if ( events_->append(fragment->id, fragment->node, ref) ) // completed
                {
                    LOG4CPLUS_DEBUG(getApplicationLogger(), "builder - completed event for " << fragment->id  );
                    
                    counter_++;
                    std::list<toolbox::mem::Reference *> event = events_->get(fragment->id);
                    LOG4CPLUS_TRACE(getApplicationLogger(), "builder - recycle frame for fragment " << fragment->id << " num fragments " << event.size()  );
                    size_t index = 0;
                    for ( auto r : event)
                    {
                        freedatalist_->push_back(r); // done with data, give back for local node input data
                        resources_[index++]--;
                    }
                    events_->clear(fragment->id);
                }
                //char * frame = (char*)ref->getDataLocation();
                //toolbox::hexdump ((void*) frame, 16);
            }
        }
    }
}

void shub::Node::measure (std::string name)
{
    while(1)
    {
        ::sleep(4);
        toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();
        double delta = (double)now - (double)lastTime_;
        double rate = counter_ / delta;
        counter_ = 0;
        lastTime_ =  now;
        
        std::cout << "rate is " << rate <<  "Hz band " << (rate * MESSAGE_SIZE)/0x100000 << "MB/s" << std::endl;
        imutex_.lock();
        std::list<size_t> eventids = events_->get();
        std::pair<size_t,size_t> stats = events_->stats();
        std::cout << "pending events " << eventids.size() << "stats " << stats.first << " partial " << stats.second  << std::endl;
        imutex_.unlock();
    }
}


