// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "shub/Protocol.h"

std::string shub::statusToString(shub::ConnectionStatus status)
{
	switch (status)
    {
        case shub::ConnectionStatus::idle:
            return "idle";
            break;
        case shub::ConnectionStatus::connecting:
            return "connecting";
            break;
        case shub::ConnectionStatus::established:
            return "established";
            break;
        default:
            return "unknown";
        
    }
}
