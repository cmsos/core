/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simlevicius					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <thread>
#include <chrono>

#include "shub/Arbiter.h"
#include "shub/Protocol.h"

#include "xcept/Exception.h"
#include "xcept/tools.h"

#include "toolbox/BSem.h"
#include "toolbox/rlist.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/CommittedHeapAllocator.h"
#include "toolbox/task/TimerTask.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"


#include "xdaq/Application.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationGroup.h"
#include "xoap/MessageFactory.h"
#include "xdaq/NamespaceURI.h"
#include "xdaq/pipe/Input.h"
#include "xdaq/pipe/Output.h"
#include "xdaq/pipe/Interface.h"

#include "xgi/Method.h"
#include "xgi/framework/Method.h"

XDAQ_INSTANTIATOR_IMPL(shub::Arbiter)

shub::Arbiter::Arbiter (xdaq::ApplicationStub* c): xdaq::Application(c), xgi::framework::UIManager(this)
{
    c->getDescriptor()->setAttribute("icon", "/shub/images/arbiter.png");
    c->getDescriptor()->setAttribute("icon16x16", "/shub/images/arbiter.png");
    
    poolName_ =  "tpi";
    this->getApplicationInfoSpace()->fireItemAvailable("poolName", &poolName_);
    this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
    this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:ParameterSet");
    this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:ParameterGet");
    this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:ParameterQuery");
    
    this->getApplicationContext()->addActionListener(this); //listen to executive events

    this->maxResources_ = 8;
    this->maxBlockSize_ = 4096;
    this->masterArbiterInstance_ = 0;
    this->maxGather_ = 1;
    eventRate_ = 0;
    getApplicationInfoSpace()->fireItemAvailable("maxResources", &maxResources_);
    getApplicationInfoSpace()->fireItemAvailable("maxBlockSize", &maxBlockSize_);
    getApplicationInfoSpace()->fireItemAvailable("masterArbiterInstance", &masterArbiterInstance_);
    getApplicationInfoSpace()->fireItemAvailable("maxGather", &maxGather_);
    getApplicationInfoSpace()->fireItemAvailable("eventRate", &eventRate_);
    counter_ = 0;
    currentRate_ = eventRate_;
    
    // default accepted network names
    acceptedCtrlNetwork_  = "arbiterctrl";
    acceptedInputNetwork_ = "arbiterinput";
    getApplicationInfoSpace()->fireItemAvailable("acceptedCtrlNetwork", &acceptedCtrlNetwork_);
    getApplicationInfoSpace()->fireItemAvailable("acceptedInputNetwork", &acceptedInputNetwork_);
    
    // Bind CGI callbacks
    xgi::framework::deferredbind(this, this, &shub::Arbiter::Default, "Default");
    
    
    connectedness_ = false;
    getApplicationInfoSpace()->fireItemAvailable("connectedness", &connectedness_);
}

shub::Arbiter::~Arbiter()
{
}

void shub::Arbiter::actionPerformed (xdata::Event& event)
{
    
    if (event.type() == "urn:xdaq-event:setDefaultValues")
    {
        try
        {
            counter_ = 0;
            
            auto nodes = getApplicationContext()->getDefaultZone()->getApplicationDescriptors("shub::NodeRC");
            nodes_.resize(nodes.size());
            for (size_t n = 0; n <nodes_.size(); n++ )
            {
                nodes_[n] = 0;
            }
            
            // add Nodes
            for ( auto d: nodes )
            {
                size_t inum = d->getInstance();
                if ( nodes_[inum] != 0 )
                {
                    XCEPT_RAISE(xdaq::exception::Exception, "instance already exisiting for node");
                }
                
                nodes_[inum] = const_cast<xdaq::ApplicationDescriptor*>(d);
                LOG4CPLUS_INFO(getApplicationLogger(), "Found NodeRC " <<  d->getContextDescriptor()->getURL() <<  "/" <<  d->getURN() << " instance " << d->getInstance() );
                
                connections_[nodes_[inum]] = ConnectionStatus::idle;
            }
            
            
            replicas_ =  nodes.size();
            for ( size_t c = 0; c < replicas_; c++)
            {
                ocounters_.push_back(0);
                icounters_.push_back(0);
                mcounters_.push_back(0);
                lcounters_.push_back(0);
                pcounters_.push_back(0);
            }
            
            LOG4CPLUS_INFO(getApplicationLogger(), "Found " << replicas_ << " replica NodesRC");
        }
        catch (xdaq::exception::Exception& e)
        {
            LOG4CPLUS_FATAL(getApplicationLogger(), xcept::stdformat_exception_history(e));
            return;
        }
        
        try
        {
            std::string pname = poolName_.toString();
            toolbox::net::URN urn("toolbox-mem-pool", pname);
            pool_ = toolbox::mem::getMemoryPoolFactory()->findPool(urn);
            
        }
        catch (toolbox::mem::exception::Exception & e)
        {
            LOG4CPLUS_FATAL(getApplicationLogger(), xcept::stdformat_exception_history(e));
            return;
        }
        
        size_t totalfree =  maxResources_ * replicas_ * replicas_; // Exact would be (maxResources_ / maxGather) * replicas * replicas e.g. (5 S/G) * (4 replicas input) * (4 replicas propagate)
        
        freelist_ = toolbox::rlist<toolbox::mem::Reference*>::create("shub-arbiter-freelist", totalfree * 2); // double size to cope with local node pipe (block shared)
        // Fill with only
        for ( size_t i = 0; i < totalfree; i++)
        {
            toolbox::mem::Reference * ref = toolbox::mem::getMemoryPoolFactory()->getFrame(pool_, maxBlockSize_);
            ref->setDataSize(maxBlockSize_);
            freelist_->push_back(ref);
        }
        
        pipeService_ = xdaq::getPipeInterface()->join(this,this);
            
    }
    else if ((event.type() == "urn:xdaq-event:ParameterGet") || (event.type() == "urn:xdaq-event:ParameterQuery"))
    {
        eventRate_ = currentRate_.load();
        
        LOG4CPLUS_INFO(getApplicationLogger(), "Get currentRate  " << eventRate_ );
        
        {
            std::lock_guard<std::mutex> guardo(mutex_);
            
            if ( octrlpipes_.size() == replicas_  && ictrlpipes_.size() == replicas_ )
            {
                connectedness_ = true;
            }
            else
                connectedness_ = false;
        }
        
    }
}

void shub::Arbiter::actionPerformed(xdaq::pipe::ConnectionError & ce ) 
{
    std::lock_guard<std::mutex> guard(mutex_);
    //std::cout << "Connection error from " << ce.getSourceDescriptor()->getURN() <<  " to " <<  ce.getDestinationDescriptor()->getURN()<< std::endl;
    const xdaq::ApplicationDescriptor* d = ce.getDestinationDescriptor();
    std::string durl = d->getContextDescriptor()->getURL() + "/" + d->getURN();
    LOG4CPLUS_WARN(getApplicationLogger(), "Connection failed for " << durl <<  ", with error " << ce.getException().what());
    connections_[d] = ConnectionStatus::idle;
    // disable input generator
}

void shub::Arbiter::actionPerformed(xdaq::pipe::BrokenPipe & bp)
{
    LOG4CPLUS_WARN(getApplicationLogger(), "Broken Pipe, shoud destroy output pipe here");
    std::lock_guard<std::mutex> guard(mutex_);
    
    for ( auto octrlit = octrlpipes_.begin(); octrlit != octrlpipes_.end(); octrlit++ )
    {
        if ( (*octrlit).second->match(bp.getPipeHandle()))
        {
            LOG4CPLUS_DEBUG(getApplicationLogger(),"Found matching output pipe, going to destroy") ;
            
            xdaq::pipe::Output * octrlpipe = (*octrlit).second;
            connections_[(*octrlit).first] = ConnectionStatus::idle;
            pipeService_->destroyOutputPipe(octrlpipe);
            octrlpipes_.erase(octrlit);
            return;
        }
    }
}

void shub::Arbiter::actionPerformed(xdaq::pipe::EstablishedConnection & ec ) 
{
    
    LOG4CPLUS_DEBUG(getApplicationLogger(), "Connection is established from " << ec.getSourceDescriptor()->getURN() <<  " to " <<  ec.getDestinationDescriptor()->getURN());
    size_t n = ((maxResources_/ maxGather_) +1) * replicas_; // exact would be (maxResources_ / maxGather_ ) * replicas_
    xdaq::pipe::Output* opipe = pipeService_->createOutputPipe(ec, n);
    
    std::lock_guard<std::mutex> guard(mutex_);
    octrlpipes_[ec.getDestinationDescriptor()] = opipe;
    connections_[ec.getDestinationDescriptor()] = ConnectionStatus::established;
}

void shub::Arbiter::actionPerformed(xdaq::pipe::ConnectionClosedByPeer & ccbp )
{
    std::lock_guard<std::mutex> guard(mutex_);
    
    LOG4CPLUS_WARN(getApplicationLogger(), "Connection closed by peer in server going to delete input pipe from input list");
    for ( auto ictrlpipeit = ictrlpipes_.begin(); ictrlpipeit != ictrlpipes_.end(); ictrlpipeit++)
    {
        if ( (*ictrlpipeit)->match(ccbp.getPipeHandle()))
        {
            LOG4CPLUS_WARN(getApplicationLogger(), "Found matching input data pipe, going to destroy");
            pipeService_->destroyInputPipe(*ictrlpipeit);
            ictrlpipes_.erase(ictrlpipeit);
            return;
        }
    }
    
}


bool shub::Arbiter::actionPerformed(xdaq::pipe::ConnectionRequest & cr)
{               
    std::string network = cr.getNetwork();
    LOG4CPLUS_DEBUG(getApplicationLogger(),"Server - Connection request received for network " << network);
    auto provide =  [&](xdaq::pipe::Input * ip, size_t nblocks, size_t blockSize) {
        for (size_t k = 0; k < nblocks; k++)
        {
            toolbox::mem::Reference * reference = 0;
            try
            {
                reference = toolbox::mem::getMemoryPoolFactory()->getFrame(pool_, blockSize);
            }
            catch (toolbox::mem::exception::Exception & e)
            {
                LOG4CPLUS_FATAL(getApplicationLogger(), toolbox::toString("Could not allocate block for input, %s", e.what()));
            }
            ip->post(reference);
        }
    }; // end of lambda expression
    
    // if ( network.find("arbiterctrl") != std::string::npos  )
    if ( network.find(acceptedCtrlNetwork_.toString()) != std::string::npos  )
    {
        // n*2 used to obtin a ouble buffer effect
        size_t n = (this->maxResources_ / this->maxGather_) + 1;
        xdaq::pipe::Input * ictrlpipe =  pipeService_->createInputPipe(cr, n*2);
        LOG4CPLUS_DEBUG(getApplicationLogger(),"register ictrl pipe in ictrlpipe vector" << network);
        provide(ictrlpipe, n*2 ,this->maxBlockSize_); // exact would be  (this->maxResources_/ maxGather_)
        
        std::lock_guard<std::mutex> guard(mutex_);
        ictrlpipes_.push_back(ictrlpipe);
    }
    //else if ( network.find("arbiterinput") != std::string::npos  )
    else if ( network.find(acceptedInputNetwork_.toString()) != std::string::npos  )
    {
        size_t n = this->maxResources_ * replicas_; // accept many triggers for as many resources
        xdaq::pipe::Input * inputpipe =  pipeService_->createInputPipe(cr,n);
        provide(inputpipe, n ,this->maxBlockSize_); // alway get maxResources_ in addition from loopbak idatapipe
        LOG4CPLUS_DEBUG(getApplicationLogger(),"register input pipe in inputpipe vector" << network);
        
        std::lock_guard<std::mutex> guard(mutex_);
        inputpipes_.push_back(inputpipe);
    }
    else
    {
        LOG4CPLUS_WARN(getApplicationLogger(), "no support for network " << cr.getNetwork() << ", ignore");
        return false;
    }
    return true;
}

void shub::Arbiter::connect(std::string name)
{
    LOG4CPLUS_DEBUG (this->getApplicationLogger(), name);
    
    // validate readiness example
    auto readiness = dynamic_cast<xdata::Boolean*>(this->getApplicationInfoSpace()->find("readiness"));
    *readiness = true;
    this->getApplicationInfoSpace()->fireItemValueChanged("readiness", this);

    
    while(1)
    {
        ::sleep(5);
        std::lock_guard<std::mutex> guard(mutex_);
        
        LOG4CPLUS_DEBUG(getApplicationLogger(),  "connection status  opipes " << octrlpipes_.size() << " ipipes " << ictrlpipes_.size());
        
        std::string surl = this->getApplicationContext()->getContextDescriptor()->getURL() + "/" + this->getApplicationDescriptor()->getURN();
        
        for ( auto& [descriptor, status]: connections_ )
        {
            if ( status == ConnectionStatus::idle )
            {
                std::string durl = descriptor->getContextDescriptor()->getURL() + "/" + descriptor->getURN();
                LOG4CPLUS_DEBUG(getApplicationLogger(), "requesting data connection from " << surl << " to " << durl << " status Idle to Connecting");
                status  = ConnectionStatus::connecting;
                pipeService_->connect(this->getApplicationDescriptor(), descriptor);
                LOG4CPLUS_DEBUG(getApplicationLogger(), "done requesting data connection");
            }
        }
    }
}



void shub::Arbiter::control(std::string name)
{
    
    this->applyThreadPolicy(name);
    
    
    bool established = false;
    while ( ! established )
    {
        LOG4CPLUS_INFO(getApplicationLogger(), "waiting nodes connection to arbiter..." );
        ::sleep(5);
        
        std::lock_guard<std::mutex> guardo(mutex_);
        
        LOG4CPLUS_INFO(getApplicationLogger(),  "connection status: (ctrl opipes " << octrlpipes_.size() << " of " << replicas_ <<  ") (ctrl ipipes " << ictrlpipes_.size() << " of " << replicas_ <<  ")");
        
        
        if ( octrlpipes_.size() == replicas_  && ictrlpipes_.size() == replicas_ )
        {
            established = true;
            LOG4CPLUS_INFO(getApplicationLogger(), "nodes RC connected to arbiter.");
        }
        
    }
    
    LOG4CPLUS_INFO(getApplicationLogger(), "start controller loop..." << name);
    size_t fragmentid = 0;
    while(1)
    {
        
        //sleep(2);
        std::lock_guard<std::mutex> oguard(mutex_);
        size_t index = 0;
        //for (auto ipipe: ictrlpipes_ )
    	for ( auto ictrlpipeit = ictrlpipes_.begin(); ictrlpipeit != ictrlpipes_.end(); ictrlpipeit++)
        {
            
            LOG4CPLUS_TRACE(getApplicationLogger(), "check input control requests ");
            //char c;
            //std::cout << "enter char to to process a request :";
            //std::cin >> c;
            
	    /*
            while ((*ipipeit)->empty() ) // fair scheduling each ipipe before moving to the next
            {
                //std::chrono::microseconds us = 100;
                auto end = std::chrono::high_resolution_clock::now() + std::chrono::microseconds(100); // us
                do
                {
                       std::this_thread::yield();
                } while (std::chrono::high_resolution_clock::now() < end);
            }
	    */
            
            if ( ! (*ictrlpipeit)->empty() )
            {
            	LOG4CPLUS_TRACE(getApplicationLogger(), "processing with current fragmentid " << fragmentid);
                // requests from node
                toolbox::mem::Reference * ref = (*ictrlpipeit)->get();
                shub::ResourceAvailable * rap = (shub::ResourceAvailable *)ref->getDataLocation();

                pcounters_[index]++;
                
                LOG4CPLUS_TRACE(getApplicationLogger(), "request from node " << rap->node << " received with " << rap->num  << " resources" << std::hex << " ptr " << (size_t)(*ictrlpipeit));
                lcounters_[index] = rap->num;
                // prepare propagate frame
                shub::ShuffleDescriptor shuffle[rap->num];
                for (size_t i = 0; i < rap->num; i++)
                {
                    LOG4CPLUS_DEBUG(getApplicationLogger(), "pairing request " << rap->resource[i] << " with fragment " << fragmentid);
                    shuffle[i].resource = rap->resource[i];
                    shuffle[i].id = fragmentid++;
                    counter_++;
                    icounters_[index]++;
                }
                
		//    &&  (freelist_->elements() >= replicas_) )

                // pairs requests <fragmentids,resource,node>
                for ( auto & [descriptor,octrlpipe] : octrlpipes_ )
                {
                    // get free buffer
                    if ( ! freelist_->empty() )
                    {
                        size_t fsize = sizeof(rap->num) + sizeof(rap->node) +  rap->num * sizeof(shub::ShuffleDescriptor);
                        if ( fsize > this->maxBlockSize_ )
                        {
                            LOG4CPLUS_ERROR(getApplicationLogger(), "buffer overflow, block to small for size " << fsize << " max allowed  " << this->maxBlockSize_);
                            exit(-1);
                        }
                        toolbox::mem::Reference * r  = freelist_->front();
                        freelist_->pop_front();
                        
                        // fill frame
                        shub::ShuffleRequest * shr = (shub::ShuffleRequest *)r->getDataLocation();
                        shr->node = rap->node;
                        shr->num = rap->num;
                        for (size_t i = 0; i < rap->num; i++)
                        {
                            shr->shuffle[i].resource = shuffle[i].resource;
                            shr->shuffle[i].id = shuffle[i].id;
                        }
                        
                        // send control frame to node
                        LOG4CPLUS_TRACE(getApplicationLogger(), "send to node " << descriptor->getInstance() << "total fsize is " << fsize << " maxblocksize is " << this->maxBlockSize_ );
                        r->setDataSize(fsize);
                        octrlpipe->post(r);
                        ocounters_[descriptor->getInstance()]++;
                    }
                    else
                    {
                        LOG4CPLUS_ERROR(getApplicationLogger(), "buffer not available for propagate to nodes, cannot process pairing, lost request");
                        //ipipe->post(ref);
                        exit(-1);
                    }
                    
                }
                // done with input request
                (*ictrlpipeit)->post(ref);
            }
            else
            {
                //LOG4CPLUS_WARN(getApplicationLogger(), "no input requests from ictrlpipe at index (unbalanced requests might occurs)" << index);
                mcounters_[index]++;
            }
            ++index;
        }
        
        // recycles completed frames
        for ( auto & [descriptor,octrlpipe] : octrlpipes_ )
        {
            while ( ! octrlpipe->empty() )
            {
                LOG4CPLUS_TRACE(getApplicationLogger(), "free request back to freelist");
                toolbox::mem::Reference * r = octrlpipe->get();
                freelist_->push_back(r);
            }
        }
    }
}

void shub::Arbiter::measure (std::string name)
{
    while(1)
    {
        ::sleep(10);
        toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();
        double delta = (double)now - (double)lastTime_;
        double rate = counter_ / delta;
        currentRate_ = rate;
        counter_ = 0;
        lastTime_ =  now;
        
        //std::cout << "arbiter rate is " << rate/1000  << " kHz" <<  std::endl;
    }
}



void shub::Arbiter::Default (xgi::Input * in, xgi::Output * out)
{
    *out << "<div class=\"xdaq-tab-wrapper\">" << std::endl;

    *out << "<div class=\"xdaq-tab\" title=\"Connections\">" << std::endl;
    *out << cgicc::table().set("class", "xdaq-table") << std::endl;
    *out << cgicc::caption("Output Control Connections to NodeRCs") << std::endl;
    
    *out << cgicc::thead() << std::endl;
    
    *out << cgicc::tr();
    *out << cgicc::th("Instance");
    *out << cgicc::th("Status");
    *out << cgicc::th("URL");
    *out << cgicc::th("Instance cross check");
    *out << cgicc::tr() << std::endl;
    
    *out << cgicc::thead() << std::endl;
   
    *out << cgicc::tbody() << std::endl;
    for ( size_t i = 0; i < nodes_.size(); i++)
    {
	const xdaq::ApplicationDescriptor*  c = nodes_[i];
        *out << cgicc::tr();
        *out << "<td>" << i << "</td>";
        *out << "<td>" << shub::statusToString(connections_[c]) << "</td>";
        *out << "<td>" <<  c->getContextDescriptor()->getURL() <<  "/" <<  c->getURN() << " instance " << c->getInstance() << "</td>";
        *out << "<td>" <<  c->getInstance() << "</td>";
        *out << cgicc::tr();
    }
    
    *out << cgicc::tbody();
    *out << cgicc::table();
    
    *out << "</div>";

    *out << "<div class=\"xdaq-tab\" title=\"Statistics\">" << std::endl;
    *out << cgicc::table().set("class", "xdaq-table") << std::endl;
    *out << cgicc::caption("Total Input Requests per Node") << std::endl;

    *out << cgicc::thead() << std::endl;

    *out << cgicc::tr();
    *out << cgicc::th("Node");
    *out << cgicc::th("Input Requests");
    *out << cgicc::tr() << std::endl;

    *out << cgicc::thead() << std::endl;


    *out << cgicc::tbody() << std::endl;
    for ( size_t i = 0; i < icounters_.size(); i++)
    {
        *out << cgicc::tr();
        *out << "<td>" << i << "</td>";
        *out << "<td>" << icounters_[i] << "</td>";
        *out << cgicc::tr();
    }
    
    *out << cgicc::tbody();
    *out << cgicc::table();
    
    /* ---- */
    *out << cgicc::table().set("class", "xdaq-table") << std::endl;
    *out << cgicc::caption("Total Output Requests per Node") << std::endl;

    *out << cgicc::thead() << std::endl;

    *out << cgicc::tr();
    *out << cgicc::th("Node");
    *out << cgicc::th("Output Requests");
    *out << cgicc::tr() << std::endl;

    *out << cgicc::thead() << std::endl;


    *out << cgicc::tbody() << std::endl;
    for ( size_t i = 0; i < icounters_.size(); i++)
    {
        *out << cgicc::tr();
        *out << "<td>" << i << "</td>";
        *out << "<td>" << ocounters_[i] << "</td>";
        *out << cgicc::tr();
    }

    *out << cgicc::tbody();
    *out << cgicc::table();



    *out << cgicc::table().set("class", "xdaq-table") << std::endl;
    *out << cgicc::caption("Total Missing Input Requests per Node") << std::endl;

    *out << cgicc::thead() << std::endl;

    *out << cgicc::tr();
    *out << cgicc::th("Node");
    *out << cgicc::th("Missed");
    *out << cgicc::tr() << std::endl;

    *out << cgicc::thead() << std::endl;


    *out << cgicc::tbody() << std::endl;
    for ( size_t i = 0; i < mcounters_.size(); i++)
    {
        *out << cgicc::tr();
        *out << "<td>" << i << "</td>";
        *out << "<td>" << mcounters_[i] << "</td>";
        *out << cgicc::tr();
    }

    *out << cgicc::tbody();
    *out << cgicc::table();

    *out << cgicc::table().set("class", "xdaq-table") << std::endl;
    *out << cgicc::caption("Total Input Processed per Node") << std::endl;

    *out << cgicc::thead() << std::endl;

    *out << cgicc::tr();
    *out << cgicc::th("Node");
    *out << cgicc::th("Processed");
    *out << cgicc::tr() << std::endl;

    *out << cgicc::thead() << std::endl;


    *out << cgicc::tbody() << std::endl;
    for ( size_t i = 0; i < pcounters_.size(); i++)
    {
        *out << cgicc::tr();
        *out << "<td>" << i << "</td>";
        *out << "<td>" << pcounters_[i] << "</td>";
        *out << cgicc::tr();
    }

    *out << cgicc::tbody();
    *out << cgicc::table();

    //std::cout << "DEBUG hyperdaq delta processed " << pcounters_[0] - pcounters_[1] << std::endl;


    *out << cgicc::table().set("class", "xdaq-table") << std::endl;
    *out << cgicc::caption("Last gathered requests per Node") << std::endl;

    *out << cgicc::thead() << std::endl;

    *out << cgicc::tr();
    *out << cgicc::th("Node");
    *out << cgicc::th("Last Gathered");
    *out << cgicc::tr() << std::endl;

    *out << cgicc::thead() << std::endl;


    *out << cgicc::tbody() << std::endl;
    for ( size_t i = 0; i < mcounters_.size(); i++)
    {
        *out << cgicc::tr();
        *out << "<td>" << i << "</td>";
        *out << "<td>" << lcounters_[i] << "</td>";
        *out << cgicc::tr();
    }

    *out << cgicc::tbody();
    *out << cgicc::table();

    //
    *out << cgicc::table().set("class", "xdaq-table-vertical") << std::endl;
    *out << cgicc::caption("Performance (Fragment Rate)") << std::endl;
    
    *out << cgicc::thead() << std::endl;
    *out << cgicc::tr();
    *out << cgicc::th();
    *out << "Rate";
    *out << cgicc::th();
    *out << cgicc::tr();
    *out << cgicc::thead() << std::endl;
    
    *out << cgicc::tbody() << std::endl;
    *out << cgicc::tr();
    *out << cgicc::td();
    *out << currentRate_.load()/1000 << " KHz";
    *out << cgicc::td();
    *out << cgicc::tr() << std::endl;
    *out << cgicc::tbody() << std::endl;
    
    *out << cgicc::table() << std::endl;


    *out << "</div>";


    *out << "</div>";
}

void shub::Arbiter::actionPerformed(toolbox::Event& e)
{
    // Either configuration loaded or initialization completed (without a configuration) resources are initialized
    if( e.type() == "urn:xdaq-event:configuration-loaded" || e.type() == "urn:xdaq-event:initialization-completed")
    {
       if ( this->getApplicationDescriptor()->getInstance() ==  masterArbiterInstance_)
        {
            // start control thread for pairing resources and triggers
            std::thread ths(&Arbiter::control, this, "control");
            ths.detach();
            
            // start measurement generator thread
            std::thread thm(&Arbiter::measure, this, "benchmark loop");
            thm.detach();
            
            // start connector thread
            std::thread thc(&Arbiter::connect, this, "connector loop");
            thc.detach();
        }
        else
        {
            LOG4CPLUS_INFO(getApplicationLogger(), "this arbiter is disabled");
        }
    }
}



