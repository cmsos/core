// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "shub/Loopback.h" 



shub::Input::Input(toolbox::rlist<toolbox::mem::Reference*> * io, toolbox::rlist<toolbox::mem::Reference*> * cq): io_(io),cq_(cq)
{}

shub::Output::Output(toolbox::rlist<toolbox::mem::Reference*> * io, toolbox::rlist<toolbox::mem::Reference*> * cq): io_(io),cq_(cq)
{}

shub::Input::~Input()
{}

shub::Output::~Output()
{}

void shub::Input::postFrame(toolbox::mem::Reference * ref )
{
    cq_->push_back(ref);	
}

bool shub::Input::empty()
{
    return io_->empty();
}

toolbox::mem::Reference * shub::Input::completed()
{
    toolbox::mem::Reference * ref = io_->front();
    io_->pop_front();
    return ref;	
}


void shub::Output::postFrame(toolbox::mem::Reference * ref )
{
    io_->push_back(ref);
}

bool shub::Output::empty()
{
    return cq_->empty();
}

toolbox::mem::Reference * shub::Output::completed()
{
    toolbox::mem::Reference * ref = cq_->front();
    cq_->pop_front();
    return ref;       
}


shub::InputLoopback::InputLoopback(pt::pipe::Input * handle, pt::pipe::Service* pts): xdaq::pipe::Input(handle,pts)
{}

shub::OutputLoopback::OutputLoopback(pt::pipe::Output * handle, pt::pipe::Service* pts): xdaq::pipe::Output(handle,pts)
{}

shub::InputLoopback::~InputLoopback()
{}

shub::OutputLoopback::~OutputLoopback()
{}

