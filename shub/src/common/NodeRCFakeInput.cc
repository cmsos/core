
/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simlevicius					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <thread>
#include <algorithm>

#include "shub/NodeRC.h"

#include "xcept/Exception.h"
#include "xcept/tools.h"

#include "toolbox/BSem.h"
#include "toolbox/rlist.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/CommittedHeapAllocator.h"
#include "toolbox/task/TimerTask.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"

#include "xdaq/Application.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationGroup.h"
#include "xoap/MessageFactory.h"
#include "xdaq/NamespaceURI.h"
#include "xdaq/pipe/Input.h"
#include "xdaq/pipe/Output.h"
#include "xdaq/pipe/Interface.h"

#include "xgi/Method.h"
#include "xgi/framework/Method.h"

XDAQ_INSTANTIATOR_IMPL(shub::NodeRC)

//#define MESSAGE_SIZE 8196
#define MESSAGE_SIZE 131072

shub::NodeRC::NodeRC (xdaq::ApplicationStub* c): xdaq::Application(c), xgi::framework::UIManager(this)
{
    c->getDescriptor()->setAttribute("icon", "/shub/images/node.png");
    c->getDescriptor()->setAttribute("icon16x16", "/shub/images/node.png");
    
    poolName_ =  "tpi";
    this->getApplicationInfoSpace()->fireItemAvailable("poolName", &poolName_);
    this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
    this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:ParameterSet");
    this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:ParameterGet");
    this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:ParameterQuery");

    this->maxReceiveBuffers_ = 8;
    this->maxBlockSize_ = 4096;
    this->ioQueueSize_ = 4096;
    this->maxGather_ = 1;
    getApplicationInfoSpace()->fireItemAvailable("maxReceiveBuffers", &maxReceiveBuffers_);
    getApplicationInfoSpace()->fireItemAvailable("maxBlockSize", &maxBlockSize_);
    getApplicationInfoSpace()->fireItemAvailable("ioQueueSize", &ioQueueSize_);
    getApplicationInfoSpace()->fireItemAvailable("maxGather", &maxGather_);
    counter_ = 0;
    currentFragmentId_ = 0;
    enabled_ = false;
    
    superFragmentSize_ = MESSAGE_SIZE;
    eventRate_ = 0;
    getApplicationInfoSpace()->fireItemAvailable("superFragmentSize", &superFragmentSize_);
    getApplicationInfoSpace()->fireItemAvailable("eventRate", &eventRate_);
    
    currentLen_ = superFragmentSize_;
    currentRate_ = eventRate_;

    // Bind CGI callbacks
    xgi::framework::deferredbind(this, this, &shub::NodeRC::Default, "Default");
    
}

shub::NodeRC::~NodeRC()
{
}

//
// Initialization
//
void shub::NodeRC::actionPerformed (xdata::Event& event)
{
    if (event.type() == "urn:xdaq-event:setDefaultValues")
    {
        try
        {
            counter_ = 0;
            
            auto nodes = getApplicationContext()->getDefaultZone()->getApplicationDescriptors("shub::NodeRC");
            nodes_.resize(nodes.size());
            for (size_t n = 0; n <nodes_.size(); n++ )
            {
                nodes_[n] = 0;
            }
            
            auto controllers = getApplicationContext()->getDefaultZone()->getApplicationDescriptors("shub::Arbiter");
            controllers_.resize(controllers.size());
            for (size_t c = 0; c < 1; c++ )
            {
                controllers_[c] = 0;
            }
            
            // add Nodes
            for ( auto d: nodes )
            {
                size_t inum = d->getInstance();
                if ( nodes_[inum] != 0 )
                {
                    XCEPT_RAISE(xdaq::exception::Exception, "instance already exisiting for node");
                }
                
                nodes_[inum] = const_cast<xdaq::ApplicationDescriptor*>(d);
                LOG4CPLUS_INFO(getApplicationLogger(), "Found NodeRC " <<  d->getContextDescriptor()->getURL() <<  "/" <<  d->getURN() << " instance " << d->getInstance() );
                
                if ( this->getApplicationDescriptor()->getInstance() != d->getInstance() ) // found remote node
                {
                    dconnections_[nodes_[inum]] = ConnectionStatus::idle;
                }
                else
                {
                    // loopback pipe always connected
                    dconnections_[nodes_[inum]] = ConnectionStatus::established;
                }
            }
            
            // add Arbiter
            for ( auto c: controllers )
            {
                size_t inum = c->getInstance();
                if ( controllers_[inum] != 0 )
                {
                    XCEPT_RAISE(xdaq::exception::Exception, "instance already exisiting for arbiter");
                }
                controllers_[inum] = const_cast<xdaq::ApplicationDescriptor*>(c);
                cconnections_[controllers_[inum]] = ConnectionStatus::idle;
            }
            
            replicas_ =  nodes.size();
            
            rpool_ = new shub::ResourcePool(replicas_,maxReceiveBuffers_);
            // Only one counter for control
            octrlcounters_.push_back(0);
            ictrlcounters_.push_back(0);
            
            LOG4CPLUS_INFO(getApplicationLogger(), "Found " << replicas_ << " replica Nodes RC");
        }
        catch (xdaq::exception::Exception& e)
        {
            LOG4CPLUS_FATAL(getApplicationLogger(), xcept::stdformat_exception_history(e));
            return;
        }
        
        try
        {
            std::string pname = poolName_.toString();
            toolbox::net::URN urn("toolbox-mem-pool", pname);
            pool_ = toolbox::mem::getMemoryPoolFactory()->findPool(urn);
            
        }
        catch (toolbox::mem::exception::Exception & e)
        {
            LOG4CPLUS_FATAL(getApplicationLogger(), xcept::stdformat_exception_history(e));
            return;
        }
        
        freerequestslist_ = toolbox::rlist<toolbox::mem::Reference*>::create("shub-node-freerequestlist",ioQueueSize_);
        pendingrequests_ = toolbox::rlist<toolbox::mem::Reference*>::create("shub-node-pendingrequests",ioQueueSize_);;
        shufflerequests_ = toolbox::rlist<ShuffleCommand>::create("shub-node-shufflerequests",ioQueueSize_);;
        
        // initialize requests frame for arbiter
        for (size_t k = 0; k < maxReceiveBuffers_; k++)
        {
            toolbox::mem::Reference * reference = 0;
            try
            {
                reference = toolbox::mem::getMemoryPoolFactory()->getFrame(pool_, this->maxBlockSize_);
            }
            catch (toolbox::mem::exception::Exception & e)
            {
                LOG4CPLUS_FATAL(getApplicationLogger(), toolbox::toString("Could not allocate block for input, %s", e.what()));
            }
            freerequestslist_->push_back(reference);
        }
        
        idata_ = toolbox::rlist<toolbox::mem::Reference*>::create("shub-node-idata", ioQueueSize_ );
        idatafree_ = toolbox::rlist<toolbox::mem::Reference*>::create("shub-node-idatafree", ioQueueSize_ );
        for (size_t k = 0; k < this->maxReceiveBuffers_  * replicas_; k++)
        {
            toolbox::mem::Reference * reference = 0;
            
            try
            {
                reference = toolbox::mem::getMemoryPoolFactory()->getFrame(pool_, this->maxBlockSize_);
            }
            catch (toolbox::mem::exception::Exception & e)
            {
                LOG4CPLUS_FATAL(getApplicationLogger(), toolbox::toString("Could not allocate block for input, %s", e.what()));
		exit(-1);
            }
            
            idatafree_->push_back(reference);
        }
        // create loopback pipes
        auto io = toolbox::rlist<toolbox::mem::Reference*>::create("shub-node-loopback-io", ioQueueSize_ );
        auto cq = toolbox::rlist<toolbox::mem::Reference*>::create("shub-node-loopback-cq", ioQueueSize_ );
        ilb_ = new shub::InputLoopback(new shub::Input(io,cq), 0);
        olb_ = new shub::OutputLoopback(new shub::Output(io,cq), 0);
        opipes_[this->getApplicationDescriptor()] = olb_;
        ipipes_.push_back(ilb_);
        
        pipeService_ = xdaq::getPipeInterface()->join(this,this);

        currentLen_.store(superFragmentSize_);

	size_t fsize = sizeof(uint64_t) + sizeof(uint64_t) + (sizeof(uint64_t) * this->maxGather_);
	if ( fsize > this->maxBlockSize_ )
	{
                LOG4CPLUS_FATAL(getApplicationLogger(), "request to arbiter could overflows with size" << fsize << " max allowed is " << this->maxBlockSize_);
		exit(-1);
	}	

        
        // start connector thread
        std::thread thc(&NodeRC::connect, this, "connector loop");
        thc.detach();
        
        // start shuffle thread
        std::thread ths(&NodeRC::shuffle, this, "shuffle loop");
        ths.detach();
        
        // start builder thread
        std::thread thb(&NodeRC::builder, this, "builder loop");
        thb.detach();
        
        // start input generator thread
        std::thread thi(&NodeRC::input, this, "input generator loop");
        thi.detach();
        
        // start measurement generator thread
        std::thread thm(&NodeRC::measure, this, "benchmark loop");
        thm.detach();
    }
    else if (event.type() == "urn:xdaq-event:ParameterSet")
    {
        LOG4CPLUS_INFO(getApplicationLogger(), "Set currentLen to  " << superFragmentSize_ );

        currentLen_.store(superFragmentSize_);
    }
    else if ((event.type() == "urn:xdaq-event:ParameterGet") || (event.type() == "urn:xdaq-event:ParameterQuery"))
    {
        eventRate_ = currentRate_.load();
        
        LOG4CPLUS_INFO(getApplicationLogger(), "Get currentRate  " << eventRate_ );

    }

}

//
// Connection establishment handling
//
void shub::NodeRC::actionPerformed(xdaq::pipe::EstablishedConnection & ec ) 
{
    const xdaq::ApplicationDescriptor * d = ec.getDestinationDescriptor();
    
    LOG4CPLUS_INFO(getApplicationLogger(), "Connection is established from " << d->getURN() <<  " to " <<  d->getURN());
    
    {
        std::lock_guard<std::mutex> guard(smutex_);
        if ( std::find(nodes_.begin(), nodes_.end(), d) != nodes_.end() )
        {
            opipes_[d] = pipeService_->createOutputPipe(ec);
            dconnections_[d] = ConnectionStatus::established;
            LOG4CPLUS_INFO(getApplicationLogger(), "outputpipe created for data status is Established" );
            return;
        }
    }
    
    {
        std::lock_guard<std::mutex> guard(bmutex_);
        if ( std::find(controllers_.begin(), controllers_.end(), d) != controllers_.end() )
        {
            octrlpipes_[d]  = pipeService_->createOutputPipe(ec);
            cconnections_[d] = ConnectionStatus::established;
            LOG4CPLUS_INFO(getApplicationLogger(), "outputpipe created for ctrl status is Established" );
            return;
        }
    }
}

bool shub::NodeRC::actionPerformed(xdaq::pipe::ConnectionRequest & cr)
{               
    std::string network = cr.getNetwork();
    LOG4CPLUS_INFO(getApplicationLogger(),"NodeRC - Connection request received from network " << network);
    
    
    size_t nblocks = this->maxReceiveBuffers_;
    size_t blockSize = this->maxBlockSize_;

    if ( network.find("nodercinput") != std::string::npos  )
    {
	    	nblocks = this->maxReceiveBuffers_ * replicas_;
    }

    auto provide =  [&](xdaq::pipe::Input * ipipe) {
    	for (size_t k = 0; k < nblocks; k++)
    	{
        	toolbox::mem::Reference * reference = 0;
        	try
        	{
            		reference = toolbox::mem::getMemoryPoolFactory()->getFrame(pool_, blockSize);
        	}
        	catch (toolbox::mem::exception::Exception & e)
        	{
            		LOG4CPLUS_FATAL(getApplicationLogger(), toolbox::toString("Could not allocate block for input, %s", e.what()));
        	}
        	ipipe->post(reference);
    	}
    }; // end of lambda expression
    
    if ( network.find("nodercdata") != std::string::npos )
    {
        std::lock_guard<std::mutex> guard(bmutex_);
    	xdaq::pipe::Input * ipipe =  pipeService_->createInputPipe(cr);
	provide(ipipe);
        LOG4CPLUS_INFO(getApplicationLogger(),"regiter data pipe in ipipe vector" << network);
        ipipes_.push_back(ipipe);
    }
    else if ( network.find("nodercctrl") != std::string::npos  )
    {
        std::lock_guard<std::mutex> guard(smutex_);
    	xdaq::pipe::Input * ipipe =  pipeService_->createInputPipe(cr);
	provide(ipipe);
        LOG4CPLUS_INFO(getApplicationLogger(),"register ctrl pipe in ictrlpipe vector" << network);
        ictrlpipes_.push_back(ipipe);
    }
    else if ( network.find("nodercinput") != std::string::npos  )
    {
        std::lock_guard<std::mutex> guard(smutex_);
    	xdaq::pipe::Input * ipipe =  pipeService_->createInputPipe(cr);
	provide(ipipe);
        LOG4CPLUS_INFO(getApplicationLogger(),"register input pipe in dpipe vector" << network);
        dpipes_.push_back(ipipe);
    }
    else
    {
        LOG4CPLUS_WARN(getApplicationLogger(), "no pipe support for network " << cr.getNetwork() << ", ignore");
        return false;
    }
    return true;
}

//
// Connection error handing
//
void shub::NodeRC::actionPerformed(xdaq::pipe::ConnectionError & ce )
{
    //std::cout << "Connection error from " << ce.getSourceDescriptor()->getURN() <<  " to " <<  ce.getDestinationDescriptor()->getURN()<< std::endl;
    const xdaq::ApplicationDescriptor* d = ce.getDestinationDescriptor();
    std::string durl = d->getContextDescriptor()->getURL() + "/" + d->getURN();
    LOG4CPLUS_WARN(getApplicationLogger(), "Connection failed for " << durl <<  ", with error " << ce.getException().what());
    
    {
        std::lock_guard<std::mutex> guard(smutex_);
        for ( auto& [descriptor, status]: dconnections_ )
        {
            if ( descriptor == d )
            {
                dconnections_[d] = ConnectionStatus::idle;
                break;
            }
        }
    }
    
    {
        std::lock_guard<std::mutex> guard(bmutex_);
        for ( auto& [descriptor, status]: cconnections_ )
        {
            if ( descriptor == d )
            {
                cconnections_[d] = ConnectionStatus::idle;
                break;
            }
        }
    }
    // disable input generator
    enabled_ = false;
}

void shub::NodeRC::actionPerformed(xdaq::pipe::BrokenPipe & bp)
{
    LOG4CPLUS_WARN(getApplicationLogger(), "Broken Pipe, shoud destroy output pipe here");
    
    {
        std::lock_guard<std::mutex> guard(smutex_);
        for ( auto it = opipes_.begin(); it != opipes_.end(); it++ )
        {
            if ( (*it).second->match(bp.getPipeHandle()))
            {
                LOG4CPLUS_DEBUG(getApplicationLogger(),"Found matching output data pipe, going to destroy") ;
                
                xdaq::pipe::Output * opipe = (*it).second;
                dconnections_[(*it).first] = ConnectionStatus::idle;
                pipeService_->destroyOutputPipe(opipe);
                opipes_.erase(it);
                // disable input generator
                enabled_ = false;
                return;
            }
        }
    }
    
    {
        std::lock_guard<std::mutex> guard(bmutex_);
        for ( auto it = octrlpipes_.begin(); it != octrlpipes_.end(); it++ )
        {
            if ( (*it).second->match(bp.getPipeHandle()))
            {
                LOG4CPLUS_DEBUG(getApplicationLogger(),"Found matching output control pipe, going to destroy") ;
                
                xdaq::pipe::Output * opipe = (*it).second;
                cconnections_[(*it).first] = ConnectionStatus::idle;
                pipeService_->destroyOutputPipe(opipe);
                octrlpipes_.erase(it);
                // disable input generator
                enabled_ = false;
                return;
            }
        }
    }
}

void shub::NodeRC::actionPerformed(xdaq::pipe::ConnectionClosedByPeer & ccbp )
{
    
    LOG4CPLUS_WARN(getApplicationLogger(), "Connection closed by peer in server going to delete input pipe from input list");
    {
        std::lock_guard<std::mutex> guard(bmutex_);
        
        for ( auto pipeit = ipipes_.begin(); pipeit != ipipes_.end(); pipeit++ )
        {
            if ( (*pipeit)->match(ccbp.getPipeHandle()))
            {
                LOG4CPLUS_WARN(getApplicationLogger(), "Found matching input data pipe, going to destroy");
                pipeService_->destroyInputPipe(*pipeit);
                ipipes_.erase(pipeit);
                // disable input generator
                enabled_ = false;
                return;
            }
        }
    }
    
    {
        std::lock_guard<std::mutex> guard(smutex_);
        
        for ( auto pipeit =  ictrlpipes_.begin(); pipeit != ictrlpipes_.end(); pipeit++ )
        {
            if ( (*pipeit)->match(ccbp.getPipeHandle()))
            {
                LOG4CPLUS_WARN(getApplicationLogger(), "Found matching input ctrl pipe, going to destroy");
                pipeService_->destroyInputPipe(*pipeit);
                ictrlpipes_.erase(pipeit);
                // disable input generator
                enabled_ = false;
                return;
            }
        }
    }
    
}

//
// Multithread work loops (connect, input, shuffling, building, measure )
//
void shub::NodeRC::connect(std::string name)
{
    LOG4CPLUS_DEBUG (this->getApplicationLogger(), "starting connection thread " << name);
    
    while(1)
    {
        ::sleep(10);
        
        std::string surl = this->getApplicationContext()->getContextDescriptor()->getURL() + "/" + this->getApplicationDescriptor()->getURN();

        LOG4CPLUS_INFO(getApplicationLogger(), "connection status: (opipes " <<  opipes_.size()<< " of " << replicas_ << ")" << "(ipipes " << ipipes_.size() << " of " << replicas_ << ")" << "(ictrlpipes " <<  ictrlpipes_.size() << " of 1)" << "(octrlpipe " << octrlpipes_.size() << " of 1)"  << "(dpipe " << dpipes_.size() << " of 1)");

        
        {
            std::lock_guard<std::mutex> guard(smutex_);
            for ( auto& [descriptor, status]: dconnections_ )
            {
                if ( status == ConnectionStatus::idle )
                {
                    std::string durl = descriptor->getContextDescriptor()->getURL() + "/" + descriptor->getURN();
                    LOG4CPLUS_INFO(getApplicationLogger(), "requesting data connection from " << surl << " to " << durl << " status Idle to Connecting");
                    status  = ConnectionStatus::connecting;
                    pipeService_->connect(this->getApplicationDescriptor(), descriptor);
                    LOG4CPLUS_DEBUG(getApplicationLogger(), "done requesting data connection");
                }
            }
        }
        
        {
            std::lock_guard<std::mutex> guard(smutex_);
            for ( auto& [descriptor, status]: cconnections_ )
            {
                if ( status == ConnectionStatus::idle )
                {
                    std::string durl = descriptor->getContextDescriptor()->getURL() + "/" + descriptor->getURN();
                    LOG4CPLUS_INFO(getApplicationLogger(), "requesting ctrl connection from " << surl << " to " << durl << " status Idle to Connecting");
                    status  = ConnectionStatus::connecting;
                    pipeService_->connect(this->getApplicationDescriptor(), descriptor);
                    LOG4CPLUS_DEBUG(getApplicationLogger(), "done requesting ctrl connection");
                }
            }
        }
    }
}

void shub::NodeRC::input(std::string name)
{
    LOG4CPLUS_DEBUG(getApplicationLogger(), "starting input generation loop..." << name);
    for(;;)
    {
        // reset for a rough synchonization
        currentFragmentId_ = 0;
        
        bool established = false;
        while ( ! established )
        {
            LOG4CPLUS_INFO(getApplicationLogger(), "waiting rc nodes connection...");
            ::sleep(5);
            
            std::lock_guard<std::mutex> guardb(bmutex_);
            std::lock_guard<std::mutex> guards(smutex_);
            
            LOG4CPLUS_INFO(getApplicationLogger(), "(opipes " <<  opipes_.size()<< " of " << replicas_ << ")" << "(ipipes " << ipipes_.size() << " of " << replicas_ << ")" << "(ictrlpipes " <<  ictrlpipes_.size() << " of 1)" << "(octrlpipe " << octrlpipes_.size() << " of 1)" );
            
            if ( opipes_.size() == replicas_  && ipipes_.size() == replicas_  && ictrlpipes_.size() == 1 && octrlpipes_.size() == 1)
            {
                established = true;
            }
            
        }
        
        LOG4CPLUS_INFO(getApplicationLogger(), "all rc nodes connected");
        //sleep(5);
        enabled_ = true;
        while (enabled_)
        {
            
            while ( ! idatafree_->empty() )
            {
                LOG4CPLUS_TRACE(getApplicationLogger(), "input - going to input recycled fragment");
                toolbox::mem::Reference * ref = idatafree_->front();
                idatafree_->pop_front();
                
                //std::cout << " NodeRC post frame of size" << ref->getDataSize() << " offset " << ref->getDataOffset() << std::endl;
                shub::FragmentDescriptor * fragment = (FragmentDescriptor*)ref->getDataLocation();
                fragment->id = currentFragmentId_;
                fragment->node = this->getApplicationDescriptor()->getInstance();
                fragment->resource = 0;
                ref->setDataSize(currentLen_);
                LOG4CPLUS_TRACE(getApplicationLogger(), "input - going to input recycled fragment with fragmentid " << currentFragmentId_  );
                idata_->push_back(ref);
                currentFragmentId_++;
                //::sleep(10);
                
            }
            
            //LOG4CPLUS_TRACE(getApplicationLogger(), "input - idatafree is empty sleep a bit");
            //::sleep(1);
            
        } // input enabled loop
        LOG4CPLUS_WARN(getApplicationLogger(), "input - input generator was disabled");
        
    } // forever loop
    
    return;
}

void shub::NodeRC::shuffle(std::string name)
{
    while (! enabled_)
    {
        sleep(1);
    }
    
    while(1)
    {
        // get request from arbiter
        std::lock_guard<std::mutex> guard(smutex_);
        //sleep(2);
        
        
        for (auto ictrlpipe : ictrlpipes_)
        {
            if ( ! ictrlpipe->empty() )
            {
                ictrlcounters_[0]++;
                toolbox::mem::Reference * ref = ictrlpipe->get();
                shub::ShuffleRequest * request = (shub::ShuffleRequest*)ref->getDataLocation();
                LOG4CPLUS_DEBUG(getApplicationLogger(), "shuffle - receive network requests from arbiter requests num " << request->num << " for node " << request->node);
                for (size_t i = 0; i < request->num; i++)
                {
                    LOG4CPLUS_DEBUG(getApplicationLogger(), "shuffle -  total requests:" << request->num << " fragmentid: " << request->shuffle[i].id  << " on resource: " << request->shuffle[i].resource << " for node: " << request->node );
                    shub::NodeRC::ShuffleCommand cmd = {request->shuffle[i].id, request->node, request->shuffle[i].resource};
                    shufflerequests_->push_back(cmd);
                }
                
                ictrlpipe->post(ref);
            }
        }
        
        // associate arbiter requests with resource and destination with input
        while ( ! idata_->empty() && ! shufflerequests_->empty())
        {
            LOG4CPLUS_DEBUG(getApplicationLogger(), "shuffle - processing shuffle request, pairing to input frame");
            
            shub::NodeRC::ShuffleCommand cmd = shufflerequests_->front();
            shufflerequests_->pop_front();
            
            toolbox::mem::Reference * ref = idata_->front();
            idata_->pop_front();
            
            //std::cout << "--- Node --- >>> received size  " << ref->getDataSize()  << " offset " << ref->getDataOffset() << std::endl;
            // Assume input data has reserved size for descriptor
            FragmentDescriptor * fragment = (FragmentDescriptor*)ref->getDataLocation();
            fragment->id = cmd.id;
            fragment->resource = cmd.resource;
            fragment->node = this->getApplicationDescriptor()->getInstance(); // from node
            size_t destination = cmd.node;
            
            // shuffle fragment id to corresponding Node
            LOG4CPLUS_DEBUG(getApplicationLogger(), "shuffle - Found input fragmentid " << fragment->id << "  for destination " << cmd.node  );
            if ( destination < nodes_.size() )
            {
                xdaq::ApplicationDescriptor *remoteNode = nodes_[destination];
                
                LOG4CPLUS_DEBUG(getApplicationLogger(), "shuffle - matching descriptor on remote  instance " << remoteNode->getInstance() );
                
                auto oit = opipes_.find(remoteNode);
                if ( oit != opipes_.end() )
                {
                    LOG4CPLUS_DEBUG(getApplicationLogger(), "shuffle - posting send fragmentid " << fragment->id << " to outputpipe on destination instance  " << remoteNode->getInstance() << " matching " << destination);
                    oit->second->post(ref);  // send frame to node
                }
                else
                {
                    std::cout << "*** warning: frame cannot be send to output , build will be incompleted " << std::endl;
                    idatafree_->push_back(ref);
                }
            }
            else
            {
                std::cout << "*** warning: frame cannot be send to output , build will be incompleted  missing destination" << std::endl;
                idatafree_->push_back(ref);
            }
        }
        
        // recycles completed frames
        for (auto oit = opipes_.begin() ; oit != opipes_.end(); oit++ )
        {
            while ( ! oit->second->empty() )
            {
                toolbox::mem::Reference * r = oit->second->get();
                //std::cout << "Shuffle recycle buffer in pool " << std::hex << (size_t)r << std::dec << " offset " << r->getDataOffset() << " original size " << r->getBuffer()->getSize() << std::endl;
                LOG4CPLUS_TRACE(getApplicationLogger(), "shuffle -  recycle output completed frame send in idatafree");
                idatafree_->push_back(r);
            }
        }
    }
}

void shub::NodeRC::builder(std::string name)
{
    while (! enabled_)
    {
        sleep(1);
    }
    
    while(1)
    {
        std::lock_guard<std::mutex> guard(bmutex_);
        
        for (auto ipipe: ipipes_ )
        {
            if ( ! ipipe->empty() )
            {
                toolbox::mem::Reference * ref = ipipe->get();
                
                //std::cout << "--- Node --- >>> received size  " << ref->getDataSize()  << " offset " << ref->getDataOffset() << std::endl;
                FragmentDescriptor * fragment = (FragmentDescriptor*)ref->getDataLocation();
                size_t id = fragment->id;
                size_t node = fragment->node;
                size_t resource = fragment->resource;
                
                LOG4CPLUS_DEBUG(getApplicationLogger(), "builder - going to build event with fragmentid " << id << " from node " << node << " resource " << resource );
                if ( rpool_->append(resource, id, node, ref) ) // completed
                {
                    LOG4CPLUS_DEBUG(getApplicationLogger(), "builder - completed event for fragment " << id <<" resource " << resource   );
                    counter_++;
                    const shub::ResourcePool::Resource & rdes =  rpool_->get(resource);
                    
                    LOG4CPLUS_DEBUG(getApplicationLogger(), "builder - recycle frames for fragment " << rdes.fragmentid << " num fragments " << rdes.total << " completed " << rdes.completed  );
                    for ( size_t index = 0; index < rdes.fragments.size(); index++)
                    {
                        if ( index < ipipes_.size() ) // is pipe still available for this index
                        {
                            LOG4CPLUS_DEBUG(getApplicationLogger(), "builder - free buffer back to input pipe " << index);
                            ipipes_[index]->post(rdes.fragments[index]);
                        }
                        else
                        {
                            LOG4CPLUS_WARN(getApplicationLogger(), "spurious frame, pipe not available");
                        }
                    }
                    rpool_->free(resource);
                }
                //char * frame = (char*)ref->getDataLocation();
                //toolbox::hexdump ((void*) frame, 16);
            }
        }
        
        //prepare arbiter about free resources available
        while ( ! rpool_->empty() && ! freerequestslist_->empty()  )
        {
            size_t resource = rpool_->alloc();
            LOG4CPLUS_DEBUG(getApplicationLogger(), "processing available resources from rpool resource " << resource);
            
            toolbox::mem::Reference * ref = freerequestslist_->front();
            ResourceAvailable * request = (ResourceAvailable*)ref->getDataLocation();
            request->node = this->getApplicationDescriptor()->getInstance();
            request->resource[request->num] = resource;
            request->num++;
            if ( request->num >= this->maxGather_ ) // test with gather default 1
            {
	    	size_t fsize = sizeof(uint64_t) + sizeof(uint64_t) + (sizeof(uint64_t) * request->num);
                ref->setDataSize(fsize);
                freerequestslist_->pop_front();
                pendingrequests_->push_back(ref);
                LOG4CPLUS_DEBUG(getApplicationLogger(), "pending requests ready for arbiter gather is " << request->num << " frame size is " << fsize << " total allowed is " << this->maxBlockSize_);
            }
        }
        
        // send to arbiter
        for ( auto & [descriptor,opipe] : octrlpipes_)
        {
            if ( ! pendingrequests_->empty() )
            {
                LOG4CPLUS_DEBUG(getApplicationLogger(), "going  to send to arbiter");
                toolbox::mem::Reference * ref = pendingrequests_->front();
                ResourceAvailable * request = (ResourceAvailable*)ref->getDataLocation();
                LOG4CPLUS_DEBUG(getApplicationLogger(), "build - send arbiter num requests " << request->num << " for node " << request->node );
                pendingrequests_->pop_front();
                opipe->post(ref);
                octrlcounters_[descriptor->getInstance()]++;

            }
            
            while ( ! opipe->empty() ) // recycle processed requests
            {
                LOG4CPLUS_DEBUG(getApplicationLogger(), "recycle frame sent to arbiter back  to freelist ");
                toolbox::mem::Reference * ref = opipe->get();
                ResourceAvailable * request = (ResourceAvailable*)ref->getDataLocation();
                request->num = 0;
                freerequestslist_->push_back(ref);
            }
        }
    }
}

void shub::NodeRC::measure (std::string name)
{
    while(1)
    {
        ::sleep(4);
        
        toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();
        double delta = (double)now - (double)lastTime_;
        double rate = counter_ / delta;
        currentRate_ = rate;
        counter_ = 0;
        lastTime_ =  now;
        
        std::cout << "event rate is " << rate/1000 <<  " kHz bandwidth " << (rate * currentLen_* replicas_)/1073741824 << " GB/s" << std::endl;

        //std::list<size_t> eventids = events_->get();
        //std::pair<size_t,size_t> stats = events_->stats();
        //std::cout << "pending events " << eventids.size() << "stats " << stats.first << " partial " << stats.second  << std::endl;
        /*
         {
         std::lock_guard<std::mutex> guard(bmutex_);
         std::cout << "stat build - freerequestslist  " << freerequestslist_->elements() << std::endl;
         std::cout  <<"stat build - pendingrequests   " << pendingrequests_->elements() << std::endl;
         std::cout  <<"stat build - resource pool     " << rpool_->elements() << std::endl;
         }
         {
         std::lock_guard<std::mutex> guard(smutex_);
         std::cout  <<"stat shuffle - idata             " << idata_->elements() << std::endl;
         std::cout  <<"stat shuffle - shufflerequests   " << shufflerequests_->elements() << std::endl;
         }
         */
    }
}

void shub::NodeRC::Default (xgi::Input * in, xgi::Output * out)
{
    *out << "<div class=\"xdaq-tab-wrapper\">" << std::endl;

    *out << "<div class=\"xdaq-tab\" title=\"Statistics\">" << std::endl;
    *out << cgicc::table().set("class", "xdaq-table") << std::endl;
    *out << cgicc::caption("Total Input Requests per Node") << std::endl;

    *out << cgicc::thead() << std::endl;

    *out << cgicc::tr();
    *out << cgicc::th("Index");
    *out << cgicc::th("Input Control Requests");
    *out << cgicc::tr() << std::endl;

    *out << cgicc::thead() << std::endl;


    *out << cgicc::tbody() << std::endl;
    for ( size_t i = 0; i < ictrlcounters_.size(); i++)
    {
        *out << cgicc::tr();
        *out << "<td>" << i << "</td>";
        *out << "<td>" << ictrlcounters_[i] << "</td>";
        *out << cgicc::tr();
    }
    
    *out << cgicc::tbody();
    *out << cgicc::table();
    
    *out << cgicc::table().set("class", "xdaq-table") << std::endl;
    *out << cgicc::caption("Total Output Control Request") << std::endl;

    *out << cgicc::thead() << std::endl;

    *out << cgicc::tr();
    *out << cgicc::th("Index");
    *out << cgicc::th("Output Control Requests");
    *out << cgicc::tr() << std::endl;

    *out << cgicc::thead() << std::endl;


    *out << cgicc::tbody() << std::endl;
    for ( size_t i = 0; i < octrlcounters_.size(); i++)
    {
        *out << cgicc::tr();
        *out << "<td>" << i << "</td>";
        *out << "<td>" << octrlcounters_[i] << "</td>";
        *out << cgicc::tr();
    }

    *out << cgicc::tbody();
    *out << cgicc::table();




    *out << "</div>";


    *out << "</div>";
}

