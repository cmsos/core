
/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simlevicius					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <thread>
#include <algorithm>

#include "shub/NodeRC.h"

#include "xcept/Exception.h"
#include "xcept/tools.h"

#include "toolbox/BSem.h"
#include "toolbox/rlist.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/CommittedHeapAllocator.h"
#include "toolbox/task/TimerTask.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"

#include "xdaq/Application.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationGroup.h"
#include "xoap/MessageFactory.h"
#include "xdaq/NamespaceURI.h"
#include "xdaq/pipe/Input.h"
#include "xdaq/pipe/Output.h"
#include "xdaq/pipe/Interface.h"

#include "xgi/Method.h"
#include "xgi/framework/Method.h"

XDAQ_INSTANTIATOR_IMPL(shub::NodeRC)

//#define MESSAGE_SIZE 8196
#define MESSAGE_SIZE 131072

shub::NodeRC::NodeRC (xdaq::ApplicationStub* c): xdaq::Application(c), xgi::framework::UIManager(this)
{
    c->getDescriptor()->setAttribute("icon", "/shub/images/node.png");
    c->getDescriptor()->setAttribute("icon16x16", "/shub/images/node.png");
    
    poolName_ =  "tpi";
    this->getApplicationInfoSpace()->fireItemAvailable("poolName", &poolName_);
    this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
    this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:ParameterSet");
    this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:ParameterGet");
    this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:ParameterQuery");
    
    this->getApplicationContext()->addActionListener(this); //listen to executive events

    this->maxResources_ = 8;
    this->maxBlockSize_ = 4096;
    this->maxGather_ = 1;
    this->masterArbiterInstance_ = 0; // default arbiter instance 0
    getApplicationInfoSpace()->fireItemAvailable("maxResources", &maxResources_);
    getApplicationInfoSpace()->fireItemAvailable("maxBlockSize", &maxBlockSize_);
    getApplicationInfoSpace()->fireItemAvailable("maxGather", &maxGather_);
    getApplicationInfoSpace()->fireItemAvailable("masterArbiterInstance", &masterArbiterInstance_);
    counter_ = 0;
    currentFragmentId_ = 0;
    enabled_ = false;
    superFragmentSize_ = MESSAGE_SIZE;
    eventRate_ = 0;
    getApplicationInfoSpace()->fireItemAvailable("superFragmentSize", &superFragmentSize_);
    getApplicationInfoSpace()->fireItemAvailable("eventRate", &eventRate_);
    
    // default accepted network names
    acceptedDataNetwork_  = "nodercdata";
    acceptedCtrlNetwork_  = "nodercctrl";
    acceptedInputNetwork_ = "nodercinput";
    getApplicationInfoSpace()->fireItemAvailable("acceptedDataNetwork", &acceptedDataNetwork_);
    getApplicationInfoSpace()->fireItemAvailable("acceptedCtrlNetwork", &acceptedCtrlNetwork_);
    getApplicationInfoSpace()->fireItemAvailable("acceptedInputNetwork", &acceptedInputNetwork_);
    
    currentLen_ = superFragmentSize_;
    currentRate_ = eventRate_;
    
    // Bind CGI callbacks
    xgi::framework::deferredbind(this, this, &shub::NodeRC::Default, "Default");
    
    connectedness_ = false;
    getApplicationInfoSpace()->fireItemAvailable("connectedness", &connectedness_);

    
}

shub::NodeRC::~NodeRC()
{
}

//
// Initialization
//
void shub::NodeRC::actionPerformed (xdata::Event& event)
{
    if (event.type() == "urn:xdaq-event:setDefaultValues")
    {
        try
        {
            counter_ = 0;
            
            auto nodes = getApplicationContext()->getDefaultZone()->getApplicationDescriptors("shub::NodeRC");
            nodes_.resize(nodes.size());
            for (size_t n = 0; n <nodes_.size(); n++ )
            {
                nodes_[n] = 0;
            }
            
            auto controllers = getApplicationContext()->getDefaultZone()->getApplicationDescriptors("shub::Arbiter");
            controllers_.resize(controllers.size());
            for (size_t c = 0; c < controllers.size(); c++ )
            {
                controllers_[c] = 0;
            }
            
            // add Nodes
            for ( auto d: nodes )
            {
                size_t inum = d->getInstance();
                if ( nodes_[inum] != 0 )
                {
                    XCEPT_RAISE(xdaq::exception::Exception, "instance already exisiting for node");
                }
                
                nodes_[inum] = const_cast<xdaq::ApplicationDescriptor*>(d);
                LOG4CPLUS_INFO(getApplicationLogger(), "Found NodeRC " <<  d->getContextDescriptor()->getURL() <<  "/" <<  d->getURN() << " instance " << d->getInstance() );
                
                dconnections_[nodes_[inum]] = ConnectionStatus::idle;
            }
            
            // add Arbiter
            for ( auto c: controllers )
            {
                size_t inum = c->getInstance();
                if ( controllers_[inum] != 0 )
                {
                    XCEPT_RAISE(xdaq::exception::Exception, "instance already exisiting for arbiter");
                }
                controllers_[inum] = const_cast<xdaq::ApplicationDescriptor*>(c);
                LOG4CPLUS_INFO(getApplicationLogger(), "Found Arbiter " <<  c->getContextDescriptor()->getURL() <<  "/" <<  c->getURN() << " instance " << c->getInstance() );
                cconnections_[controllers_[inum]] = ConnectionStatus::idle;
            }
            
            replicas_ =  nodes.size();
            
            //rpool_ = new shub::ResourcePool(replicas_,maxReceiveBuffers_);
            rpool_ = new shub::ResourcePool(replicas_,maxResources_);
            
            // Only one counter for control
            octrlcounters_.push_back(0);
            ictrlcounters_.push_back(0);
            
            LOG4CPLUS_INFO(getApplicationLogger(), "Found " << replicas_ << " replica Nodes RC");
            LOG4CPLUS_INFO(getApplicationLogger(), "Found " << controllers.size() << " Arbiters");
        }
        catch (xdaq::exception::Exception& e)
        {
            LOG4CPLUS_FATAL(getApplicationLogger(), xcept::stdformat_exception_history(e));
            return;
        }
        
        try
        {
            std::string pname = poolName_.toString();
            toolbox::net::URN urn("toolbox-mem-pool", pname);
            pool_ = toolbox::mem::getMemoryPoolFactory()->findPool(urn);
            
        }
        catch (toolbox::mem::exception::Exception & e)
        {
            LOG4CPLUS_FATAL(getApplicationLogger(), xcept::stdformat_exception_history(e));
            return;
        }
        

        size_t nrequests = (maxResources_ / maxGather_)+1;
        freerequestslist_ = toolbox::rlist<toolbox::mem::Reference*>::create("shub-node-freerequestlist",nrequests * 2); // *2 is security margin to cope with local fifo if applies
        pendingrequests_ = toolbox::rlist<toolbox::mem::Reference*>::create("shub-node-pendingrequests",nrequests * 2);
        size_t nshuffles = (maxResources_ * replicas_);
        shufflerequests_ = toolbox::rlist<ShuffleCommand>::create("shub-node-shufflerequests",nshuffles);
        
        
        // initialize requests frame for arbiter
        for (size_t k = 0; k < nrequests; k++)
        {
            toolbox::mem::Reference * reference = 0;
            try
            {
                reference = toolbox::mem::getMemoryPoolFactory()->getFrame(pool_, this->maxBlockSize_);
            }
            catch (toolbox::mem::exception::Exception & e)
            {
                LOG4CPLUS_FATAL(getApplicationLogger(), toolbox::toString("Could not allocate block for input, %s", e.what()));
            }
            freerequestslist_->push_back(reference);
        }
        
        
        pipeService_ = xdaq::getPipeInterface()->join(this,this);
        
        currentLen_.store(superFragmentSize_);
        
        size_t fsize = sizeof(uint64_t) + sizeof(uint64_t) + (sizeof(uint64_t) * this->maxGather_);
        if ( fsize > this->maxBlockSize_ )
        {
            LOG4CPLUS_FATAL(getApplicationLogger(), "request to arbiter could overflows with size" << fsize << " max allowed is " << this->maxBlockSize_);
            exit(-1);
        }
    }
    else if (event.type() == "urn:xdaq-event:ParameterSet")
    {
        LOG4CPLUS_INFO(getApplicationLogger(), "Set currentLen to  " << superFragmentSize_ );
        
        currentLen_.store(superFragmentSize_);
    }
    else if ((event.type() == "urn:xdaq-event:ParameterGet") || (event.type() == "urn:xdaq-event:ParameterQuery"))
    {
        eventRate_ = currentRate_.load();
        
        LOG4CPLUS_INFO(getApplicationLogger(), "Get currentRate  " << eventRate_ );
        
        {
            size_t expected_odatapipes = 0;
            size_t expected_idatapipes = 0;
            size_t expected_ictrlpipes = 0;
            size_t expected_octrlpipes = 0;
            size_t expected_inputpipes = 0;

            {
                std::lock_guard<std::mutex> guardb(bmutex_);
                expected_idatapipes = idatapipes_.size();
                expected_octrlpipes = octrlpipes_.size();
            }

            {
                std::lock_guard<std::mutex> guards(smutex_);
                expected_odatapipes = odatapipes_.size();
                expected_ictrlpipes = ictrlpipes_.size();
                expected_inputpipes = inputpipes_.size();

            }
        
            if ( expected_odatapipes == replicas_  && expected_idatapipes == replicas_  && expected_ictrlpipes == 1 && expected_octrlpipes == 1 && expected_inputpipes == 1 )
                connectedness_ = true;
            else
                connectedness_ = false;
        }
    }
    
}

//
// Connection establishment handling
//
void shub::NodeRC::actionPerformed(xdaq::pipe::EstablishedConnection & ec ) 
{
    const xdaq::ApplicationDescriptor * d = ec.getDestinationDescriptor();
    
    LOG4CPLUS_DEBUG(getApplicationLogger(), "Connection is established from " << d->getURN() <<  " to " <<  d->getURN());
 
    if ( std::find(nodes_.begin(), nodes_.end(), d) != nodes_.end() )
    {
        size_t n = this->maxResources_ *2; //at most n packets will be sent to another NodeRC ( *2 security margin might not be necessary )
        xdaq::pipe::Output* opipe = pipeService_->createOutputPipe(ec, n);
        
        std::lock_guard<std::mutex> guard(smutex_);
        odatapipes_[d] = opipe;
        dconnections_[d] = ConnectionStatus::established;
        LOG4CPLUS_DEBUG(getApplicationLogger(), "outputpipe created for data status is Established" );
        return;
    }
    
    
    if ( std::find(controllers_.begin(), controllers_.end(), d) != controllers_.end() )
    {
        size_t n = ((this->maxResources_ / this->maxGather_)+1) * 2; // at most n packets sent to Arbiter ( *2 security margin might not be necessary )
        xdaq::pipe::Output* opipe = pipeService_->createOutputPipe(ec, n);
        
        std::lock_guard<std::mutex> guard(bmutex_);
        octrlpipes_[d]  = opipe;
        cconnections_[d] = ConnectionStatus::established;
        LOG4CPLUS_INFO(getApplicationLogger(), "outputpipe created for ctrl status is Established to instance " << d->getInstance() );
        return;
    }
}

bool shub::NodeRC::actionPerformed(xdaq::pipe::ConnectionRequest & cr)
{               
    std::string network = cr.getNetwork();
    LOG4CPLUS_DEBUG(getApplicationLogger(),"NodeRC - Connection request received from network " << network);
    
    auto provide =  [&](xdaq::pipe::Input * ip, size_t nblocks, size_t blockSize) {
        for (size_t k = 0; k < nblocks; k++)
        {
            toolbox::mem::Reference * reference = 0;
            try
            {
                reference = toolbox::mem::getMemoryPoolFactory()->getFrame(pool_, blockSize);
            }
            catch (toolbox::mem::exception::Exception & e)
            {
                LOG4CPLUS_FATAL(getApplicationLogger(), toolbox::toString("Could not allocate block for input, %s", e.what()));
            }
            ip->post(reference);
        }
    }; // end of lambda expression
    if ( network.find(acceptedDataNetwork_.toString()) != std::string::npos )
    {
        size_t n = this->maxResources_; // at most n packets received from other NodeRC ( *2 security margin might not be necessary )
        xdaq::pipe::Input * idatapipe =  pipeService_->createInputPipe(cr, n * 2);
        provide(idatapipe, n, this->maxBlockSize_);

        LOG4CPLUS_DEBUG(getApplicationLogger(),"regiter data pipe in idatapipe vector" << network);
        std::lock_guard<std::mutex> guard(bmutex_);
        idatapipes_.push_back(idatapipe);
    }
    else if ( network.find(acceptedCtrlNetwork_.toString()) != std::string::npos  )
    {
        size_t n = ((this->maxResources_ / this->maxGather_) +1 ) * replicas_; // at most n packets received from Arbiter ( *2 security margin might not be necessary )

        xdaq::pipe::Input * ctrlipipe =  pipeService_->createInputPipe(cr, n * 2);
        provide(ctrlipipe, n*2, this->maxBlockSize_); // *2 used to obtin a ouble buffer effect

        LOG4CPLUS_DEBUG(getApplicationLogger(),"register ctrl pipe in ictrlpipe vector" << network);
        std::lock_guard<std::mutex> guard(smutex_);
        ictrlpipes_.push_back(ctrlipipe);
    }
    else if ( network.find(acceptedInputNetwork_.toString()) != std::string::npos  )
    {
        size_t n = (this->maxResources_* replicas_); // at most n packets from Input ( *2 security margin might not be necessary )
        xdaq::pipe::Input * inputpipe =  pipeService_->createInputPipe(cr, n * 2);
        // *2 used to obtin a ouble buffer effect
        provide(inputpipe, n*2, this->maxBlockSize_); // alway get maxReceiveBuffers_ in addition from loopbak idatapipe

        LOG4CPLUS_DEBUG(getApplicationLogger(),"register input pipe in inputpipe vector" << network);
        std::lock_guard<std::mutex> guard(smutex_);
        inputpipes_.push_back(inputpipe);
    }
    else
    {
        LOG4CPLUS_WARN(getApplicationLogger(), "no pipe support for network " << cr.getNetwork() << ", ignore");
        return false;
    }
    return true;
}

//
// Connection error handing
//
void shub::NodeRC::actionPerformed(xdaq::pipe::ConnectionError & ce )
{
    //std::cout << "Connection error from " << ce.getSourceDescriptor()->getURN() <<  " to " <<  ce.getDestinationDescriptor()->getURN()<< std::endl;
    const xdaq::ApplicationDescriptor* d = ce.getDestinationDescriptor();
    std::string durl = d->getContextDescriptor()->getURL() + "/" + d->getURN();
    LOG4CPLUS_WARN(getApplicationLogger(), "Connection failed for " << durl <<  ", with error " << ce.getException().what());
    
    {
        std::lock_guard<std::mutex> guard(smutex_);
        for ( auto& [descriptor, status]: dconnections_ )
        {
            if ( descriptor == d )
            {
                dconnections_[d] = ConnectionStatus::idle;
                break;
            }
        }
    }
    
    {
        std::lock_guard<std::mutex> guard(bmutex_);
        for ( auto& [descriptor, status]: cconnections_ )
        {
            if ( descriptor == d )
            {
                cconnections_[d] = ConnectionStatus::idle;
                break;
            }
        }
    }
    // disable input generator
    enabled_ = false;
}

void shub::NodeRC::actionPerformed(xdaq::pipe::BrokenPipe & bp)
{
    LOG4CPLUS_WARN(getApplicationLogger(), "Broken Pipe, shoud destroy output pipe here");
    
    {
        std::lock_guard<std::mutex> guard(smutex_);
        for ( auto it = odatapipes_.begin(); it != odatapipes_.end(); it++ )
        {
            if ( (*it).second->match(bp.getPipeHandle()))
            {
                LOG4CPLUS_DEBUG(getApplicationLogger(),"Found matching output data pipe, going to destroy") ;
                
                xdaq::pipe::Output * odatapipe = (*it).second;
                dconnections_[(*it).first] = ConnectionStatus::idle;
                pipeService_->destroyOutputPipe(odatapipe);
                odatapipes_.erase(it);
                // disable input generator
                enabled_ = false;
                return;
            }
        }
    }
    
    {
        std::lock_guard<std::mutex> guard(bmutex_);
        for ( auto it = octrlpipes_.begin(); it != octrlpipes_.end(); it++ )
        {
            if ( (*it).second->match(bp.getPipeHandle()))
            {
                LOG4CPLUS_DEBUG(getApplicationLogger(),"Found matching output control pipe, going to destroy") ;
                
                xdaq::pipe::Output * octrlpipe = (*it).second;
                cconnections_[(*it).first] = ConnectionStatus::idle;
                pipeService_->destroyOutputPipe(octrlpipe);
                octrlpipes_.erase(it);
                // disable input generator
                enabled_ = false;
                return;
            }
        }
    }
}

void shub::NodeRC::actionPerformed(xdaq::pipe::ConnectionClosedByPeer & ccbp )
{
    
    LOG4CPLUS_WARN(getApplicationLogger(), "Connection closed by peer in server going to delete input pipe from input list");
    {
        std::lock_guard<std::mutex> guard(bmutex_);
        
        for ( auto pipeit = idatapipes_.begin(); pipeit != idatapipes_.end(); pipeit++ )
        {
            if ( (*pipeit)->match(ccbp.getPipeHandle()))
            {
                LOG4CPLUS_WARN(getApplicationLogger(), "Found matching input data pipe, going to destroy");
                pipeService_->destroyInputPipe(*pipeit);
                idatapipes_.erase(pipeit);
                // disable input generator
                enabled_ = false;
                return;
            }
        }
    }
    
    {
        std::lock_guard<std::mutex> guard(smutex_);
        
        for ( auto pipeit =  ictrlpipes_.begin(); pipeit != ictrlpipes_.end(); pipeit++ )
        {
            if ( (*pipeit)->match(ccbp.getPipeHandle()))
            {
                LOG4CPLUS_WARN(getApplicationLogger(), "Found matching input ctrl pipe, going to destroy");
                pipeService_->destroyInputPipe(*pipeit);
                ictrlpipes_.erase(pipeit);
                // disable input generator
                enabled_ = false;
                return;
            }
        }
    }
    
}

//
// Multithread work loops (connect, input, shuffling, building, measure )
//
void shub::NodeRC::connect(std::string name)
{
    
    // validate readiness example
    auto readiness = dynamic_cast<xdata::Boolean*>(this->getApplicationInfoSpace()->find("readiness"));
    *readiness = true;
    this->getApplicationInfoSpace()->fireItemValueChanged("readiness", this);

    LOG4CPLUS_DEBUG (this->getApplicationLogger(), "starting connection thread " << name);
    
    while(1)
    {
        ::sleep(10);
        
        std::string surl = this->getApplicationContext()->getContextDescriptor()->getURL() + "/" + this->getApplicationDescriptor()->getURN();
        
        LOG4CPLUS_DEBUG(getApplicationLogger(), "connection status: (odatapipes " <<  odatapipes_.size()<< " of " << replicas_ << ")" << "(idatapipes " << idatapipes_.size() << " of " << replicas_ << ")" << "(ictrlpipes " <<  ictrlpipes_.size() << " of 1)" << "(octrlpipe " << octrlpipes_.size() << " of 1)"  << "(inputpipe " << inputpipes_.size() << " of 1)");
        
        {
            std::lock_guard<std::mutex> guard(smutex_);
            for ( auto& [descriptor, status]: dconnections_ )
            {
                if ( status == ConnectionStatus::idle )
                {
                    std::string durl = descriptor->getContextDescriptor()->getURL() + "/" + descriptor->getURN();
                    LOG4CPLUS_INFO(getApplicationLogger(), "requesting data connection from " << surl << " to " << durl << " status Idle to Connecting");
                    status  = ConnectionStatus::connecting;
                    pipeService_->connect(this->getApplicationDescriptor(), descriptor);
                    LOG4CPLUS_DEBUG(getApplicationLogger(), "done requesting data connection");
                }
            }
        }
        
        {
            std::lock_guard<std::mutex> guard(bmutex_);
            for ( auto& [descriptor, status]: cconnections_ )
            {
                if ( status == ConnectionStatus::idle  && (descriptor->getInstance() == masterArbiterInstance_))
                {
                    std::string durl = descriptor->getContextDescriptor()->getURL() + "/" + descriptor->getURN();
                    LOG4CPLUS_INFO(getApplicationLogger(), "requesting ctrl connection from " << surl << " to " << durl << " status Idle to Connecting");
                    status  = ConnectionStatus::connecting;
                    pipeService_->connect(this->getApplicationDescriptor(), descriptor);
                    LOG4CPLUS_DEBUG(getApplicationLogger(), "done requesting ctrl connection");
                }
            }
        }
    }
}

void shub::NodeRC::autostart(std::string name)
{
    LOG4CPLUS_DEBUG(getApplicationLogger(), "starting input generation loop..." << name);
    
    // reset for a rough synchonization
    currentFragmentId_ = 0;
    
    bool established = false;
    while ( ! established )
    {
        LOG4CPLUS_INFO(getApplicationLogger(), "waiting rc nodes connection...");
        ::sleep(5);
        size_t expected_odatapipes = 0;
        size_t expected_idatapipes = 0;
        size_t expected_ictrlpipes = 0;
        size_t expected_octrlpipes = 0; 
        size_t expected_inputpipes = 0;

        {
        	std::lock_guard<std::mutex> guardb(bmutex_);
            expected_idatapipes = idatapipes_.size();
            expected_octrlpipes = octrlpipes_.size();
        }

        {
        	std::lock_guard<std::mutex> guards(smutex_);
            expected_odatapipes = odatapipes_.size();
            expected_ictrlpipes = ictrlpipes_.size();
            expected_inputpipes = inputpipes_.size();
        }
        
        LOG4CPLUS_INFO(getApplicationLogger(), "connection status: (odatapipes " <<  expected_odatapipes << " of " << replicas_ << ")" << "(idatapipes " << expected_idatapipes << " of " << replicas_ << ")" << "(ictrlpipes " <<  expected_ictrlpipes << " of 1)" << "(octrlpipe " << expected_octrlpipes << " of 1)" << "(inputpipes " << expected_inputpipes << " of 1)" );
        
        if ( expected_odatapipes == replicas_  && expected_idatapipes == replicas_  && expected_ictrlpipes == 1 && expected_octrlpipes == 1 && expected_inputpipes == 1 )
        {
            established = true;
        }
        
    }
    
    LOG4CPLUS_INFO(getApplicationLogger(), "all rc nodes connected");
    //sleep(5);
    enabled_ = true;
    return;
    
}

void shub::NodeRC::shuffle(std::string name)
{
    this->applyThreadPolicy(name);
    
    while (! enabled_)
    {
        sleep(1);
    }
    
    while(1)
    {
        // get request from arbiter
        std::lock_guard<std::mutex> guard(smutex_);
        //sleep(2);
        
        
        for (auto ictrlpipe : ictrlpipes_)
        {
            if ( ! ictrlpipe->empty() )
            {
                ictrlcounters_[0]++;
                toolbox::mem::Reference * ref = ictrlpipe->get();
                shub::ShuffleRequest * request = (shub::ShuffleRequest*)ref->getDataLocation();
                LOG4CPLUS_DEBUG(getApplicationLogger(), "shuffle - receive network requests from arbiter requests num " << request->num << " for node " << request->node);
                for (size_t i = 0; i < request->num; i++)
                {
                    LOG4CPLUS_DEBUG(getApplicationLogger(), "shuffle -  total requests:" << request->num << " fragmentid: " << request->shuffle[i].id  << " on resource: " << request->shuffle[i].resource << " for node: " << request->node );
                    shub::NodeRC::ShuffleCommand cmd = {request->shuffle[i].id, request->node, request->shuffle[i].resource};
                    shufflerequests_->push_back(cmd);
                }
                
                ictrlpipe->post(ref);
            }
        }
        
        // associate arbiter requests with resource and destination with input
        auto inputpipe = *(inputpipes_.begin()); // one input pipe only
        while ( ! inputpipe->empty() && ! shufflerequests_->empty())
        {
            LOG4CPLUS_DEBUG(getApplicationLogger(), "shuffle - processing shuffle request, pairing to input frame");
            
            shub::NodeRC::ShuffleCommand cmd = shufflerequests_->front();
            shufflerequests_->pop_front();
            
            toolbox::mem::Reference * ref = inputpipe->get();
            
            //std::cout << "--- Node --- >>> received size  " << ref->getDataSize()  << " offset " << ref->getDataOffset() << std::endl;
            // Assume input data has reserved size for descriptor
            FragmentDescriptor * fragment = (FragmentDescriptor*)ref->getDataLocation();
            fragment->id = cmd.id;
            fragment->resource = cmd.resource;
            fragment->node = this->getApplicationDescriptor()->getInstance(); // from node
            size_t destination = cmd.node;
            
            // shuffle fragment id to corresponding Node
            LOG4CPLUS_DEBUG(getApplicationLogger(), "shuffle - Found input fragmentid " << fragment->id << "  for destination " << cmd.node  );
            if ( destination < nodes_.size() )
            {
                xdaq::ApplicationDescriptor *remoteNode = nodes_[destination];
                
                LOG4CPLUS_DEBUG(getApplicationLogger(), "shuffle - matching descriptor on remote  instance " << remoteNode->getInstance() );
                
                auto oit = odatapipes_.find(remoteNode);
                if ( oit != odatapipes_.end() )
                {
                    LOG4CPLUS_DEBUG(getApplicationLogger(), "shuffle - posting send fragmentid " << fragment->id << " to outputpipe on destination instance  " << remoteNode->getInstance() << " matching " << destination);
                    oit->second->post(ref);  // send frame to node
                }
                else
                {
                    LOG4CPLUS_WARN(getApplicationLogger(), "frame cannot be send to output , build will be incompleted, cannot find remote pipe ");
                    inputpipe->post(ref);
                }
            }
            else
            {
                LOG4CPLUS_WARN(getApplicationLogger(), "frame cannot be send to output , build will be incompleted, missing destination");
                inputpipe->post(ref);
            }
        }
        
        // recycles completed frames
        for (auto oit = odatapipes_.begin() ; oit != odatapipes_.end(); oit++ )
        {
            while ( ! oit->second->empty() )
            {
                toolbox::mem::Reference * r = oit->second->get();
                //std::cout << "Shuffle recycle buffer in pool " << std::hex << (size_t)r << std::dec << " offset " << r->getDataOffset() << " original size " << r->getBuffer()->getSize() << std::endl;
                LOG4CPLUS_TRACE(getApplicationLogger(), "shuffle -  recycle output completed frame send in idatafree");
                inputpipe->post(r);
            }
        }
    }
}

void shub::NodeRC::builder(std::string name)
{
    this->applyThreadPolicy(name);
    
    while (! enabled_)
    {
        sleep(1);
    }
    
    while(1)
    {
        std::lock_guard<std::mutex> guard(bmutex_);
        
        for (auto idatapipe: idatapipes_ )
        {
            if ( ! idatapipe->empty() )
            {
                toolbox::mem::Reference * ref = idatapipe->get();
                
                //std::cout << "--- Node --- >>> received size  " << ref->getDataSize()  << " offset " << ref->getDataOffset() << std::endl;
                FragmentDescriptor * fragment = (FragmentDescriptor*)ref->getDataLocation();
                size_t id = fragment->id;
                size_t node = fragment->node;
                size_t resource = fragment->resource;
                
                LOG4CPLUS_DEBUG(getApplicationLogger(), "builder - going to build event with fragmentid " << id << " from node " << node << " resource " << resource );
                if ( rpool_->append(resource, id, node, ref) ) // completed
                {
                    LOG4CPLUS_DEBUG(getApplicationLogger(), "builder - completed event for fragment " << id <<" resource " << resource   );
                    counter_++;
                    const shub::ResourcePool::Resource & rdes =  rpool_->get(resource);
                    
                    LOG4CPLUS_DEBUG(getApplicationLogger(), "builder - recycle frames for fragment " << rdes.fragmentid << " num fragments " << rdes.total << " completed " << rdes.completed  );
                    for ( size_t index = 0; index < rdes.fragments.size(); index++)
                    {
                        if ( index < idatapipes_.size() ) // is pipe still available for this index
                        {
                            LOG4CPLUS_DEBUG(getApplicationLogger(), "builder - free buffer back to input pipe " << index);
                            idatapipes_[index]->post(rdes.fragments[index]);
                        }
                        else
                        {
                            LOG4CPLUS_WARN(getApplicationLogger(), "spurious frame, pipe not available");
                        }
                    }
                    rpool_->free(resource);
                }
                //char * frame = (char*)ref->getDataLocation();
                //toolbox::hexdump ((void*) frame, 16);
            }
        }
        
        //prepare arbiter about free resources available
        while ( ! rpool_->empty() && ! freerequestslist_->empty()  )
        {
            size_t resource = rpool_->alloc();
            LOG4CPLUS_DEBUG(getApplicationLogger(), "processing available resources from rpool resource " << resource);
            
            toolbox::mem::Reference * ref = freerequestslist_->front();
            ResourceAvailable * request = (ResourceAvailable*)ref->getDataLocation();
            request->node = this->getApplicationDescriptor()->getInstance();
            request->resource[request->num] = resource;
            request->num++;
            if ( request->num >= this->maxGather_ ) // test with gather default 1
            {
                size_t fsize = sizeof(uint64_t) + sizeof(uint64_t) + (sizeof(uint64_t) * request->num);
                ref->setDataSize(fsize);
                freerequestslist_->pop_front();
                pendingrequests_->push_back(ref);
                LOG4CPLUS_DEBUG(getApplicationLogger(), "pending requests ready for arbiter gather is " << request->num << " frame size is " << fsize << " total allowed is " << this->maxBlockSize_);
            }
        }
        
        // send to arbiter
	// NEW
	xdaq::ApplicationDescriptor *arbiter = controllers_[masterArbiterInstance_];
        auto octrlit = octrlpipes_.find(arbiter);
        if ( octrlit != octrlpipes_.end() )
	{
            if ( ! pendingrequests_->empty() )
            {
                LOG4CPLUS_DEBUG(getApplicationLogger(), "going  to send to arbiter");
                toolbox::mem::Reference * ref = pendingrequests_->front();
                ResourceAvailable * request = (ResourceAvailable*)ref->getDataLocation();
                LOG4CPLUS_DEBUG(getApplicationLogger(), "build - send arbiter num requests " << request->num << " for node " << request->node );
                pendingrequests_->pop_front();
                (*octrlit).second->post(ref);
                octrlcounters_[arbiter->getInstance()]++;

            }

            while ( ! (*octrlit).second->empty() ) // recycle processed requests
            {
                LOG4CPLUS_DEBUG(getApplicationLogger(), "recycle frame sent to arbiter back  to freelist ");
                toolbox::mem::Reference * ref = (*octrlit).second->get();
                ResourceAvailable * request = (ResourceAvailable*)ref->getDataLocation();
                request->num = 0;
		try
		{
                	freerequestslist_->push_back(ref);
		}
		catch(toolbox::exception::RingListFull & e)
		{
			LOG4CPLUS_FATAL(getApplicationLogger(), "Failed to push in freerequestslist_, with error: " <<  xcept::stdformat_exception_history(e));			
			return;
		}
            }

	}
        else
        {
               LOG4CPLUS_ERROR(getApplicationLogger(), "cannot find arbiter output control pipe at instance " << 0 << ", stop builder");
	       return;
        }

    }
}

void shub::NodeRC::measure (std::string name)
{
    while(1)
    {
        ::sleep(10);
        
        toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();
        double delta = (double)now - (double)lastTime_;
        double rate = counter_ / delta;
        currentRate_ = rate;
        counter_ = 0;
        lastTime_ =  now;
        
        //std::cout << "noderc event rate is " << rate/1000 <<  " kHz bandwidth " << (rate * currentLen_* replicas_)/1073741824 << " GB/s" << std::endl;
        std::cout << "noderc event rate is " << rate/1000 <<  " kHz bandwidth " << (rate * currentLen_* replicas_)/1000000000 << " GB/s (replicas="<< replicas_ << ",currentLen_=" << currentLen_ << ")" << std::endl;
        
        //std::list<size_t> eventids = events_->get();
        //std::pair<size_t,size_t> stats = events_->stats();
        //std::cout << "pending events " << eventids.size() << "stats " << stats.first << " partial " << stats.second  << std::endl;
        /*
         {
         std::lock_guard<std::mutex> guard(bmutex_);
         std::cout << "stat build - freerequestslist  " << freerequestslist_->elements() << std::endl;
         std::cout  <<"stat build - pendingrequests   " << pendingrequests_->elements() << std::endl;
         std::cout  <<"stat build - resource pool     " << rpool_->elements() << std::endl;
         }
         {
         std::lock_guard<std::mutex> guard(smutex_);
         std::cout  <<"stat shuffle - idata             " << idata_->elements() << std::endl;
         std::cout  <<"stat shuffle - shufflerequests   " << shufflerequests_->elements() << std::endl;
         }
         */
    }
}

void shub::NodeRC::Default (xgi::Input * in, xgi::Output * out)
{
    *out << "<div class=\"xdaq-tab-wrapper\">" << std::endl;
    
    *out << "<div class=\"xdaq-tab\" title=\"Connections\">" << std::endl;
    *out << cgicc::table().set("class", "xdaq-table") << std::endl;
    *out << cgicc::caption("Arbiter's Output Connections") << std::endl;
    
    *out << cgicc::thead() << std::endl;
    
    *out << cgicc::tr();
    *out << cgicc::th("Instance");
    *out << cgicc::th("Status");
    *out << cgicc::th("URL");
    *out << cgicc::th("Instance cross check");
    *out << cgicc::tr() << std::endl;
    
    *out << cgicc::thead() << std::endl;
    
    *out << cgicc::tbody() << std::endl;
    for ( size_t i = 0; i < controllers_.size(); i++)
    {
        const xdaq::ApplicationDescriptor*  c = controllers_[i];
        *out << cgicc::tr();
        *out << "<td>" << i << "</td>";
        *out << "<td>" << shub::statusToString(cconnections_[c]) << "</td>";
        *out << "<td>" <<  c->getContextDescriptor()->getURL() <<  "/" <<  c->getURN() << " instance " << c->getInstance() << "</td>";
        *out << "<td>" <<  c->getInstance() << "</td>";
        *out << cgicc::tr();
    }
    
    *out << cgicc::tbody();
    *out << cgicc::table();
    *out << cgicc::table().set("class", "xdaq-table") << std::endl;
    *out << cgicc::caption("NodeRC's Output Connections") << std::endl;
    
    *out << cgicc::thead() << std::endl;
    
    *out << cgicc::tr();
    *out << cgicc::th("Instance");
    *out << cgicc::th("Status");
    *out << cgicc::th("URL");
    *out << cgicc::th("Instance cross check");
    *out << cgicc::tr() << std::endl;
    
    *out << cgicc::thead() << std::endl;
    
    *out << cgicc::tbody() << std::endl;
    for ( size_t i = 0; i < nodes_.size(); i++)
    {
        const xdaq::ApplicationDescriptor*  c = nodes_[i];
        *out << cgicc::tr();
        *out << "<td>" << i << "</td>";
        *out << "<td>" << shub::statusToString(dconnections_[c]) << "</td>";
        *out << "<td>" <<  c->getContextDescriptor()->getURL() <<  "/" <<  c->getURN() << " instance " << c->getInstance() << "</td>";
        *out << "<td>" <<  c->getInstance() << "</td>";
        *out << cgicc::tr();
    }
    
    *out << cgicc::tbody();
    *out << cgicc::table();
    *out << "</div>";
    
    *out << "<div class=\"xdaq-tab\" title=\"Statistics\">" << std::endl;
    *out << cgicc::table().set("class", "xdaq-table") << std::endl;
    *out << cgicc::caption("Total Input Requests per Node") << std::endl;
    
    *out << cgicc::thead() << std::endl;
    
    *out << cgicc::tr();
    *out << cgicc::th("Index");
    *out << cgicc::th("Input Control Requests");
    *out << cgicc::tr() << std::endl;
    
    *out << cgicc::thead() << std::endl;
    
    
    *out << cgicc::tbody() << std::endl;
    for ( size_t i = 0; i < ictrlcounters_.size(); i++)
    {
        *out << cgicc::tr();
        *out << "<td>" << i << "</td>";
        *out << "<td>" << ictrlcounters_[i] << "</td>";
        *out << cgicc::tr();
    }
    
    *out << cgicc::tbody();
    *out << cgicc::table();
    
    *out << cgicc::table().set("class", "xdaq-table") << std::endl;
    *out << cgicc::caption("Total Output Control Request") << std::endl;
    
    *out << cgicc::thead() << std::endl;
    
    *out << cgicc::tr();
    *out << cgicc::th("Index");
    *out << cgicc::th("Output Control Requests");
    *out << cgicc::tr() << std::endl;
    
    *out << cgicc::thead() << std::endl;
    
    
    *out << cgicc::tbody() << std::endl;
    for ( size_t i = 0; i < octrlcounters_.size(); i++)
    {
        *out << cgicc::tr();
        *out << "<td>" << i << "</td>";
        *out << "<td>" << octrlcounters_[i] << "</td>";
        *out << cgicc::tr();
    }
    
    *out << cgicc::tbody();
    *out << cgicc::table();
    
    //
    *out << cgicc::table().set("class", "xdaq-table-vertical") << std::endl;
    *out << cgicc::caption("Performance (Event Rate and Bandwidth)") << std::endl;
    
    *out << cgicc::thead() << std::endl;
    *out << cgicc::tr();
    *out << cgicc::th();
    *out << "Rate";
    *out << cgicc::th();
    *out << cgicc::th();
    *out << "Bandwidth";
    *out << cgicc::th();
    *out << cgicc::tr();
    *out << cgicc::thead() << std::endl;
    
    *out << cgicc::tbody() << std::endl;
    *out << cgicc::tr();
    *out << cgicc::td();
    *out << currentRate_.load()/1000 << " KHz";
    *out << cgicc::td();
    *out << cgicc::td();
    *out << (currentRate_ * currentLen_* replicas_)/1000000000 << " GB/s";
    *out << cgicc::td();
    *out << cgicc::tr();
    *out << cgicc::tbody() << std::endl;
    
    *out << cgicc::table() << std::endl;
    
    
    *out << "</div>";
    
    
    *out << "</div>";
}

void shub::NodeRC::actionPerformed(toolbox::Event& e)
{
    // Either configuration loaded or initialization completed (without a configuration) resources are initialized
    if( e.type() == "urn:xdaq-event:configuration-loaded" || e.type() == "urn:xdaq-event:initialization-completed")
    {
    
        // start shuffle thread
        std::thread ths(&NodeRC::shuffle, this, "shuffle");
        ths.detach();
#ifdef CUT
#endif
        
        // start builder thread
        std::thread thb(&NodeRC::builder, this, "builder");
        thb.detach();
        
        // start input generator thread
        //std::thread thi(&NodeRC::autostart, this, "autostart loop");
        //thi.detach();
        
        // start measurement generator thread
        std::thread thm(&NodeRC::measure, this, "benchmark loop");
        thm.detach();
#ifdef CUT
#endif
        // start connector thread
        std::thread thc(&NodeRC::connect, this, "connector loop");
        thc.detach();
    }
}





