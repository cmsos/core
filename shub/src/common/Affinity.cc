// $Id$

/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.			                             *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					                 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************
 */
#include "shub/Affinity.h"


#include <sys/syscall.h>    /* For SYS_xxx definitions */

#include <signal.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <iostream>
#include <unistd.h>

#include "toolbox/string.h"
#include "toolbox/PolicyFactory.h"
#include "toolbox/Policy.h"
#include "toolbox/exception/Exception.h"
#include "xcept/tools.h"


void shub::Affinity::applyThreadPolicy(const std::string & name)
{
    // Apply policies
    toolbox::PolicyFactory * factory = toolbox::getPolicyFactory();
    toolbox::Policy * policy = 0;
    
    try
    {
        toolbox::net::URN urn("shub-thread", name);
        policy = factory->getPolicy(urn, "thread");
        policy->setDefaultPolicy();
#ifdef linux
        // add SYS_gettid
        pid_t const thread_id = pid_t(::syscall(SYS_gettid));
        toolbox::net::URN tidurn("shub-arbiter-thread-SYS_gettid",name);
        policy->setProperty(tidurn.toString(),toolbox::toString("%d",thread_id));
#else
        // not implemented yet
#endif

    }
    catch (toolbox::exception::Exception & e)
    {
        std::stringstream oss;
        oss << "Failed to set thread policy for thread '" << name << "'" ;
        XCEPT_RETHROW(toolbox::exception::Exception, oss.str(),e);
    }
}
