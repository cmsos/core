/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.			                             *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simlevicius					                 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************
 */

#include <thread>
#include <algorithm>

#include "shub/InputRC.h"

#include "xcept/Exception.h"
#include "xcept/tools.h"

#include "toolbox/BSem.h"
#include "toolbox/rlist.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/CommittedHeapAllocator.h"
#include "toolbox/task/TimerTask.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"

#include "xdaq/Application.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationGroup.h"
#include "xoap/MessageFactory.h"
#include "xdaq/NamespaceURI.h"
#include "xdaq/pipe/Input.h"
#include "xdaq/pipe/Output.h"
#include "xdaq/pipe/Interface.h"

#include "xgi/Method.h"
#include "xgi/framework/Method.h"

XDAQ_INSTANTIATOR_IMPL(shub::InputRC)

//#define MESSAGE_SIZE 8196
#define MESSAGE_SIZE 131072

shub::InputRC::InputRC (xdaq::ApplicationStub* c): xdaq::Application(c), xgi::framework::UIManager(this)
{
    c->getDescriptor()->setAttribute("icon", "/shub/images/input.png");
    c->getDescriptor()->setAttribute("icon16x16", "/shub/images/input.png");
    
    poolName_ =  "tpi";
    this->getApplicationInfoSpace()->fireItemAvailable("poolName", &poolName_);
    this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
    this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:ParameterSet");
    this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:ParameterGet");
    this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:ParameterQuery");
    
    this->getApplicationContext()->addActionListener(this); //listen to executive events

    this->maxBlockSize_ = 4096;
    this->masterArbiterInstance_ = 0; // default arbiter instance 0

    getApplicationInfoSpace()->fireItemAvailable("maxBlockSize", &maxBlockSize_);
    getApplicationInfoSpace()->fireItemAvailable("masterArbiterInstance", &masterArbiterInstance_);
    counter_ = 0;
    currentFragmentId_ = 0;
    enabled_ = false;
    
    superFragmentSize_ = MESSAGE_SIZE;
    eventRate_ = 0;
    getApplicationInfoSpace()->fireItemAvailable("superFragmentSize", &superFragmentSize_);
    getApplicationInfoSpace()->fireItemAvailable("eventRate", &eventRate_);
    
    currentLen_ = superFragmentSize_;
    currentRate_ = eventRate_;
    
    // Bind CGI callbacks
    xgi::framework::deferredbind(this, this, &shub::InputRC::Default, "Default");
    
    connectedness_ = false;
    getApplicationInfoSpace()->fireItemAvailable("connectedness", &connectedness_);
}

shub::InputRC::~InputRC()
{
}

//
// Initialization
//
void shub::InputRC::actionPerformed (xdata::Event& event)
{
    if (event.type() == "urn:xdaq-event:setDefaultValues")
    {
        try
        {
            counter_ = 0;
            
            auto arbiters = getApplicationContext()->getDefaultZone()->getApplicationDescriptors("shub::Arbiter");
            arbiters_.resize(arbiters.size());
            for (size_t a = 0; a < arbiters.size(); a++ )
            {
                arbiters_[a] = 0;
            }
            
            auto nodes = getApplicationContext()->getDefaultZone()->getApplicationDescriptors("shub::NodeRC");
            nodes_.resize(nodes.size());
            for (size_t n = 0; n <nodes_.size(); n++ )
            {
                nodes_[n] = 0;
            }
            
            // add Nodes
            for ( auto d: nodes )
            {
                size_t inum = d->getInstance();
                if ( nodes_[inum] != 0 )
                {
                    XCEPT_RAISE(xdaq::exception::Exception, "instance already exisiting for node");
                }
                
                nodes_[inum] = const_cast<xdaq::ApplicationDescriptor*>(d);
                LOG4CPLUS_INFO(getApplicationLogger(), "Found NodeRC " <<  d->getContextDescriptor()->getURL() <<  "/" <<  d->getURN() << " instance " << d->getInstance() );
                
                dconnections_[nodes_[inum]] = ConnectionStatus::idle;
            }
            
            // add Arbiter
            for ( auto a: arbiters )
            {
                size_t inum = a->getInstance();
                if ( arbiters_[inum] != 0 )
                {
                    XCEPT_RAISE(xdaq::exception::Exception, "instance already exisiting for arbiter");
                }
                arbiters_[inum] = const_cast<xdaq::ApplicationDescriptor*>(a);
                LOG4CPLUS_INFO(getApplicationLogger(), "Found Arbiter " <<  a->getContextDescriptor()->getURL() <<  "/" <<  a->getURN() << " instance " << a->getInstance() );
                tconnections_[arbiters_[inum]] = ConnectionStatus::idle;
            }
            
            
            replicas_ =  nodes.size();
            
            
            LOG4CPLUS_INFO(getApplicationLogger(), "Found " << replicas_ << " replica Nodes RC");
            LOG4CPLUS_INFO(getApplicationLogger(), "Found " << arbiters_.size() << " Arbiters ");
        }
        catch (xdaq::exception::Exception& e)
        {
            LOG4CPLUS_FATAL(getApplicationLogger(), xcept::stdformat_exception_history(e));
            return;
        }
        
        try
        {
            std::string pname = poolName_.toString();
            toolbox::net::URN urn("toolbox-mem-pool", pname);
            pool_ = toolbox::mem::getMemoryPoolFactory()->findPool(urn);
            
        }
        catch (toolbox::mem::exception::Exception & e)
        {
            LOG4CPLUS_FATAL(getApplicationLogger(), xcept::stdformat_exception_history(e));
            return;
        }
        
        
        pipeService_ = xdaq::getPipeInterface()->join(this,this);
        
        currentLen_.store(superFragmentSize_);
        
        /*
         size_t fsize = sizeof(uint64_t) + sizeof(uint64_t) + (sizeof(uint64_t) * this->maxGather_);
         if ( fsize > this->maxBlockSize_ )
         {
         LOG4CPLUS_FATAL(getApplicationLogger(), "request to arbiter could overflows with size" << fsize << " max allowed is " << this->maxBlockSize_);
         exit(-1);
         }
         */
        
    }
    else if (event.type() == "urn:xdaq-event:ParameterSet")
    {
        LOG4CPLUS_INFO(getApplicationLogger(), "Set currentLen to  " << superFragmentSize_ );
        
        currentLen_.store(superFragmentSize_);
    }
    else if ((event.type() == "urn:xdaq-event:ParameterGet") || (event.type() == "urn:xdaq-event:ParameterQuery"))
    {
        eventRate_ = currentRate_.load();
        
        LOG4CPLUS_INFO(getApplicationLogger(), "Get currentRate  " << eventRate_ );
        
        {
            std::lock_guard<std::mutex> guard(dmutex_);
        
            if ( opipes_.size() == 1 && tpipes_.size() == 1)
                connectedness_ = true;
            else
                connectedness_ = false;
        }
        
    }
    
}

//
// Connection establishment handling
//
void shub::InputRC::actionPerformed(xdaq::pipe::EstablishedConnection & ec )
{
    const xdaq::ApplicationDescriptor * d = ec.getDestinationDescriptor();
    
    LOG4CPLUS_DEBUG(getApplicationLogger(), "Connection is established from " << d->getURN() <<  " to " <<  d->getURN());
    
    {
        
        if ( std::find(nodes_.begin(), nodes_.end(), d) != nodes_.end() )
        {
            xdaq::pipe::Output* opipe = pipeService_->createOutputPipe(ec);
            
            std::lock_guard<std::mutex> guard(dmutex_);
            opipes_[d] = opipe;
            dconnections_[d] = ConnectionStatus::established;
            LOG4CPLUS_DEBUG(getApplicationLogger(), "outputpipe created for data, status is Established" );
            return;
        }
    }
    
    {
        
        if ( std::find(arbiters_.begin(), arbiters_.end(), d) != arbiters_.end() )
        {
            xdaq::pipe::Output* opipe = pipeService_->createOutputPipe(ec);
            std::lock_guard<std::mutex> guard(dmutex_);
            tpipes_[d] = opipe;
            tconnections_[d] = ConnectionStatus::established;
            LOG4CPLUS_INFO(getApplicationLogger(), "outputpipe created for i/o, status is Established to instance " << d->getInstance() );
            return;
        }
    }
    
}

bool shub::InputRC::actionPerformed(xdaq::pipe::ConnectionRequest & cr)
{               
    std::string network = cr.getNetwork();
    LOG4CPLUS_WARN(getApplicationLogger(),"InputRC - input connection not supported " << network);
    
    return false;
}

//
// Connection error handing
//
void shub::InputRC::actionPerformed(xdaq::pipe::ConnectionError & ce )
{
    //std::cout << "Connection error from " << ce.getSourceDescriptor()->getURN() <<  " to " <<  ce.getDestinationDescriptor()->getURN()<< std::endl;
    const xdaq::ApplicationDescriptor* d = ce.getDestinationDescriptor();
    std::string durl = d->getContextDescriptor()->getURL() + "/" + d->getURN();
    LOG4CPLUS_WARN(getApplicationLogger(), "Connection failed for " << durl <<  ", with error " << ce.getException().what());
    
    {
        std::lock_guard<std::mutex> guard(dmutex_);
        for ( auto& [descriptor, status]: dconnections_ )
        {
            if ( descriptor == d )
            {
                dconnections_[d] = ConnectionStatus::idle;
                break;
            }
        }
    }
    {
        std::lock_guard<std::mutex> guard(dmutex_);
        for ( auto& [descriptor, status]: tconnections_ )
        {
            if ( descriptor == d )
            {
                tconnections_[d] = ConnectionStatus::idle;
                break;
            }
        }
    }
    
    
    
    // disable input generator
    enabled_ = false;
}

void shub::InputRC::actionPerformed(xdaq::pipe::BrokenPipe & bp)
{
    LOG4CPLUS_WARN(getApplicationLogger(), "Broken Pipe, shoud destroy output pipe here");
    
    {
        std::lock_guard<std::mutex> guard(dmutex_);
        for ( auto it = opipes_.begin(); it != opipes_.end(); it++ )
        {
            if ( (*it).second->match(bp.getPipeHandle()))
            {
                LOG4CPLUS_DEBUG(getApplicationLogger(),"Found matching output data pipe, going to destroy") ;
                
                xdaq::pipe::Output * opipe = (*it).second;
                dconnections_[(*it).first] = ConnectionStatus::idle;
                pipeService_->destroyOutputPipe(opipe);
                opipes_.erase(it);
                // disable input generator
                enabled_ = false;
                return;
            }
        }
    }

    {
        std::lock_guard<std::mutex> guard(dmutex_);
        for ( auto it = tpipes_.begin(); it != tpipes_.end(); it++ )
        {
            if ( (*it).second->match(bp.getPipeHandle()))
            {
                LOG4CPLUS_DEBUG(getApplicationLogger(),"Found matching output data pipe, going to destroy") ;
                
                xdaq::pipe::Output * tpipe = (*it).second;
                tconnections_[(*it).first] = ConnectionStatus::idle;
                pipeService_->destroyOutputPipe(tpipe);
                tpipes_.erase(it);
                // disable input generator
                enabled_ = false;
                return;
            }
        }
    }
}

void shub::InputRC::actionPerformed(xdaq::pipe::ConnectionClosedByPeer & ccbp )
{
    LOG4CPLUS_WARN(getApplicationLogger(), "Connection closed by peer not supported");
}

//
// Multithread work loops (connect, input, shuffling, building, measure )
//
void shub::InputRC::connect(std::string name)
{
    LOG4CPLUS_DEBUG (this->getApplicationLogger(), "starting connection thread " << name);
    
    // validate readiness example
    auto readiness = dynamic_cast<xdata::Boolean*>(this->getApplicationInfoSpace()->find("readiness"));
    *readiness = true;
    this->getApplicationInfoSpace()->fireItemValueChanged("readiness", this);

    
    while(1)
    {
        ::sleep(10);
        LOG4CPLUS_DEBUG (this->getApplicationLogger(), "connection status: " << opipes_.size() << " out of 1");
        
        std::string surl = this->getApplicationContext()->getContextDescriptor()->getURL() + "/" + this->getApplicationDescriptor()->getURN();
        
        {
            std::lock_guard<std::mutex> guard(dmutex_);
            for ( auto& [descriptor, status]: dconnections_ )
            {
                if ( this->getApplicationDescriptor()->getInstance() == descriptor->getInstance() ) // found one matching node
                {
                    if ( status == ConnectionStatus::idle )
                    {
                        std::string durl = descriptor->getContextDescriptor()->getURL() + "/" + descriptor->getURN();
                        LOG4CPLUS_DEBUG(getApplicationLogger(), "requesting data connection from " << surl << " to " << durl << " status Idle to Connecting");
                        status  = ConnectionStatus::connecting;
                        pipeService_->connect(this->getApplicationDescriptor(), descriptor);
                        LOG4CPLUS_DEBUG(getApplicationLogger(), "done requesting data connection");
                    }
                }
            }
        }
        
        {
            std::lock_guard<std::mutex> guard(dmutex_);
            for ( auto& [descriptor, status]: tconnections_ )
            {
                if ( this->getApplicationDescriptor()->getInstance() == descriptor->getInstance() ) // found one matching node
                {
                    if ( status == ConnectionStatus::idle )
                    {
                        std::string durl = descriptor->getContextDescriptor()->getURL() + "/" + descriptor->getURN();
                        LOG4CPLUS_INFO(getApplicationLogger(), "requesting i/o connection to arbiter, from " << surl << " to " << durl << " status Idle to Connecting");
                        status  = ConnectionStatus::connecting;
                        pipeService_->connect(this->getApplicationDescriptor(), descriptor);
                        LOG4CPLUS_DEBUG(getApplicationLogger(), "done requesting io connection to arbiter");
                    }
                }
            }
        }
        
    }
}

void shub::InputRC::input(std::string name)
{
    this->applyThreadPolicy(name);
    
    auto setframe = [&] (toolbox::mem::Reference * ref)
    {
        shub::FragmentDescriptor * fragment = (FragmentDescriptor*)ref->getDataLocation();
        fragment->id = this->currentFragmentId_;
        fragment->node = this->getApplicationDescriptor()->getInstance();
        fragment->resource = 0;
        ref->setDataSize(this->currentLen_);
        this->currentFragmentId_++;
        
    };
    
    LOG4CPLUS_DEBUG(getApplicationLogger(), "starting input generation loop..." << name);
    for(;;)
    {
        // reset for a rough synchonization
        currentFragmentId_ = 0;
        
        bool established = false;
        while ( ! established )
        {
            LOG4CPLUS_INFO(getApplicationLogger(), "waiting rc nodes connection...");
            ::sleep(5);
            
            std::lock_guard<std::mutex> guards(dmutex_);
            
            LOG4CPLUS_INFO(getApplicationLogger(), "connection status: (opipes " <<  opipes_.size() << " of " << 1 << ")");
            
            if ( opipes_.size() == 1 )
            {
                established = true;
            }
            
        }
        
        LOG4CPLUS_INFO(getApplicationLogger(), "all rc nodes connected");
        
        sleep(15);

        
        enabled_ = true;
        while (enabled_)
        {
            for ( auto it = opipes_.begin(); it != opipes_.end(); it++ )
            {
                while ( ! (*it).second->empty() )
                {
                    LOG4CPLUS_TRACE(getApplicationLogger(), "input - going to input recycled fragment");
                    toolbox::mem::Reference * ref = (*it).second->get();
                    
                    //std::cout << " InputRC post frame of size" << ref->getDataSize() << " offset " << ref->getDataOffset() << std::endl;
                    
                    LOG4CPLUS_TRACE(getApplicationLogger(), "input - going to input recycled fragment with fragmentid " << currentFragmentId_  );
                    
                    setframe(ref);
                    (*it).second->post(ref);
                    
                    counter_++;
                    //::sleep(10);
                    
                }
            }
            
            //LOG4CPLUS_TRACE(getApplicationLogger(), "input - idatafree is empty sleep a bit");
            //::sleep(1);
            
        } // input enabled loop
        LOG4CPLUS_WARN(getApplicationLogger(), "input - input generator was disabled");
        
    } // forever loop
    
    return;
}



void shub::InputRC::measure (std::string name)
{
    while(1)
    {
        ::sleep(10);
        
        toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();
        double delta = (double)now - (double)lastTime_;
        double rate = counter_ / delta;
        currentRate_ = rate;
        counter_ = 0;
        lastTime_ =  now;
        
        //std::cout << "inputrc fragment rate is " << rate/1000 <<  " kHz bandwidth " << (rate * currentLen_)/1000000000 << " GB/s (currentLen_=" << currentLen_ << ")"  << std::endl;
        //std::cout << "inputrc fragment rate is " << rate/1000 <<  " kHz bandwidth " << (rate * currentLen_)/1073741824 << " GB/s" << std::endl;
        
    }
}

void shub::InputRC::Default (xgi::Input * in, xgi::Output * out)
{
	 std::lock_guard<std::mutex> guard(dmutex_);
    *out << "<div class=\"xdaq-tab-wrapper\">" << std::endl;
    *out << "<div class=\"xdaq-tab\" title=\"Connections\">" << std::endl;
    *out << cgicc::table().set("class", "xdaq-table") << std::endl;
    *out << cgicc::caption("Output Data Connections to NodeRCs") << std::endl;
    
    *out << cgicc::thead() << std::endl;
    
    *out << cgicc::tr();
    *out << cgicc::th("Instance");
    *out << cgicc::th("Status");
    *out << cgicc::th("URL");
    *out << cgicc::th("Instance cross check");
    *out << cgicc::tr() << std::endl;
    
    *out << cgicc::thead() << std::endl;
   
    *out << cgicc::tbody() << std::endl;
    for ( size_t i = 0; i < nodes_.size(); i++)
    {
	const xdaq::ApplicationDescriptor*  c = nodes_[i];
        *out << cgicc::tr();
        *out << "<td>" << i << "</td>";
        *out << "<td>" <<  shub::statusToString(dconnections_[c]) << "</td>";
        *out << "<td>" <<  c->getContextDescriptor()->getURL() <<  "/" <<  c->getURN() << " instance " << c->getInstance() << "</td>";
        *out << "<td>" <<  c->getInstance() << "</td>";
        *out << cgicc::tr();
    }
    
    *out << cgicc::tbody();
    *out << cgicc::table();
    
    *out << cgicc::table().set("class", "xdaq-table") << std::endl;
    *out << cgicc::caption("Output Data Connections to Arbiters") << std::endl;
    
    *out << cgicc::thead() << std::endl;
    
    *out << cgicc::tr();
    *out << cgicc::th("Instance");
    *out << cgicc::th("Status");
    *out << cgicc::th("URL");
    *out << cgicc::th("Instance cross check");
    *out << cgicc::tr() << std::endl;
    
    *out << cgicc::thead() << std::endl;
   
    *out << cgicc::tbody() << std::endl;
    for ( size_t i = 0; i < arbiters_.size(); i++)
    {
    const xdaq::ApplicationDescriptor*  a = arbiters_[i];
        *out << cgicc::tr();
        *out << "<td>" << i << "</td>";
        *out << "<td>" << shub::statusToString(tconnections_[a]) << "</td>";
        *out << "<td>" <<  a->getContextDescriptor()->getURL() <<  "/" <<  a->getURN() << " instance " << a->getInstance() << "</td>";
        *out << "<td>" <<  a->getInstance() << "</td>";
        *out << cgicc::tr();
    }
    
    *out << cgicc::tbody();
    *out << cgicc::table();
    *out << "</div>";
    
    *out << "<div class=\"xdaq-tab\" title=\"Statistics\">" << std::endl;
    
    //
    *out << cgicc::table().set("class", "xdaq-table-vertical") << std::endl;
    *out << cgicc::caption("Performance (Fragment Rate and Bandwidth)") << std::endl;
    
    *out << cgicc::thead() << std::endl;
    *out << cgicc::tr();
    *out << cgicc::th();
    *out << "Rate";
    *out << cgicc::th();
    *out << cgicc::th();
    *out << "Bandwidth";
    *out << cgicc::th();
    *out << cgicc::tr();
    *out << cgicc::thead() << std::endl;
    
    *out << cgicc::tbody() << std::endl;
    *out << cgicc::tr();
    *out << cgicc::td();
    *out << currentRate_.load()/1000 << " KHz";
    *out << cgicc::td();
    *out << cgicc::td();
    *out << (currentRate_ * currentLen_* replicas_)/1000000000 << " GB/s";
    *out << cgicc::td();
    *out << cgicc::tr() << std::endl;
    *out << cgicc::tbody() << std::endl;
    
    *out << cgicc::table() << std::endl;
    
        
    *out << "</div>";
    
    *out << "</div>";
}

void shub::InputRC::actionPerformed(toolbox::Event& e)
{
    // Either configuration loaded or initialization completed (without a configuration) resources are initialized
    if( e.type() == "urn:xdaq-event:configuration-loaded" || e.type() == "urn:xdaq-event:initialization-completed")
    {
         // start input generator thread
        std::thread thi(&InputRC::input, this, "input");
        thi.detach();
        
        // start measurement generator thread
        std::thread thm(&InputRC::measure, this, "benchmark loop");
        thm.detach();
#ifdef CUT
#endif
        
        // start connector thread
        std::thread thc(&InputRC::connect, this, "connector loop");
        thc.detach();
    }
}
