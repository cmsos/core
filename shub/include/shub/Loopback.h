// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _shub_Loopback_h_
#define _shub_Loopback_h_

#include "pt/pipe/Input.h"
#include "pt/pipe/Output.h"
#include "xdaq/pipe/Input.h"
#include "xdaq/pipe/Output.h"

namespace shub
{

class Input: public pt::pipe::Input
{
public:
    
    Input(toolbox::rlist<toolbox::mem::Reference*> * io, toolbox::rlist<toolbox::mem::Reference*> * cq );
    virtual ~Input();
    void postFrame(toolbox::mem::Reference * ref );
    bool empty();
    toolbox::mem::Reference * completed();
    
protected:
    toolbox::rlist<toolbox::mem::Reference*> * io_;
    toolbox::rlist<toolbox::mem::Reference*> * cq_;
};

class Output: public pt::pipe::Output
{
public:
    
    Output(toolbox::rlist<toolbox::mem::Reference*> * io, toolbox::rlist<toolbox::mem::Reference*> * cq);
    virtual ~Output();
    void postFrame(toolbox::mem::Reference * ref );
    bool empty();
    toolbox::mem::Reference * completed();
    
protected:
    toolbox::rlist<toolbox::mem::Reference*> * io_;
    toolbox::rlist<toolbox::mem::Reference*> * cq_;
};

class InputLoopback: public xdaq::pipe::Input
{	
public:
    
    InputLoopback(pt::pipe::Input * handle, pt::pipe::Service * pts);
    virtual ~InputLoopback();
    
};

class OutputLoopback: public xdaq::pipe::Output
{
public:
    
    OutputLoopback(pt::pipe::Output * handle, pt::pipe::Service * pts);
    virtual ~OutputLoopback();
    
};

}

#endif
