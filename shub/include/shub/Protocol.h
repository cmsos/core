// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _shub_Protocol_h_
#define _shub_Protocol_h_

#include <string>

namespace shub 
{

typedef struct
{
    uint64_t id;
    uint64_t node;
    uint64_t resource;
} FragmentDescriptor;

typedef struct
{
    uint64_t id;
    uint64_t resource;
} ShuffleDescriptor;

typedef struct
{
    uint64_t num;
    uint64_t node;
    ShuffleDescriptor shuffle[1];
} ShuffleRequest;

typedef struct
{
    uint64_t node;
    uint64_t num;
    uint64_t resource[1];
} ResourceAvailable;

enum class ConnectionStatus { idle, connecting, established };

std::string statusToString(ConnectionStatus status);

}

#endif
