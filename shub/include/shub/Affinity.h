// $Id$

/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.			                             *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					                 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************
 */

#ifndef _shub_Affinity_h_
#define _shub_Affinity_h_

#include <string>

namespace shub
{

class Affinity
{
    public:
    
    void applyThreadPolicy(const std::string & name);

};

}

#endif
