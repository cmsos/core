// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _shub_Arbiter_h_
#define _shub_Arbiter_h_

#include <set>
#include <mutex>
#include <cmath>

#include "toolbox/mem/Reference.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/rlist.h"
#include "toolbox/TimeVal.h"
#include "toolbox/ActionListener.h"

#include "xdata/UnsignedLong.h"
#include "xdaq/ApplicationDescriptor.h"
#include "xdata/Boolean.h"
#include "xdata/String.h"
#include "xdata/Event.h"
#include "xdata/ActionListener.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"

#include "xgi/Utils.h"
#include "xgi/Input.h"
#include "xgi/Output.h"
#include "xgi/framework/UIManager.h"

#include "xdaq/Application.h"
#include "xdaq/pipe/ServiceListener.h"
#include "xdaq/pipe/Service.h"

#include "shub/Loopback.h"
#include "shub/Protocol.h"
#include "shub/Affinity.h"

namespace shub 
{

class Arbiter: public xdaq::Application, public xgi::framework::UIManager, public xdata::ActionListener, public xdaq::pipe::ServiceListener, public shub::Affinity, public toolbox::ActionListener
{	
public:
    XDAQ_INSTANTIATOR();
    
    Arbiter(xdaq::ApplicationStub* c);
    ~Arbiter();
    
    void Default (xgi::Input * in, xgi::Output * out);
    
protected:
    
    void actionPerformed(xdaq::pipe::EstablishedConnection & ec);
    bool actionPerformed(xdaq::pipe::ConnectionRequest & ec);
    void actionPerformed(xdaq::pipe::ConnectionError & ce);
    void actionPerformed(xdaq::pipe::ConnectionClosedByPeer & ccbp);
    void actionPerformed(xdaq::pipe::BrokenPipe & bp);
    
    
    void control(std::string name);
    void connect(std::string name);
    void measure(std::string name);
    
    void actionPerformed (xdata::Event& e);
    void actionPerformed(toolbox::Event& e);
    
    
    std::vector<xdaq::ApplicationDescriptor *> nodes_;
    
    toolbox::mem::Pool* pool_;			// Memory pool for allocating messages for sending
    
    xdaq::pipe::Service * pipeService_;
    std::map<const xdaq::ApplicationDescriptor*, xdaq::pipe::Output*> octrlpipes_;
    std::vector<xdaq::pipe::Input*> ictrlpipes_;
    
    std::map<const xdaq::ApplicationDescriptor*, shub::ConnectionStatus> connections_;
    
    std::mutex mutex_;
    
    xdata::String poolName_;
    toolbox::TimeVal lastTime_;
    xdata::UnsignedInteger maxResources_;
    xdata::UnsignedInteger maxGather_;
    xdata::UnsignedInteger maxBlockSize_;
    xdata::UnsignedInteger masterArbiterInstance_;
    xdata::UnsignedInteger eventRate_;
    xdata::String acceptedCtrlNetwork_;
    xdata::String acceptedInputNetwork_;
    xdata::Boolean connectedness_;


    size_t counter_;
    
    std::vector<size_t> icounters_; // one per ipipe
    std::vector<size_t> ocounters_; // one per opipe
    std::vector<size_t> mcounters_; // missing one per ipipe
    std::vector<size_t> lcounters_; // last num requests per ipipe
    std::vector<size_t> pcounters_; // last num requests per ipipe
    
    size_t replicas_;
    
    toolbox::rlist<toolbox::mem::Reference*>* freelist_;
    
    // trigger data 
    std::vector<xdaq::pipe::Input*> inputpipes_;
    
    std::atomic<unsigned int> currentRate_;

    
};
}

#endif
