// $Id$

/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.			                             *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					                 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************/

//
// Version definition for shub 
//
#ifndef _shub_version_h_
#define _shub_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define CORE_SHUB_VERSION_MAJOR 1
#define CORE_SHUB_VERSION_MINOR 5
#define CORE_SHUB_VERSION_PATCH 1
// If any previous versions available E.g. #define SHUBPREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef CORE_SHUB_PREVIOUS_VERSIONS


//
// Template macros
//
#define CORE_SHUB_VERSION_CODE PACKAGE_VERSION_CODE(CORE_SHUB_VERSION_MAJOR,GEVB2_GVERSION_MINOR,CORE_SHUB_VERSION_PATCH)
#ifndef CORE_SHUB_PREVIOUS_VERSIONS
#define CORE_SHUB_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(CORE_SHUB_VERSION_MAJOR,CORE_SHUB_VERSION_MINOR,CORE_SHUB_VERSION_PATCH)
#else 
#define CORE_SHUB_FULL_VERSION_LIST  CORE_SHUB_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(CORE_SHUB_VERSION_MAJOR,CORE_SHUB_VERSION_MINOR,CORE_SHUB_VERSION_PATCH)
#endif 

namespace shub
{
const std::string project = "core";
const std::string package  =  "shub";
const std::string versions = CORE_SHUB_FULL_VERSION_LIST;
const std::string summary = "Shuffle Event Builder";
const std::string description = "";
const std::string authors = "Luciano Orsini, Dainius Simelevicius";
const std::string link = "http://xdaq.cern.ch/";
config::PackageInfo getPackageInfo();
void checkPackageDependencies() ;
std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
