// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _shub_InputRC_h_
#define _shub_InputRC_h_

#include <set>
#include <mutex>
#include <queue>
#include <cmath>
#include <atomic>


#include "toolbox/mem/Reference.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/rlist.h"
#include "toolbox/TimeVal.h"
#include "toolbox/ActionListener.h"

#include "xdata/UnsignedLong.h"
#include "xdaq/ApplicationDescriptor.h"
#include "xdata/Boolean.h"
#include "xdata/String.h"
#include "xdata/Event.h"
#include "xdata/ActionListener.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"

#include "xgi/Utils.h"
#include "xgi/Input.h"
#include "xgi/Output.h"
#include "xgi/framework/UIManager.h"

#include "xdaq/Application.h"
#include "xdaq/pipe/ServiceListener.h"
#include "xdaq/pipe/Service.h"

#include "shub/Protocol.h"
#include "shub/Affinity.h"


namespace shub 
{


class InputRC: public xdaq::Application, public xgi::framework::UIManager, public xdata::ActionListener, public xdaq::pipe::ServiceListener, public shub::Affinity, public toolbox::ActionListener
{	
public:
    XDAQ_INSTANTIATOR();
    
    InputRC(xdaq::ApplicationStub* c);
    ~InputRC();
    
    void Default (xgi::Input * in, xgi::Output * out);

protected:
    
    // pipe events handlers
    void actionPerformed(xdaq::pipe::EstablishedConnection & ec);
    bool actionPerformed(xdaq::pipe::ConnectionRequest & ec);
    void actionPerformed(xdaq::pipe::ConnectionError & ce);
    void actionPerformed(xdaq::pipe::ConnectionClosedByPeer & ccbp);
    void actionPerformed(xdaq::pipe::BrokenPipe & bp);
    
    //void recycle(std::list<std::pair<toolbox::mem::Reference *, xdaq::pipe::Input*>> & event);
    
    // threads
    void measure(std::string name);
    void connect(std::string name);
    void input(std::string name);
    
    void actionPerformed (xdata::Event& e);
    void actionPerformed(toolbox::Event& e);
    
    // exported parameters
    xdata::UnsignedInteger maxBlockSize_;
    xdata::Boolean connectedness_;

    
    xdata::UnsignedInteger superFragmentSize_;
    xdata::UnsignedInteger eventRate_;
    xdata::UnsignedInteger masterArbiterInstance_;


    std::atomic<unsigned int> currentLen_;
    std::atomic<unsigned int> currentRate_;
    
    std::vector<xdaq::ApplicationDescriptor *> nodes_;		//  ordered by instance number
    std::vector<xdaq::ApplicationDescriptor *> arbiters_;

    std::map<const xdaq::ApplicationDescriptor*, ConnectionStatus> dconnections_; // for data fragments to noderc
    std::map<const xdaq::ApplicationDescriptor*, ConnectionStatus> tconnections_; // for triggers to arbiter
    
    
    xdata::String poolName_;
    toolbox::mem::Pool* pool_;			// Memory pool for allocating messages for sending
    
    xdaq::pipe::Service * pipeService_;
    
    
    // nodes interconnection I/O
    std::map<const xdaq::ApplicationDescriptor*, xdaq::pipe::Output*> opipes_;
    
    // arbiter interconnection I/O
    std::map<const xdaq::ApplicationDescriptor*, xdaq::pipe::Output*> tpipes_;
    
    std::mutex dmutex_;
    
    toolbox::TimeVal lastTime_;
    size_t counter_;
    
    size_t replicas_;
    
    size_t currentFragmentId_; // self generated id
        
    
    // used to inhibit input generator
    bool enabled_;

};
}

#endif
