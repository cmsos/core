// $Id$

/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.			                             *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					                 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                             *
 * For the list of contributors see CREDITS.   			                 *
 *************************************************************************/

#ifndef _shub_NodeRC_h_
#define _shub_NodeRC_h_

#include <set>
#include <mutex>
#include <queue>
#include <cmath>
#include <atomic>


#include "toolbox/mem/Reference.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/rlist.h"
#include "toolbox/TimeVal.h"
#include "toolbox/ActionListener.h"

#include "xdata/UnsignedLong.h"
#include "xdaq/ApplicationDescriptor.h"
#include "xdata/Boolean.h"
#include "xdata/String.h"
#include "xdata/Event.h"
#include "xdata/ActionListener.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"

#include "xgi/Utils.h"
#include "xgi/Input.h"
#include "xgi/Output.h"
#include "xgi/framework/UIManager.h"

#include "xdaq/Application.h"
#include "xdaq/pipe/ServiceListener.h"
#include "xdaq/pipe/Service.h"

#include "shub/Loopback.h"
#include "shub/Protocol.h"
#include "shub/Affinity.h"


namespace shub 
{

class ResourcePool
{
public:
    
    struct Resource
    {
        std::vector<toolbox::mem::Reference *> fragments;
        size_t total;
        size_t fragmentid;
        bool allocated;
        bool completed;
    };
    
    ResourcePool(size_t replicas, size_t resources): replicas_(replicas)
    {
        freeresources_ = toolbox::rlist<size_t>::create("shub-noderc-resurcepool-freeresources", resources);
        resources_.resize(resources);
        for (size_t r = 0; r < resources; r++)
        {
            resources_[r].fragments.resize(replicas);
            resources_[r].total = 0;
            resources_[r].completed = false;
            resources_[r].allocated = false;
            resources_[r].fragmentid = 0;
            for (size_t f = 0; f < replicas; f++)
            {
                resources_[r].fragments[f] = 0;
            }
            freeresources_->push_back(r);
        }
    }
    
    bool append(size_t resource, size_t fragmentid, size_t node, toolbox::mem::Reference * ref)
    {
        
        if ( resources_[resource].allocated && (! resources_[resource].completed) )
        {
            resources_[resource].fragmentid = fragmentid;
            if ( resources_[resource].fragments[node] == 0 )
            {
                resources_[resource].fragments[node] = ref;
                resources_[resource].total++;
            }
            else
            {
                std::cerr << "failed to append to resource " << resource << " is not empty for node " << node << " fragmentid " << fragmentid << std::endl;
                std::cerr << " resources_[resource].fragments[node] is " << (size_t)resources_[resource].fragments[node]  << std::endl;
            }
        }
        else
        {
            std::cerr << "failed to append to not allocated or full resource" << std::endl;
        }
        
        if (resources_[resource].total == replicas_ )
        {
            resources_[resource].completed = true;
            return true;
        }
        return false;
    }
    
    const Resource & get(size_t resource) const
    {
        return resources_[resource];
    }
    
    void clear()
    {
        std::cerr << " clear not implemented " << std::endl;
        exit(0);
    }
    
    void free(size_t r)
    {
        if ( ! resources_[r].allocated  )
        {
            std::cerr << "invalid resource, not allocated cannot be free" << std::endl;
            return;
        }
        
        for (size_t f = 0; f < replicas_; f++)
        {
            resources_[r].fragments[f] = 0; // if not recycled before by user then leaks
        }
        resources_[r].allocated = false;
        resources_[r].completed = false;
        resources_[r].total = 0;
        resources_[r].fragmentid = 0;
        freeresources_->push_back(r);
        
    }
    
    size_t alloc()
    {
        if ( ! freeresources_->empty() )
        {
            size_t r = freeresources_->front();
            freeresources_->pop_front();
            if ( ! resources_[r].allocated  )
            {
                resources_[r].allocated = true;
                resources_[r].completed = false;
                for (size_t f = 0; f < replicas_; f++)
                {
                    resources_[r].fragments[f] = 0; // if not recycled before by user then leaks
                }
                return r;
            }
            else
            {
                std::cerr << "failed to allocate resource, no actual resources" << std::endl;
            }
        }
        else
        {
            std::cerr << "failed to allocate resource, no resources available " << std::endl;
        }
        return 0;
    }
    
    bool empty()
    {
        return freeresources_->empty();
    }
    
    size_t elements()
    {
        return freeresources_->elements();
    }
    
protected:
    
    size_t replicas_;
    std::vector<Resource> resources_;
    toolbox::rlist<size_t> * freeresources_;
    
};

class NodeRC: public xdaq::Application, public xgi::framework::UIManager, public xdata::ActionListener, public xdaq::pipe::ServiceListener, public shub::Affinity, public toolbox::ActionListener
{	
public:
    XDAQ_INSTANTIATOR();
    
    NodeRC(xdaq::ApplicationStub* c);
    ~NodeRC();
    
    void Default (xgi::Input * in, xgi::Output * out);

protected:
    
    // pipe events handlers
    void actionPerformed(xdaq::pipe::EstablishedConnection & ec);
    bool actionPerformed(xdaq::pipe::ConnectionRequest & ec);
    void actionPerformed(xdaq::pipe::ConnectionError & ce);
    void actionPerformed(xdaq::pipe::ConnectionClosedByPeer & ccbp);
    void actionPerformed(xdaq::pipe::BrokenPipe & bp);
        
    // threads
    void measure(std::string name);
    void shuffle(std::string name);
    void builder(std::string name);
    void connect(std::string name);
    void autostart(std::string name);
    
    void actionPerformed (xdata::Event& e);
    void actionPerformed(toolbox::Event& e);

    
    // exported parameters
    xdata::UnsignedInteger maxResources_;
    xdata::UnsignedInteger maxBlockSize_;
    xdata::UnsignedInteger maxGather_;
    xdata::UnsignedInteger masterArbiterInstance_;
    xdata::String poolName_;
    xdata::UnsignedInteger superFragmentSize_;
    xdata::UnsignedInteger eventRate_;
    xdata::String acceptedDataNetwork_;
    xdata::String acceptedCtrlNetwork_;
    xdata::String acceptedInputNetwork_;
    xdata::Boolean connectedness_;

    
    std::atomic<unsigned int> currentLen_;
    std::atomic<unsigned int> currentRate_;
    
    std::vector<xdaq::ApplicationDescriptor *> nodes_;		//  ordered by instance number
    std::vector<xdaq::ApplicationDescriptor *> controllers_;	//  ordered by instance number
    
    std::map<const xdaq::ApplicationDescriptor*, ConnectionStatus> dconnections_;
    std::map<const xdaq::ApplicationDescriptor*, ConnectionStatus> cconnections_;
    
    toolbox::mem::Pool* pool_;			// Memory pool for allocating messages for sending
    
    xdaq::pipe::Service * pipeService_;
    
    // nodes interconnection I/O
    std::map<const xdaq::ApplicationDescriptor*, xdaq::pipe::Output*> odatapipes_;
    std::vector<xdaq::pipe::Input*> idatapipes_;
    std::vector<xdaq::pipe::Input*> inputpipes_;
    
    // arbiter interconnection I/O
    std::map<const xdaq::ApplicationDescriptor*, xdaq::pipe::Output*> octrlpipes_;
    std::vector<xdaq::pipe::Input*> ictrlpipes_;
    
    std::mutex smutex_; // shuffling mutex
    std::mutex bmutex_; // building mutex
    
    toolbox::TimeVal lastTime_;
    size_t counter_;
    
    size_t replicas_;
    
    size_t currentFragmentId_; // self generated id
    
    ResourcePool * rpool_;
    
    // used to inhibit shuffle and builder threads
    bool enabled_;
    
    toolbox::rlist<toolbox::mem::Reference*> * freerequestslist_;
    toolbox::rlist<toolbox::mem::Reference*> * pendingrequests_;
    
    typedef struct
    {
        uint64_t id;
        uint64_t node;
        uint64_t resource;
    } ShuffleCommand;
    
    toolbox::rlist<ShuffleCommand> * shufflerequests_;
    
    std::vector<size_t> ictrlcounters_; // one per ictrlpipe
    std::vector<size_t> octrlcounters_; // one per octrlpipe

};
}

#endif
