// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _shub_Node_h_
#define _shub_Node_h_

#include <set>
#include <mutex>
#include <cmath>


#include "toolbox/mem/Reference.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/rlist.h"
#include "toolbox/TimeVal.h"

#include "xdata/UnsignedLong.h"
#include "xdaq/ApplicationDescriptor.h"
#include "xdata/Boolean.h"
#include "xdata/String.h"
#include "xdata/Event.h"
#include "xdata/ActionListener.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"

#include "xgi/Utils.h"
#include "xgi/Input.h"
#include "xgi/Output.h"
#include "xgi/framework/UIManager.h"

#include "xdaq/Application.h"
#include "xdaq/pipe/ServiceListener.h"
#include "xdaq/pipe/Service.h"

#include "shub/Loopback.h"
#include "shub/Protocol.h"


namespace shub 
{

class Events
{
public:
    
    Events(size_t replicas): replicas_(replicas)
    {}
    
    bool append(size_t fragmentid, size_t node, toolbox::mem::Reference * ref)
    {
        fragments_[fragmentid].push_back(ref);
        
        if (fragments_[fragmentid].size() == replicas_ )
        {
            return true;
        }
        return false;
    }

    std::list<toolbox::mem::Reference *> get(size_t fragmentid)
    {
        return fragments_[fragmentid];
    }
    
    std::list<size_t> get()
    {
        std::list<size_t> fragmentids;
        for(auto const& i: fragments_)
        {
            fragmentids.push_back(i.first);
        }
        return fragmentids;
        
    }
    
    std::pair<size_t,size_t> stats()
    {
        size_t fragments = 0;
        size_t partial = 0;
        for(auto const& e: fragments_)
        {
            fragments++;
            if( e.second.size() == 1)
                partial++;
        }
        return  std::make_pair(fragments,partial);
        
    }
    
    
    void clear()
    {
        fragments_.clear();
    }
    
    void clear(size_t fragmentid)
    {
        fragments_.erase(fragmentid);
    }
    
protected:
    
    size_t replicas_;
    // old algorithm std::map<size_t, std::list< std::pair<toolbox::mem::Reference *, xdaq::pipe::Input*> > > fragments_;
    std::map<size_t, std::list<toolbox::mem::Reference *> > fragments_;
};


class Node: public xdaq::Application, public xgi::framework::UIManager, public xdata::ActionListener, public xdaq::pipe::ServiceListener 
{	
public:
    XDAQ_INSTANTIATOR();
    
    /*
     typedef struct
     {
     uint64_t node;
     uint64_t id;
     } FragmentDescriptor;
     */
    
    
    Node(xdaq::ApplicationStub* c);
    ~Node();
    
    
protected:
    
    void actionPerformed(xdaq::pipe::EstablishedConnection & ec);
    bool actionPerformed(xdaq::pipe::ConnectionRequest & ec);
    void actionPerformed(xdaq::pipe::ConnectionError & ce);
    void actionPerformed(xdaq::pipe::ConnectionClosedByPeer & ccbp);
    void actionPerformed(xdaq::pipe::BrokenPipe & bp);
    
    void recycle(std::list<std::pair<toolbox::mem::Reference *, xdaq::pipe::Input*>> & event);
    
    
    void measure(std::string name);
    void shuffle(std::string name);
    void builder(std::string name);
    void connect(std::string name);
    void input(std::string name);
    
    //
    // used to create the memory pool upon parametrization
    //
    void actionPerformed (xdata::Event& e);
    
    
    std::set<const xdaq::ApplicationDescriptor*> destination_;
    xdaq::ApplicationDescriptor **servers_;	// array of all instance
    
    toolbox::mem::Pool* pool_;			// Memory pool for allocating messages for sending
    
    xdaq::pipe::Service * pipeService_;
    std::map<const xdaq::ApplicationDescriptor*, xdaq::pipe::Output*> opipes_;
    
    enum class ConnectionStatus { idle, connecting, established };
    
    std::map<const xdaq::ApplicationDescriptor*, ConnectionStatus> connections_;
    
    
    std::mutex omutex_;
    std::mutex imutex_;
    
    xdata::String poolName_;
    toolbox::TimeVal lastTime_;
    xdata::UnsignedInteger maxReceiveBuffers_;
    xdata::UnsignedInteger maxBlockSize_;
    xdata::UnsignedInteger ioQueueSize_;
    size_t counter_;
    std::vector<xdaq::pipe::Input*> ipipes_;
    
    size_t replicas_;
    size_t currentFragmentId_;
    
    shub::InputLoopback * ilb_;
    shub::OutputLoopback * olb_;
    
    Events * events_;
    
    // fake data input
    toolbox::rlist<toolbox::mem::Reference*>* idata_;
    toolbox::rlist<toolbox::mem::Reference*>* idatafree_;
    
    // used to inhibit input generator
    bool enabled_;
    
    // free blocks available
    toolbox::rlist<toolbox::mem::Reference*>* freeinputlist_;
    toolbox::rlist<toolbox::mem::Reference*>* freedatalist_;
    
    
    //size_t numAllocatedFrames_;
    std::vector<size_t> resources_;
    
};
}

#endif
