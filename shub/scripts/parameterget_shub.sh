#!/bin/sh

echo "host " $1 " lid "$2 " namespace " $3

curl --stderr /dev/null \
-H "SOAPAction: urn:xdaq-application:lid=$2" \
-d "<SOAP-ENV:Envelope SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\"
	  xmlns:SOAP-ENV=\"http://schemasxmlsoap.org/soap/envelope/\"
	  xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
	  xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\">
	<SOAP-ENV:Header>
	</SOAP-ENV:Header>
	<SOAP-ENV:Body>
		<xdaq:ParameterGet xmlns:xdaq=\"urn:xdaq-soap:3.0\">
			<p:properties xmlns:p=\"urn:xdaq-application:$3\" xsi:type=\"soapenc:Struct\">
				<p:eventRate xsi:type=\"xsd:unsignedInt\"></p:eventRate>
			</p:properties>
		</xdaq:ParameterGet>
	</SOAP-ENV:Body>
	</SOAP-ENV:Envelope>" $1
