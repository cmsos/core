#!/usr/bin/tclsh

set tests {
128
256
512
1024
2048
4096
8192
12288
16384
20480
24064
26112
30720
32768
36864
40960
45056
51200
55296
62720
65536
86016
106496
126976
131072
151552
172032
192512
212992
233472
256000
}


set bootline {
        { "shub::InputRC" "http://d3vrubu-c2e33-06-01.cms:20000" 20 }
        { "shub::InputRC" "http://d3vrubu-c2e33-08-01.cms:20000" 20 }
        { "shub::InputRC" "http://d3vrubu-c2e33-10-01.cms:20000" 20 }
        { "shub::InputRC" "http://d3vrubu-c2e33-12-01.cms:20000" 20 }
        { "shub::NodeRC" "http://d3vrubu-c2e33-06-01.cms:20000" 40 }
        { "shub::NodeRC" "http://d3vrubu-c2e33-10-01.cms:20000" 40 }
        { "shub::NodeRC" "http://d3vrubu-c2e33-12-01.cms:20000" 40 }
        { "shub::NodeRC" "http://d3vrubu-c2e33-08-01.cms:20000" 40 }
}

puts "start test"


puts "Wait for nodes to be connected"
after 5000
puts "go"

foreach test $tests {

	set size [lindex $test 0]

	puts [format "testing size %d " $size]

	foreach item $bootline {
#       puts $item
        	set name [lindex $item 0]
        	set url  [lindex $item 1]
        	set lid  [lindex $item 2]

		puts [format "configuring Node on %s " $url]

		exec ./parameterset_shub.sh $url $lid $size $name 
	}

	puts "wait for benchmarking (60s) ..."
	after 60000


	puts "collect measurements"
	foreach item $bootline {
	 	set name [lindex $item 0]
                set url  [lindex $item 1]
                set lid  [lindex $item 2]

                puts [format "Getting eventRate from %s " $url]
		#exec ./parameterget_shub.sh $url $lid $name

	}

	puts "move to next size"

}

puts "test finished"

