rpm: _rpmall

installrpm: _installrpmall

cleanrpm: _cleanrpmall

changelog: _changelogall


ifndef INSTALL_PATH
INSTALL_PATH = /opt/xdaq
endif

PackagePath  = $(shell cd $(BUILD_HOME)/$(Package);pwd)
PWD          = $(shell pwd)


ifndef PackageName
ERROR="Missing PackageName for '$(Package)'
endif

ifndef PACKAGE_VER_MAJOR
ERROR="Missing major version number for '$(Package)'
endif

ifndef PACKAGE_VER_MINOR
ERROR="Missing minor version number for '$(Package)'
endif

ifndef PACKAGE_VER_PATCH
ERROR="Missing patch version number for '$(Package)'
endif

ifndef PACKAGE_RELEASE
ERROR="Missing package release for '$(Package)'
endif

ifndef BUILD_VERSION
BUILD_VERSION=1
endif

ifndef BUILD_COMPILER
BUILD_COMPILER :=$(CC)$(shell $(CC) -dumpversion | sed -e 's/\./_/g')
endif

ifndef BUILD_DISTRIBUTION
BUILD_DISTRIBUTION := $(shell $(XDAQ_ROOT)/$(BUILD_SUPPORT)/checkos.sh)
endif

PACKING_DIR=$(PROJECT_NAMESPACE)$(Project)-$(PackageName)-$(PACKAGE_VER_MAJOR).$(PACKAGE_VER_MINOR).$(PACKAGE_VER_PATCH)

#
# Extract summary, description and authors
#
ifndef Description
ERROR="Missing Description for '$(Package)'
endif

ifndef Summary
ERROR="Missing Summary for '$(Package)'
endif

ifndef Authors
ERROR="Missing Authors for '$(Package)'
endif

ifndef Link
ERROR="Missing Link for '$(Package)'
endif



.PHONY: _rpmall

ifneq ($(ERROR),)
_rpmall: fail
fail:
	$(error $(ERROR))
else
_rpmall: makerpm
endif


.PHONY: makerpm
makerpm: spec_update
	mkdir -p $(PackagePath)/rpm/rpmbuild/{RPMS/$(XDAQ_PLATFORM),SPECS,BUILD,SOURCES,SRPMS}
	tar -P -X $(XDAQ_ROOT)/$(BUILD_SUPPORT)/src.exclude -zcf $(PackagePath)/rpm/rpmbuild/SOURCES/$(PackageName)-$(PACKAGE_VER_MAJOR).$(PACKAGE_VER_MINOR).$(PACKAGE_VER_PATCH)-$(BUILD_VERSION).$(PACKAGE_RELEASE).$(BUILD_DISTRIBUTION).$(BUILD_COMPILER).tar.gz -C $(PackagePath) $(PACKING_DIR)
	rpmbuild $(QUIET) -bb --define "_topdir $(PackagePath)/rpm/rpmbuild" $(PackagePath)/rpm/$(PackageName).spec || exit 1
	find $(PackagePath)/rpm/rpmbuild -name "*.rpm" -exec mv {} $(PackagePath)/rpm \;

.PHONY: spec_update
spec_update: 
	mkdir -p $(PackagePath)/rpm
	if [ -e $(PackagePath)/spec.template ]; then \
		cp $(PackagePath)/spec.template $(PackagePath)/rpm/$(PackageName).spec; \
	else \
		cp $(XDAQ_ROOT)/$(BUILD_SUPPORT)/ExternSpec.template $(PackagePath)/rpm/$(PackageName).spec; \
	fi
	perl -p -i -e 's#__release__#$(BUILD_VERSION).$(PACKAGE_RELEASE).$(BUILD_DISTRIBUTION).$(BUILD_COMPILER)#' $(PackagePath)/rpm/$(PackageName).spec
	perl -p -i -e 's#__version__#$(PACKAGE_VER_MAJOR).$(PACKAGE_VER_MINOR).$(PACKAGE_VER_PATCH)#' $(PackagePath)/rpm/$(PackageName).spec
	perl -p -i -e 's#__prefix__#$(INSTALL_PATH)#' $(PackagePath)/rpm/$(PackageName).spec
	perl -p -i -e 's#__package__#$(Package)#' $(PackagePath)/rpm/$(PackageName).spec
	perl -p -i -e 's#__packagedir__#$(PackagePath)#' $(PackagePath)/rpm/$(PackageName).spec
	perl -p -i -e 's#__packagename__#$(PackageName)#' $(PackagePath)/rpm/$(PackageName).spec
	perl -p -i -e 's#__os__#$(XDAQ_OS)#' $(PackagePath)/rpm/$(PackageName).spec
	perl -p -i -e 's#__platform__#$(XDAQ_PLATFORM)#' $(PackagePath)/rpm/$(PackageName).spec
	perl -p -i -e 's#__project__#$(PROJECT_NAMESPACE)$(Project)#' $(PackagePath)/rpm/$(PackageName).spec
	perl -p -i -e 's#__author__#$(Authors)#' $(PackagePath)/rpm/$(PackageName).spec
	perl -p -i -e 's#__summary__#$(Summary)#' $(PackagePath)/rpm/$(PackageName).spec
	perl -p -i -e 's#__description__#$(Description)#' $(PackagePath)/rpm/$(PackageName).spec
	perl -p -i -e 's#__url__#$(Link)#' $(PackagePath)/rpm/$(PackageName).spec
	perl -p -i -e 's#__buildarch__#$(XDAQ_ARCH)#' $(PackagePath)/rpm/$(PackageName).spec
ifdef NO_DEBUG_PACKAGE
	perl -p -i -e 's#__builddebugpackage__#%define debug_package %{nil}#' $(PackagePath)/rpm/$(PackageName).spec
else
	perl -p -i -e 's#__builddebugpackage__#%{nil}#' $(PackagePath)/rpm/$(PackageName).spec
endif

.PHONY: _cleanrpmall
_cleanrpmall:
	-rm -rf $(PackagePath)/rpm

.PHONY: _installrpmall
_installrpmall:
	mkdir -p $(INSTALL_PREFIX)/rpm
	cp $(PackagePath)/rpm/*.rpm $(INSTALL_PREFIX)/rpm

.PHONY: _changelogall
_changelogall:
	cd $(PackagePath);\
	git show --summary > ChangeLog

