/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2024, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, L. Orsini and D. Simelevicius                   *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _xdaq_version_h_
#define _xdaq_version_h_

#include "config/PackageInfo.h"

// !!! Edit this line to reflect the latest package version !!!

#define CORE_XDAQ_VERSION_MAJOR 5
#define CORE_XDAQ_VERSION_MINOR 14
#define CORE_XDAQ_VERSION_PATCH 0
// If any previous versions available E.g. #define CORE_XDAQ_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define CORE_XDAQ_PREVIOUS_VERSIONS "5.1.0,5.1.1,5.1.2,5.2.0,5.3.0,5.3.1,5.4.0,5.5.0,5.5.1,5.6.0,5.7.0,5.8.0,5.9.0,5.10.0,5.11.0,5.12.0,5.13.0"

//
// Template macros
//
#define CORE_XDAQ_VERSION_CODE PACKAGE_VERSION_CODE(CORE_XDAQ_VERSION_MAJOR,CORE_XDAQ_VERSION_MINOR,CORE_XDAQ_VERSION_PATCH)
#ifndef CORE_XDAQ_PREVIOUS_VERSIONS
#define CORE_XDAQ_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(CORE_XDAQ_VERSION_MAJOR,CORE_XDAQ_VERSION_MINOR,CORE_XDAQ_VERSION_PATCH)
#else 
#define CORE_XDAQ_FULL_VERSION_LIST  CORE_XDAQ_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(CORE_XDAQ_VERSION_MAJOR,CORE_XDAQ_VERSION_MINOR,CORE_XDAQ_VERSION_PATCH)
#endif 


namespace xdaq 
{
	const std::string project = "core";
	const std::string package  =  "xdaq";
	const std::string versions = CORE_XDAQ_FULL_VERSION_LIST;
	const std::string summary = "XDAQ core package";
	const std::string description = "";
	const std::string authors = "Johannes Gutleber, Luciano Orsini, Dainius Simelevicius";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/xdaq";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
