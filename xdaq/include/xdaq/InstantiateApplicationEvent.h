// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _xdaq_InstantiateApplicationEvent_h_
#define _xdaq_InstantiateApplicationEvent_h_

#include <string>
#include "xdaq/Event.h"
#include "xdaq/ApplicationDescriptor.h"

namespace xdaq 
{

class InstantiateApplicationEvent: public xdaq::Event
{
	public:
	
	// typedef toolbox::mem::CountingPtr<InstantiateApplicationEvent, toolbox::mem::ThreadSafeReferenceCount, toolbox::mem::StandardObjectPolicy> Reference;
	
	InstantiateApplicationEvent(const xdaq::ApplicationDescriptor* d)
		: xdaq::Event("urn:xdaq-event:InstantiateApplication", 0)
	{
		descriptor_ = d;
	}
	
	InstantiateApplicationEvent(const xdaq::ApplicationDescriptor* d, xdaq::Application* originator)
			: xdaq::Event("urn:xdaq-event:InstantiateApplication", originator)
	{
		descriptor_ = d;
	}

	const xdaq::ApplicationDescriptor* getApplicationDescriptor() const
	{
		return descriptor_;
	}
		
	protected:
	
	const xdaq::ApplicationDescriptor* descriptor_;
	
};

}

#endif
