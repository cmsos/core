// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _xdaq_pipe_EstablishedConnection_h_
#define _xdaq_pipe_EstablishedConnection_h_

#include "xdaq/ApplicationDescriptor.h"
#include "pt/pipe/EstablishedConnection.h"

namespace xdaq 
{
	namespace pipe 
	{

		class EstablishedConnection
		{
			public:

			EstablishedConnection(const xdaq::ApplicationDescriptor * source, const xdaq::ApplicationDescriptor * destination, std::shared_ptr<pt::pipe::EstablishedConnection> & pec): 
				source_(source), destination_(destination), pec_(pec)
			{
			}

			virtual ~EstablishedConnection()
			{
			}

			const xdaq::ApplicationDescriptor * getSourceDescriptor() const
			{
				return source_;
			}

			const xdaq::ApplicationDescriptor * getDestinationDescriptor() const
			{
				return destination_;
			}

			std::shared_ptr<pt::pipe::EstablishedConnection>  getPEC()
			{
				return pec_;
			}
			
			protected:

			const xdaq::ApplicationDescriptor * source_;
			const xdaq::ApplicationDescriptor * destination_;
			std::shared_ptr<pt::pipe::EstablishedConnection> pec_;

			

		};

	}
}

#endif
