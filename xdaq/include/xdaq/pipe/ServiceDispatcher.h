// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _xdaq_pipe_ServiceDispatcher_h_
#define _xdaq_pipe_ServiceDispatcher_h_

#include <map>
#include "pt/pipe/ServiceListener.h"
#include "xdaq/Application.h"

namespace xdaq 
{
	namespace pipe 
	{

		class ServiceDispatcher : public pt::Listener, public std::list<pt::pipe::ServiceListener*>
		{
			public:

			std::string getService()
			{
				return "pipe";
			}
		};

	}
}

#endif
