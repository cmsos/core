// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _xdaq_pipe_Service_h_
#define _xdaq_pipe_Service_h_

#include <memory>

#include "xdaq/pipe/ServiceListener.h"

namespace xdaq 
{
	class ApplicationDescriptor;
	class ApplicationContext;
	class Application;

	namespace pipe 
	{

		class Output;
		class Input;
		class ConnectionRequest;
		class EstablishedConnection;
		

		class Service
		{
			public:

				virtual void connect(const xdaq::ApplicationDescriptor * source,  const xdaq::ApplicationDescriptor * destination)  = 0;
				virtual xdaq::pipe::Output * createOutputPipe(xdaq::pipe::EstablishedConnection & ec)  = 0;
				virtual xdaq::pipe::Input * createInputPipe(xdaq::pipe::ConnectionRequest & cr)  = 0;
				virtual xdaq::pipe::Output * createOutputPipe(xdaq::pipe::EstablishedConnection & ec, size_t n)  = 0;
				virtual xdaq::pipe::Input * createInputPipe(xdaq::pipe::ConnectionRequest & cr, size_t n)  = 0;
				virtual void destroyInputPipe(xdaq::pipe::Input * ip)  = 0;
				virtual void destroyOutputPipe(xdaq::pipe::Output * ip)  = 0;


		};

	}
}

#endif
