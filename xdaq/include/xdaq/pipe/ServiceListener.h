// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _xdaq_pipe_ServiceListener_h_
#define _xdaq_pipe_ServiceListener_h_

#include "xdaq/pipe/EstablishedConnection.h"
#include "xdaq/pipe/ConnectionRequest.h"
#include "xdaq/pipe/ConnectionError.h"
#include "xdaq/pipe/ConnectionClosedByPeer.h"
#include "xdaq/pipe/BrokenPipe.h"

namespace xdaq 
{
	namespace pipe 
	{
		class ServiceListener
		{
			public:
	
			virtual void actionPerformed(xdaq::pipe::EstablishedConnection & ec) = 0;
			virtual bool actionPerformed(xdaq::pipe::ConnectionRequest & cr) = 0;
			virtual void actionPerformed(xdaq::pipe::ConnectionError & ce) = 0;
			virtual void actionPerformed(xdaq::pipe::ConnectionClosedByPeer & ccbp) = 0;
			virtual void actionPerformed(xdaq::pipe::BrokenPipe & bp) = 0;
		};

	}
}

#endif
