// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _xdaq_pipe_ConnectionClosedByPeer_h_
#define _xdaq_pipe_ConnectionClosedByPeer_h_

#include <memory>

#include "xdaq/ApplicationDescriptor.h"
#include "pt/pipe/ConnectionClosedByPeer.h"

namespace xdaq 
{
	namespace pipe 
	{

		class ConnectionClosedByPeer
		{
			public:

                        ConnectionClosedByPeer(const xdaq::ApplicationDescriptor * source, const xdaq::ApplicationDescriptor * destination, void * phandle ): 
				source_(source), destination_(destination), phandle_(phandle) 
			{}

                        const xdaq::ApplicationDescriptor * getSourceDescriptor() const
                        {
                                return source_;
                        }

                        const xdaq::ApplicationDescriptor * getDestinationDescriptor() const
                        {
                                return destination_;
                        }

			void * getPipeHandle()
                        {
                                return phandle_;
                        }

                        protected:

                        const xdaq::ApplicationDescriptor * source_;
                        const xdaq::ApplicationDescriptor * destination_;
			void * phandle_;

				 

		};

	}
}

#endif
