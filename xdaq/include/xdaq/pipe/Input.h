// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _xdaq_pipe_Input_h_
#define _xdaq_pipe_Input_h_

#include "pt/pipe/Input.h"
#include "toolbox/mem/Reference.h"
#include "pt/pipe/Service.h"

namespace xdaq 
{
	namespace pipe 
	{


		class Input
		{
			friend class ServiceImpl;

			public:

			Input(pt::pipe::Input * handle, pt::pipe::Service* pts): handle_(handle), pts_(pts)
                        {
                        }


                        virtual ~Input()
                        {
                        }

			toolbox::mem::Reference * get()
			{
				return handle_->completed();
			}

			void post(toolbox::mem::Reference * r)
			{
				handle_->postFrame(r);
			}

			bool empty()
			{
				return handle_->empty();
			}
			
			bool match( void * handle)
			{
				return (void*)handle_ == handle;
			}

			protected:

			pt::pipe::Service *  getPeerTransportService()
			{
				return pts_;
			}

			pt::pipe::Input *  getPipeHandle()
			{
				return handle_;
			}
			
                        public:

                        pt::pipe::Input * handle_;
			pt::pipe::Service* pts_;



		};

	}
}

#endif
