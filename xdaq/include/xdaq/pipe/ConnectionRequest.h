// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _xdaq_pipe_ConnectionRequest_h_
#define _xdaq_pipe_ConnectionRequest_h_

#include <memory>

#include "xdaq/ApplicationDescriptor.h"
#include "pt/pipe/ConnectionRequest.h"

namespace xdaq 
{
	namespace pipe 
	{

		class ConnectionRequest
		{
			public:

                        ConnectionRequest(const xdaq::ApplicationDescriptor * source, const xdaq::ApplicationDescriptor * destination, std::shared_ptr<pt::pipe::ConnectionRequest> & pcr ): 
				source_(source), destination_(destination), pcr_(pcr) 
			{}

                        const xdaq::ApplicationDescriptor * getSourceDescriptor() const
                        {
                                return source_;
                        }

                        const xdaq::ApplicationDescriptor * getDestinationDescriptor() const
                        {
                                return destination_;
                        }

			std::string getNetwork()
			{
				return pcr_->getNetwork();
			}


			std::shared_ptr<pt::pipe::ConnectionRequest>  getPCR()
                        {
                                return pcr_;
                        }

                        protected:

                        const xdaq::ApplicationDescriptor * source_;
                        const xdaq::ApplicationDescriptor * destination_;
			std::shared_ptr<pt::pipe::ConnectionRequest> pcr_;

				 

		};

	}
}

#endif
