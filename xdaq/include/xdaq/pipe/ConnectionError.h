// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _xdaq_pipe_ConnectionError_h_
#define _xdaq_pipe_ConnectionError_h_

#include "xcept/Exception.h"
#include "xdaq/ApplicationDescriptor.h"

namespace xdaq 
{
	namespace pipe 
	{

		class ConnectionError
		{
			public:
			ConnectionError(const xdaq::ApplicationDescriptor * source, const xdaq::ApplicationDescriptor * destination, xcept::Exception & e): source_(source), destination_(destination), e_(e){}

			virtual ~ConnectionError()
			{
				//std::cout << "DTOR for xdaq::pipe::ConnectionError" << std::endl;
			}

			const xdaq::ApplicationDescriptor * getSourceDescriptor() const
			{
				return source_;
			}

			const xdaq::ApplicationDescriptor * getDestinationDescriptor() const
			{
				return destination_;
			}

	                xcept::Exception getException()
                	{
                        	return e_;
                	}
			
			protected:

			const xdaq::ApplicationDescriptor * source_;
			const xdaq::ApplicationDescriptor * destination_;
			xcept::Exception e_;
			

			

		};

	}
}

#endif
