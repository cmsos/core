// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _xdaq_pipe_Interface_h_
#define _xdaq_pipe_Interface_h_


#include "xdaq/pipe/ServiceListener.h"
#include "xdaq/pipe/Service.h"
#include "xdaq/Application.h"

namespace xdaq 
{
	namespace pipe 
	{

		class Interface
		{
			public:

				virtual xdaq::pipe::Service * join(xdaq::Application * application, xdaq::pipe::ServiceListener * listener) = 0;
				virtual void  detach(xdaq::pipe::Service * service)  = 0;

		};

	}

	xdaq::pipe::Interface * getPipeInterface();
}

#endif
