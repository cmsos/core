// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _xdaq_pipe_InterfaceImpl_h_
#define _xdaq_pipe_InterfaceImpl_h_

#include "xdaq/pipe/Interface.h"
#include "xdaq/pipe/ServiceDispatcher.h"
#include "xdaq/pipe/ServiceImpl.h"

namespace xdaq 
{
	namespace pipe 
	{

		class InterfaceImpl: public xdaq::pipe::Interface
		{
			public:

			InterfaceImpl();
			
			static InterfaceImpl * instance();

			xdaq::pipe::Service * join(xdaq::Application * application, xdaq::pipe::ServiceListener * listener);

			void detach(xdaq::pipe::Service * service);

			protected:

               		std::map<xdaq::Application*, xdaq::pipe::ServiceImpl*>  members_;
               		xdaq::pipe::ServiceDispatcher pipeServiceDispatcher_;

			private:

			static xdaq::pipe::InterfaceImpl * instance_;


		};

	}

}

#endif
