// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _xdaq_pipe_ServiceImpl_h_
#define _xdaq_pipe_ServiceImpl_h_

#include "xdaq/pipe/Service.h"
#include <memory>

#include "xdaq/pipe/Input.h"
#include "xdaq/pipe/Output.h"
#include "xdaq/pipe/ConnectionRequest.h"
#include "xdaq/pipe/EstablishedConnection.h"
#include "xdaq/pipe/ConnectionError.h"
#include "xdaq/pipe/ServiceListener.h"
#include "xdaq/pipe/BrokenPipe.h"

#include "xdaq/ApplicationDescriptor.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/Application.h"
#include "xdaq/Object.h"

#include  "pt/PeerTransportAgent.h"
#include  "pt/PeerTransportSender.h"
#include  "pt/PeerTransportReceiver.h"
#include  "pt/pipe/ServiceListener.h"
#include  "pt/pipe/ConnectionError.h"

#include "pt/pipe/Output.h"
#include "pt/pipe/OutputListener.h"
#include "pt/pipe/InputListener.h"
#include "pt/pipe/Service.h"

namespace xdaq 
{
	namespace pipe 
	{

		class ServiceImpl : public xdaq::Object, public xdaq::pipe::Service, public pt::pipe::ServiceListener, public pt::pipe::OutputListener, public pt::pipe::InputListener
		{
			public:

				ServiceImpl(xdaq::Application * a, xdaq::pipe::ServiceListener * l);

				// Input/Output Pipe listener
				void actionPerformed(std::shared_ptr<pt::pipe::ConnectionError> pec);
                        	bool actionPerformed(std::shared_ptr<pt::pipe::ConnectionRequest> pcr);
				void actionPerformed(std::shared_ptr<pt::pipe::EstablishedConnection>  pec);

                        	void actionPerformed(pt::pipe::UndeliverableBlock & event);
                        	void actionPerformed(pt::pipe::BrokenPipe & bp); 
                        	void actionPerformed(pt::pipe::UnusedBlock & event);
                        	void actionPerformed(pt::pipe::ConnectionClosedByPeer & ccbp);

				// API
				void connect(const xdaq::ApplicationDescriptor * from,  const xdaq::ApplicationDescriptor * to);
				xdaq::pipe::Output * createOutputPipe(xdaq::pipe::EstablishedConnection & ec);
				xdaq::pipe::Output * createOutputPipe(xdaq::pipe::EstablishedConnection & ec, size_t n);
				xdaq::pipe::Input * createInputPipe(xdaq::pipe::ConnectionRequest & cr); 
				xdaq::pipe::Input * createInputPipe(xdaq::pipe::ConnectionRequest & cr, size_t n); 
				void destroyInputPipe(xdaq::pipe::Input * ip); 
				void destroyOutputPipe(xdaq::pipe::Output * op);

			protected:

				xdaq::pipe::ServiceListener * listener_; // client application

		};

	}
}

#endif
