// --  DOMAIN:
// --    XDAQ
// --
// --  FACILITY:
// --    An example server that demonstrates how to instantiate a
// --    XMI dispatcher driven server in the DAQ domain. For communication
// --    UDP vxiostreams are used.
// --    This file may serve as a programming example for DAQ developers.
// --
// --  ABSTRACT:
// --    TBD.
// --
// --  VERSION:
// --    1.0
// --
// --  MODIFICATION:
// --    Date           Purpose                               Author
// --    26-Jul-1999    Creation.                             JG
// --

//
// Include the server interface
//
#include "XRUListener.h"
#include "XFUListener.h"
#include "XEVMListener.h"
#include "XRCListener.h"
#include "XMultiCasterDispatcher.h"
//
// Include the communication library 
//
#ifdef vxtoolbox
#include "vxioslib.h"
#endif

//
// Include other useful stuff
//
#include "iostream.h"
class myTest3: public XFUListener , public XRUListener, public XRCListener
{
	void cache (FragmentIdentifier* fragid, EventIdentifier* eid,
		  DestinationCookie* dcookie, FragmentData* data)
	{
		cout << "CIAO Baby" << endl;
	}
	void confirm( EventIdentifier* event )
	{
	  cout << "CIAO BUBY " << *event << endl;
	}


	void readout(EventIdentifier* event)
	{
	  cout << "CIAO BYBY! " << *event << endl;

	}
	
	void send(EventIdentifier* eid, DestinationCookie* dcookie) {
	}
	 void config (char * str) {};
         void start (char * str){};
         void stop (char * str){};
         void shutdown (char * str){};
         void action (char * str){}; 
         void suspend (char * str){};
         void resume (char * str){};
};
class myTest: public XRUListener,  public XFUListener
{

	void confirm( EventIdentifier* event )
	{
	  cout << "Got confirmation for event: " << *event << endl;
	}

	void cache (FragmentIdentifier* fragid, EventIdentifier* eid,
		  DestinationCookie* dcookie, FragmentData* data)
	{
	  cout << "Received a cache for event " << *eid 
	       << " with fragment " 
	       << *fragid 
 	      << " len: " 
  	     << data->length 
  	     << ", data: " 
   	    << data->data
   	    << endl;
	}


	virtual void readout(EventIdentifier* event)
	{
	  cout << "Hello I am a trigger! " << *event << endl;

	}
	virtual void send(EventIdentifier* eid, DestinationCookie* dcookie) {
	}
};

class myTest4: public myTest
{
	//void readout(EventIdentifier* event)
	//{
	 // cout << "HOLA TIO! " << *event << endl;

	//}

};
//
// The main program
//
#ifdef vxworks
int Main()
#else
int main(int argc, char* argv[])
#endif
{
  //
  // The dispatcher should be bound to an open stream later
  //
  XMultiCasterDispatcher disp;

  // 
  // Create the server and bind it to the dispatcher
  //
  myTest * test = new myTest();
  
  myTest * test2 = new myTest();
  myTest3 * test3 = new myTest3();
  myTest4 * test4 = new myTest4();
  
  disp.addListener(test);
  disp.addListener(test2);
   disp.addListener(test3);
    disp.addListener(test4);
  //
  // Prepare a buffer for receiving
  //
  char* buf = new char[1024];   
  
  //
  // Open a UDP stream
  //
  vxiostream*  streamSrv_;
  vxhandle*    handleSrv_;
  udpAddr      sock_;
  handleSrv_ = new vxudphandle();
  streamSrv_ = new vxdgramiostream (static_cast<vxudphandle*>(handleSrv_));

  //
  // Receiving on port 20200
  //
  sock_.port = 20200;

  if ( getenv("SERVER_ADDRESS") == NULL )
    {
      cout << "SERVER_ADDRESS environment variable is missing" << endl;
      exit(-1);
    }
  
  strcpy(sock_.host,getenv("SERVER_ADDRESS"));
  static_cast<vxudphandle*>(handleSrv_)->bind(&sock_);

  DAQProtocolHeader* proto;

  int count = 10;
  cout << "Going to Receive:" << count << " Messages" << endl;
  for( int i = 0; i < count; i++)
    {
      //
      // Read the header consisting of length in longwords and
      // the protocol
      //
      *streamSrv_ >> setl(sizeof(DAQProtocolHeader)) >> (char*) buf;
      proto = (DAQProtocolHeader*) buf;
      cout << "Receive size: " << proto->length * 4 << endl;

      //
      // Read the remaining message into the same buffer for forwarding
      // it to the dispatcher. Read ulong bytes less, as the header contains
      // the length including the size of the length+protocol variable
      *streamSrv_ >> setl( (proto->length*4)-sizeof(unsigned long) ) >> 
	(char*) (&buf[sizeof(DAQProtocolHeader)]);

      //
      // Create XObject from stream and make upcall
      //
      disp.dispatch( proto->protocol, buf);
    }

  delete buf;
  delete streamSrv_;
  delete handleSrv_;
  delete test;


  return 0;
}


