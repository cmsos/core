#include "XConfigurator.h"
#include "TcpDispatcherAutoAcceptorTask.h"
#include "Environment.h"
#include "DAQConfig.h"
#include "daqcRU.h"

#ifdef vxworks
#include "MuxSingleCasterDispatcher.h"
#include "SensSingleCasterDispatcherTask.h"
#endif

#include "TaskAttributes.h"
int G_sent = 0;
void usage() {
cout << "usage: ru  [-rc host port | -auto] " << endl;
}
int Main(int argc, char **argv)
{
	//
	// Parse Args
	//
	char  host[256];
	int port;
	int autoinit = 0;
 	for (int i = 1; i < argc; i++)
	{
		if  (strcmp(argv[i],"-auto") == 0 ) {
			autoinit = 1;
			Loader l;
			l.loadEnvironment();
		
			port = (l.DaqCfgRUIpAddrList_[l.DaqCfgSysUAddress_ & 0x0000ffff]).port;

			strcpy(host,(l.DaqCfgRUIpAddrList_[l.DaqCfgSysUAddress_ & 0x0000ffff]).host);
		}
		else if (strcmp(argv[i],"-rc" ) == 0) {
			if ( argc < 4 ) {
				usage();
				return(-1);
			}
			strcpy(host,argv[i+1]);
			sscanf(argv[i+2],"%d",&port);

		}
	
	}
	
	
	Try
	{
		XConfigurator configurator; //general environment configurator
		TcpDispatcherAutoAcceptorTask * dispatcher = new TcpDispatcherAutoAcceptorTask(host,port); //TCP run control dispatcher
  		//SensSingleCasterDispatcherTask  * t = new SensSingleCasterDispatcherTask("fei",0,"dllInput",0x1921 );
#ifdef vxworks
		MuxSingleCasterDispatcher * t = new MuxSingleCasterDispatcher("fei",0,"dllInput",0x1921 );
#endif		
		daqcRU * ru = new daqcRU(dispatcher);

		TaskGroup g;
		dispatcher->initTaskGroup(&g);
		dispatcher->activate();
#ifdef vxworks
		t->setListener(ru);
#endif
  		dispatcher->addListener(&configurator);
		dispatcher->addListener(ru);
		
		
		//Sens task only
		TaskAttributes attrib;
		attrib.stacksize(40000);
		attrib.priority(100);
		attrib.name("MuxDisp");
		//t->set(&attrib);
		//t->activate();
		if ( autoinit ) {
			cout << "Autoinitialize..." << endl;
			ru->init("Autoinitialize");
		}
 		g.join();
	}
	Catch(vxException e)
	{
		cout << e;
		
	};
	return(0);	
}



//
// This include file permits command
// line argument passing under vxworks.
//

#ifndef vxworks
int main(int argc, char* argv[])
{
	return Main(argc,argv);
}
#else
#define MAINFUNC ru
#define MAINNAME "ru"
extern "C" int MAINFUNC(char *args) {
	int argc = 0;
  	char *argv[256];
  	int rv;

  	argv[argc++] = MAINNAME;
  	do {
   		while (*args == ' ') args++;
   		if (*args == '\0') break;
   		argv[argc++] = args;
   		while ((*args != ' ') && (*args != '\0')) args++;
   		if (*args == ' ') *args++ = '\0';
  	} while((*args != '\0') && (argc < 20));
	return Main(argc,argv);
}
#endif
