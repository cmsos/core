#include "TriggerGenerator.h"
#include "daqcEVM.h"

void TriggerGenerator::addListener(daqcEVM * evm) {
		evm_ = evm;
}
	
	
int TriggerGenerator::svc() {
	      cout << " Going to trigger on "<< hex <<  (int)evm_ << dec << endl;
	      for (;;) {
	      	evm_->trigger();
	      }
	      return(0);
}
	
