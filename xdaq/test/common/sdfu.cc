#include "sdfu.h"

extern "C" int Main(char * evmhost, char * ruhost)
{

	
	TcpXIOConnectorS * c1 = new TcpXIOConnectorS(ruhost, 20300);
	TcpXIOConnectorS * c2 = new TcpXIOConnectorS(evmhost, 20200);
	
  	SDispatcherTask * d1 = new SDispatcherTask(c1->getXIO());
	SDispatcherTask * d2 = new SDispatcherTask(c2->getXIO());
	
	XRU * xru = new XRU(c1->getXIO());
	XEVM * xevm = new XEVM(c2->getXIO());
	SDFU * sdfu = new SDFU(xru,xevm);
	
	c1->connect();
	c2->connect();
	

  	TaskGroup g;
  	d1->initTaskGroup(&g);
  	d1->setListener(sdfu);
  	d1->activate();
	
	d2->initTaskGroup(&g);
  	d2->setListener(sdfu);
  	d2->activate();
	
	sleep(3);
	sdfu->start("Go!");
 	g.join();
	delete d1;
	delete d2;
	delete c1;
	delete c2;
	delete xru;
	delete xevm;
	delete sdfu;
	return(0);	
}
#ifndef vxworks
int main(int argc, char* argv[])
{

	return Main(argv[1], argv[2]);
}
#endif
