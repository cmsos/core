#include "XRUAdapter.h"
#include "XRCAdapter.h"
#include "XEVMAdapter.h"
#include "XFUAdapter.h"
#include "XMDispatcherLoop.h"
#include "XSDispatcherLoop.h"
#include "XIO.h"
#include "vxtExceptionHandler.h"

#include "IOCS.h"
#include "vxIOCS.h"
#include "vxRTL.h"
#include "IOCSExceptions.h"

class RU: public XRUAdapter, public XRCAdapter
{	 

private:
	IOCS *RUScript;
	RTL *rtl;
	FILE *scriptfile;

public:
		RU () {
			rtl = new vxRTL();
			RUScript = NULL;
			scriptfile = NULL;
			argscount = 0;
		}
		
		~RU () {
			delete rtl;
			if (RUScript != NULL) delete RUScript;
			if (scriptfile != NULL) fclose(scriptfile);
		}

private:
       void readout (EventIdentifier* eid) {
       	cout << "readout:" << hex << *eid << endl;
			// parseArgs(str);
			// bufPtr = create(eid, &bufSize)
	   		// Err = RUScript->eval(Read,2,bufPtr,bufSize);
			// close(eid);
       }
	   
       void send (EventIdentifier* eid, DestinationCookie* dcookie) {
        cout << "send:" << hex << *eid << dcookie->addr << dcookie->cookie << endl;
       	 // bufPtr = open(eid, &bufSize)
		 // fu->cache( bufPtr,bufSize);
		 // close(eid);
	   }
	   
       void config (char * str) {
			cout << "config:" << str << endl;
			cout << "No of Args: " << argscount << endl;
			int Err;
			if (parseArgs(str)) {
		   		Err = RUScript->eval(Init,argscount,dynargs[0],dynargs[1],dynargs[2],dynargs[3],dynargs[4],dynargs[5],dynargs[6],dynargs[7],dynargs[8],dynargs[9]);
				cout << "Return Value:" << Err << endl;
			}
       }
       
       void start (char * str) {
       		cout << "start:" << str << endl;
			int Err;
			if (parseArgs(str)) {
		   		Err = RUScript->eval(Start,argscount,dynargs[0],dynargs[1],dynargs[2],dynargs[3],dynargs[4],dynargs[5],dynargs[6],dynargs[7],dynargs[8],dynargs[9]);
				cout << "Return Value:" << Err << endl;
			}
       }
       
       void stop (char * str) {
       		cout << "stop:" << str << endl;
			int Err;
			if (parseArgs(str)) {
		   		Err = RUScript->eval(Stop,argscount,dynargs[0],dynargs[1],dynargs[2],dynargs[3],dynargs[4],dynargs[5],dynargs[6],dynargs[7],dynargs[8],dynargs[9]);
				cout << "Return Value:" << Err << endl;
			}
       }
       
       void shutdown (char * str) {
			cout << "shutdown:" << str << endl;
			int Err;
			if (parseArgs(str)) {
		   		Err = RUScript->eval(Shutdown,argscount,dynargs[0],dynargs[1],dynargs[2],dynargs[3],dynargs[4],dynargs[5],dynargs[6],dynargs[7],dynargs[8],dynargs[9]);
				cout << "Return Value:" << Err << endl;
			}
       }
       
       void action (char * str) {
			cout << "action:" << str << endl;
			Try { 
			
				if (RUScript == NULL) {
					cout << "Script not Loaded!" << endl;
					return;
				}
				
				char * c = str;
				int count = 0;
				while ((*c != ',') && (*c != '\0')) {
					count++;
					c++;
				}
				char * ep = new char[count + 1];
				for (int i = 0; i < count; i++) {
					ep[i] = str[i];
				}
				ep[count] = '\0';
			
				if (*c != ',') c++;
				if (parseArgs(c)) {
					int Err;
					if (!strcmp(ep,"Write")) {
						Err = RUScript->eval(Write,argscount,dynargs[0],dynargs[1],dynargs[2],dynargs[3],dynargs[4],dynargs[5],dynargs[6],dynargs[7],dynargs[8],dynargs[9]);
						cout << "Return Value:" << Err << endl;
					}
				}
				
			} Catch(vxException e) {
				cout << "Exception ********" << endl;
				cout << e.name_ << endl;
				cout << e.what_ << endl;
				cout << e.who_ << endl;
				cout << "********" << endl;
			}
       }
        
       void suspend (char * str) {
        cout << "suspend:" << str << endl;
       
       }
	   
       void resume (char * str) {
         cout << "resume:" << str << endl;
       }
	   
       void load (char * str) {
			cout << "load:" << str << endl;
			if (scriptfile != NULL) fclose(scriptfile);
			scriptfile = fopen(str,"rb");
			if (scriptfile == NULL) {
				cout << "File " << str << " not opened!" << endl;
				return;
			}
			Try {
				// Create script object
				if (RUScript != NULL) {
					delete RUScript;
				}
				RUScript = new vxIOCS(scriptfile,rtl);
			}
			Catch(vxException e)
			{
				cout << "Exception ********" << endl;
				cout << e.name_ << endl;
				cout << e.what_ << endl;
				cout << e.who_ << endl;
				cout << "********" << endl;
			}
		 
       }
       
		
		bool parseArgs(char * str) {
			
			// get dynargs
			argscount = 0;
			char * c = str;
			int val = 0;
			
			while ((*c != '\0') && (argscount < 10)) {
			
				// Remove blanks in string
				while (*c == ' ') c++;

				// Get argument value
				if ((c[0] == '0') && (c[1] == 'x')) {
					// Value is hex
					if (sscanf(c,"0x%x",&val) == EOF) {
						cout << "Argument " << argscount + 1 << " not valid!" << endl;
						return false;
					}
				
				} else {
					// Value is decimal
					if (sscanf(c,"%d",&val) == EOF) {
						cout << "Argument " << argscount + 1 << " not valid!" << endl;
						return false;
					}
				}
				
				// Store value
				dynargs[argscount++] = val;

				// Go to next argument
				while ((*c != ',') && (*c != '\0')) {
					c++;
				}

				if (*c == ',') c++;
			}

			// Check that there are not too many arguments
			if (*c != '\0') {
				cout << "Too many arguments! (Maximum of 10)" << endl;
				return false;	
			}
			
			// Parse successfull
			return true;
		}



/*
			for (; *c != '\0' && argscount < 10; c++) {
				
				// Remove blanks in string
				while (*c == ' ') c++;
				
				if (*c == ',') {
					// Argument read, Convert to value

					buf[count] = '\0';

					if ((buf[0] == '0') and (buf[1] == 'x')) {
						// Value is hex, get value
						if (sscanf(buf,"0x%x",&val) != EOF) {
							cout << "Argument " << argscount + 1 << " not valid!" << endl;
							return false;
						}
					
					} else {
						// Value is decimal, get value
						if (sscanf(buf,"%d",&val) != EOF) {
							cout << "Argument " << argscount + 1 << " not valid!" << endl;
							return false;
						}
					}
					
					// Store value
					dynargs[argscount++] = val;
					count = 0;

					// Remove blanks in string
					while (c[1] == ' ') c++;

				} else {
					// Read next character of argument
					
					if (count < 25) {
						buf[count++] = *c;
					} else {
						cout << "Argument " << argscount + 1 << " too big!" << endl;
						return false;
					}
					
				}
			}

					if (*c != '\0') {
						cout << "Too many arguments! (Maximum of 10)" << endl;
						cout << "Action stoped!" << endl;
						return;	
					} else  {
						buf[count] = '\0';
						dynargs[argscount++] = atoi(buf);
						count = 0;
					}
				}
	   }
*/

	   protected:
	   
	   int dynargs[10];
	   int argscount;

};

extern "C" void Main(char * str)
{
  RU * ru = new RU();
  tcpAddr      addr;
  addr.port = 20200;
  strcpy(addr.host,str);
  XIOStreamVector v(1);
  vxtcphandle * handle = new vxtcphandle();
  v[0] = new vxtcpiostream(handle);
  handle->bind(&addr);
  handle->listen(1);
  XIO * xio = new XIO(v);
  
 
  //XSDispatcherLoop * dloop = new XSDispatcherLoop(xio,0);
  XMDispatcherLoop * dloop = new XMDispatcherLoop(xio,0);
  //dloop->setListener(test);
  dloop->addListener(ru);
  //XFU * xfu = new XFU(xio,0);
  cout << "Loop forever" << endl;
  
  retry: // if lost connection
  
  Try
  {

  	handle->accept();

  	dloop->foreverloop();
  }
  Catch(vxException e)
  {
  	cout << e;
	goto retry;
  }
  
  
}
//
// The main program
//
// Usage: Main "137.138.96.204"
//
#ifndef vxworks
int main(int argc, char* argv[])
{
	Main(argv[1]);
 	
}
#endif
