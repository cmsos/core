#include "strstream.h"
#include "XRCAdapter.h"
#include "XRC.h"
#include "XRU.h"
#include "XFU.h"
#include "XIO.h"
#include "vxioslib.h"
#include "vxTiming.h"
#include "TcpDispatcherAcceptorTaskS.h"
#include "TcpXIOConnectorS.h"

#include <stdio.h>


//Single trip
extern "C" int Main(char * host, char* lens)
{

	int len = atoi(lens);
	cout << "Send cache of: " << len << endl;
	TcpXIOConnectorS  tcpio(host,20222); 
	tcpio.connect();
	
	XRC xrc(tcpio.getXIO());
	XRU xru(tcpio.getXIO());
	XFU xfu(tcpio.getXIO());
	char buf[1024*64];
	char str[256];
	int count = 0;
	EventIdentifier  eid = 0;
	FragmentIdentifier fid = 0x0000ffff;
	DestinationCookie dcookie;
	FragmentData  fdata;
	for (int i=0; i < 200000; i++) {
		
		sprintf (buf, "Hello %d", i);
		fdata.data = buf;
		fdata.length = len;
		//xrc.stop("BU");
		//xru.readout(&eid);	
		xfu.cache( &fid, &eid, &dcookie, &fdata);
	}
	
	return(0);
	
}

int main(int argc, char* argv[])
{
	return Main(argv[1], argv[2]);
}

