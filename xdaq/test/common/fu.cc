#include "XConfigurator.h"
#include "TcpDispatcherAcceptorTaskM.h"
#include "Environment.h"
#include "DAQConfig.h"
#include "daqcFU.h"

#ifdef vxworks
#include "MuxSingleCasterDispatcher.h"
#include "SensSingleCasterDispatcherTask.h"
#endif

#include "TaskAttributes.h"
void usage() {
cout << "usage: fu  [-rc host port | -auto] " << endl;
}
daqcFU *fut;
int counter_ = 0;
extern "C" int restart() {
fut->start("Autostart");
return(0);
}
extern "C" int Main(int argc, char **argv)
{
	//
	// Parse Args
	//
	char  host[256];
	int port;
	int autoinit = 0;
 	for (int i = 1; i < argc; i++)
	{
		if  (strcmp(argv[i],"-auto") == 0 ) {
			autoinit = 1;
			Loader l;
			l.loadEnvironment();
			port = (l.DaqCfgBUIpAddrList_[l.DaqCfgSysUAddress_ & 0x0000ffff]).port;
			strcpy(host,(l.DaqCfgBUIpAddrList_[l.DaqCfgSysUAddress_ & 0x0000ffff]).host);
			
		}
		else if (strcmp(argv[i],"-rc" ) == 0) {
			if ( argc < 4 ) {
				usage();
				return(-1);
			}
			strcpy(host,argv[i+1]);
			sscanf(argv[i+2],"%d",&port);

		}
	
	}
	
	Try
	{
		TaskAttributes attrib;
		int DaqCfgBUPort_;
		XConfigurator configurator; //general environment configurator
		TcpDispatcherAcceptorTaskM  * dispatcher = new TcpDispatcherAcceptorTaskM(host,port); //TCP run control dispatcher
#ifdef vxworks  		
		MuxSingleCasterDispatcher * t = new MuxSingleCasterDispatcher("fei",0,"dllInput",0x1923 );
		//SensSingleCasterDispatcherTask  * t = new SensSingleCasterDispatcherTask("fei",0,"dllInput",0x1923 ); 
#endif
		daqcFU *fu = new daqcFU();
		fut = fu;
		TaskGroup g;
		dispatcher->initTaskGroup(&g);
		
		attrib.stacksize(40000);
		attrib.priority(20);
		attrib.name("TcpDisp");
		dispatcher->set(&attrib);
		dispatcher->activate();
#ifdef vxworks
		t->setListener(fu);
#endif
  		dispatcher->addListener(&configurator);
		dispatcher->addListener(fu);
		
		//Sens task only
		
		attrib.stacksize(40000);
		attrib.priority(20);
		attrib.name("MuxDisp");
		//t->set(&attrib);
		//t->activate();
		
		char str[10];

		if ( autoinit ) {
			
			cout << "Init..." << endl;
			//cin >> str;
			fu->init("Autoinitialize");
			fu->start("Autostart");
		}
		pause();
 		g.join();
	}
	Catch(vxException e)
	{
		cout << e;
		
	};
	return(0);	
}

//
// This include file permits command
// line argument passing under vxworks.
//

#ifndef vxworks
int main(int argc, char* argv[])
{
	return Main(argc,argv);
}
#else
#define MAINFUNC fu
#define MAINNAME "fu"
extern "C" int MAINFUNC(char *args) {
	int argc = 0;
  	char *argv[256];
  	int rv;

  	argv[argc++] = MAINNAME;
  	do {
   		while (*args == ' ') args++;
   		if (*args == '\0') break;
   		argv[argc++] = args;
   		while ((*args != ' ') && (*args != '\0')) args++;
   		if (*args == ' ') *args++ = '\0';
  	} while((*args != '\0') && (argc < 20));
	return Main(argc,argv);
}
#endif
