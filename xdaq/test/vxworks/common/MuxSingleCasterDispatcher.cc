

#include "vxNet.h"
#include "MuxSingleCasterDispatcher.h"
int _debug_MuxSingleCasterDispatcher_ =0;


MuxSingleCasterDispatcher::MuxSingleCasterDispatcher( const char* interface,
			  const int unit,
			  const char* protoName,
			  const int type )

{
	status_    = OK;



	::strcpy ( interface_, interface );
	unit_ = unit;
	::strcpy ( protoName_, protoName );
	type_ = type;
	
	muxCookie_ = ::muxBind ( interface_,
			         unit_,
			         ( FUNCPTR ) MuxSingleCasterDispatcher::wrapRcvRtn,
			         ( FUNCPTR ) MuxSingleCasterDispatcher::wrapShutdownRtn,
			         ( FUNCPTR ) MuxSingleCasterDispatcher::wrapTxRestartRtn,
			         MuxSingleCasterDispatcher::wrapErrorRtn,
			         type_,
			         protoName_,
			         ( void* ) this );

	if ( muxCookie_ == NULL ) {
		status_ = ERROR;
		cout << "vxsenshandle::vxsenshandle:: Error muxCookie_ " << endl;
		return; // THROW
	}


}



MuxSingleCasterDispatcher::~MuxSingleCasterDispatcher ( )
{
	STATUS s;
	
	if ( muxCookie_ != NULL ) {
		s = ::muxUnbind(muxCookie_, type_, (FUNCPTR)MuxSingleCasterDispatcher::wrapRcvRtn);
	}

}
BOOL MuxSingleCasterDispatcher::stackRcvRtn ( long type, M_BLK_ID pNetBuff, LL_HDR_INFO* pLinkHdr)
{

	char* buf = pNetBuff->mBlkHdr.mData + pLinkHdr->dataOffset + 2;
	int len = pNetBuff->mBlkHdr.mLen - pLinkHdr->dataOffset;
	
	if (_debug_MuxSingleCasterDispatcher_) {
		::logMsg("stackRcvRtn dump len %d\n", len, 0, 0, 0, 0, 0);
		::logMsg("stackRcvRtn dump LL_HDR_INFO\n", 0, 0, 0, 0, 0, 0);
		::logMsg("stackRcvRtn dump dataOffset 0x%x\n", pLinkHdr->dataOffset, 0, 0, 0, 0, 0);
		::logMsg("stackRcvRtn dump mBlkHdr\n", 0, 0, 0, 0, 0, 0);
		::logMsg("stackRcvRtn dump mBlkHdr.mLen %d\n", pNetBuff->mBlkHdr.mLen, 0, 0, 0, 0, 0);
		
		::logMsg("stackRcvRtn buf 0x%x\n", (int)buf, 0, 0,0, 0, 0);
		char* dbgbuf = pNetBuff->mBlkHdr.mData;
		for(int i=0; i<24; i++)
		::logMsg("stackRcvRtn mData[%d]:0x%x\n", i, dbgbuf[i], 0, 0, 0, 0);
	}

	DAQProtocolHeader* proto;
	proto = (DAQProtocolHeader*) buf;
	if (_debug_MuxSingleCasterDispatcher_) {
		::logMsg("daqProto received size 0x%x\n", (int)(ntohs(proto->length) * 4), 0, 0, 0, 0, 0);
		::logMsg("daqProto Receive protocol 0x%x\n", (int)ntohs(proto->protocol), 0, 0, 0, 0, 0);
	}
	
	this->dispatch(ntohs(proto->protocol),buf);
	
}




void MuxSingleCasterDispatcher::stackShutdownRtn ( )
{
}

void MuxSingleCasterDispatcher::stackTxRestartRtn ( )
{

}

void MuxSingleCasterDispatcher::stackErrorRtn ( END_OBJ* pEnd, END_ERR* pError )
{

}


//
// wrapper members
//

BOOL MuxSingleCasterDispatcher::wrapRcvRtn ( void* muxCookie, long type, M_BLK_ID pNetBuff, LL_HDR_INFO* pLinkHdr,
			        void* pObj )
{
	//::logMsg("MuxSingleCasterDispatcher::wrapRcvRtn\n", 0, 0, 0, 0, 0, 0);

	( ( MuxSingleCasterDispatcher* ) pObj )->stackRcvRtn ( type, pNetBuff, pLinkHdr );
	//It has handled the packet, therefore free MBLK chain
	netMblkClChainFree(pNetBuff);
	return(TRUE);
}

void MuxSingleCasterDispatcher::wrapShutdownRtn ( void* muxCookie, void* pObj )
{
	( ( MuxSingleCasterDispatcher* ) pObj )->stackShutdownRtn ( );
}

void MuxSingleCasterDispatcher::wrapTxRestartRtn ( void* muxCookie, void* pObj )
{
	( ( MuxSingleCasterDispatcher* ) pObj )->stackTxRestartRtn ( );
}

void MuxSingleCasterDispatcher::wrapErrorRtn ( END_OBJ* pEnd, END_ERR* pError, void* pObj )
{
	( ( MuxSingleCasterDispatcher* ) pObj )->stackErrorRtn( pEnd, pError );
}



