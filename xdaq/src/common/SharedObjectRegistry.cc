// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xdaq/SharedObjectRegistry.h"
#include "toolbox/utils.h"
#include "xdaq/exception/LoadFailed.h"
#include "xdaq/exception/SymbolLookupFailed.h"
#include <link.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdexcept>
#include <libelf.h>
#include <gelf.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <cxxabi.h>

static int callback(struct dl_phdr_info *info, size_t size, void *data)
{
	//xdaq::SharedObjectRegistry * so = (xdaq::SharedObjectRegistry *)data;
	std::map <std::string, void*, std::less<std::string> > * so = (std::map <std::string, void*, std::less<std::string> > *)data;
   
   	//printf("name=%s (%d segments)\n", info->dlpi_name, info->dlpi_phnum);
	
   	std::string  pathname = info->dlpi_name;
   	if ( pathname == "" )
		return 0;
	
   	//void* handle = ::dlopen (pathname.c_str(), RTLD_NOLOAD| RTLD_GLOBAL);
	//if ( handle == (void*)NULL)
	//{
//
//	std::cout << "handle ==>" << std::hex << (unsigned long)handle << "-> " << dlerror() << std::endl;
//	}
	
	
	//so->sharedObjects_[pathname] = handle;
	
	//(*so)[pathname] = handle;
	(*so)[pathname] = RTLD_DEFAULT;

	return 0;
}
xdaq::SharedObjectRegistry::SharedObjectRegistry()
{
	// trigger loading info of directly linked libraries ( gcc )
        ::dl_iterate_phdr(callback, &(this->sharedObjects_));

	//std::map <std::string, void*, std::less<std::string> >::iterator i;
        //for (i = sharedObjects_.begin(); i != sharedObjects_.end(); i++)
        //{
//		std::cout << "found ==>" << (*i).first << " " << std::hex << (unsigned long)(*i).second << std::endl;
//	}
}

xdaq::SharedObjectRegistry::~SharedObjectRegistry()
{
	// close all loaded files
	std::map <std::string, void*, std::less<std::string> >::iterator i;
	for (i = sharedObjects_.begin(); i != sharedObjects_.end(); i++)
	{
		if ( (*i).second != RTLD_DEFAULT )
		{
			if (::dlclose( (*i).second ) != 0)
			{
			// no error handling here
			}
		}
	}
}

void xdaq::SharedObjectRegistry::checkdependency(const std::string & pathname) 
	
{	
	std::vector<std::string> const funcs = this->getMangledMatches(pathname, "checkPackageDependencies");
	std::string functionStr = "";
	if (funcs.size() == 0) 
	{
		XCEPT_RAISE ( xdaq::exception::SymbolLookupFailed, "missing function getPackage dependency" );
	}
	if (funcs.size() == 1) // allow only one function per package 
	{
		functionStr = funcs.at(0);
	}
	else
	{
//		for ( auto i = funcs.begin(); i != funcs.end(); i++ )
//		{
//			std::cout << " element: " << *i << std::endl;
//		}
		XCEPT_RAISE ( xdaq::exception::SymbolLookupFailed, "ambiguous function checkPackageDependencies(), multiple instances present in a single package" );
	}

	symbolVFPtrT symPtr = (symbolVFPtrT) this->lookup ( functionStr );

	if (symPtr == (symbolVFPtrT) 0) 
	{
		XCEPT_RAISE ( xdaq::exception::SymbolLookupFailed, ::dlerror() );
	}
	else 
	{
		// Call the check dependencies function
		(*symPtr)();
	}

}


config::PackageInfo xdaq::SharedObjectRegistry::getPackageInfo (const std::string & pathname) const
	
{
	std::vector<std::string> const funcs = this->getMangledMatches(pathname, "getPackageInfo");
	std::string functionStr = "";
	if (funcs.size() == 0) 
	{
		XCEPT_RAISE ( xdaq::exception::SymbolLookupFailed, "missing function getPackage dependency" );
	}
	if (funcs.size() == 1) // allow only one function per package 
	{
		functionStr = funcs.at(0);
	}
	else
	{
		XCEPT_RAISE ( xdaq::exception::SymbolLookupFailed, "ambiguous function getPackageInfo(), multiple instances present in a single package" );
	}

	symbolSFPtrT symPtr = (symbolSFPtrT) this->lookup ( functionStr );

	if (symPtr == (symbolSFPtrT) 0)
	{
		XCEPT_RAISE ( xdaq::exception::SymbolLookupFailed, ::dlerror() );
	}
	else
	{
		// Call the check dependencies function
		return (*symPtr)();
	}
}

// Returns file handle
void xdaq::SharedObjectRegistry::load (const std::string & pathname) 
	
{
	if (pathname == "")
	{
		XCEPT_RAISE ( xdaq::exception::LoadFailed, "Missing filename" );
	}

	// Check if already loaded: ignore if loaded
	if (sharedObjects_.find(pathname) == sharedObjects_.end())
	{
		// load
		//string filename = chop_path((char*)pathname.c_str());
		
		// libXXX.so, lib counts 3, remains XXX
		//string modulename = filename.substr(3,library.size() - (extension.size()+3));
		
		// open the file here
		void* handle = ::dlopen (pathname.c_str(), RTLD_LAZY | RTLD_GLOBAL);
		
		if (handle == 0) 
		{
			XCEPT_RAISE ( xdaq::exception::LoadFailed, ::dlerror() );
		}
		
		// remember handle
		sharedObjects_[pathname] = handle;
	}
}

void xdaq::SharedObjectRegistry::load (const std::string & pathname, bool checkDependencies) 
	
{
	this->load (pathname);				
	
	if (checkDependencies)
	{
		this->checkdependency(pathname);
	} 
}

void xdaq::SharedObjectRegistry::unload(const std::string & pathname) 
	
{
	// Check if already loaded: ignore if loaded
	std::map <std::string, void*, std::less<std::string> >::iterator i = sharedObjects_.find(pathname);
	if (i != sharedObjects_.end())
	{
		if (::dlclose( (*i).second ) != 0)
		{
			XCEPT_RAISE ( xdaq::exception::UnloadFailed, dlerror() );
		}
		
		sharedObjects_.erase(i);
	} 
	else 
	{
		std::string msg = "Cannot close shared library (not unloaded): ";
		msg += pathname;
		XCEPT_RAISE ( xdaq::exception::UnloadFailed, msg );
	}
}

void* xdaq::SharedObjectRegistry::lookup (const std::string & name) const
	
{
	// lookup symbol
	void* symbol = 0;
	std::map <std::string, void*, std::less<std::string> >::const_iterator i;
	for (i = sharedObjects_.begin(); i != sharedObjects_.end(); i++)
	{
		//std::cout << "going to lookup handle :" << std::hex << (unsigned long)(*i).second << " on path " << (*i).first << " symbol:" << name << std::endl;
		symbol = dlsym ( (*i).second, name.c_str());
		if (symbol != 0)
		{
			// std::cout << " found " << std::hex << (unsigned long)symbol << std::endl;
			return symbol;
		}
	}
	
	std::string msg = "Symbol not found: ";
	msg += name;
	XCEPT_RAISE ( xdaq::exception::SymbolLookupFailed, msg );		
}

std::vector<std::string> 
xdaq::SharedObjectRegistry::getObjectNames() const
{
	std::vector<std::string> v;
	std::map <std::string, void*, std::less<std::string> >::const_iterator i;
	for (i = sharedObjects_.begin(); i != sharedObjects_.end(); i++)
	{
		v.push_back((*i).first);
	}
	return v;
}

std::vector<std::string> xdaq::SharedObjectRegistry::getMangledMatches(std::string const& pathName, std::string const& funcName) const
{
	std::string const library = toolbox::chop_path(pathName);
	std::string extension = ".so";
#ifdef macosx
	extension = ".dylib";
#endif
	
	// This is compulsory...
	elf_version(EV_CURRENT);
	
	std::vector<std::string> res;
	int handle = open(pathName.c_str(), O_RDONLY);
	Elf* elf = elf_begin(int(handle), ELF_C_READ, NULL);
	
	Elf_Scn* scn = 0;
	GElf_Shdr shdr;
	while ((scn = elf_nextscn(elf, scn)) != 0)
	{
		gelf_getshdr(scn, &shdr);
		if (shdr.sh_type == SHT_DYNSYM) 
		{
			// Found a symbol table, so let's process it.
			Elf_Data* data = elf_getdata(scn, 0);
        		int const count = shdr.sh_size / shdr.sh_entsize;
	
        		// Loop over symbols in table.
        		for (int i = 0; i < count; ++i)
          		{
            			GElf_Sym sym;
            			gelf_getsym(data, i, &sym);
	
            			// Let's skip undefined symbols. These are of no interest
            			// to us here.
            			if (sym.st_shndx != SHN_UNDEF)
              			{
                			// Get the raw (i.e., mangled) symbol name.
                			char const* symName = elf_strptr(elf, shdr.sh_link, sym.st_name);
                			// Try and demangle the symbol name.
                			int status;
                			char* realNameTmp = abi::__cxa_demangle(symName, 0, 0, &status);
                			std::string realName = "";
                			if (realNameTmp != 0)
                  			{
                    				realName = realNameTmp;
                  			}
	
                			// Here we are only interested in the specified
                			// symbols.
                			if (realName.find(funcName) != std::string::npos)
                  			{
                    				res.push_back(std::string(symName));
                  			}
                			free(realNameTmp);
              			}
          		}
      		}
	}
	elf_end(elf);
	close(handle);
	
	return res;
}

