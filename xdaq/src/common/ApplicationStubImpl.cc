/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, L. Orsini and D. Simelevicius                   *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "xdaq/ApplicationStubImpl.h"
#include "xdaq/ApplicationDescriptorImpl.h"

#include "xdata/InfoSpaceFactory.h"

#include "toolbox/string.h"
#include "xdata/InfoSpace.h"

xdaq::ApplicationStubImpl::ApplicationStubImpl
(
	xdaq::ApplicationContext* c, 
	xdaq::ApplicationDescriptor* d,
	Logger& logger
)

	: logger_ (logger)
{
	this->context_ = c;
	
	
	//this->infoSpace_ = xdata::InfoSpace::get( d->getURN() );

        if (xdata::getInfoSpaceFactory()->hasItem(d->getURN()))
	{
		std::stringstream msg;
		msg << "failed to create application stub associated infospace '" << d->getURN() << "' (already existing)";
		XCEPT_RAISE(xdaq::exception::Exception, msg.str());	
        }
        else
        {
                this->infoSpace_ = xdata::getInfoSpaceFactory()->create(d->getURN());
        }

	this->descriptor_ = d;
	this->infoSpace_->fireItemAvailable ("descriptor",dynamic_cast<xdaq::ApplicationDescriptorImpl*>(d));
	
	this->infoSpace_->fireItemAvailable ("stub",this);

	this->infoSpace_->fireItemAvailable ("liveness", new xdata::Boolean(true));
	this->infoSpace_->fireItemAvailable ("readiness", new xdata::Boolean(false));
}


xdaq::ApplicationContext* xdaq::ApplicationStubImpl::getContext() const
{
	return this->context_;
}

Logger & xdaq::ApplicationStubImpl::getLogger()
{
	return this->logger_;
}

xdaq::ApplicationDescriptor* xdaq::ApplicationStubImpl::getDescriptor() const
{
	return this->descriptor_;
}

xdata::InfoSpace* xdaq::ApplicationStubImpl::getInfoSpace()	 const
{
	return this->infoSpace_;
}




