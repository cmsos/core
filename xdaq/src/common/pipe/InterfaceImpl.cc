// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "xdaq/pipe/InterfaceImpl.h"

xdaq::pipe::Interface * xdaq::getPipeInterface()
{
        return xdaq::pipe::InterfaceImpl::instance();
}

// singleton static implementation
xdaq::pipe::InterfaceImpl * xdaq::pipe::InterfaceImpl::instance_ = 0;

xdaq::pipe::InterfaceImpl* xdaq::pipe::InterfaceImpl::instance()
{
        // Would actually need a Thread lock for making it threas safe

        if (instance_ == 0)
        {
                instance_ = new xdaq::pipe::InterfaceImpl();
        }
        return instance_;
}
// static initialization end

xdaq::pipe::InterfaceImpl::InterfaceImpl()
{
	std::string service = pipeServiceDispatcher_.getService();
        //std::cout << " does listener exists : " << pt::getPeerTransportAgent()->hasListener (service) << std::endl;
        if ( ! pt::getPeerTransportAgent()->hasListener (service) )
        {
                //std::cout << " adding listener : " << service << std::endl;
                pt::getPeerTransportAgent()->addListener(&pipeServiceDispatcher_);
        }
}

xdaq::pipe::Service * xdaq::pipe::InterfaceImpl::join(xdaq::Application * application, xdaq::pipe::ServiceListener * listener)
{

        //std::cout << "Going to join pipe member to member list" << std::endl;
        if ( members_.find(application) == members_.end() )
        {
                //std::cout << "Finally join pipe member to list" << std::endl;
                members_[application] = new xdaq::pipe::ServiceImpl(application,listener);
        }

        //std::cout << "Going to join pipe member to dispatcher std::list" << std::endl;
        pipeServiceDispatcher_.push_back(dynamic_cast<pt::pipe::ServiceListener*>(members_[application]));
        return members_[application];
}


void xdaq::pipe::InterfaceImpl::detach(xdaq::pipe::Service * service)
{
}

