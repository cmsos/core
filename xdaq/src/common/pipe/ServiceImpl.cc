// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#include "xdaq/pipe/ServiceImpl.h"
#include <memory>
#include <uuid/uuid.h>

#include "xdaq/pipe/Input.h"
#include "xdaq/pipe/Output.h"
#include "xdaq/pipe/ConnectionRequest.h"
#include "xdaq/pipe/EstablishedConnection.h"
#include "xdaq/pipe/ConnectionError.h"
#include "xdaq/pipe/ServiceListener.h"
#include "xdaq/pipe/BrokenPipe.h"

#include "xdaq/ApplicationDescriptor.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/Application.h"
#include "xdaq/Object.h"

#include  "pt/PeerTransportAgent.h"
#include  "pt/PeerTransportSender.h"
#include  "pt/PeerTransportReceiver.h"
#include  "pt/pipe/ServiceListener.h"
#include  "pt/pipe/ConnectionError.h"

#include "pt/pipe/Output.h"
#include "pt/pipe/OutputListener.h"
#include "pt/pipe/InputListener.h"
#include "pt/pipe/Service.h"

//// https://www.nextptr.com/tutorial/ta1227747841/the-stdshared_ptrvoid-as-arbitrary-userdata-pointer
xdaq::pipe::ServiceImpl::ServiceImpl(xdaq::Application * a, xdaq::pipe::ServiceListener * l): xdaq::Object(a) , listener_(l) 
{ 
}

void xdaq::pipe::ServiceImpl::connect(const xdaq::ApplicationDescriptor * from,  const xdaq::ApplicationDescriptor * to) 
{
 	 // Resolve network path 
	std::string networkName = this->getOwnerApplication()->getApplicationContext()->getRoutingTable()->getNetworkPath(from, to);
	const xdaq::Network * network =  this->getOwnerApplication()->getApplicationContext()->getNetGroup()->getNetwork(networkName);
	pt::Address::Reference localAddress = network->getAddress(from->getContextDescriptor());
	pt::Address::Reference remoteAddress = network->getAddress( to->getContextDescriptor() );	

	// Trigger a connection asynchronously 
	pt::pipe::Service* psi  = dynamic_cast<pt::pipe::Service*>(pt::getPeerTransportAgent()->getPeerTransport ( remoteAddress->getProtocol(), remoteAddress->getService(), pt::SenderReceiver  ));
	if ( psi == NULL )
	{
		XCEPT_RAISE(xdaq::exception::Exception, "cannot find peer transport for network (check for valid network name in XML statements)");
	}
	using PipeRequestContext = std::tuple<std::string, const xdaq::ApplicationDescriptor*, const xdaq::ApplicationDescriptor*>;	
	toolbox::net::UUID id;

	// create a Pipe Request Context
	std::shared_ptr<void> prc = std::make_shared<PipeRequestContext>(id.toString(),from, to);

	psi->connect( localAddress, remoteAddress, this,  prc );

	//requests_.insert( std::make_pair(id.toString(), prc ));
	
} 

void xdaq::pipe::ServiceImpl::actionPerformed(std::shared_ptr<pt::pipe::EstablishedConnection> pec) 
{
	using PipeRequestContext = std::tuple<std::string, const xdaq::ApplicationDescriptor*, const xdaq::ApplicationDescriptor*>;
	PipeRequestContext * prc  = (PipeRequestContext*)pec->getContext();

	const xdaq::ApplicationDescriptor * from = std::get<1>(*prc);
	const xdaq::ApplicationDescriptor * to = std::get<2>(*prc);
	xdaq::pipe::EstablishedConnection ec(from,to,pec);
	listener_->actionPerformed(ec);
}

void xdaq::pipe::ServiceImpl::actionPerformed(std::shared_ptr<pt::pipe::ConnectionError> pec) 
{
	using PipeRequestContext = std::tuple<std::string, const xdaq::ApplicationDescriptor*, const xdaq::ApplicationDescriptor*>;
	PipeRequestContext*  prc  = (PipeRequestContext*)pec->getContext();
	const xdaq::ApplicationDescriptor * from = std::get<1>(*prc);
	const xdaq::ApplicationDescriptor * to = std::get<2>(*prc);
	xcept::Exception e = pec->getException();
	xdaq::pipe::ConnectionError ce(from,to,e);
	listener_->actionPerformed(ce);

}

bool xdaq::pipe::ServiceImpl::actionPerformed(std::shared_ptr<pt::pipe::ConnectionRequest> pcr) 
{
        const xdaq::ApplicationDescriptor * from = 0;
        const xdaq::ApplicationDescriptor * to = 0;
        xdaq::pipe::ConnectionRequest cr(from,to,pcr);
	return listener_->actionPerformed(cr);

}


// Input/Output Pipe listener
void xdaq::pipe::ServiceImpl::actionPerformed(pt::pipe::UndeliverableBlock & event) 
{
	toolbox::mem::Reference * ref = event.getReference();
	ref->release();

}

void xdaq::pipe::ServiceImpl::actionPerformed(pt::pipe::BrokenPipe & bp) 
{
        const xdaq::ApplicationDescriptor * from = 0;
        const xdaq::ApplicationDescriptor * to = 0;
        xdaq::pipe::BrokenPipe event(from, to, bp.getPipe() );
        listener_->actionPerformed(event);
}

void xdaq::pipe::ServiceImpl::actionPerformed(pt::pipe::UnusedBlock & event) 
{
	toolbox::mem::Reference * ref = event.getReference();
	ref->release();

}

void xdaq::pipe::ServiceImpl::actionPerformed(pt::pipe::ConnectionClosedByPeer & ccbp) 
{
	//std::cout << " pt::pipe::ConnectionClosedByPeer action perfomed, forward to server application" << std::endl;
        const xdaq::ApplicationDescriptor * from = 0;
        const xdaq::ApplicationDescriptor * to = 0;
        xdaq::pipe::ConnectionClosedByPeer event(from, to, ccbp.getPipe() );
        listener_->actionPerformed(event);
}

xdaq::pipe::Output * xdaq::pipe::ServiceImpl::createOutputPipe(xdaq::pipe::EstablishedConnection & ec, size_t n)
{
        std::shared_ptr<pt::pipe::EstablishedConnection> pec = ec.getPEC();
        std::string protocol = pec->getPeerTransport()->getProtocol();
        pt::pipe::Service* pts  = dynamic_cast<pt::pipe::Service*>(pt::getPeerTransportAgent()->getPeerTransport ( protocol, "pipe", pt::SenderReceiver  ));
        xdaq::pipe::Output * opipe = new xdaq::pipe::Output(pts->createOutputPipe(pec, this, n), pts);
        return opipe;
}

xdaq::pipe::Output * xdaq::pipe::ServiceImpl::createOutputPipe(xdaq::pipe::EstablishedConnection & ec) 
{
	std::shared_ptr<pt::pipe::EstablishedConnection> pec = ec.getPEC();
	std::string protocol = pec->getPeerTransport()->getProtocol();
	pt::pipe::Service* pts  = dynamic_cast<pt::pipe::Service*>(pt::getPeerTransportAgent()->getPeerTransport ( protocol, "pipe", pt::SenderReceiver  ));
	xdaq::pipe::Output * opipe = new xdaq::pipe::Output(pts->createOutputPipe(pec, this ), pts);
	return opipe;
} 
 
xdaq::pipe::Input * xdaq::pipe::ServiceImpl::createInputPipe(xdaq::pipe::ConnectionRequest & cr, size_t n)
{
        std::shared_ptr<pt::pipe::ConnectionRequest> pcr = cr.getPCR();
        std::string protocol = pcr->getPeerTransport()->getProtocol();
        pt::pipe::Service* pts  = dynamic_cast<pt::pipe::Service*>(pt::getPeerTransportAgent()->getPeerTransport ( protocol, "pipe", pt::SenderReceiver  ));
        xdaq::pipe::Input * ipipe = new xdaq::pipe::Input(pts->createInputPipe(pcr, this, n), pts);
        return ipipe;

}

xdaq::pipe::Input * xdaq::pipe::ServiceImpl::createInputPipe(xdaq::pipe::ConnectionRequest & cr) 
{
	std::shared_ptr<pt::pipe::ConnectionRequest> pcr = cr.getPCR();
	std::string protocol = pcr->getPeerTransport()->getProtocol();
	pt::pipe::Service* pts  = dynamic_cast<pt::pipe::Service*>(pt::getPeerTransportAgent()->getPeerTransport ( protocol, "pipe", pt::SenderReceiver  ));
	xdaq::pipe::Input * ipipe = new xdaq::pipe::Input(pts->createInputPipe(pcr, this ), pts);
	return ipipe;

} 
	
void xdaq::pipe::ServiceImpl::destroyInputPipe(xdaq::pipe::Input * ip)  
{
	ip->getPeerTransportService()->destroyInputPipe(ip->getPipeHandle());
	delete ip;
} 

void xdaq::pipe::ServiceImpl::destroyOutputPipe(xdaq::pipe::Output * op) 
{
	op->getPeerTransportService()->destroyOutputPipe(op->getPipeHandle());
	delete op;
} 

