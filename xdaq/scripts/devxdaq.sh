#!/bin/bash
#-------------------------------------------------------------------------------
# Some useful shortcuts
#-------------------------------------------------------------------------------

prepend() {
  if [ ".${LD_LIBRARY_PATH}" == "." ] ; then
    export LD_LIBRARY_PATH=$1
  else
    export LD_LIBRARY_PATH=$1:${LD_LIBRARY_PATH}
  fi

}
#-------------------------------------------------------------------------------
if test ".$XDAQ_ROOT" = "."; then
  echo "Error: XDAQ_ROOT environment variable not set"
  exit 1
fi

if test ".$XDAQ_PLATFORM" = "."; then
  export XDAQ_PLATFORM=`uname -m`
  if test ".$XDAQ_PLATFORM" != ".x86_64"; then
    export XDAQ_PLATFORM=x86
  fi
  checkos=`$XDAQ_ROOT/config/checkos.sh`
  echo $checkos
  export XDAQ_PLATFORM=$XDAQ_PLATFORM"_"$checkos
  echo "Warning: PLATFORM env. variable not set, guessed to " ${XDAQ_PLATFORM}
fi

if test ".$XDAQ_OS" = "."; then
  export XDAQ_OS=`uname`
  if test ".$XDAQ_OS" = ".Linux"; then
    export XDAQ_OS=linux
  fi
  echo "Warning: OS env. variable not set, guessed to " ${XDAQ_OS}
fi

# Start with a clean slate
# export LD_LIBRARY_PATH="";

# Add the various paths
prepend "$XDAQ_ROOT/daq/extern/xerces/${XDAQ_PLATFORM}/lib"
prepend "./:$XDAQ_ROOT/daq/xdaq/lib/$XDAQ_OS/${XDAQ_PLATFORM}"
prepend "$XDAQ_ROOT/daq/xtuple/lib/$XDAQ_OS/${XDAQ_PLATFORM}"
prepend "$XDAQ_ROOT/daq/config/lib/$XDAQ_OS/${XDAQ_PLATFORM}"
prepend "$XDAQ_ROOT/daq/extern/asyncresolv/${XDAQ_PLATFORM}/lib"
prepend "$XDAQ_ROOT/daq/toolbox/lib/$XDAQ_OS/${XDAQ_PLATFORM}"
prepend "$XDAQ_ROOT/daq/xoap/lib/$XDAQ_OS/${XDAQ_PLATFORM}"
prepend "$XDAQ_ROOT/daq/pt/lib/$XDAQ_OS/${XDAQ_PLATFORM}"
prepend "$XDAQ_ROOT/daq/executive/lib/$XDAQ_OS/${XDAQ_PLATFORM}"
prepend "$XDAQ_ROOT/daq/hyperdaq/lib/$XDAQ_OS/${XDAQ_PLATFORM}"
prepend "$XDAQ_ROOT/daq/pt/fifo/lib/$XDAQ_OS/${XDAQ_PLATFORM}"
prepend "$XDAQ_ROOT/daq/pt/http/lib/$XDAQ_OS/${XDAQ_PLATFORM}"
prepend "$XDAQ_ROOT/daq/extern/log4cplus/${XDAQ_PLATFORM}/lib"
prepend "$XDAQ_ROOT/daq/extern/mimetic/${XDAQ_PLATFORM}/lib"
prepend "$XDAQ_ROOT/daq/log/udpappender/lib/$XDAQ_OS/${XDAQ_PLATFORM}"
prepend "$XDAQ_ROOT/daq/log/xmlappender/lib/$XDAQ_OS/${XDAQ_PLATFORM}"
prepend "$XDAQ_ROOT/daq/extern/cgicc/${XDAQ_PLATFORM}/lib"
prepend "$XDAQ_ROOT/daq/xdata/lib/$XDAQ_OS/${XDAQ_PLATFORM}"
prepend "$XDAQ_ROOT/daq/xcept/lib/$XDAQ_OS/${XDAQ_PLATFORM}"
prepend "$XDAQ_ROOT/daq/xgi/lib/$XDAQ_OS/${XDAQ_PLATFORM}"
prepend "$XDAQ_ROOT/daq/xrelay/lib/$XDAQ_OS/${XDAQ_PLATFORM}"

echo Run xdaq with options: $@
echo "LD_LIBRARY_PATH is " ${LD_LIBRARY_PATH}
exec ${XDAQ_ROOT}/daq/xdaq/bin/$XDAQ_OS/${XDAQ_PLATFORM}/xdaq.exe $@

