//
//  File Name:
//
//      RCInterface.h
//
//  File Type:
//
//      C++ Header file
//
//  Creative Authors:
//
//      Luciano B. ORSINI
//      CERN, EP Division
//      CMD group
//      CMS TriDAS
//      Compact Muon Solenoid - Trigger and Data Acquisition
//      Geneva, Switzerland
//      Tel: (+41) 22 76 71615 
//      E-mail:Luciano.Orsini@cern.ch
//      
//      
//      Johannes Gutleber
//      CERN, EP Division
//      CMD group
//      CMS TriDAS
//      Compact Muon Solenoid - Trigger and Data Acquisition
//      Geneva, Switzerland
//      Tel: (+41) 22 76 71536
//	E-mail:Johannes.Gutleber@cern.ch
//
//  Change History:
//
//      $Log: RCInterface.h,v $
//      Revision 1.4  1999/10/05 10:03:46  lorsini
//      Reference: LO
//      Software Change:
//      	Adde init method.
//
//      Revision 1.3  1999/08/13 10:28:25  lorsini
//      Reference: LO
//      Software Change:
//      	First release supporting linux/i686, sun4u/sparc, vxworks/mv2304
//
//      Revision 1.2  1999/08/03 13:37:18  lorsini
//      Reference: LO
//      Software Change:
//      	Corrected comment.
//
//      Revision 1.1  1999/08/03 08:55:41  lorsini
//      Reference: LO
//      Software Change:
//      	Source created.
//
//
//
//  Version Number
//
//      @(#)$Id: RCInterface.h,v 1.4 1999/10/05 10:03:46 lorsini Exp $
//
//

//  ------------------------------------------------------------------------
//  ------------------  Pre-processor Control Directives  ------------------
//  ------------------------------------------------------------------------
 
// The following pre-processor directives ensure that spurious errors will not
// be generated in the event that this header file is included several times.
 
#ifndef __RCInterface_h__
#define __RCInterface_h__

//  ------------------------------------------------------------------------ 
//  ------------------------  Include Header Files  ------------------------
//  ------------------------------------------------------------------------
 
// Include the following system header files:
 

  
// Include the following vxRU  header files:
 
#include "DAQBinding.h" 
 
//  ------------------------------------------------------------------------ 
//  ----------------------  Public Class Declarations  --------------------- 
//  ------------------------------------------------------------------------ 
//

//
//  Class:
//
//     RCInterface 
//
//  Superclasses:
//
//     NONE 
//
//
// Class Arguments
//
//
//  Description:
//
//  The interface for the Run Control. From this file the stubs and 
//  skeletons have to be derived using an agreed protocol.
// 
class RCInterface
{

//
//  Member Functions:
//

//
//      This class defines the following public member functions:
//
//	
	public:
 	 //
	 // str can be any character string
	 //
  	 virtual void config (char * str)   = 0;
	 virtual void start (char * str)    = 0;
	 virtual void stop (char * str)     = 0;
  	 virtual void shutdown (char * str) = 0;
	 virtual void action (char * str)   = 0; 
	 virtual void suspend (char * str)  = 0;
	 virtual void resume (char * str)   = 0;
	 virtual void load( char * str)     = 0;
	 virtual void init( char * str)     = 0;
};


//  ------------------------------------------------------------------------
//  ------------------  Pre-processor Control Directives  ------------------
//  ------------------------------------------------------------------------
 
// Terminate the #if ... #endif pre-processor control directive located at the
// start of this header file.
 
#endif /* ifndef  __RCInterface_h__ */

