//
//  File Name:
//
//      DAQBinding.h
//
//  File Type:
//
//      C++ Header file
//
//  Creative Authors:
//
//      Luciano B. ORSINI
//      CERN, EP Division
//      CMD group
//      CMS TriDAS
//      Compact Muon Solenoid - Trigger and Data Acquisition
//      Geneva, Switzerland
//      Tel: (+41) 22 76 71615 
//      E-mail:Luciano.Orsini@cern.ch
//      
//      
//      Johannes Gutleber
//      CERN, EP Division
//      CMD group
//      CMS TriDAS
//      Compact Muon Solenoid - Trigger and Data Acquisition
//      Geneva, Switzerland
//      Tel: (+41) 22 76 71536
//	E-mail:Johannes.Gutleber@cern.ch
//
//  Change History:
//
//      $Log: DAQBinding.h,v $
//      Revision 1.3  1999/11/13 18:24:51  gutleber
//      JG: Added a flag for allowing to retrieve full events using the profile
//
//      Revision 1.2  1999/08/26 07:37:26  meschi
//      Reference: EM, LO
//      Software Change:
//      	Added FN interface modules. Added new definition of FragmentData
//      	and FragmentDataSet. Extended FUInterface according FN vs FU IRS.
//
//      Revision 1.1  1999/07/30 16:12:24  lorsini
//      Reference: LORSINI
//      Software Change:
//      	Source Created.
//

//  Version Number
//
//      @(#)$Id: DAQBinding.h,v 1.3 1999/11/13 18:24:51 gutleber Exp $
//
//

//  ------------------------------------------------------------------------
//  ------------------  Pre-processor Control Directives  ------------------
//  ------------------------------------------------------------------------
 
// The following pre-processor directives ensure that spurious errors will not
// be generated in the event that this header file is included several times.
 
#ifndef __DAQBinding_h__
#define __DAQBinding_h__


//  ------------------------------------------------------------------------ 
//  ----------------------  Public Types Declarations  --------------------- 
//  ------------------------------------------------------------------------ 

//
//  Type: EventIdentifier
//
//
//  Description:
//

typedef unsigned long EventIdentifier;

//
//  Type: EventProfile
//
//
//  Description:
//

typedef unsigned long EventProfile;

#define FULLEVENT 0x0001

//
//  Type: FragmentSet
//
//
//  Description:
//

typedef  unsigned long FragmentSet;
  
//
//  Type: Address
//
//
//  Description:
//

struct DestinationCookie {
  unsigned long addr;	
  unsigned long cookie;
};


//
//  Type: FragmentIdentifier
//
//
//  Description:
//

typedef unsigned long FragmentIdentifier;

//
//  Type: FragmentData
//
//
//  Description:
//

struct FragmentData {
  unsigned long length;
  char     *    data;
};


//
//  Type: FragmentDataSet
//
//
//  Description:
//
 
struct FragmentDataSet {
  unsigned long     num;
  FragmentData ** fdata;
};
//
//  Type: UAddress
//
//
//  Description:
//

typedef unsigned long UAddress;

//  ------------------------------------------------------------------------
//  ------------------  Pre-processor Control Directives  ------------------
//  ------------------------------------------------------------------------
 
// Terminate the #if ... #endif pre-processor control directive located at the
// start of this header file.
 
#endif /* ifndef  __DAQBinding_h__ */
	
