//
//  File Name:
//
//      DAQProtocol.h
//
//  File Type:
//
//      C++ Header file
//
//  Creative Authors:
//
//      Luciano B. ORSINI
//      CERN, EP Division
//      CMD group
//      CMS TriDAS
//      Compact Muon Solenoid - Trigger and Data Acquisition
//      Geneva, Switzerland
//      Tel: (+41) 22 76 71615 
//      E-mail:Luciano.Orsini@cern.ch
//      
//      
//      Johannes Gutleber
//      CERN, EP Division
//      CMD group
//      CMS TriDAS
//      Compact Muon Solenoid - Trigger and Data Acquisition
//      Geneva, Switzerland
//      Tel: (+41) 22 76 71536
//	E-mail:Johannes.Gutleber@cern.ch
//
//  Change History:
//
//      $Log: DAQProtocol.h,v $
//      Revision 1.6  2000/05/24 13:02:07  lorsini
//      Changed DAQProtocol short to long format.
//
//      Revision 1.5  1999/07/30 16:10:24  lorsini
//      Reference: LO
//      Software Change:
//      	Clean up.
//
//      Revision 1.4  1999/07/16 09:20:17  gutleber
//      Reference: LO, JG
//      Software Change:
//      	New definitions.
//
//      Revision 1.3  1999/07/09 14:35:43  gutleber
//      Reference: JG Shrunk the protocol header to one 32 bit longword.
//        The length is given in longwords including the length information.
//        So to get the total length to read, one would have to read (length - sizeof(unsigned long))
//        Protocol is encoded in msgType and can be OR'ed with the type.
//
//      Revision 1.2  1999/07/09 14:14:51  gutleber
//      Reference: JG Added constants for all types to be used in the DAQ system
//        Modifications for the protocol header as agreed in the meeting on
//        July 8th, 1999.
//        Constant definitions introduced by JG, TBC.
//
//      Revision 1.1  1999/07/08 10:04:29  lorsini
//      Reference: LO
//      Software Change:
//      	Source created.
//
//
//
//  Version Number
//
//      @(#)$Id: DAQProtocol.h,v 1.6 2000/05/24 13:02:07 lorsini Exp $
//
//

//  ------------------------------------------------------------------------
//  ------------------  Pre-processor Control Directives  ------------------
//  ------------------------------------------------------------------------
 
// The following pre-processor directives ensure that spurious errors will not
// be generated in the event that this header file is included several times.
 
#ifndef __DAQProtocol_h__
#define __DAQProtocol_h__

//  ------------------------------------------------------------------------ 
//  ----------------------  Public Constant Declarations  --------------------- 
//  ------------------------------------------------------------------------ 
//

//
//  Constant: Protocol Identifiers
//
//  Description:
//        Numerical Identifiers for the protocols used in the DAQ 
//

const unsigned long  PROT_DUMMY        = 0x0000; // no protocol
const unsigned long PROT_DEMONSTRATOR = 0x2000; // demonstrator protocol
const unsigned long XMI_PROTOCOL      = 0x1000; // XMI reserved codes from 0x1000 to 0x1fff


//
//  Constant: Message Type Identifiers
//
//  Description:
//        Identifiers for the message types in the system
//
const char UNUSED    = 0x00; // if not used
const char TEST_MSG  = 0x01;
const char RC_MSG    = 0x02; // a run control message
const char DATA_MSG  = 0x04; // a data message
const char FAST_MSG  = 0x08; // a fast control message


//
//  Constant: Method Type Identifiers
//
//  Description:
//        Identifiers for the methods as defined in the TriDAS IRS
//



//  ------------------------------------------------------------------------ 
//  ----------------------  Public Type Declarations  --------------------- 
//  ------------------------------------------------------------------------ 
//

//
//  Type: DAQProtocolHeader
//
//  Description:
//
// The DAQProtocol belongs to a mapping of the generic DAQ protocol interface. 
// It contains all information that has to be known in order to process 
// messages according to the agreed protocol. This mapping contains the DAQ 
// protocol information needed when using CERN/CMD compliant protocols.


typedef struct {
  unsigned long length;         // total length of the message in uint32
  unsigned long protocol;       // numerical identifier for protocol
} DAQProtocolHeader;


//  ------------------------------------------------------------------------
//  ------------------  Pre-processor Control Directives  ------------------
//  ------------------------------------------------------------------------
 
// Terminate the #if ... #endif pre-processor control directive located at the
// start of this header file.
 
#endif /* ifndef  __DAQProtocol_h__ */

