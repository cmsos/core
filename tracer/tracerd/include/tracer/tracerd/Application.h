// $Id$

/*************************************************************************
 * XDAQ Tracer Daemon                      								 *
 * Copyright (C) 2000-2013, CERN.			                  			 *
 * All rights reserved.                                                  *
 * Authors: A. Forrest and L. Orsini				 					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                       		 *
 * For the list of contributors see CREDITS.   			         		 *
 *************************************************************************/

#ifndef _tracer_tracerd_Application_h_
#define _tracer_tracerd_Application_h_

#include "xdaq/Application.h"
#include "xgi/framework/UIManager.h"

#include "toolbox/ActionListener.h"
#include "xdata/ActionListener.h"
#include "toolbox/task/TimerListener.h"
#include "toolbox/task/WorkLoop.h"

#include "xgi/Method.h"
#include "xgi/Output.h"
#include "xgi/exception/Exception.h"

#include "b2in/nub/exception/Exception.h"

#include "xdata/String.h"
#include "xdata/Boolean.h"
#include "xdata/UnsignedInteger32.h"

#include "tracer/tracerd/exception/Exception.h"

#include <fcntl.h>
#include <sys/stat.h>
#include <mqueue.h>

#include "eventing/api/Member.h"

namespace tracer
{
	namespace tracerd
	{
		class Application : public xdaq::Application, public xgi::framework::UIManager, public xdata::ActionListener, public toolbox::task::TimerListener, public eventing::api::Member
		{
			public:

				XDAQ_INSTANTIATOR();

				Application (xdaq::ApplicationStub* s) ;
				~Application ();

				void actionPerformed (xdata::Event& e);

				void timeExpired (toolbox::task::TimerEvent& e);

				void Default (xgi::Input * in, xgi::Output * out) ;
				//void unlinkmq (xgi::Input * in, xgi::Output * out) ;

				void StatisticsTabPage (xgi::Output * out);

				void publishEvent (xdata::Properties& pList) ;

				bool process (toolbox::task::WorkLoop * w1);

			protected:

				toolbox::task::ActionSignature* process_;

				//////////

				xdata::UnsignedInteger totalCounter_;
				xdata::UnsignedInteger publishedCounter_;
				xdata::UnsignedInteger lostCounter_;
				xdata::UnsignedInteger lostNoNetworkCounter_;
				xdata::UnsignedInteger lostNotEstablishedCounter_;

				xdata::UnsignedInteger maxIPCQueueSize_;
				xdata::UnsignedInteger maxIPCMessageSize_;

				xdata::Boolean resetOnStart_;

				mqd_t daemonQueue_;

				xdata::String outputBus_;

		};
	}
}

#endif

