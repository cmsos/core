/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: R. Moser and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _tracerd_version_h_
#define _tracerd_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define CORE_TRACERD_VERSION_MAJOR 2
#define CORE_TRACERD_VERSION_MINOR 0
#define CORE_TRACERD_VERSION_PATCH 0
// If any previous versions available E.g. #define CORE_TRACERD_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define CORE_TRACERD_PREVIOUS_VERSIONS

//
// Template macros
//
#define CORE_TRACERD_VERSION_CODE PACKAGE_VERSION_CODE(CORE_TRACERD_VERSION_MAJOR,CORE_TRACERD_VERSION_MINOR,CORE_TRACERD_VERSION_PATCH)
#ifndef CORE_TRACERD_PREVIOUS_VERSIONS
#define CORE_TRACERD_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(CORE_TRACERD_VERSION_MAJOR,CORE_TRACERD_VERSION_MINOR,CORE_TRACERD_VERSION_PATCH)
#else 
#define CORE_TRACERD_FULL_VERSION_LIST  CORE_TRACERD_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(CORE_TRACERD_VERSION_MAJOR,CORE_TRACERD_VERSION_MINOR,CORE_TRACERD_VERSION_PATCH)
#endif 

namespace tracerd
{
	const std::string project = "core";
	const std::string package = "tracerd";
	const std::string versions = CORE_TRACERD_FULL_VERSION_LIST;
	const std::string summary = "Publish XDAQ events";
	const std::string description = "Publish XDAQ events";
	const std::string authors = "Andy Forrest, Luciano Orsini";
	const std::string link = "http://xdaq.web.cern.ch/";
	config::PackageInfo getPackageInfo ();
	void checkPackageDependencies () ;
	std::set<std::string, std::less<std::string> > getPackageDependencies ();
}

#endif

