/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: R. Moser and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _tracer_probe_version_h_
#define _tracer_probe_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define CORE_TRACERPROBE_VERSION_MAJOR 2
#define CORE_TRACERPROBE_VERSION_MINOR 1
#define CORE_TRACERPROBE_VERSION_PATCH 0
// If any previous versions available E.g. #define CORE_TRACERPROBE_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef CORE_TRACERPROBE_PREVIOUS_VERSIONS 

//
// Template macros
//
#define CORE_TRACERPROBE_VERSION_CODE PACKAGE_VERSION_CODE(CORE_TRACERPROBE_VERSION_MAJOR,CORE_TRACERPROBE_VERSION_MINOR,CORE_TRACERPROBE_VERSION_PATCH)
#ifndef CORE_TRACERPROBE_PREVIOUS_VERSIONS
#define CORE_TRACERPROBE_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(CORE_TRACERPROBE_VERSION_MAJOR,CORE_TRACERPROBE_VERSION_MINOR,CORE_TRACERPROBE_VERSION_PATCH)
#else 
#define CORE_TRACERPROBE_FULL_VERSION_LIST  CORE_TRACERPROBE_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(CORE_TRACERPROBE_VERSION_MAJOR,CORE_TRACERPROBE_VERSION_MINOR,CORE_TRACERPROBE_VERSION_PATCH)
#endif 

namespace tracerprobe
{
	const std::string project = "core";
	const std::string package = "tracerprobe";
	const std::string versions = CORE_TRACERPROBE_FULL_VERSION_LIST;
	const std::string summary = "Publish XDAQ events";
	const std::string description = "Publish XDAQ events";
	const std::string authors = "Andy Forrest, Luciano Orsini";
	const std::string link = "http://xdaq.web.cern.ch/";
	config::PackageInfo getPackageInfo ();
	void checkPackageDependencies () ;
	std::set<std::string, std::less<std::string> > getPackageDependencies ();
}

#endif

