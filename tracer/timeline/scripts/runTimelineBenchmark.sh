#!/bin/sh

url="http://$1:$2/urn:xdaq-application:lid=37/runBenchmark?n=$3&type=profile"
echo $url 
curl $url

startport=1972

for (( i = 0 ; i < $3; i++ ))
do
	
	p=$((startport+i))
	./startXDAQExecutive srv-c2d06-18.cms 9999 10 $p $4
done
