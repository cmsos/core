function consoleDataReceived(xmlhttp)
{
	var data = JSON.parse(xmlhttp.responseText);

	var newData = "";
	
	for (var i = 0; i < data.messages.length; i++) 
	{
		var newRow = '<tr><td class="xdaq-console-row"><a href="' + data.messages[i].src + '" target=\"_blank\">' + data.messages[i].src + '</a> ' + data.messages[i].id + '<br />' + data.messages[i].msg + "</td></tr>";
		newData = newData + newRow;
	}
	
	return newData;
}
													