// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors:  P. Roberts and D. Simelevicius                              *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _xmem_probe_version_h_
#define _xmem_probe_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define CORE_XMEMPROBE_VERSION_MAJOR 1
#define CORE_XMEMPROBE_VERSION_MINOR 6
#define CORE_XMEMPROBE_VERSION_PATCH 0
// If any previous versions available E.g. #define CORE_XMEMPROBE_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef CORE_XMEMPROBE_PREVIOUS_VERSIONS


//
// Template macros
//
#define CORE_XMEMPROBE_VERSION_CODE PACKAGE_VERSION_CODE(CORE_XMEMPROBE_VERSION_MAJOR,CORE_XMEMPROBE_VERSION_MINOR,CORE_XMEMPROBE_VERSION_PATCH)
#ifndef CORE_XMEMPROBE_PREVIOUS_VERSIONS
#define CORE_XMEMPROBE_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(CORE_XMEMPROBE_VERSION_MAJOR,CORE_XMEMPROBE_VERSION_MINOR,CORE_XMEMPROBE_VERSION_PATCH)
#else 
#define CORE_XMEMPROBE_FULL_VERSION_LIST  CORE_XMEMPROBE_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(CORE_XMEMPROBE_VERSION_MAJOR,CORE_XMEMPROBE_VERSION_MINOR,CORE_XMEMPROBE_VERSION_PATCH)
#endif 

namespace xmemprobe 
{
	const std::string project = "core";
	const std::string package = "xmemprobe";
   	const std::string versions = CORE_XMEMPROBE_FULL_VERSION_LIST;
	const std::string summary = "XDAQ memory pools analyzer";
	const std::string description = "";
	const std::string authors = "Matthew Bowen, Penelope Roberts, Dainius Simelevicius";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/xdaq";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies();
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
