// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2014, CERN.			                			 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, A. Forrest										 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                        	 *
 * For the list of contributors see CREDITS.   			        		 *
 *************************************************************************/

#ifndef _eventing_api_Subscriber_h_
#define _eventing_api_Subscriber_h_

#include "xdaq/Object.h"

#include "toolbox/mem/Reference.h"

#include "xdata/Properties.h"

#include "eventing/api/exception/Exception.h"

namespace eventing
{
	namespace core
	{

		class SubscriberInterface;

	}
	namespace api
	{
		class Subscriber : public xdaq::Object
		{
			public:

				Subscriber (xdaq::Application * owner) ;
				Subscriber (xdaq::Application * owner, eventing::core::SubscriberInterface * subscriber) ;

				void subscribe (const std::string & topic) ;

				void unsubscribe (const std::string & topic) ;

			protected:
				Subscriber (const Subscriber& other) {};
				Subscriber() {};

				eventing::core::SubscriberInterface * subscriber_;
		};
	}
}

#endif
