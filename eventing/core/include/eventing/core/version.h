// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2021, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, L. Orsini and D. Simelevicius                   *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

//
// Version definition for MStreamIO
//
#ifndef _eventing_core_Version_h_
#define _eventing_core_Version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define CORE_EVENTINGCORE_VERSION_MAJOR 5
#define CORE_EVENTINGCORE_VERSION_MINOR 4
#define CORE_EVENTINGCORE_VERSION_PATCH 0
// If any previous versions available E.g. #define CORE_EVENTINGCORE_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define CORE_EVENTINGCORE_PREVIOUS_VERSIONS "5.1.0,5.1.1,5.1.2,5.2.0,5.3.0"


//
// Template macros
//
#define CORE_EVENTINGCORE_VERSION_CODE PACKAGE_VERSION_CODE(CORE_EVENTINGCORE_VERSION_MAJOR,CORE_EVENTINGCORE_VERSION_MINOR,CORE_EVENTINGCORE_VERSION_PATCH)
#ifndef CORE_EVENTINGCORE_PREVIOUS_VERSIONS
#define CORE_EVENTINGCORE_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(CORE_EVENTINGCORE_VERSION_MAJOR,CORE_EVENTINGCORE_VERSION_MINOR,CORE_EVENTINGCORE_VERSION_PATCH)
#else 
#define CORE_EVENTINGCORE_FULL_VERSION_LIST  CORE_EVENTINGCORE_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(CORE_EVENTINGCORE_VERSION_MAJOR,CORE_EVENTINGCORE_VERSION_MINOR,CORE_EVENTINGCORE_VERSION_PATCH)
#endif 

namespace eventingcore
{
	const std::string project = "core";
	const std::string package  =  "eventingcore";
	const std::string versions = CORE_EVENTINGCORE_FULL_VERSION_LIST;
	const std::string summary = "Publisher/Subscriber XDAQ core applications";
	const std::string description = "";
	const std::string authors = "Luciano Orsini, Andrea Petrucci, Andy Forrest";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/Power_Pack";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
