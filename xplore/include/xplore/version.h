// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2017, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, L. Orsini and D. Simelevicius                   *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _xplore_version_h_
#define _xplore_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define CORE_XPLORE_VERSION_MAJOR 2
#define CORE_XPLORE_VERSION_MINOR 1
#define CORE_XPLORE_VERSION_PATCH 1
// If any previous versions available E.g. #define CORE_XPLORE_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define CORE_XPLORE_PREVIOUS_VERSIONS "2.0.0,2.0.1,2.0.2,2.0.3,2.1.0"


//
// Template macros
//
#define CORE_XPLORE_VERSION_CODE PACKAGE_VERSION_CODE(CORE_XPLORE_VERSION_MAJOR,CORE_XPLORE_VERSION_MINOR,CORE_XPLORE_VERSION_PATCH)
#ifndef CORE_XPLORE_PREVIOUS_VERSIONS
#define CORE_XPLORE_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(CORE_XPLORE_VERSION_MAJOR,CORE_XPLORE_VERSION_MINOR,CORE_XPLORE_VERSION_PATCH)
#else 
#define CORE_XPLORE_FULL_VERSION_LIST  CORE_XPLORE_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(CORE_XPLORE_VERSION_MAJOR,CORE_XPLORE_VERSION_MINOR,CORE_XPLORE_VERSION_PATCH)
#endif 
namespace xplore
{
	const std::string project = "core";
	const std::string package  =  "xplore";
	const std::string versions = CORE_XPLORE_FULL_VERSION_LIST;
	const std::string summary = "Service discovery explorer and retrieval services";
	const std::string description = "";
	const std::string authors = "Johannes Gutleber, Luciano Orsini, Andrew Forrest, Dainius Simelevicius";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/Xplore";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
