// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _xploreutils_version_h_
#define _xploreutils_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define CORE_XPLOREUTILS_VERSION_MAJOR 1
#define CORE_XPLOREUTILS_VERSION_MINOR 10
#define CORE_XPLOREUTILS_VERSION_PATCH 2
// If any previous versions available E.g. #define CORE_XPLOREUTILS_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define CORE_XPLOREUTILS_PREVIOUS_VERSIONS "1.10.0,1.10.1"


//
// Template macros
//
#define CORE_XPLOREUTILS_VERSION_CODE PACKAGE_VERSION_CODE(CORE_XPLOREUTILS_VERSION_MAJOR,CORE_XPLOREUTILS_VERSION_MINOR,CORE_XPLOREUTILS_VERSION_PATCH)
#ifndef CORE_XPLOREUTILS_PREVIOUS_VERSIONS
#define CORE_XPLOREUTILS_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(CORE_XPLOREUTILS_VERSION_MAJOR,CORE_XPLOREUTILS_VERSION_MINOR,CORE_XPLOREUTILS_VERSION_PATCH)
#else 
#define CORE_XPLOREUTILS_FULL_VERSION_LIST  CORE_XPLOREUTILS_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(CORE_XPLOREUTILS_VERSION_MAJOR,CORE_XPLOREUTILS_VERSION_MINOR,CORE_XPLOREUTILS_VERSION_PATCH)
#endif 

namespace xploreutils
{
	const std::string project = "core";
	const std::string package  =  "xploreutils";
   	const std::string versions = CORE_XPLOREUTILS_FULL_VERSION_LIST;
	const std::string description = "";
	const std::string authors = "Johannes Gutleber, Luciano Orsini";
	const std::string summary = "Xplore support interfaces";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/I2O_Messaging";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
