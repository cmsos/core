function xploreSearch()
{
	//console.log("Searching...");
	
	var baseurl = $("#xdaq-xplore").attr("data-xplore-callback");

	$(".xdaq-ajax-loader").addClass("xdaq-ajax-loading");

	var url = baseurl + '/doaction?zone=' + $("#zone").val() + '&service=' + $("#service").val() + '&filter=' + $("#filterInput").val();
	
	var options = {
		url: url,
		type: "GET",
		error: function (xhr, textStatus, errorThrown) {
			$(".xdaq-ajax-loader").removeClass("xdaq-ajax-loading");
			console.error(xhr.status);
			console.error(errorThrown);
		}
	};
	
	xdaqAJAX(options, function (data, textStatus, xhr) {
		var data = JSON.parse(xhr.responseText);

		var newData = "";
		
		for (var i = 0; i < data.messages.length; i++) 
		{
			var url = "'" + baseurl + '/display?category=properties&url=' + data.messages[i].url + "'";
			newData = newData + '<tr><td>' + data.messages[i].display + '</td><td><a onclick="xploreDisplay(' + url + ');">View</a></td></tr>';
		}
		
		$("#xdaq-xplore-search-results tbody").first().html(newData);
		$(".xdaq-ajax-loader").removeClass("xdaq-ajax-loading");
	});
}

function xploreDisplay(url)
{
	//console.log("Displaying " + url);
	
	var options = {
		url: url,
		type: "GET"
	};
	
	xdaqAJAX(options, function (data, textStatus, xhr) {
		$("#xdaq-xplore-display-results tbody").html(xhr.responseText);
	});
}