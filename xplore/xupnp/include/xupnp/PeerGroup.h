
/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, G. Lo Presti and L. Orsini                      *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

#ifndef _xplore_xupnp_PeerGroup_h
#define _xplore_xupnp_PeerGroup_h

#include <string>

#include "xplore/PeerGroup.h"
#include "xupnp/DiscoveryService.h"
#include "xupnp/Notifier.h"

#include "toolbox/mem/CountingPtr.h"
#include "toolbox/mem/ThreadSafeReferenceCount.h"
#include "toolbox/mem/StandardObjectPolicy.h"

namespace xplore {
namespace xupnp {

//! This class represents a Jxta Peer Group.
//! It provides all the services associated with a Jxta peer.
//! The Jxta NetPeerGroup is a singleton istance of this class.
class PeerGroup: public xplore::PeerGroup
{
	public:
    typedef toolbox::mem::CountingPtr<PeerGroup, toolbox::mem::ThreadSafeReferenceCount, toolbox::mem::StandardObjectPolicy> Reference;

	PeerGroup(const std::string& name, xplore::xupnp::Notifier *notifier);

	~PeerGroup();

	//! Returns the local peer name.
	std::string getPeerName();

	//! Returns the local peer ID as string.
	std::string getPeerID();

	//! Returns the peer group name.
	std::string getPeerGroupName();

	//! Returns the peer group ID as string.
	std::string getPeerGroupID();

	//! Returns the Discovery Service associated with the PeerGroup.
	/*!
		To get the DiscoveryService that is associated with the default NetPeerGroup,
		retrieve first the NetPeerGroup and get the DiscoveryService from it
	*/
	xplore::DiscoveryService* getDiscoveryService();

	protected:

	std::string name_; // name/id of the peer group
	std::string id_; // local id/name of the PeerGroup Device
	xplore::xupnp::DiscoveryService* discoveryService_; // the discovery service associated with this group
};


}
}

#endif
