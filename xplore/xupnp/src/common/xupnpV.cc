// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
 
#include "toolboxV.h"
#include "xupnp/xupnpV.h"

GETPACKAGEINFO(xupnp)

void xupnp::checkPackageDependencies() 
{
        CHECKDEPENDENCY(toolbox)
}

std::set<std::string, std::less<std::string> > xupnp::getPackageDependencies()
{
    std::set<std::string, std::less<std::string> > dependencies;
    ADDDEPENDENCY(dependencies,toolbox);
    return dependencies;

}	
	
