// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _xaccess_Application_h_
#define _xaccess_Application_h_

#include "xdaq/Application.h"
#include "xgi/framework/UIManager.h"

#include <map>

#include "xdata/String.h"
#include "xdata/ActionListener.h"
#include "xgi/Utils.h"
#include "xgi/Method.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"
#include "cgicc/HTTPResponseHeader.h"
#include "cgicc/HTMLDoctype.h"

#include "pt/SecurityPolicy.h"

namespace xaccess {

class Application: public xdaq::Application, public xgi::framework::UIManager, public xdata::ActionListener
{	
	public:
		
	XDAQ_INSTANTIATOR();
	
	Application(xdaq::ApplicationStub * s) ;
	
	//! Default web page displayed when showing the application
	//
	void Default(xgi::Input * in, xgi::Output * out ) ;
	
	//! Implementation of interface from xdata::ActionListener
	//
	void actionPerformed(xdata::Event& e);
	
	protected:
	
	xdata::String htaccess_;	
};
}
#endif
