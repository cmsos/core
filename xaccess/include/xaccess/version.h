// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _xaccess_version_h_
#define _xaccess_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define CORE_XACCESS_VERSION_MAJOR 2
#define CORE_XACCESS_VERSION_MINOR 0
#define CORE_XACCESS_VERSION_PATCH 1
// If any previous versions available E.g. #define CORE_XACCESS_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef CORE_XACCESS_PREVIOUS_VERSIONS 


//
// Template macros
//
#define CORE_XACCESS_VERSION_CODE PACKAGE_VERSION_CODE(CORE_XACCESS_VERSION_MAJOR,CORE_XACCESS_VERSION_MINOR,CORE_XACCESS_VERSION_PATCH)
#ifndef CORE_XACCESS_PREVIOUS_VERSIONS
#define CORE_XACCESS_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(CORE_XACCESS_VERSION_MAJOR,CORE_XACCESS_VERSION_MINOR,CORE_XACCESS_VERSION_PATCH)
#else 
#define CORE_XACCESS_FULL_VERSION_LIST  CORE_XACCESS_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(CORE_XACCESS_VERSION_MAJOR,CORE_XACCESS_VERSION_MINOR,CORE_XACCESS_VERSION_PATCH)
#endif 

namespace xaccess 
{
	const std::string project = "core";
	const std::string package  =  "xaccess";
   	const std::string versions = CORE_XACCESS_FULL_VERSION_LIST;
	const std::string description = "";
	const std::string authors = "Johannes Gutleber, Luciano Orsini";
	const std::string summary = "User authentication/authorization agent";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/xdaq";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
