#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/dom/DOM.hpp>

#include <string>
#include <iostream>
#include <fstream>

#include <malloc.h>
#include <stdio.h>
#include <stdlib.h>
#include "toolbox/task/EventReference.h"
#include "xoap/Event.h"
#include "xoap/SOAPMessage.h"
#include "xoap/SOAPPart.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPBodyElement.h"
#include "xoap/SOAPName.h"
#include "xoap/AttachmentPart.h"
#include "xoap/SOAPAllocator.h"
#include "xoap/MessageFactory.h"

void printmeminfo(void)
{
  printf("arena: %d\n",mallinfo().arena);
  printf("ordblks: %d\n",mallinfo().ordblks);
  printf("max total allocated space: %d\n",mallinfo().usmblks);
  printf("total allocated space: %d\n",mallinfo().uordblks);
  printf("total free space: %d\n",mallinfo().fordblks);
}


int main (int argc, char** argv)
{
	XMLPlatformUtils::Initialize();
	std::string buffer = "<SOAP-ENV:Envelope SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\" ";
	buffer += "xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" ";
	buffer += "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" ";
	buffer += "xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\">";
	buffer += "<SOAP-ENV:Header>";
	buffer += "</SOAP-ENV:Header>";
	buffer += "<SOAP-ENV:Body>";
	buffer += "<psx:dpGet xmlns:psx=\"http://xdaq.cern.ch/xdaq/xsd/2006/psx-pvss-10.xsd\">";
	buffer += "<psx:dp name=\"MyRawHtChannel1.actualVoltate:_online.._value\"/>";
	buffer += "<psx:dp name=\"MyRawHtChannel2.actualVoltate:_online.._value\"/>";
	buffer += "<psx:dp name=\"MyRawHtChannel3.actualVoltate:_online.._value\"/>";
	buffer += "<psx:dp name=\"MyRawHtChannel4.actualVoltate:_online.._value\"/>";
	buffer += "<psx:dp name=\"MyRawHtChannel5.actualVoltate:_online.._value\"/>";
	buffer += "<psx:dp name=\"MyRawHtChannel6.actualVoltate:_online.._value\"/>";
	buffer += "<psx:dp name=\"MyRawHtChannel7.actualVoltate:_online.._value\"/>";
	buffer += "<psx:dp name=\"MyRawHtChannel8.actualVoltate:_online.._value\"/>";
	buffer += "<psx:dp name=\"MyRawHtChannel9.actualVoltate:_online.._value\"/>";
	buffer += "<psx:dp name=\"MyRawHtChannel10.actualVoltate:_online.._value\"/>";
	buffer += "<psx:dp name=\"MyRawHtChannel11.actualVoltate:_online.._value\"/>";
	buffer += "<psx:dp name=\"MyRawHtChannel12.actualVoltate:_online.._value\"/>";
	buffer += "<psx:dp name=\"MyRawHtChannel13.actualVoltate:_online.._value\"/>";
	buffer += "<psx:dp name=\"MyRawHtChannel2.actualVoltate:_online.._value\"/>";
	buffer += "<psx:dp name=\"MyRawHtChannel2.actualVoltate:_online.._value\"/>";
	buffer += "<psx:dp name=\"MyRawHtChannel2.actualVoltate:_online.._value\"/>";
	buffer += "<psx:dp name=\"MyRawHtChannel2.actualVoltate:_online.._value\"/>";
	buffer += "<psx:dp name=\"MyRawHtChannel2.actualVoltate:_online.._value\"/>";
	buffer += "<psx:dp name=\"MyRawHtChannel2.actualVoltate:_online.._value\"/>";

	buffer += "</psx:dpGet>";
	buffer += "</SOAP-ENV:Body>";
	buffer += "</SOAP-ENV:Envelope>";
	
	std::vector<toolbox::task::EventReference> v;
	
	std::cout << "Allocating..." << std::endl;
	
	size_t old = mallinfo().uordblks;
	
	for (size_t i = 0; i < 1000; ++i)
	{	
        	xoap::MessageReference m = xoap::createMessage((char*) buffer.c_str(), buffer.size());
		toolbox::task::EventReference e(new xoap::Event(m,""));
		std::cout << "MSG size: " << buffer.size() << ", allocated: " << mallinfo().uordblks << ", delta: " << mallinfo().uordblks - old << std::endl;
		old = mallinfo().uordblks ;

		v.push_back(e);
		//std::cout << "message: " << i << std::endl;
		//getchar();
	}
	
	std::cout << "Releasing..." << std::endl;
	
	getchar();
	
	while (v.size() > 0)
	{
		v.pop_back();
		std::cout << "allocated: " << mallinfo().uordblks << std::endl;
		std::cout << "message: " << v.size() << std::endl;
		// getchar();
	}
		
	return 0;
}

