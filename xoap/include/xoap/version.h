// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _xoap_version_h_
#define _xoap_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define CORE_XOAP_VERSION_MAJOR 6
#define CORE_XOAP_VERSION_MINOR 3
#define CORE_XOAP_VERSION_PATCH 2
// If any previous versions available E.g. #define CORE_XOAP_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define CORE_XOAP_PREVIOUS_VERSIONS "6.2.0,6.3.0,6.3.1"


//
// Template macros
//
#define CORE_XOAP_VERSION_CODE PACKAGE_VERSION_CODE(CORE_XOAP_VERSION_MAJOR,CORE_XOAP_VERSION_MINOR,CORE_XOAP_VERSION_PATCH)
#ifndef CORE_XOAP_PREVIOUS_VERSIONS
#define CORE_XOAP_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(CORE_XOAP_VERSION_MAJOR,CORE_XOAP_VERSION_MINOR,CORE_XOAP_VERSION_PATCH)
#else 
#define CORE_XOAP_FULL_VERSION_LIST  CORE_XOAP_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(CORE_XOAP_VERSION_MAJOR,CORE_XOAP_VERSION_MINOR,CORE_XOAP_VERSION_PATCH)
#endif 

namespace xoap 
{
	const std::string project = "core";
	const std::string package  =  "xoap";
   	const std::string versions = CORE_XOAP_FULL_VERSION_LIST;
	const std::string authors = "Johannes Gutleber, Luciano Orsini";
	const std::string summary = "SOAP C++ programming package based on Xerces 3";
	const std::string description = "";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/xoap";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
