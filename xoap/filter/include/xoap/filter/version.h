// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _xoap_filter_version_h_
#define _xoap_filter_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define CORE_XOAPFILTER_VERSION_MAJOR 4
#define CORE_XOAPFILTER_VERSION_MINOR 2
#define CORE_XOAPFILTER_VERSION_PATCH 0
// If any previous versions available E.g. #define CORE_XOAPFILTER_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef CORE_XOAPFILTER_PREVIOUS_VERSIONS 


//
// Template macros
//
#define CORE_XOAPFILTER_VERSION_CODE PACKAGE_VERSION_CODE(CORE_XOAPFILTER_VERSION_MAJOR,CORE_XOAPFILTER_VERSION_MINOR,CORE_XOAPFILTER_VERSION_PATCH)
#ifndef CORE_XOAPFILTER_PREVIOUS_VERSIONS
#define CORE_XOAPFILTER_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(CORE_XOAPFILTER_VERSION_MAJOR,CORE_XOAPFILTER_VERSION_MINOR,CORE_XOAPFILTER_VERSION_PATCH)
#else 
#define CORE_XOAPFILTER_FULL_VERSION_LIST  CORE_XOAPFILTER_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(CORE_XOAPFILTER_VERSION_MAJOR,CORE_XOAPFILTER_VERSION_MINOR,CORE_XOAPFILTER_VERSION_PATCH)
#endif 
namespace xoapfilter 
{
	const std::string project = "core";
	const std::string package  =  "xoapfilter";
	const std::string versions = CORE_XOAPFILTER_FULL_VERSION_LIST;
	const std::string summary = "XOAP XPath filter library";
	const std::string description = "";
	const std::string authors = "Johannes Gutleber, Luciano Orsini";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/Core_Tools";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
