#include <iostream>
#include <string>
#include "xoap/MessageFactory.h"
#include "xoap/MessageReference.h"
#include "xoap/exception/Exception.h"
#include "xcept/tools.h"
#include "xoap/filter/MessageFilter.h"

int main (int argc, char** argv)
{
	if (argc < 3)
	{
		std::cout << argv[0] << " [file] [filter]" << std::endl;
		return 1;
	}

	XMLPlatformUtils::Initialize();

	try
	{
		xoap::MessageReference msg = xoap::createMessage(argv[1]);
			
		std::cout << "-- Message ---------------------------------------------------" << std::endl;
		msg->writeTo(std::cout);
		std::cout << std::endl << "--------------------------------------------------------------" << std::endl;

		xoap::filter::MessageFilter filter(argv[2]);

		std::string result = filter.evaluate(msg);

		std::cout << "Applying filter '" << argv[2] << "' to message yielded " << result << std::endl;
	}
	catch (xoap::exception::Exception& e)
	{
		std::cout << xcept::stdformat_exception_history(e) << std::endl;
		return 1;
	}
	return 0;
}
