// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xoap/SOAPHeaderElement.h"
#include "xoap/SOAPAllocator.h"

xoap::SOAPHeaderElement::SOAPHeaderElement ( DOMNode* node ) : xoap::SOAPElement (node)
{
}

std::string xoap::SOAPHeaderElement::getActor()
{
	return "";
}
	    
	
bool xoap::SOAPHeaderElement::getMustUnderstand()
{
	return true;
}
	
	
void xoap::SOAPHeaderElement::setActor(const std::string & URI)
{
}
	
	
void xoap::SOAPHeaderElement::setMustUnderstand(bool mustUnderstand)
{

}
