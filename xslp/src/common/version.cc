// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/
 
#include "toolbox/version.h"
#include "xslp/version.h"

GETPACKAGEINFO(xslp)

void xslp::checkPackageDependencies() 
{
        CHECKDEPENDENCY(toolbox)
}

std::set<std::string, std::less<std::string> > xslp::getPackageDependencies()
{
    std::set<std::string, std::less<std::string> > dependencies;
    ADDDEPENDENCY(dependencies,toolbox);
    return dependencies;
}	
	
