// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _xslp_version_h_
#define _xslp_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define CORE_XSLP_VERSION_MAJOR 1
#define CORE_XSLP_VERSION_MINOR 10
#define CORE_XSLP_VERSION_PATCH 1
// If any previous versions available E.g. #define CORE_XSLP_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define CORE_XSLP_PREVIOUS_VERSIONS "1.10.0"


//
// Template macros
//
#define CORE_XSLP_VERSION_CODE PACKAGE_VERSION_CODE(CORE_XSLP_VERSION_MAJOR,CORE_XSLP_VERSION_MINOR,CORE_XSLP_VERSION_PATCH)
#ifndef CORE_XSLP_PREVIOUS_VERSIONS
#define CORE_XSLP_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(CORE_XSLP_VERSION_MAJOR,CORE_XSLP_VERSION_MINOR,CORE_XSLP_VERSION_PATCH)
#else 
#define CORE_XSLP_FULL_VERSION_LIST  CORE_XSLP_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(CORE_XSLP_VERSION_MAJOR,CORE_XSLP_VERSION_MINOR,CORE_XSLP_VERSION_PATCH)
#endif 

namespace xslp 
{
	const std::string project = "core";
    const std::string package  =  "xslp";
    const std::string versions = CORE_XSLP_FULL_VERSION_LIST;
    const std::string description = "";
    const std::string authors = "Johannes Gutleber, Luciano Orsini";
    const std::string summary = "SLP based discovery service package";
    const std::string link = "http://xdaqwiki.cern.ch/index.php/Power_Pack";
    config::PackageInfo getPackageInfo();
    void checkPackageDependencies() ;
    std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
