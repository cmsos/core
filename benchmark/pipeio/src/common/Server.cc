// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.                 			 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius	 				 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   		                 *
 *************************************************************************/

#include <mutex>
#include <thread>

#include "benchmark/pipeio/Server.h"

#include "toolbox/BSem.h"
#include "toolbox/PerformanceMeter.h"
#include "toolbox/rlist.h"
#include "toolbox/string.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/task/TimerTask.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/CommittedHeapAllocator.h"
#include "toolbox/hexdump.h"


#include "xdata/UnsignedLong.h"
#include "xdata/Double.h"
#include "xdata/Boolean.h"
#include "xdata/ActionListener.h"

#include "xcept/Exception.h"
#include "xcept/tools.h"

#include "xgi/framework/Method.h"
#include "xgi/Method.h"

#include "xdaq/Application.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationGroup.h"
#include "xdaq/NamespaceURI.h"
#include "xdaq/pipe/Input.h"
#include "xdaq/pipe/Interface.h"

Server::Server (xdaq::ApplicationStub* c): xdaq::Application(c), xgi::framework::UIManager(this)
{
	poolName_ =  "";
	this->getApplicationInfoSpace()->fireItemAvailable("poolName", &poolName_);


	this->maxReceiveBuffers_ = 8;
        this->maxBlockSize_ = 8192; 
	getApplicationInfoSpace()->fireItemAvailable("maxReceiveBuffers", &maxReceiveBuffers_);
        getApplicationInfoSpace()->fireItemAvailable("maxBlockSize", &maxBlockSize_);

	counter_ = 0;

	this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
}


void Server::timeExpired (toolbox::task::TimerEvent& e)
{
        toolbox::TimeVal now = toolbox::TimeVal::gettimeofday();
        double delta = (double)now - (double)lastTime_;
        double rate = counter_ / delta;
        counter_ = 0;
        lastTime_ =  now;

	std::cout << "rate is " << rate << std::endl;
}
void Server::actionPerformed( xdata::Event& event)
{
	counter_ = 0;

	if (event.type() == "urn:xdaq-event:setDefaultValues")
	{
		try
                {
			std::string pname = poolName_.toString();
                        toolbox::net::URN urn("toolbox-mem-pool", pname);
                        pool_ = toolbox::mem::getMemoryPoolFactory()->findPool(urn);

                }
                catch (toolbox::mem::exception::Exception & e)
                {
                        LOG4CPLUS_FATAL(getApplicationLogger(), toolbox::toString("Could not set up memory pool, %s (exiting thread)", e.what()));
                        return;
                }
                pipeService_ = xdaq::getPipeInterface()->join(this,this);

                try
                {
                        toolbox::task::Timer * timer = 0;
                        if (!toolbox::task::getTimerFactory()->hasTimer("timer"))
                        {
                                timer = toolbox::task::getTimerFactory()->createTimer("timer");
                        }
                        else
                        {
                                timer = toolbox::task::getTimerFactory()->getTimer("timer");
                        }

                        toolbox::TimeVal startTime;
                        startTime = toolbox::TimeVal::gettimeofday();
                        toolbox::TimeInterval interval;
                        interval.fromString("PT5S"); // in seconds
			std::string tname = toolbox::toString("checkrate-%d",this->getApplicationDescriptor()->getInstance());
                        timer->scheduleAtFixedRate(startTime, this, interval,  0, tname);
                }
                catch (toolbox::task::exception::Exception& te)
                {
                        std::cout <<  "Cannot run timer" <<  xcept::stdformat_exception_history(te) << std::endl;
                }

		// start sender thread
		std::thread th(&Server::execute, this, "sender");
		th.detach();


	}
}

void Server::actionPerformed(xdaq::pipe::ConnectionError & ce )
{
        std::cout << "Connection error from " << ce.getSourceDescriptor()->getURN() <<  " to " <<  ce.getDestinationDescriptor()->getURN()<< std::endl;
        std::cout << ce.getException().what() << std::endl;
}


void Server::actionPerformed(xdaq::pipe::BrokenPipe & bp )
{
}

void Server::actionPerformed(xdaq::pipe::ConnectionClosedByPeer & ccbp )
{
	std::lock_guard<std::mutex> guard(mutex_);

	std::cout << "Connection closed by peer in server going to delete pipe from input list" << std::endl;
	for ( auto it = ipipes_.begin(); it != ipipes_.end(); it++ )
	{
		if ( (*it)->match(ccbp.getPipeHandle()))
		{
			std::cout << "Found matching input pipe, going to destroy" << std::endl;
			xdaq::pipe::Input * ipipe = *it;
			ipipes_.erase(it); 
			pipeService_->destroyInputPipe(ipipe);			
			return;
		}
	}	
	
}

void Server::actionPerformed(xdaq::pipe::EstablishedConnection & ec )
{
// not used in server
}

bool Server::actionPerformed(xdaq::pipe::ConnectionRequest & cr)
{               

	std::cout << "Server - Connection request received " << std::endl;
	xdaq::pipe::Input * ipipe =  pipeService_->createInputPipe(cr);

        size_t blockSize = maxBlockSize_;
        for (size_t k = 0; k < this->maxReceiveBuffers_; k++)
        {
                toolbox::mem::Reference * reference = 0;

                try
                {
                        reference = toolbox::mem::getMemoryPoolFactory()->getFrame(pool_, blockSize);
                }
                catch (toolbox::mem::exception::Exception & e)
                {               
                        LOG4CPLUS_FATAL(getApplicationLogger(), "Could not allocate block for input" << xcept::stdformat_exception_history(e));
			return true;
                }               

		try
		{
                	ipipe->post(reference);
		}
		catch( ... )
                {               
                        LOG4CPLUS_FATAL(getApplicationLogger(), "Failed to post buffer to input pipe for receive");
			return true;
                }               
		std::cout << "done post buffer " << blockSize << std::endl;
        }

	std::lock_guard<std::mutex> guard(mutex_);

	ipipes_.push_back(ipipe);
	return true;

}

void Server::execute(std::string name)
{
	while(1)
	{
		std::lock_guard<std::mutex> guard(mutex_);

		for (auto i =  ipipes_.begin(); i != ipipes_.end(); i++ )
		{
			if ( ! (*i)->handle_->empty() )
			{
				toolbox::mem::Reference * ref = (*i)->get();
				//std::cout << "--- Server --- >>> received size  " << ref->getDataSize()  << " offset " << ref->getDataOffset() << std::endl;
                        	LOG4CPLUS_DEBUG(getApplicationLogger(), "--- Server --- >>> received size  " << ref->getDataSize()  << " offset " << ref->getDataOffset());
				//char * frame = (char*)ref->getDataLocation();
                                //toolbox::hexdump ((void*) frame, 16);
				counter_++;
				(*i)->post(ref);
			}
		}
	
	}
}
