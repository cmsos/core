

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simlevicius					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include <thread>

#include "benchmark/pipeio/Client.h"

#include "xcept/Exception.h"
#include "xcept/tools.h"

#include "toolbox/BSem.h"
#include "toolbox/rlist.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/CommittedHeapAllocator.h"
#include "toolbox/task/TimerTask.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerFactory.h"

#include "xdaq/Application.h"
#include "xdaq/ApplicationContext.h"
#include "xdaq/ApplicationGroup.h"
#include "xoap/MessageFactory.h"
#include "xdaq/NamespaceURI.h"
#include "xdaq/pipe/Output.h"
#include "xdaq/pipe/Interface.h"

#include "xgi/Method.h"
#include "xgi/framework/Method.h"


Client::Client (xdaq::ApplicationStub* c): xdaq::Application(c), xgi::framework::UIManager(this)
{
	poolName_ =  "tpi";
	this->getApplicationInfoSpace()->fireItemAvailable("poolName", &poolName_);
	this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
	this->maxBlockSize_ = 8192;
        getApplicationInfoSpace()->fireItemAvailable("maxBlockSize", &maxBlockSize_);


}

Client::~Client()
{
}


void Client::actionPerformed (xdata::Event& event)
{

	if (event.type() == "urn:xdaq-event:setDefaultValues")
	{

		try
		{
			destination_ = getApplicationContext()->getDefaultZone()->getApplicationDescriptors("Server");

			int counter = 0;
			servers_ = new xdaq::ApplicationDescriptor*[destination_.size()];

			std::set<const xdaq::ApplicationDescriptor*>::iterator iter;
			for (iter = destination_.begin(); iter != destination_.end(); iter++)
			{
				servers_[counter] = const_cast<xdaq::ApplicationDescriptor*>(*iter);
				connections_[servers_[counter]] = ConnectionStatus::idle;
				counter++;
			}

			LOG4CPLUS_INFO(getApplicationLogger(), "Found " << destination_.size() << " servers");
		}
		catch (xdaq::exception::Exception& e)
		{
			LOG4CPLUS_FATAL(getApplicationLogger(), xcept::stdformat_exception_history(e));
			return;
		}

		try
		{
			std::string pname = poolName_.toString();
 			toolbox::net::URN urn("toolbox-mem-pool", pname);
                        pool_ = toolbox::mem::getMemoryPoolFactory()->findPool(urn);

		}
		catch (toolbox::mem::exception::Exception & e)
		{
			LOG4CPLUS_FATAL(getApplicationLogger(), xcept::stdformat_exception_history(e));
			return;
		}


		pipeService_ = xdaq::getPipeInterface()->join(this,this);

		// start connector thread
		std::thread thc(&Client::connect, this, "sender connector");
		thc.detach();

		// start sender thread
		std::thread ths(&Client::execute, this, "sender loop");
		ths.detach();

	}
}

void Client::connect(std::string name)
{
	while(1)
	{
		::sleep(2);
 		std::lock_guard<std::mutex> guard(mutex_);

        	LOG4CPLUS_DEBUG (this->getApplicationLogger(), name);
	
		std::string surl = this->getApplicationContext()->getContextDescriptor()->getURL() + "/" + this->getApplicationDescriptor()->getURN();
			
       		for (unsigned int i = 0; i < destination_.size(); i++)
       		{
			xdaq::ApplicationDescriptor* d = servers_[i];
			if ( connections_[d] == ConnectionStatus::idle )
			{
				std::string durl = d->getContextDescriptor()->getURL() + "/" + d->getURN();
       				LOG4CPLUS_DEBUG(getApplicationLogger(), "requesting connection from " << surl << " to " << durl);
				connections_[d]  = ConnectionStatus::connecting;
				pipeService_->connect(this->getApplicationDescriptor(), d);
       				LOG4CPLUS_DEBUG(getApplicationLogger(), "done requesting connection");
			}
		}
	}
}

void Client::actionPerformed(xdaq::pipe::ConnectionError & ce ) 
{
 	std::lock_guard<std::mutex> guard(mutex_);
	//std::cout << "Connection error from " << ce.getSourceDescriptor()->getURN() <<  " to " <<  ce.getDestinationDescriptor()->getURN()<< std::endl;
	const xdaq::ApplicationDescriptor* d = ce.getDestinationDescriptor();
	std::string durl = d->getContextDescriptor()->getURL() + "/" + d->getURN();
 	LOG4CPLUS_DEBUG(getApplicationLogger(), "Connection failed for " << durl <<  ", with error " << ce.getException().what());
	connections_[d] = ConnectionStatus::idle;	
}

bool Client::actionPerformed(xdaq::pipe::ConnectionRequest & cr ) 
{
	return false;
	//XCEPT_RAISE(xcept::Exception, " void Client::actionPerformed(xdaq::pipe::ConnectionRequest & cr ) not implemented");
}

void Client::actionPerformed(xdaq::pipe::BrokenPipe & bp)
{
	std::cout << "Client::actionPerformed Broken Pipe, shoud destroy output pipe here" << std::endl;
 	std::lock_guard<std::mutex> guard(mutex_);

	for ( std::map<const xdaq::ApplicationDescriptor*, xdaq::pipe::Output*>::iterator it = opipes_.begin(); it != opipes_.end(); it++ )
        {
                if ( (*it).second->match(bp.getPipeHandle()))
                {
                        std::cout << "Found matching output pipe, going to destroy" << std::endl;
                        xdaq::pipe::Output * ipipe = (*it).second;
			connections_[(*it).first] = ConnectionStatus::idle;	
                        opipes_.erase(it);
                        pipeService_->destroyOutputPipe(ipipe);
			std::cout << "Done with destroy pipeService_->destroyOutputPipe(ipipe) in client" << std::endl;
                        return;
                }
        }
}

void Client::actionPerformed(xdaq::pipe::ConnectionClosedByPeer & ccbp)
{

}

void Client::actionPerformed(xdaq::pipe::EstablishedConnection & ec ) 
{
 	std::lock_guard<std::mutex> guard(mutex_);

	std::cout << "Connection is established from " << ec.getSourceDescriptor()->getURN() <<  " to " <<  ec.getDestinationDescriptor()->getURN()<< std::endl;
        opipes_[ec.getDestinationDescriptor()] = pipeService_->createOutputPipe(ec);
	connections_[ec.getDestinationDescriptor()] = ConnectionStatus::established;
}


void Client::execute(std::string name)
{
	LOG4CPLUS_DEBUG(getApplicationLogger(), "starting send loop...");

	size_t len = 128;
	for (;;)
	{


			if (!pool_->isHighThresholdExceeded())
			{
				//std::cout << "sleeping 2 second len " << len << std::endl;
				//::sleep(10);
				// Stop if there is an error in sending
				if (this->sendMessage(len) == 1)
				{
					LOG4CPLUS_ERROR(getApplicationLogger(), toolbox::toString("Error in frameSend. retry.."));
					continue;
				}
				// Comment to send same len
				len = (len + 256 ) % (size_t)maxBlockSize_;
				if ( len > 128000 )
				{ 
					len = 256;
					std::cout << "reset len " << len << std::endl;
					sleep(1);
				}

			}
			else
			{

				LOG4CPLUS_DEBUG(getApplicationLogger(), "high threshold is exceeded");
				while (pool_->isLowThresholdExceeded())
				{
					//LOG4CPLUS_DEBUG(getApplicationLogger(), "yield till low threshold reached");
					//this->yield(1);
					this->recycle();
					::sched_yield();
				}
			}

			this->recycle();

	}

	return;
}

void Client::recycle()
{
 	std::lock_guard<std::mutex> guard(mutex_);

	std::set<xdaq::ApplicationDescriptor*>::iterator iter;
	for (unsigned int i = 0; i < destination_.size(); i++)
	{

		if ( opipes_.find(servers_[i]) != opipes_.end() )
		{
			//LOG4CPLUS_DEBUG(getApplicationLogger(), "recycle frames for destination " << i );
			while ( ! opipes_[servers_[i]]->empty() )
			{
				toolbox::mem::Reference * r = opipes_[servers_[i]]->get();
				//std::cout << "recycle buffer in pool " << std::hex << (size_t)r << std::dec << " offset " << r->getDataOffset() << " original size " << r->getBuffer()->getSize() << std::endl;
				r->release();	
			}
		}
	}
}


int Client::sendMessage (size_t len)
{

	//char c;
	//std::cout << "enter char to send message of size (" << currentSize_ << "):";
	//std::cin >> c;

 	std::lock_guard<std::mutex> guard(mutex_);

	std::set<xdaq::ApplicationDescriptor*>::iterator iter;
	for (unsigned int i = 0; i < destination_.size(); i++)
	{
		toolbox::mem::Reference * ref = 0;
		try
		{
			if ( opipes_.find(servers_[i]) != opipes_.end() )
			{
				LOG4CPLUS_DEBUG(getApplicationLogger(), "going to send one message for destination " << i);
				ref = toolbox::mem::getMemoryPoolFactory()->getFrame(pool_, (size_t)maxBlockSize_ );
				char * buf = (char*)ref->getDataLocation();
				ref->setDataSize(len);
				buf[0] = this->getApplicationDescriptor()->getInstance();
				if (this->getApplicationDescriptor()->getInstance() == 0 )
				{
				buf[1] = 0xa;
				buf[2] = 0xa;
				buf[3] = 0xa;
				buf[4] = 0xa;
				buf[5] = 0xa;
				buf[6] = 0xa;
				buf[7] = 0xa;
				}
				else
				{
				buf[1] = 0xb;
				buf[2] = 0xb;
				buf[3] = 0xb;
				buf[4] = 0xb;
				buf[5] = 0xb;
				buf[6] = 0xb;
				buf[7] = 0xb;
				}
				buf[8] = 0xc;
				buf[9] = 0xa;
				buf[10] = 0xf;
				buf[11] = 0xe;
				buf[12] = 0xc;
				buf[13] = 0xa;
				buf[14] = 0xf;
				buf[15] = 0xe;
				//std::cout << " Client post frame of size" << ref->getDataSize() << " offset " << ref->getDataOffset() << std::endl;
				opipes_[servers_[i]]->post(ref);
			}

		}
		catch (toolbox::mem::exception::Exception & me)
		{

			LOG4CPLUS_ERROR(getApplicationLogger(), xcept::stdformat_exception_history(me));
			ref->release();
			return 1; // error
		}
		catch (xdaq::exception::Exception & e)
		{

			LOG4CPLUS_ERROR(getApplicationLogger(), xcept::stdformat_exception_history(e));
			ref->release();
			return 1;

		}
		catch (...)
		{
		}
	}

	return 0; // o.k.
}
