// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _Client_h_
#define _Client_h_

#include <set>
#include <mutex>
#include <cmath>


#include "toolbox/mem/Reference.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/rlist.h"

#include "xdata/UnsignedLong.h"
#include "xdaq/ApplicationDescriptor.h"
#include "xdata/Boolean.h"
#include "xdata/String.h"
#include "xdata/Event.h"
#include "xdata/ActionListener.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"

#include "xgi/Utils.h"
#include "xgi/Input.h"
#include "xgi/Output.h"
#include "xgi/framework/UIManager.h"

#include "xdaq/Application.h"
#include "xdaq/pipe/ServiceListener.h"
#include "xdaq/pipe/Service.h"


class Client: public xdaq::Application, public xgi::framework::UIManager, public xdata::ActionListener, public xdaq::pipe::ServiceListener 
{	
	public:
		XDAQ_INSTANTIATOR();

		Client(xdaq::ApplicationStub* c);
		~Client();


	protected:

		void actionPerformed(xdaq::pipe::EstablishedConnection & ec);
		bool actionPerformed(xdaq::pipe::ConnectionRequest & ec);
		void actionPerformed(xdaq::pipe::ConnectionError & ce);
		void actionPerformed(xdaq::pipe::ConnectionClosedByPeer & ccbp);
		void actionPerformed(xdaq::pipe::BrokenPipe & bp);
	
		int sendMessage(size_t len);
		void recycle();

		void execute(std::string name);
		void connect(std::string name);

		//
		// used to create the memory pool upon parametrization
		//	
		void actionPerformed (xdata::Event& e);

		
		std::set<const xdaq::ApplicationDescriptor*> destination_;	// Vector of all server tids
		xdaq::ApplicationDescriptor **servers_;	// array of all server tids

		toolbox::mem::Pool* pool_;			// Memory pool for allocating messages for sending

		xdaq::pipe::Service * pipeService_;
		std::map<const xdaq::ApplicationDescriptor*, xdaq::pipe::Output*> opipes_; 

		enum class ConnectionStatus { idle, connecting, established };

		std::map<const xdaq::ApplicationDescriptor*, ConnectionStatus> connections_; 
		// fake data input
		toolbox::rlist<toolbox::mem::Reference*>* idata;

		std::mutex mutex_;

		xdata::String poolName_;
                xdata::UnsignedInteger maxBlockSize_;

		

};

#endif
