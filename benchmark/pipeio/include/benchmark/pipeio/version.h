// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

//
// Version definition for MStreamIO
//
#ifndef _pipeio_Version_h_
#define _pipe_Version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define CORE_PIPEIO_VERSION_MAJOR 1
#define CORE_PIPEIO_VERSION_MINOR 2
#define CORE_PIPEIO_VERSION_PATCH 0
// If any previous versions available E.g. #define CORE_PIPEIO_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef CORE_PIPEIO_PREVIOUS_VERSIONS


//
// Template macros
//
#define CORE_PIPEIO_VERSION_CODE PACKAGE_VERSION_CODE(CORE_PIPEIO_VERSION_MAJOR,CORE_PIPEIO_VERSION_MINOR,CORE_PIPEIO_VERSION_PATCH)
#ifndef CORE_PIPEIO_PREVIOUS_VERSIONS
#define CORE_PIPEIO_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(CORE_PIPEIO_VERSION_MAJOR,CORE_PIPEIO_VERSION_MINOR,CORE_PIPEIO_VERSION_PATCH)
#else 
#define CORE_PIPEIO_FULL_VERSION_LIST  CORE_PIPEIO_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(CORE_PIPEIO_VERSION_MAJOR,CORE_PIPEIO_VERSION_MINOR,CORE_PIPEIO_VERSION_PATCH)
#endif 

namespace pipeio
{
	const std::string project = "core";
	const std::string package  =  "pipeio";
	const std::string versions = CORE_PIPEIO_FULL_VERSION_LIST;
	const std::string summary = "Simple I/O loop ";
	const std::string description = "";
	const std::string authors = "Luciano Orsini, Dainius, Simelevicius";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/Power_Pack";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies();
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
