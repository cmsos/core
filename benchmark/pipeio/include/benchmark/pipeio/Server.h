// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2020, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _Server_h_
#define _Server_h_

#include <set>


#include "toolbox/mem/Reference.h"
#include "toolbox/mem/Pool.h"
#include "toolbox/fsm/FiniteStateMachine.h"
#include "toolbox/PerformanceMeter.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/Timer.h"

#include "xdata/UnsignedLong.h"
#include "xdata/UnsignedInteger.h"
#include "xdata/String.h"
#include "xdata/Double.h"
#include "xdata/Boolean.h"
#include "xdata/Event.h"
#include "xdata/ActionListener.h"

#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"

#include "xgi/Utils.h"
#include "xgi/Input.h"
#include "xgi/Output.h"
#include "xgi/framework/UIManager.h"

#include "xdaq/pipe/ServiceListener.h"
#include "xdaq/pipe/Service.h"
#include "xdaq/Application.h"


class Server: public xdaq::Application, public xgi::framework::UIManager, xdata::ActionListener, public xdaq::pipe::ServiceListener, public toolbox::task::TimerListener
{	
	public:
		XDAQ_INSTANTIATOR();

		Server(xdaq::ApplicationStub* c);

		void execute(std::string name);

		void actionPerformed (xdata::Event& e);
		void timeExpired (toolbox::task::TimerEvent& e);

		void actionPerformed(xdaq::pipe::EstablishedConnection & ec);
		bool actionPerformed(xdaq::pipe::ConnectionRequest & ec);
		void actionPerformed(xdaq::pipe::ConnectionError & ce);
		void actionPerformed(xdaq::pipe::ConnectionClosedByPeer & ccbp);
		void actionPerformed(xdaq::pipe::BrokenPipe & bp);

	protected:
		size_t counter_;
		xdaq::pipe::Service * pipeService_;
		std::vector<xdaq::pipe::Input*> ipipes_;
                std::mutex  mutex_;
		toolbox::mem::Pool* pool_;			// Memory pool for allocating messages for sending
		xdata::UnsignedInteger maxReceiveBuffers_; 
                xdata::UnsignedInteger maxBlockSize_;
		toolbox::TimeVal lastTime_; 

		xdata::String poolName_;



	
};

#endif
