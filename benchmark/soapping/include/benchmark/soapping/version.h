/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2007, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber, L. Orsini, R. Moser                             *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************/

//
// Version definition for SOAPtestV
//
#ifndef _benchmark_soapping_Version_H_
#define _benchmark_soapping_Version_H_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define CORE_SOAPPING_VERSION_MAJOR 1
#define CORE_SOAPPING_VERSION_MINOR 9
#define CORE_SOAPPING_VERSION_PATCH 0
// If any previous versions available E.g. #define CORE_SOAPPING_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef CORE_SOAPPING_PREVIOUS_VERSIONS 


//
// Template macros
//
#define CORE_SOAPPING_VERSION_CODE PACKAGE_VERSION_CODE(CORE_SOAPPING_VERSION_MAJOR,CORE_SOAPPING_VERSION_MINOR,CORE_SOAPPING_VERSION_PATCH)
#ifndef CORE_SOAPPING_PREVIOUS_VERSIONS
#define CORE_SOAPPING_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(CORE_SOAPPING_VERSION_MAJOR,CORE_SOAPPING_VERSION_MINOR,CORE_SOAPPING_VERSION_PATCH)
#else
#define CORE_SOAPPING_FULL_VERSION_LIST  CORE_SOAPPING_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(CORE_SOAPPING_VERSION_MAJOR,CORE_SOAPPING_VERSION_MINOR,CORE_SOAPPING_VERSION_PATCH)
#endif

namespace soapping 
{
	const std::string project = "core";
	const std::string package  =  "soapping";
	const std::string versions = CORE_SOAPPING_FULL_VERSION_LIST;
	const std::string summary = "service which reassembles ping using SOAP messages";
	const std::string description = "";
	const std::string authors = "Roland Moser";
	const std::string link = "http://xdaqwiki.cern.ch";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif

