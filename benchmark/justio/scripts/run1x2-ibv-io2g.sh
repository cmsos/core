#!/usr/bin/tclsh

set tests {
256
512
1024
2048
4096
8192
12288
16384
20480
24064
26112
30720
32768
36864
40960
45056
51200
55296
62720
65536
86016
106496
126976
131072
151552
172032
192512
212992
233472
256000
}



set bootline {
        { "Server" "http://dvrubu-c2f34-21-03.cms:20000" 11 }
        { "Server" "http://dvrubu-c2f34-21-04.cms:20000" 11 }
        { "Client" "http://dvrubu-c2f34-21-02.cms:20000" 10 }
        
}

foreach item $bootline {
	puts $item
	set name [lindex $item 0]
	set url  [lindex $item 1]
	set lid  [lindex $item 2]
	 
	if {$name == "Client"} {
		set cmddaq ${url}/urn:xdaq-application:class=pt::utcp::Application,instance=0/init
		puts $cmddaq
	
		exec ./init-io2g.sh $cmddaq
	}	
}
 puts "waiting connections..." 
	
	after 3000

puts "start test"

foreach test $tests {
	set size [lindex $test 0]

	puts [format "testing size %d " $size]

	foreach item $bootline {
#       puts $item
        	set name [lindex $item 0]
        	set url  [lindex $item 1]
        	set lid  [lindex $item 2]

		puts [format "configuring Input on %s " $url]

		exec ./parameterset-io2g.sh $url $lid $size $name
	}


	foreach item $bootline {
	 	set name [lindex $item 0]
                set url  [lindex $item 1]
                set lid  [lindex $item 2]

                puts [format "starting Input on %s " $url]
		exec ./start-io2g.sh $url $lid

	}

	#test is running here and data is being collected
	after 60000

	 foreach item $bootline {
                set name [lindex $item 0]
                set url  [lindex $item 1]
                set lid  [lindex $item 2]
        
                puts [format "halting Input on %s " $url]
		exec ./stop-io2g.sh $url $lid

        }

	after 5000


}

foreach item $bootline {
        set name [lindex $item 0]
        set url  [lindex $item 1]
        set lid  [lindex $item 2]

        puts [format "halting Input on %s " $url]
        exec ./stop-io2g.sh $url $lid

}


puts "test finished"

