// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "toolbox/TimeVal.h"
#include "toolbox/net/UUID.h"
#include "xcept/Exception.h"
#include "xcept/tools.h"
#include <stdarg.h>
#include <sstream>
#include <iterator>
#ifdef CUT
#include "xcept/ExceptionIterator.h"
#endif

xcept::Exception::Info::Info()
{
}

xcept::Exception::Info::Info(const std::string& name, const std::string& message, const std::string& module, const std::string& function, int line)
{

	(*this)["identifier"] = name;
	(*this)["message"] = message;
	(*this)["module"] = module;
	(*this)["function"] = function;
		
	std::stringstream ss;
	ss << line;	
	(*this)["line"] = ss.str();
	
	// support for error notification schema http://xdaq.web.cern.ch/xdaq/xsd/2005/ErrorNotification-11.xsd
	(*this)["qualifiedErrorSchemaURI"] = "http://xdaq.web.cern.ch/xdaq/xsd/2005/QualifiedSoftwareErrorRecord-10.xsd";
	(*this)["sessionID"] = "undefined";
	(*this)["notifier"] = "undefined";
	(*this)["dateTime"] =  toolbox::TimeVal::gettimeofday().toString(toolbox::TimeVal::gmt);
	(*this)["severity"] = "undefined";
	
	toolbox::net::UUID uniqueid;
	(*this)["uniqueid"] = uniqueid.toString();
	
	// optional value fields, they are accepted to be empty if it is not required a value
	(*this)["tag"] = "";
}

void xcept::Exception::Info::setProperty(const std::string& name, const std::string& value)
{
	(*this)[name] = value;
}

std::string xcept::Exception::Info::getProperty(const std::string& name)  const
{
	std::map<std::string, std::string, std::less<std::string> >::const_iterator i =  (*this).find(name); 
	if ( i != (*this).end() )
		return (*i).second;
	else 
		return "";
}

xcept::Exception* xcept::Exception::clone() const
{
	xcept::Exception* e = new xcept::Exception();
	e->history_ = history_;
	return e;
}

xcept::Exception::Exception() 
{
	xcept::Exception::Info i;
	history_.push_back(i);
}

xcept::Exception::~Exception()
{

}


xcept::Exception::Exception( const std::string& name, const std::string& message, const std::string& module, int line, const std::string& function )
{
	xcept::Exception::Info i(name, message, module, function, line);
	history_.push_back(i);
}

xcept::Exception::Exception( const std::string& name, const std::string& message, const std::string& module, int line, const std::string& function, 
				xcept::Exception const & previous)
{
	history_ = previous.history_;
	// Copy stack
	xcept::Exception::Info i(name, message, module, function, line);	
	history_.push_back(i);
}

std::string xcept::Exception::message() const
{
	return history_.back().getProperty("message");
}

std::string xcept::Exception::line() const
{
	return history_.back().getProperty("line");
}

std::string xcept::Exception::function() const
{
	return history_.back().getProperty("function");
}

std::string xcept::Exception::module() const
{
	return history_.back().getProperty("module");
}

const char* xcept::Exception::what () const noexcept
{
	return history_.back().find("message")->second.c_str();
}

std::string xcept::Exception::name()  const
{
	return history_.back().getProperty("identifier");
}

void xcept::Exception::setProperty(const std::string& name, const std::string& value)
{
	history_.back().setProperty(name,value);
}

std::string xcept::Exception::getProperty(const std::string& name) const
{
	return history_.back().getProperty(name);
}

bool xcept::Exception::equals( const xcept::Exception & e) const
{
	if ( history_.size() != e.history_.size() )
	{
		return false;
	}

	std::vector<xcept::Exception::Info>::size_type size = history_.size();
	
	for (std::vector<xcept::Exception::Info>::size_type i = 0; i < size; i++ )
	{
		if (!(( history_[i].getProperty("line") == e.history_[i].getProperty("line") ) 
		    && ( history_[i].getProperty("function") == e.history_[i].getProperty("function") ) 
		    && ( history_[i].getProperty("message") == e.history_[i].getProperty("message") )
		    && ( history_[i].getProperty("module") == e.history_[i].getProperty("module") ) 
			&& ( history_[i].getProperty("tag") == e.history_[i].getProperty("tag") ) 
		    && ( history_[i].getProperty("identifier") == e.history_[i].getProperty("identifier") ) ))
		{
		    	return false;
		}    
	}
	
	return true;
}


namespace xcept {
std::ostream & operator << (std::ostream &out, const xcept::Exception &e)
{
	out << "Caught exception: ";
	xcept::Exception::const_reverse_iterator it = e.rbegin();
    	while ( it != e.rend() )
    	{
                out <<  (*it).getProperty("identifier") << " '" << (*it).getProperty("message") << "' raised at ";
                out << (*it).getProperty("function") << "(" << (*it).getProperty("module") << ":" << (*it).getProperty("line") << ")";
                it++;

                if (it != e.rend())
                {
                        out << ";" << std::endl << "\toriginated by ";
                }
    	}

    return out;
}

std::istream & operator >> (std::istream &in, xcept::Exception &e)
{
	XCEPT_RAISE(xcept::Exception,"Not implemented");
    //cout << "Enter Real Part ";
    //in >> c.real;
    //cout << "Enter Imagenory Part ";
    //in >> c.imag;
    return in;
}

}

xcept::Exception::const_iterator xcept::Exception::begin() const noexcept
{
        return history_.begin();
}

xcept::Exception::const_iterator xcept::Exception::end() const noexcept
{
        return history_.end();
}

xcept::Exception::const_reverse_iterator xcept::Exception::rbegin() const noexcept
{
        return history_.rbegin();
}

xcept::Exception::const_reverse_iterator xcept::Exception::rend() const noexcept
{
        return history_.rend();
}

void xcept::Exception::clear()
{
	history_.clear();
}

size_t xcept::Exception::size() const
{
	return history_.size();
}

xcept::Exception::const_iterator xcept::Exception::insert (const_iterator position, const  xcept::Exception::Info & val)
{
	return history_.insert(position, val);	
}

