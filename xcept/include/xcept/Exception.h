// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _xcept_Exception_h_
#define _xcept_Exception_h_

#include <iostream>
#include <string>
#include <stdio.h>
#include <exception>
#include <typeinfo>
#include <vector>
#include <map>


//
//! Macro to throw an excpetion with line number and function name
//
#define XCEPT_RAISE( EXCEPTION, MSG ) \
throw EXCEPTION( #EXCEPTION, MSG, __FILE__, __LINE__, __FUNCTION__)

#define XCEPT_RETHROW( EXCEPTION, MSG, PREVIOUS ) \
throw EXCEPTION( #EXCEPTION, MSG, __FILE__, __LINE__, __FUNCTION__, PREVIOUS)

#define XCEPT_ASSERT(COND, EXCEPTION, MSG) \
if (!(COND)) \
{\
		XCEPT_RAISE(EXCEPTION, MSG);\
}

// Create a new exception and use
// it as a variable called VAR
//
#define XCEPT_DECLARE( EXCEPTION, VAR, MSG ) \
EXCEPTION VAR( #EXCEPTION, MSG, __FILE__, __LINE__, __FUNCTION__)

// Create a new exception from a previous one and use
// it as a variable called VAR
//
#define XCEPT_DECLARE_NESTED( EXCEPTION, VAR, MSG, PREVIOUS ) \
EXCEPTION VAR( #EXCEPTION, MSG, __FILE__, __LINE__, __FUNCTION__, PREVIOUS)



namespace xcept
{

//
//! Exception class. All other exceptions shall
//! inherit from this class. Within the xdaq framework
//! and applications, only xdaq exceptions shall be raised.
//
class Exception : public std::exception
{

	public:


		//!  A record that holds all information of an exception. An exception holds
	    	//!  a list of this record, one for each stacked exception. */
		class Info : public  std::map<std::string, std::string, std::less<std::string> >
		{
			public:

			/* Empty constructor */
			Info();
			
			/* Constructor with the minimal required information entries */
			Info(const std::string& name, const std::string& message, const std::string& module, const std::string& function, int line);
						
			//! Sets a specific property of the exception information
			void setProperty(const std::string& name, const std::string& value);
		
			//! Retrieve a specific property of the exception information
			std::string getProperty(const std::string& name) const;
		
		};
	
		//! Type definitions for easy iterating over exception stack history
		typedef std::vector<xcept::Exception::Info>::const_iterator const_iterator;
		typedef std::vector<xcept::Exception::Info>::const_reverse_iterator const_reverse_iterator;

		//! Create the exception with no additional information
		Exception() ;
		
		//
		//! \param message is the error message 
		//! that this exception shall contain
		//
		Exception( const std::string& name, const std::string& message, const std::string& module, int line, const std::string& function );
		
		//
		//! \param message is the error message 
		//! that this exception shall contain
		//
		Exception( const std::string& name, const std::string& message, const std::string& module, int line, const std::string& function,
				xcept::Exception const & previous );

		virtual ~Exception();
	
		//
		//! Retrieve the exception error message
		//
		std::string message () const;
		
		//! Retrieve the line in which the exception occurred
		std::string line() const;
		
		//! Retrieve the function in which the exception occurred
		std::string function() const;
		
		//! Retrieve the module in which the exception occurred
		std::string module() const;
		
		//! Retrieve a string representation of the exception description
		const char* what () const noexcept;
		
		//! Get the name of the exception in a string
		std::string name() const;
			
		virtual Exception* clone() const;
		
		//! Sets a specific property of the exception
		void setProperty(const std::string& name, const std::string& value);
		
		//! Retrieve a specific property of the exception
		std::string getProperty(const std::string& name) const;
	
		//! compare exception s according throw stacks
		bool equals( const xcept::Exception & e) const;

		friend std::ostream & operator << (std::ostream &out, const xcept::Exception &c);

    		friend std::istream & operator >> (std::istream &in,  xcept::Exception &c);

		// ! iterator methods

		const_iterator begin() const noexcept;
		const_iterator end() const noexcept;
		const_reverse_iterator rbegin() const noexcept;
		const_reverse_iterator rend() const noexcept;

		// ! management methos
		void clear(); // clear all content 
		size_t size() const; // number of entry in exception history
		const_iterator insert (const_iterator position, const  xcept::Exception::Info & val);

	protected:	

		std::vector<xcept::Exception::Info> history_;
		
};
	std::ostream & operator << (std::ostream &out, const xcept::Exception &c);
	std::istream & operator >> (std::istream &in,  xcept::Exception &c);

} // end of namespace xcept


#define DEFINE_EXCEPTION(NAMESPACE1, EXCEPTION_NAME) \
namespace NAMESPACE1 { \
namespace exception { \
class EXCEPTION_NAME: public xcept::Exception \
{\
	public: \
	EXCEPTION_NAME( std::string name, std::string message, std::string module, int line, std::string function ): \
		xcept::Exception(name, message, module, line, function) \
	{} \
}; \
} \
}

#define XCEPT_DEFINE_EXCEPTION(NAMESPACE1, EXCEPTION_NAME) \
namespace NAMESPACE1 { \
namespace exception { \
class EXCEPTION_NAME: public xcept::Exception \
{\
	public: \
	EXCEPTION_NAME( std::string name, std::string message, std::string module, int line, std::string function ): \
		xcept::Exception(name, message, module, line, function) \
	{} \
	EXCEPTION_NAME( std::string name, std::string message, std::string module, int line, std::string function, xcept::Exception & e ): \
		xcept::Exception(name, message, module, line, function,e) \
	{} \
}; \
} \
}

#define XCEPT_DEFINE_EXCEPTION_WITH_PARENT(NAMESPACE1, EXCEPTION_NAME, PARENT_NAME) \
namespace NAMESPACE1 { \
namespace exception { \
class EXCEPTION_NAME: public NAMESPACE1::exception::PARENT_NAME \
{\
	public: \
	EXCEPTION_NAME( std::string name, std::string message, std::string module, int line, std::string function ): \
	NAMESPACE1::exception::PARENT_NAME(name, message, module, line, function) \
	{}; \
	EXCEPTION_NAME( std::string name, std::string message, std::string module, int line, std::string function, xcept::Exception& previous ): \
	NAMESPACE1::exception::PARENT_NAME(name, message, module, line, function, previous) \
	{}; \
}; \
} \
}


#endif
