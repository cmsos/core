// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#include "xcept/Exception.h"
#ifdef CUT
#include "xcept/ExceptionIterator.h"
#endif
#include "xcept/tools.h"
#include <iostream>

namespace my
{

class MyException: public xcept::Exception
{
	public:
	
	MyException(const std::string& name, const std::string& message, const std::string& module, int line, const std::string& function) : xcept::Exception(name,message,module,line,function)
	{
	}
	
	MyException( std::string name, std::string message, std::string module, int line, std::string function , const xcept::Exception& previous): xcept::Exception(name, message, module, line, function, previous)
	{
	}

};

}

namespace your
{

class YourException: public xcept::Exception
{
        public:

        YourException(const std::string& name, const std::string& message, const std::string& module, int line, const std::string& function) : xcept::Exception(name,message,module,line,function)
        {
        }

        YourException( std::string name, std::string message, std::string module, int line, std::string function , const xcept::Exception& previous): xcept::Exception(name, message, module, line, function, previous)
        {
        }

};

}

namespace his
{

class HisException: public xcept::Exception
{
        public:

        HisException(const std::string& name, const std::string& message, const std::string& module, int line, const std::string& function) : xcept::Exception(name,message,module,line,function)
        {
        }

        HisException( std::string name, std::string message, std::string module, int line, std::string function , const xcept::Exception& previous): xcept::Exception(name, message, module, line, function, previous)
        {
        }

};

}



int main()
{
	try
	{
		 XCEPT_RAISE( my::MyException, "1" );
		 
	} catch (my::MyException& m)
	{

		try
		{
			 XCEPT_RETHROW( your::YourException, "2", m );
		}
		catch ( your::YourException& y)
		{
			try
			{
				 XCEPT_RETHROW( his::HisException, "3", y );
			}
			catch ( his::HisException & h)
			{
				std::cout << "Caught: " << m.name() << std::endl;
				std::cout << "to string: " << m.what() << std::endl;
				std::cout << "History: " << xcept::stdformat_exception_history(h) << std::endl;
				std::cout << "History stream:" << h << std::endl;

				std::cout << "Test iterator" << std::endl;
#ifdef CUT
				for ( xcept::Exception::iterator it = h.begin(); it != h.end(); it++ )
				{
					std::cout << "straight trace of " ;
					 (*it).display(); 
					std::cout << it->getProperty("identifier");
					std::cout << std::endl;	
				}
#endif
				for ( xcept::Exception::const_iterator it = h.begin(); it != h.end(); it++ )
                                {
                                        std::cout << "straight const trace of " ;
                                        std::cout << it->getProperty("identifier");
                                        std::cout << std::endl;
                                }

				for ( xcept::Exception::const_reverse_iterator it = h.rbegin(); it != h.rend(); it++ )
				{
					std::cout << "revert trace of " ;
                                        std::cout << it->getProperty("identifier");
                                        std::cout << std::endl;
				}

				 for ( std::vector<xcept::Exception::Info>::const_reverse_iterator it = h.rbegin(); it != h.rend(); it++ )
				 {
                                        std::cout << "simple revert trace of " ;
                                        std::cout << it->getProperty("identifier");
                                        std::cout << std::endl;
                                }

			}
		}
	}
}
