// $Id$

/*************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2009, CERN.			                 *
 * All rights reserved.                                                  *
 * Authors: J. Gutleber and L. Orsini					 *
 *                                                                       *
 * For the licensing terms see LICENSE.		                         *
 * For the list of contributors see CREDITS.   			         *
 *************************************************************************/

#ifndef _log_xmlappender_version_h_
#define _log_xmlappender_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define CORE_LOGXMLAPPENDER_VERSION_MAJOR 3
#define CORE_LOGXMLAPPENDER_VERSION_MINOR 11 
#define CORE_LOGXMLAPPENDER_VERSION_PATCH 0
// If any previous versions available E.g. #define CORE_LOGXMLAPPENDER_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef CORE_LOGXMLAPPENDER_PREVIOUS_VERSIONS 


//
// Template macros
//
#define CORE_LOGXMLAPPENDER_VERSION_CODE PACKAGE_VERSION_CODE(CORE_LOGXMLAPPENDER_VERSION_MAJOR,CORE_LOGXMLAPPENDER_VERSION_MINOR,CORE_LOGXMLAPPENDER_VERSION_PATCH)
#ifndef CORE_LOGXMLAPPENDER_PREVIOUS_VERSIONS
#define CORE_LOGXMLAPPENDER_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(CORE_LOGXMLAPPENDER_VERSION_MAJOR,CORE_LOGXMLAPPENDER_VERSION_MINOR,CORE_LOGXMLAPPENDER_VERSION_PATCH)
#else 
#define CORE_LOGXMLAPPENDER_FULL_VERSION_LIST  CORE_LOGXMLAPPENDER_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(CORE_LOGXMLAPPENDER_VERSION_MAJOR,CORE_LOGXMLAPPENDER_VERSION_MINOR,CORE_LOGXMLAPPENDER_VERSION_PATCH)
#endif 

namespace logxmlappender 
{
	const std::string project = "core";
	const std::string package  =  "logxmlappender";
   	const std::string versions = CORE_LOGXMLAPPENDER_FULL_VERSION_LIST;
	const std::string description = "";
	const std::string authors = "Johannes Gutleber, Luciano Orsini";
	const std::string summary = "XML message over TCP network appender for log4cplus package";
	const std::string link = "http://xdaqwiki.cern.ch/index.php/log4cplus";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
