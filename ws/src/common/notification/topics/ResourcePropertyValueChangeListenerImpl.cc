/*=============================================================================*
 *  Copyright 2004 The Apache Software Foundation
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *=============================================================================*/
package org.apache.ws.notification.topics.impl;

import org.apache.ws.XmlObjectWrapper;
import org.apache.ws.notification.base.NotificationProducerResource;
import org.apache.ws.notification.topics.expression.SimpleTopicExpression;
import org.apache.ws.notification.topics.expression.TopicExpression;
import org.apache.ws.resource.properties.ResourceProperty;
import org.apache.ws.resource.properties.ResourcePropertyValueChangeEvent;
import org.apache.ws.resource.properties.ResourcePropertyValueChangeListener;

/**
 * A listener for property value change events that publishes the events to
 * an associated WSN {@link org.apache.ws.pubsub.NotificationProducer} resource.
 */
public class ResourcePropertyValueChangeListenerImpl
   implements ResourcePropertyValueChangeListener
{
   private NotificationProducerResource m_producerResource;
   private TopicExpression              m_topicExpr;

   /**
    * Creates a new {@link ResourcePropertyValueChangeListenerImpl} object.
    *
    * @param prop DOCUMENT_ME
    */
   public ResourcePropertyValueChangeListenerImpl( NotificationProducerResource producerResource,
                                                   ResourceProperty             prop )
   {
      m_producerResource    = producerResource;
      m_topicExpr           = new SimpleTopicExpression( prop.getMetaData(  ).getName(  ) );
   }

   /**
    * Gets a simple topic expression that corresponds to the property associated with this listener.
    *
    * @return
    */
   public TopicExpression getTopicExpression(  )
   {
      return m_topicExpr;
   }

   /**
    * Indicates that the value of the property associated with this listener has changed.
    * Called by the WSRF framework whenever a property is externally modified (i.e. via SetResourceProperty, etc.).
    *
    * @param event DOCUMENT_ME
    */
   public void propertyChanged( ResourcePropertyValueChangeEvent event )
   {
      Object msgXBean;
      if ( event instanceof XmlObjectWrapper )
      {
         msgXBean = ( (XmlObjectWrapper) event ).getXmlObject(  );
      }
      else
      {
         throw new IllegalStateException( "Unable to extract XmlObject from ResourcePropertyValueChangeEvent." );
      }

      try
      {
         m_producerResource.publish( m_topicExpr, msgXBean );
      }
      catch ( Exception e )
      {
         throw new RuntimeException( e );
      }
   }
}