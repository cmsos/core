/*=============================================================================*
 *  Copyright 2004 The Apache Software Foundation
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *=============================================================================*/
package org.apache.ws.notification.base.v2004_06;

#ifndef _ws_notifiation_base_Constants_h_
#define _ws_notifiation_base_Constants_h_

namespace ws {
namespace notification {
namespace base {

/**
 * Constants defined by the WS-BaseNotification 1.2 Draft 01 specification.
 */

/**
* Namespace URI of the WS-BaseNotification 1.2 Draft 01 schema.
*/
const std::string NSURI_WSNT_SCHEMA = "http://docs.oasis-open.org/wsn/2004/06/wsn-WS-BaseNotification-1.2-draft-01.xsd";

/**
* Namespace prefix for the WS-BaseNotification 1.2 Draft 01 schema.
*/
const std::string NSPREFIX_WSNT_SCHEMA = "wsnt";

/**
* Namespace URI of the WS-BaseNotification 1.2 Draft 01 WSDL.
*/
const std::string NSURI_WSNT_WSDL = "http://docs.oasis-open.org/wsn/2004/06/wsn-WS-BaseNotification-1.2-draft-01.wsdl";

/**
* Namespace prefix for the WS-BaseNotification 1.2 Draft 01 WSDL.
*/
const std::string NSPREFIX_WSNT_WSDL = "wsntw";

/**
* The value to use for the wsa:Action header in outgoing Notify messages.
*/
const std::string NOTIFY_ACTION_URI = BaseNotificationConstants.NSURI_WSNT_WSDL + "/Notify";

}}}

#endif
